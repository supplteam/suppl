%% # A subscription to a mail list policy #
%% Showcases the axioms concerning uniqueness of primary keys in tables in conflict generation

primitive type principal.

%% The subscriber only wants to receive mails with priority higher than the one given
event subscribe(principal, priority).

%% Unsuscribe me !
event unsubscribe(principal).

%% Subscription confirmation
event verify(principal, eventid).
%% Unsuscription confirmation
event unsub(principal, eventid).

%% Send a verification link or whatever for subscription
action verify_sub(principal, eventid).
%% Send a verification link for unsub
action verify_unsub(principal, eventid).

%% We are waiting for sub confirmation
table wait_verification(principal, priority, eventid) key(in, out, out) lifetime 3600000.
index wait_verification(in, out, in).
%% Waiting for unsub confirmation
table wait_unsub(principal, priority, eventid) key(in, out, out).
index wait_unsub(in, out, in).
index wait_unsub(out, out, out).

%% Subscribed to the mailing list people
table subscribed(principal, priority) key (in, out).
index subscribed(out, out).

primitive type message.
data priority ::= high | normal | low.
%% Someone wants to send this message to the mailing list, first message is tl;dr version.
event broadcast(principal, priority, message, message).
%% Action of sending a message to the person
action send(principal, message).

%% Is the first priority higher than the second one ?
predicate higher(priority in, priority in).
higher(high, high).
higher(high, normal).
higher(high, low).
higher(normal, normal).
higher(normal, low).
higher(low, low).

%% If we are already suscribed, then it is used to change the priority
handle subscribe(?P, ?PR) =>
  query
  | subscribed(P, _) => queue insert (P, PR) into subscribed;
  | wait_unsub(P, _, _) => skip;
  | _ => queue insert (P, PR, current_event) into wait_verification;
         verify_sub(P, current_event);
  end;
end.

%% We received the subscription confirmation
handle verify(?P, ?EID) =>
  query
  | wait_verification(P, ?PR, EID) => queue delete (P, PR, EID) from wait_verification;
                                 queue insert (P, PR) into subscribed;
  end;
end.

%% We want to unsubscribe, a confirmation link is sent
handle unsubscribe(?P) =>
  query
  | subscribed(P, ?PR) => queue delete (P, PR) from subscribed;
                     queue insert (P, PR, current_event) into wait_unsub;
                     verify_unsub(P, current_event);
  end;
end.

%% Unsubscription confirmation received
handle unsub(?P, ?EID) =>
  query
  | wait_unsub(P, ?PR, EID) => queue delete (P, PR, EID) from wait_unsub;
  end;
end.

axiom subscribed(A, PRA) -> not wait_unsub(A, PRB, EID).
%axiom subscribed(A, PRA) -> subscribed(B, PRB) -> A = B -> PRA = PRB.
%axiom wait_unsub(A, PRA, EIDA) -> wait_unsub(B, PRB, EIDB) -> A = B -> (PRA = PRB, EIDA = EIDB).

%% We send a message to all subscribed member, even those that are only waiting to get unsubscribed !
handle broadcast(?P, ?PR, ?M, ?N) =>
  foreach subscribed(?A, ?B) =>
    query
    | higher(PR, B), P <> A => send(A, M);
    | P <> A => send(A, N);
    end;
  end;
  foreach wait_unsub(?A, ?B, _) =>
    query
    | higher(PR, B), P <> A => send(A, M);
    | P <> A => send(A, N);
    end;
  end;
end.

conflict send(?A, ?M), send(?B, ?N) => A = B, M <> N.
