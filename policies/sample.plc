% Copyright (c) 2013 SOUND Project, Portland State University.

% This line is a comment.

% Declare "byte" as a primitive type, the details of which are opaque.
primitive type byte.

% pragma emit "this string will be output verbatim into the output file.".

% Declare "asdf" as a logic predicate with no arguments, i.e., a proposition.
predicate asdf.

% Declare "some_pred" as a logic predicate with two arguments.
predicate somepred( number, ip4address ).

% Declare that "some_pred" can be used in output mode in both arguments.
% In other words, some_pred is capable of generating pairs of numbers.
mode somepred(out,out).

% Also declare that "some_pred" can be used in a mode where the first argument
% is given and the second argument is computed.
mode somepred(in,out).

% define a datatype for rose trees.
data rosetree ::= tree(forrest).
type forrest := list(rosetree).

table sometable(forrest, integer) key (in,in).

primitive type identity.

% Declare a predicate "namedargs" with arguments given via names rather than position.
predicate namedargs { 
   princ : identity,
   src : ip4address,
   dest : ip4address,
   test: list(integer),
   test2: number }.

type integer := number.
type intlist := list(integer).

% Declare a data table called "names" with
% two columns and a row lifetime of 60 seconds.
table names( string, list(integer) ) key (in,in) lifetime 60.

% Declare that "names" may be queried by giving the first argument
% and generating the values of the second argument.
index names( in, out ).

% Names may also be used to simply produce a listing of all the tuples.
index names(out, out).

% Declare a new data type called "ip4address", together
% with a new constructor function of that type (named "ip4")
% that contains 4 numeric arguments.
data ip4address ::= ip4(number, number, number, number).


predicate test( integer in, number in ).

data tree ::= node(tree,tree)
            | leaf(integer).

predicate test2( tree in ).

% this should fail an occurs check at runtime
test2(X) :- X = node(X,X).

% this fails a compile type, type level, occurs check
%test(A,B) :- X = [ X, X, X ], A = B.

% Some example rules.
namedargs{ dest: ip4(A,B,C,D), test2:X } :- A = C, X = 42.
mode namedargs{ dest:in, test2:out }.

test(X,Q) :- X = 10, X > sin(~Q - 12.3 + 2.998e8 * X).
test(X,Q) :- findall( (Y+1), facts(?Z,?Y), RS), set_elements(L,RS),
                       sum(S,L), X < S, S <= Q.

predicate run(number out).
run(X) :- findall( Y*4, facts(_,?Y), RS), set_elements(L,RS), sum(X,L).

predicate facts(number out,number out).
facts(1,1) :-.
facts(2,2) :-.
facts(3,3) :-.

predicate sum(number out, list(number) in).

sum(0, []) :-.
sum(X+Y, [ X | LS ]) :- sum(Y,LS).


predicate element(A, list(A)).
mode element(in,in).
mode element(out,in).

element( X, [X | _]) :-.
element( X, [_ | L]) :- element(X,L).


predicate nth( number in, A out, list(A) in ).
nth( N, X, [X | _]) :- N <= 0.
nth( N, X, [_ | L]) :- N > 0, nth(N-1, X, L).


action launchmissile(string).

procedure insertmary(number).
define procedure insertmary(?X) :=
  queue insert ("mary", [X]) into names;
end.

event requestConnect
   { src : ip4address, dest : ip4address,
     sport : number, dport : number }.


handle requestConnect{ src : ?S, dest : ?D, sport:?SP } =>
  queue insert ("asdf", [SP]) into names;
% FIXME? what are the right mode rules for deletes?
%  queue delete ("mary",_) from names;
end.

handle requestConnect{ src : ?S } =>
   query
   | 42 = 12 => insertmary(99);
   | namedargs{ dest:S, test2:?X } => insertmary(X+12);
   | _ => 
      foreach names(?N,?LS) =>
         launchmissile(N);
         query
         | element(?L,LS) =>
              queue delete ([],L) from sometable;
              queue insert (N,LS) into names;
         end;
      end;
   end;
end.
