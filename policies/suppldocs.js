function setbody(x, n) { document.body.className = x; window.location='#conflict-common-'+n }

var commonRuleText = '{ background-color: lightgreen !important; font-weight: bold !important; text-decoration: underline !important; }';

var path1RuleText = '{ background-color: lightsalmon !important; font-weight: bold !important; text-decoration: underline !important; }';

var path2RuleText = '{ background-color: lavender !important; font-weight: bold !important; text-decoration: underline !important; }';

var path12RuleText = '{ text-decoration: underline !important; font-weight: bold !important; background: repeating-linear-gradient(45deg,lightsalmon,lightsalmon 10px,lavender 10px,lavender 20px) !important; }';

var lemmaRuleText = '{ background-color: lightgreen !important; font-weight: bold !important; text-decoration: underline !important; }';

var ruleRuleText = '{ background-color: lightgreen !important; font-weight: bold !important; text-decoration: underline !important; }';

function buildConflictCSS( conflicts, lemmas ) {
    try {
	var sheet = document.createElement('style');
	var text = '';
	for( var i=0; i<conflicts.length; i++ ) {
	    var j = conflicts[i];
	    text = text + 'body.conflict'+j+' .conflict-common-'+j+' '+commonRuleText+'\n';
	    text = text + 'body.conflict'+j+' .conflict-path1-'+j+' '+path1RuleText+'\n';
	    text = text + 'body.conflict'+j+' .conflict-path2-'+j+' '+path2RuleText+'\n';
	    text = text + 'body.conflict'+j+' .conflict-path1-'+j+'.conflict-path2-'+j+' '+path12RuleText+'\n';
	}
	for( var i=0; i<lemmas.length; i++ ) {
	    var j = lemmas[i];
            text = text + 'body.lemma'+j+' .lemma'+j+' '+lemmaRuleText+'\n';
	}
	sheet.innerHTML = text;
	document.head.appendChild(sheet);
    } catch(e) { alert(e); }
}

function buildDeepConflictCSS( conflicts, solved ) {
    try {
	var sheet = document.createElement('style');
	var text = '';
	for( var i=0; i<conflicts.length; i++ ) {
	    var j = conflicts[i][0];
	    var k = conflicts[i][1];
	    text = text + 'body.conflict'+j+'_'+k+' .conflict-common-'+j+' '+commonRuleText+'\n';
	    text = text + 'body.conflict'+j+'_'+k+' .conflict-path1-'+j+' '+path1RuleText+'\n';
	    text = text + 'body.conflict'+j+'_'+k+' .conflict-path2-'+j+' '+path2RuleText+'\n';
	    text = text + 'body.conflict'+j+'_'+k+' .conflict-path1-'+j+'.conflict-path2-'+j+' '+path12RuleText+'\n';
	    text = text + 'body.conflict'+j+'_'+k+' .rule'+j+'_'+k+' '+ruleRuleText+'\n';
	}
  for( var i=0; i<solved.length; i++ ) {
      var j = solved[i];
      text = text + 'body.conflict'+j+' .conflict-common-'+j+' '+commonRuleText+'\n';
      text = text + 'body.conflict'+j+' .conflict-path1-'+j+' '+path1RuleText+'\n';
      text = text + 'body.conflict'+j+' .conflict-path2-'+j+' '+path2RuleText+'\n';
      text = text + 'body.conflict'+j+' .conflict-path1-'+j+'.conflict-path2-'+j+' '+path12RuleText+'\n';
  }
	sheet.innerHTML = text;
	document.head.appendChild(sheet);
    } catch(e) { alert(e); }
}
