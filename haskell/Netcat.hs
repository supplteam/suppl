{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- module Netcat where

import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put
import Network.Socket
import qualified Network.Socket.ByteString as SB
import qualified Data.ByteString as B
import Control.Concurrent
import Data.Word
import Control.Exception 
import System.Environment
import Foreign.Marshal.Array
import Foreign.Ptr
import System.IO
import System.IO.Error
import qualified Network.Socket.ByteString as SB

import Net
import qualified Messages as M

parseArgs :: [String] -> IO (String, AddrInfo,AddrInfo,AddrInfo)
parseArgs [nm,self,proxy,pport,host,hport] = do
  selfInfo  <- lookupHost self  Nothing
  proxyInfo <- lookupHost proxy (Just pport)
  hostInfo  <- lookupHost host  (Just hport)
  return (nm, selfInfo, proxyInfo, hostInfo)
parseArgs _ = error "incorrect arguments"


checkPolicy :: String -> SockAddr -> AddrInfo -> AddrInfo -> IO ()
checkPolicy nm self proxy peer = do
    hPutStrLn stderr "checking policy!"
    hPutStrLn stderr $ unwords $ ["name:  ", nm]
    hPutStrLn stderr $ unwords $ ["self:  ", show self]
    hPutStrLn stderr $ unwords $ ["proxy: ", show (addrAddress proxy)]
    hPutStrLn stderr $ unwords $ ["host:  ", show (addrAddress peer)]
    
    bracket (socket (addrFamily proxy) (addrSocketType proxy) (addrProtocol proxy))
            close
     (\pskt -> do
         connect pskt (addrAddress proxy)
         M.sendConnectMessage pskt nm (addrProtocol peer) self (addrAddress peer)
         (_,repl) <- M.expect pskt M.reply Nothing
         case repl of
           M.OK -> hPutStrLn stderr "policy check passed!"
           M.DENIED s -> error $ "policy check failed! "++show s
     )

main = withSocketsDo $ do
  (nm,self,proxy,peer) <- getArgs >>= parseArgs
  bracket (openConnection nm self proxy peer) close mirror

openConnection :: String -> AddrInfo -> AddrInfo -> AddrInfo -> IO Socket
openConnection nm self proxy peer =
  case addrAddress self of
    SockAddrInet _ selfaddr -> do
      skt <- socket (addrFamily peer) (addrSocketType peer) (addrProtocol peer)
      bind skt (SockAddrInet aNY_PORT selfaddr)
      sktAddr <- getSocketName skt
      checkPolicy nm sktAddr proxy peer
      connect skt (addrAddress peer)
      return skt
    _ -> error "incorrect self address type"

mirror :: Socket -> IO ()
mirror skt = do
  sendVar <- newEmptyMVar
  recvVar <- newEmptyMVar
  hPutStrLn stderr $ "begin socket mirror"
  forkIO (sendMirror sendVar skt)
  forkIO (recvMirror recvVar skt)
  takeMVar sendVar
  takeMVar recvVar

bufSize = 4096

sendMirror :: MVar () -> Socket -> IO ()
sendMirror endVar skt = finally (do
   hSetBuffering stdin NoBuffering
   allocaArray bufSize sendLoop
   hPutStrLn stderr $ "close sending end"
   shutdown skt ShutdownSend)
   (putMVar endVar ())
 where sendLoop :: Ptr Word8 -> IO ()
       sendLoop buf  = do
          cnt <- hGetBufSome stdin buf bufSize
          if cnt > 0
             then pushLoop buf cnt >> sendLoop buf
             else return ()
       pushLoop :: Ptr Word8 -> Int -> IO ()
       pushLoop buf cnt = do
          if cnt > 0
             then do bsent <- sendBuf skt buf cnt
                     pushLoop (plusPtr buf bsent) (cnt - bsent)
             else return ()

recvMirror :: MVar () -> Socket -> IO ()
recvMirror endVar skt = finally (do
   hSetBuffering stdout NoBuffering
   catchIOError
     (allocaArray bufSize recvLoop)
     (\e -> if isEOFError e then return () else ioError e)
   hPutStrLn stderr $ "close recv end")
   (putMVar endVar ())
 where recvLoop :: Ptr Word8 -> IO ()
       recvLoop buf = do
          cnt <- recvBuf skt buf bufSize
          if cnt > 0
             then hPutBuf stdout buf cnt >> recvLoop buf
             else return ()
