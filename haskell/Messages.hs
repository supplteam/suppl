{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

{-# LANGUAGE OverloadedStrings #-}
module Messages where

import Data.Attoparsec.Combinator
import Data.Attoparsec.Char8

import Control.Monad
import qualified Data.ByteString as B
import qualified Network.Socket.ByteString as SB
import Network.Socket
import Data.Word
import Net
import Data.ByteString.Char8
import Control.Concurrent
import Control.Exception ( bracket, finally )


test = parseTest message . pack

data Message
  = OpenConnection String ProtocolNumber SockAddr SockAddr
  | ListenOn ProtocolNumber SockAddr Int
 deriving Show

data Reply
  = OK | DENIED ByteString
 deriving Show

showReply :: Reply -> ShowBS
showReply OK = bs "[issueVerdict(allow)]\n"
showReply (DENIED s) =
   bs "[issueVerdict(deny('" . (append s) . bs "'))]\n"

reply :: Parser Reply
reply = choice
  [ try (string "[issueVerdict(allow)]") >> return OK
  , do string "[issueVerdict(deny('"
       s <- takeTill (== '\'')
       string "'))]"
       return (DENIED s)
  ]

type ShowBS = ByteString -> ByteString


addrsToConnDesc :: ProtocolNumber -> SockAddr -> SockAddr -> ConnectionDesc
addrsToConnDesc proto src dest =
    let (SockAddrInet (PortNum srcPort) srcIP) = src
        (SockAddrInet (PortNum destPort) destIP) = dest
        desc =
         ConnectionDesc 
         { src_addr = srcIP
         , src_port = srcPort
         , protocol = fromIntegral proto
         , dest_addr = destIP
         , dest_port = destPort
         , icmp_type = 0
         , icmp_code = 0
         , icmp_echo_id = 0
         }
     in desc

expect :: Socket -> Parser a -> Maybe ByteString -> IO (Maybe ByteString,a)
expect skt p x = case x of Nothing -> loop (parse p); Just start -> handleParse (parse p start)
  where loop p = do
            str <- SB.recv skt 1024
            handleParse (p str)
        handleParse r =
            case r of
              Fail _ _ msg -> error msg
              Partial p' -> loop p'
              Done rest x -> return (if B.null rest then Nothing else Just rest,x)

listenMessages :: SockAddr -> (SockAddr -> Message -> IO Reply) -> IO ()
listenMessages listenAddr f =
   bracket (socket AF_INET Stream defaultProtocol) close
   (\lskt -> do
     bind lskt listenAddr
     listen lskt 1
     acceptLoop lskt)
 where
  acceptLoop lskt = do
   (skt,addr) <- accept lskt
   forkIO (handleConnectionRequests skt (f addr))
   acceptLoop lskt

handleConnectionRequests :: Socket -> (Message -> IO Reply) -> IO ()
handleConnectionRequests skt f =
  finally (recvLoop (parse message)) (close skt)
 where
  recvLoop p = do
     str <- SB.recv skt 1024
     if B.length str > 0
       then parseLoop (p str)
       else return ()
  parseLoop r = do
     case r of
       Fail _ _ s -> error s
       Partial p -> recvLoop p
       Done rest x -> do
           r <- f x 
           SB.sendAll skt $ showReply r B.empty
           parseLoop (parse message rest)

sendMessage :: Socket -> Message -> IO ()
sendMessage skt msg =
    SB.sendAll skt $ showMessage msg B.empty

sendConnectMessage :: Socket -> String -> ProtocolNumber -> SockAddr -> SockAddr -> IO ()
sendConnectMessage skt name proto self peer = 
   sendMessage skt $ OpenConnection name proto self peer

sendListenOnMessage :: Socket -> ProtocolNumber -> SockAddr -> Int -> IO ()
sendListenOnMessage skt proto self x =
   sendMessage skt $ ListenOn proto self x

bs :: ByteString -> ShowBS
bs = append

showBS :: Show a => a -> ShowBS
showBS = bs . pack . show

message :: Parser Message
message = do
  m <- choice [openConnection, listenOn]
  char '\n'
  return m

showMessage :: Message -> ShowBS
showMessage (OpenConnection nm p (SockAddrInet sport src) (SockAddrInet dport dest))
  = bs "connectRequest(\'"
  . bs (pack nm)
  . bs "\',"
  . showAddress p src
  . bs ","
  . showProtoPort p sport
  . bs ","
  . showAddress p dest
  . bs ","
  . showProtoPort p dport
  . bs ")\n"
showMessage (ListenOn p (SockAddrInet port addr) x)
  = bs "listenOn("
  . showAddress p addr
  . bs ","
  . showProtoPort p port
  . bs ","
  . showBS x
  . bs ")\n"

openConnection :: Parser Message
openConnection = do
  string "connectRequest(\'"
  nm <- takeTill (== '\'')
  string "\',"
  src <- address
  char ','
  (p,sport) <- ip4proto
  char ','
  (dest) <- address
  char ','
  (p',dport) <- ip4proto
  char ')'
  guard $ p == p'
  return (OpenConnection (unpack nm) p (SockAddrInet sport src) (SockAddrInet dport dest))

listenOn :: Parser Message
listenOn = do
  string "listenOn("
  addr <- address
  char ','
  (p,port) <- ip4proto
  char ','
  x <- decimal
  string ")"
  return (ListenOn p (SockAddrInet port addr) x)

showAddress :: ProtocolNumber -> HostAddress -> ShowBS
showAddress proto addr 
  = bs "ip(" . showHost addr . bs ")"

showProtoPort :: ProtocolNumber -> PortNumber -> ShowBS
showProtoPort 0  _  = bs "icmp"
showProtoPort 6  pt = if pt == 0 then bs "tcp" else bs "tcp(" . showBS pt . bs ")"
showProtoPort 17 pt = if pt == 0 then bs "udp" else bs "udp(" . showBS pt . bs ")"
showProtoPort p  _  = error $ "unknown protocol number: "++show p

showHost :: Word32 -> ShowBS 
showHost hst
  = showBS x
  . bs ","
  . showBS y
  . bs "," 
  . showBS z 
  . bs "," 
  . showBS w 
 where (x,y,z,w) = parse_inetaddr hst

address :: Parser HostAddress
address = ip4address

ip4address :: Parser HostAddress
ip4address = do
  string "ip4("
  hst <- ip4host 
  string ")"
  return hst

ip4host :: Parser HostAddress
ip4host = do
  x <- decimal
  char '.'
  y <- decimal
  char '.'
  z <- decimal
  char '.'
  w <- decimal
  return (mk_inetaddr x y z w)

ip4proto :: Parser (ProtocolNumber, PortNumber)
ip4proto = choice[ ip4tcp, ip4udp, ip4icmp ]

ip4icmp = do
   string "icmp"
   return (0, fromIntegral 0)

ip4tcp = choice
   [ try (do
     string "tcp("
     pt <- decimal
     ")"
     return (6, fromIntegral pt))
   , do
     string "tcp"
     return (6, 0)
   ]

ip4udp = choice
   [ try (do
     string "udp("
     pt <- decimal
     ")"
     return (17, fromIntegral pt))
   , do
     string "udp"
     return (17, 0)
   ]
