{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

{-# LANGUAGE ForeignFunctionInterface, DeriveDataTypeable #-}
module Api
( Conn
, init_connection_library
, teardown_connection_library
, install_connection
, teardown_connection
, ConnectionDesc (..)
, testConn
, connection_get_counters
, display_conn
, unsafeFinalizeConnection
, dumpConnections
, getConnections
, clearAllConnections
, mk_inetaddr
) where

#include "../filterlib/api.h"

import Data.Data
import Data.List (unfoldr)
import Data.Bits
import Data.Word
import Foreign.C
import Foreign.Ptr
import Foreign.ForeignPtr (ForeignPtr, withForeignPtr, finalizeForeignPtr)
import Foreign.Concurrent
import Foreign.C.Types
import Foreign.Storable
import Foreign.Marshal.Alloc
import Control.Concurrent.Chan
import Control.Concurrent.MVar

import Net

#let alignment t = "%lu", (unsigned long)offsetof(struct {char x__; t (y__); }, y__)


instance Storable ConnectionDesc where
  sizeOf _ = #{size struct connection_desc}
  alignment _ = #{alignment struct connection_desc}
  peek p = do
    saddr <- #{peek struct connection_desc, src_addr} p
    daddr <- #{peek struct connection_desc, dest_addr} p
    ptcl  <- #{peek struct connection_desc, protocol} p
    sport <- #{peek struct connection_desc, src_port} p
    dport <- #{peek struct connection_desc, dest_port} p
    icmptype <- #{peek struct connection_desc, icmp_type} p
    icmpcode <- #{peek struct connection_desc, icmp_code} p
    icmpid  <- #{peek struct connection_desc, icmp_echo_id} p
    return (ConnectionDesc saddr daddr ptcl sport dport icmptype icmpcode icmpid)
  poke p cd = do
    #{poke struct connection_desc, src_addr} p (src_addr cd)
    #{poke struct connection_desc, dest_addr} p (dest_addr cd)
    #{poke struct connection_desc, protocol} p (protocol cd)
    #{poke struct connection_desc, src_port} p (src_port cd)
    #{poke struct connection_desc, dest_port} p (dest_port cd)
    #{poke struct connection_desc, icmp_type} p (icmp_type cd)
    #{poke struct connection_desc, icmp_code} p (icmp_code cd)
    #{poke struct connection_desc, icmp_echo_id} p (icmp_echo_id cd)


int2stat :: CInt -> ConnectionStatus
int2stat #{const CONN_STAT_INSTALLED} = ConnInstalled
int2stat #{const CONN_STAT_ACTIVE}    = ConnActive
int2stat #{const CONN_STAT_DESTROYED} = ConnDestroyed
int2stat x = error $ "unknown connection status "++show x

stat2int :: ConnectionStatus -> CInt
stat2int ConnInstalled = #{const CONN_STAT_INSTALLED}
stat2int ConnActive    = #{const CONN_STAT_ACTIVE}
stat2int ConnDestroyed = #{const CONN_STAT_DESTROYED}

instance Storable ConnectionStats where
  sizeOf _ = #{size struct connection_stats}
  alignment _ = #{alignment struct connection_stats}
  peek p = do
    st      <- #{peek struct connection_stats, status} p
    o_pkts  <- #{peek struct connection_stats, orig_packets} p
    o_bytes <- #{peek struct connection_stats, orig_bytes} p
    r_pkts  <- #{peek struct connection_stats, repl_packets} p
    r_bytes <- #{peek struct connection_stats, repl_bytes} p
    return (ConnectionStats (int2stat st) o_pkts o_bytes r_pkts r_bytes)
  poke p cs = do
    #{poke struct connection_stats, status} p (stat2int (conn_status cs))
    #{poke struct connection_stats, orig_packets} p (orig_packets cs)
    #{poke struct connection_stats, orig_bytes}   p (orig_bytes cs)
    #{poke struct connection_stats, repl_packets} p (repl_packets cs)
    #{poke struct connection_stats, repl_bytes}   p (repl_bytes cs)


foreign import ccall "init_connection_library" init_connection_library :: IO CInt
foreign import ccall "teardown_connection_library" teardown_connection_library :: IO CInt
foreign import ccall "install_connection" c_install_connection
	:: Ptr ConnectionDesc 
        -> FunPtr ConnCallback
	-> FunPtr ConnCallback
	-> FunPtr FreeCallback
	-> FunPtr FreeCallback
	-> IO (Ptr ())
foreign import ccall "teardown_connection" c_teardown_connection
        :: Ptr () -> IO CInt
foreign import ccall "connection_get_counters" c_connection_get_counters
        :: Ptr () -> Ptr ConnectionStats -> IO ()
foreign import ccall "free_connection" c_free_connection
        :: Ptr () -> IO ()
foreign import ccall "dump_connections" c_dump_connections
        :: FunPtr DumpCallback -> Ptr () -> IO ()
foreign import ccall "fresh_reference" c_fresh_reference
        :: Ptr () -> IO (Ptr ())

newtype Conn = Conn (ForeignPtr ())

type ConnCallback = Ptr ConnectionDesc -> Ptr ConnectionStats -> Ptr () -> IO CInt
type DumpCallback = Ptr () -> Ptr ConnectionDesc -> Ptr ConnectionStats -> Ptr () -> IO CInt
type FreeCallback = FunPtr () -> IO ()

foreign import ccall "wrapper" mkConnCallback :: ConnCallback -> IO (FunPtr ConnCallback)
foreign import ccall "wrapper" mkFreeCallback :: FreeCallback -> IO (FunPtr FreeCallback)
foreign import ccall "wrapper" mkDumpCallback :: DumpCallback -> IO (FunPtr DumpCallback)

marshallConnCallback :: (ConnectionDesc -> ConnectionStats -> IO CInt) -> ConnCallback
marshallConnCallback f p_desc p_stats _ = do
    cd <- peek p_desc
    cs <- peek p_stats
    f cd cs

marshallDumpCallback :: MVar ()
                     -> (IO Conn -> ConnectionDesc -> ConnectionStats -> IO CInt) 
		     -> DumpCallback
marshallDumpCallback mv f conn_ptr cdesc_ptr cstats_ptr arg_ptr = do
    cd <- peek cdesc_ptr
    cs <- peek cstats_ptr
    let x = withMVar mv (\_ -> do
         c_fresh_reference conn_ptr
         foreign_conn <- newForeignPtr conn_ptr (c_free_connection conn_ptr)
         return (Conn foreign_conn))
    f x cd cs

marshallFreeCallback :: FunPtr ConnCallback -> FunPtr ConnCallback -> FunPtr () -> IO ()
marshallFreeCallback attach_fp destroy_fp free_fp = do
    freeHaskellFunPtr attach_fp
    freeHaskellFunPtr destroy_fp
    freeHaskellFunPtr free_fp
    putStrLn "inside Haskell-side free callback"

display_conn :: String -> ConnectionDesc -> ConnectionStats -> IO CInt
display_conn x cd stats = do
   putStrLn $ unwords [ x, show cd, show stats]
   return 0

install_connection :: ConnectionDesc
                   -> (ConnectionDesc -> ConnectionStats -> IO CInt)
                   -> (ConnectionDesc -> ConnectionStats -> IO CInt)
		   -> IO Conn
install_connection cd attach_f destroy_f = do
    alloca (\p -> do
      poke p cd
      attach_ptr  <- mkConnCallback $ marshallConnCallback attach_f
      destroy_ptr <- mkConnCallback $ marshallConnCallback destroy_f
      free_ptr    <- mkFreeCallback $ marshallFreeCallback attach_ptr destroy_ptr
      raw_conn <- c_install_connection p 
                         attach_ptr
			 destroy_ptr
                         free_ptr
                         free_ptr
      foreign_conn <- newForeignPtr raw_conn (c_free_connection raw_conn)
      return (Conn foreign_conn))

teardown_connection :: Conn -> IO CInt
teardown_connection (Conn fp) =
    withForeignPtr fp (\p -> c_teardown_connection p)

connection_get_counters :: Conn -> IO ConnectionStats
connection_get_counters (Conn fp) =
    alloca (\p_stats -> do
      withForeignPtr fp (\p ->c_connection_get_counters p p_stats)
      peek p_stats)

unsafeFinalizeConnection :: Conn -> IO ()
unsafeFinalizeConnection (Conn c) = finalizeForeignPtr c

dumpConnections :: (IO Conn -> ConnectionDesc -> ConnectionStats -> IO ()) -> IO ()
dumpConnections f = do
  mv <- newMVar ()
  f_ptr <- mkDumpCallback $ marshallDumpCallback mv (\x cd cs -> f x cd cs >> return 0)
  c_dump_connections f_ptr nullPtr
  takeMVar mv
  freeHaskellFunPtr f_ptr

clearAllConnections :: IO ()
clearAllConnections = do
  ch <- newChan
  dumpConnections (\x _ _ -> x >>= writeChan ch . Just)
  writeChan ch Nothing
  conns <- getChanContents ch
  sequence_ $ map teardown_connection $ unwindContents $ conns

getConnections :: IO [(ConnectionDesc,ConnectionStats)]
getConnections = do
  ch <- newChan
  dumpConnections (\_ cd cs -> writeChan ch (Just (cd,cs)))
  writeChan ch Nothing
  fmap unwindContents $ getChanContents ch

unwindContents :: [Maybe a] -> [a]
unwindContents = unfoldr (\l ->
  case l of
   Just x : xs -> Just (x, xs)
   Nothing : _ -> Nothing
   []          -> Nothing)
