{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

module Policy where

import Network
import Network.Socket
import System.Random
import Control.Monad
import Data.Word
import Control.Concurrent.MVar
import qualified Data.Map as Map
import Control.Monad
import System.IO
import qualified Data.ByteString.Char8 as BS

import Net
import qualified Api
import qualified Messages as M

type Policy a = (IO a, a -> IO (), SockAddr -> M.Message -> a -> IO (a,M.Reply))

-- deliver verdicts on connection policies
-- by flipping a coin
randpolicy :: Policy ()
randpolicy = basicPolicy (\_ _ -> randomIO >>= \x -> return (if x then M.OK else M.DENIED (BS.pack "fikle finger of fate")))

basicPolicy :: (SockAddr -> M.Message -> IO M.Reply) -> Policy ()
basicPolicy f = (return (), \_ -> return (), \addr cd _ -> fmap (\a -> ((),a)) (f addr cd))

newtype Logic st a = Logic { runLogic :: st -> IO [a] }

instance Functor (Logic st) where
  fmap f (Logic m) = Logic (\x -> fmap (fmap f) (m x))

instance Monad (Logic st) where
  return a = Logic (\x -> return (return a))
  (Logic m) >>= f = Logic (\x -> m x >>= fmap join . sequence . map (\z -> runLogic (f z) x))
  fail msg = Logic (\x -> fail msg)

instance MonadPlus (Logic st) where
  mzero = Logic (\x -> return [])
  mplus (Logic ma) (Logic mb) = Logic (\x -> ma x >>= \a -> mb x >>= \b -> return (a++b))

neg :: Logic st a -> Logic st ()
neg (Logic m) = Logic (\x -> m x >>= \a -> if null a then return [()] else return [])

size :: Logic st a -> Logic st Int
size (Logic m) = Logic (\x -> fmap (return . length) (m x))

matchSrcAddrDesc :: Word8 -> SockAddr -> ConnectionDesc -> Bool
matchSrcAddrDesc proto (SockAddrInet (PortNum pt) hst) cd =
  (protocol cd == proto &&
   src_addr cd == hst &&
   src_port cd == pt || pt == 0)

matchDestAddrDesc :: Word8 -> SockAddr -> ConnectionDesc -> Bool
matchDestAddrDesc proto (SockAddrInet (PortNum pt) hst) cd =
  (protocol cd == proto &&
   dest_addr cd == hst &&
   dest_port cd == pt || pt == 0)

getConnections :: SockAddr -> Logic a (ConnectionDesc,ConnectionStats)
getConnections addr = Logic $ \_ ->
  fmap (filter (matchDestAddrDesc 6 addr . fst)) Api.getConnections

answerSet :: Logic st a -> Logic st [a]
answerSet (Logic m) = Logic $ \st -> fmap (:[]) (m st)

listening :: SockAddr -> Logic SampleState Int
listening addr = Logic $ \(SampleState lt) -> do
       case Map.lookup addr lt of
          Nothing -> return []
          Just i  -> if i > 0 then return [i] else return []

liftIO :: IO a -> Logic st a
liftIO m = Logic $ \_ -> m >>= return . (:[])

------------------------------------------------------------------------

data SampleState =
  SampleState
  { listenTable :: Map.Map SockAddr Int
  }

initMap = Map.empty

samplePolicy :: Policy SampleState
samplePolicy =
      ( return (SampleState initMap)
      , \_ -> return ()
      , \addr msg st ->
           case msg of
             M.OpenConnection _ proto src dest -> do
               let cd = M.addrsToConnDesc proto src dest
               xs <- runLogic (searchRules addr cd) st
               return (st,if (null xs) then M.DENIED (BS.pack "connection unauthorized") else M.OK)
             M.ListenOn proto addr x -> do
               xs <- runLogic (searchListen proto addr x) st
               if (null xs)
                  then return (st,M.DENIED (BS.pack "listen unauthorized"))
                  else return (addListen st proto addr x, M.OK)
      )

announce :: String -> Logic st ()
announce msg = Logic (\_ -> putStrLn msg >> return [()])

addListen :: SampleState -> ProtocolNumber -> SockAddr -> Int -> SampleState
addListen (SampleState lt) proto addr x = SampleState (Map.insert addr x lt)

addrHasIP :: Word32 -> AddrInfo -> Bool
addrHasIP ip addr =
  case addrAddress addr of
    SockAddrInet _ hst -> hst == ip
    _ -> False

------------------------------------

searchListen :: ProtocolNumber -> SockAddr -> Int -> Logic SampleState ()
searchListen proto addr x = return ()

searchRules :: SockAddr -> ConnectionDesc -> Logic SampleState ()
searchRules addr cd =
   allowConnection addr cd
   >>
   neg (denyConnection addr cd)


allowConnection :: SockAddr -> ConnectionDesc -> Logic SampleState ()
allowConnection srcAddr cd =
  (guard ((dest_addr cd, dest_port cd) `elem` permanent_server))

  `mplus`

  (do let destAddr = (SockAddrInet (PortNum (dest_port cd)) (dest_addr cd))
      m <- listening destAddr
      i <- size (getConnections destAddr)
      guard (i < m))

denyConnection :: SockAddr -> ConnectionDesc -> Logic SampleState ()
denyConnection addr cd =
--  (guard (protocol cd == 17))  `mplus`
  badGuy (src_addr cd) `mplus`
  bandwidthHog (src_addr cd)


maxBandwidth :: Word64
maxBandwidth = 2^16

bandwidthHog :: Word32 -> Logic SampleState ()
bandwidthHog addr = do
   let fullAddr = (SockAddrInet (fromIntegral 0) addr)
   conns <- answerSet (getConnections fullAddr)
   let bw = sum (map (\(_,stats) -> orig_bytes stats + repl_bytes stats) conns)
   guard (bw >= maxBandwidth)
   announce (show fullAddr ++ " is a bandwidth hog")

badGuy :: Word32 -> Logic SampleState ()
badGuy ip = do
   hostStrings <- liftIO (fmap lines (openFile "badguys.txt" ReadMode >>= hGetContents))
   hostAddrs   <- liftIO (sequence (map (\x -> lookupHost x Nothing) hostStrings))
   let matchAddrs = filter (addrHasIP ip) hostAddrs
   guard (not (null matchAddrs))
   let fullAddr = (SockAddrInet (fromIntegral 0) ip)
   announce (show fullAddr ++ " is a bad guy!")
  

permanent_server :: [(Word32,Word16)]
permanent_server =
  [ (hostA, htons 22)
--  , (hostB, htons 22)
  ]

hostA = mk_inetaddr 192 168 101 2
hostB = mk_inetaddr 192 168 102 2 
