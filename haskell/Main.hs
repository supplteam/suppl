{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

import Api
import Net
import System.Environment
import Network.Socket
import Control.Exception 
import Control.Concurrent
import Control.Concurrent.MVar
import System.Posix.Signals

import Policy
import qualified Messages as M

runPolicyOnAddress :: SockAddr -> MVar a -> Policy a -> IO ()
runPolicyOnAddress listenAddr x p =
    M.listenMessages listenAddr (handleMessage x p)

handleMessage :: MVar a -> Policy a -> SockAddr -> M.Message -> IO M.Reply
handleMessage x (start,end,query) addr msg@(M.OpenConnection _ proto src dest) = do
    let desc = M.addrsToConnDesc proto src dest 
    modifyMVar x (\st -> do
        (st',repl) <- query addr msg st
        case repl of
           M.OK -> install_connection desc (display_conn "attach") (display_conn "destroy") >> return ()
           M.DENIED _ -> return ()
        return (st',repl))
handleMessage x (start,end,query) addr msg = modifyMVar x (query addr msg)

forkForJoin :: IO () -> IO (ThreadId, MVar ())
forkForJoin a = do
   m <- newEmptyMVar
   tid <- mask $ \restore -> forkIO $ finally (restore a) (putMVar m ())
   return (tid,m)

intHandler :: [ThreadId] -> IO ()
intHandler ids = do
   putStrLn "exiting..."
   sequence_ $ map killThread ids

runPolicy :: [AddrInfo] -> Policy a -> IO ()
runPolicy listenAddrs p@(start, end, query) = do
  bracket (start >>= newMVar) (\x -> takeMVar x >>= end) $ \x -> do
      ms <- sequence 
              $ map (\a -> forkForJoin (runPolicyOnAddress (addrAddress a) x p)) 
              $ listenAddrs
      installHandler sigINT  (CatchOnce (intHandler (map fst ms))) Nothing
      installHandler sigTERM (CatchOnce (intHandler (map fst ms))) Nothing
      sequence_ $ map (takeMVar . snd) ms

parseArgs :: [String] -> IO [AddrInfo]
parseArgs (host:port:xs) = do
  addr <- lookupHost host (Just port)
  addrs <- parseArgs xs
  return (addr:addrs)
parseArgs [_] = error "invalid arguments"
parseArgs [] = return []

main = do
  addrs <- getArgs >>= parseArgs 
  if null addrs
    then putStrLn "no addresses specified. exiting..."
    else bracket init_connection_library 
                 (\_ -> teardown_connection_library)
                 (\_ -> runPolicy addrs samplePolicy)
