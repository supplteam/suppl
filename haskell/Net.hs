{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

{-# LANGUAGE ForeignFunctionInterface, DeriveDataTypeable #-}
module Net where

import Data.Bits
import Data.Data
import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put
import Network.Socket
import qualified Network.Socket.ByteString as SB
import qualified Data.ByteString as B
import Control.Concurrent
import Data.Word
import Control.Exception 

data ConnectionDesc = ConnectionDesc
  { src_addr  :: Word32
  , dest_addr :: Word32
  , protocol  :: Word8
  , src_port  :: Word16
  , dest_port :: Word16
  , icmp_type  :: Word8
  , icmp_code  :: Word8
  , icmp_echo_id :: Word16
  } deriving (Show, Data, Typeable)

data ConnectionStatus
  = ConnInstalled
  | ConnActive
  | ConnDestroyed
  deriving (Show, Data, Typeable)

data ConnectionStats = ConnectionStats
  { conn_status  :: ConnectionStatus
  , orig_packets :: Word64
  , orig_bytes   :: Word64
  , repl_packets :: Word64
  , repl_bytes   :: Word64
  } deriving (Show, Data, Typeable)

foreign import ccall "htonl" htonl :: Word32 -> Word32
foreign import ccall "ntohl" ntohl :: Word32 -> Word32

foreign import ccall "htons" htons :: Word16 -> Word16
foreign import ccall "ntohs" ntohs :: Word16 -> Word16

mk_inetaddr :: Word32 -> Word32 -> Word32 -> Word32 -> Word32
mk_inetaddr x y z w = htonl (shift x 24 .|. shift y 16 .|. shift z 8 .|. w)

parse_inetaddr :: Word32 -> (Word32,Word32,Word32,Word32)
parse_inetaddr hst = (x,y,z,w)
  where hst' = ntohl hst
        x = shift hst' (-24) .&. 0xFF
        y = shift hst' (-16) .&. 0xFF
        z = shift hst' ( -8) .&. 0xFF
        w = shift hst'    0  .&. 0xFF

testConn = ConnectionDesc
  { src_addr  = mk_inetaddr 192 168 101 2
  , dest_addr = mk_inetaddr 192 168 102 2
  , src_port  = 0
  , dest_port = htons(22)
  , protocol  = 6
  , icmp_type = 0
  , icmp_code = 0
  , icmp_echo_id = 0
  }

{-
clientgo :: SockAddr -> ConnectionDesc -> IO ()
clientgo sock_addr cd =
   bracket (socket AF_INET Stream defaultProtocol) close
    (\skt -> do
       connect skt sock_addr
       sendConnectionRequest skt cd)
-}

lookupHost :: String -> Maybe String -> IO AddrInfo
lookupHost hostname mport = do
   let myHints = defaultHints{ addrFamily=AF_INET, addrSocketType=Stream }
   addrs <- getAddrInfo (Just myHints) (Just hostname) Nothing
   let info = head addrs
   case mport of
     Nothing -> return info
     Just pt -> do
          ptnum <- evaluate (fromIntegral $ read pt)
          case addrAddress info of
            (SockAddrInet _ haddr) ->
               return (info {addrAddress = SockAddrInet ptnum haddr })
            _ -> error "wrong address type"


{-
expect :: Socket -> String -> IO ()
expect skt str =
   if length str > 0
     then do
       str' <- recv skt (length str)
       rest <- check str' str ("expected "++ show str++" but got " ++ show str')
       expect skt rest
     else return ()
 where check [] ys msg = return ys
       check (x:xs) (y:ys) msg =
              if x == y
                 then check xs ys msg
                 else error msg
-}

{-
sendConnectionRequest :: Socket -> ConnectionDesc -> IO ()
sendConnectionRequest skt cd = do
   let str = runPut $ putConnDesc cd
   SB.sendAll skt str


listenConnectionRequests :: SockAddr -> (SockAddr -> Socket -> ConnectionDesc -> IO ()) -> IO ()
listenConnectionRequests listenAddr f =
   bracket (socket AF_INET Stream defaultProtocol) close
   (\lskt -> do
     bind lskt listenAddr
     listen lskt 1
     acceptLoop lskt)
 where
  acceptLoop lskt = do
   (skt,addr) <- accept lskt
   forkIO (handleConnectionRequests skt (f addr skt))
   acceptLoop lskt


handleConnectionRequests :: Socket -> (ConnectionDesc -> IO ()) -> IO ()
handleConnectionRequests skt f = finally (recvLoop (runGetPartial getConnDesc)) (close skt)
 where
  recvLoop p = do
     str <- SB.recv skt 1024
     if B.length str > 0
       then parseLoop (p str)
       else return ()
  parseLoop r = do
     case r of
       Fail s -> fail s
       Partial p -> recvLoop p
       Done x rest -> f x >> parseLoop (runGetPartial getConnDesc rest)
-}


putConnDesc :: ConnectionDesc -> Put
putConnDesc cd = do
   putWord32be (src_addr cd)
   putWord32be (dest_addr cd)
   putWord8    (protocol cd)
   putWord16be (src_port cd)
   putWord16be (dest_port cd)
   putWord8    (icmp_type cd)
   putWord8    (icmp_code cd) 
   putWord16be (icmp_echo_id cd)

getConnDesc :: Get ConnectionDesc
getConnDesc = do
   saddr  <- getWord32be
   daddr  <- getWord32be
   proto  <- getWord8
   sport  <- getWord16be
   dport  <- getWord16be
   i_type <- getWord8
   i_code <- getWord8
   i_id   <- getWord16be
   return (ConnectionDesc saddr daddr proto sport dport i_type i_code i_id)

