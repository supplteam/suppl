{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

import Net
import System.Environment
import Network.Socket
import qualified Messages as M
import System.IO
import Control.Exception

main = do
  (method:proxy:proxyport:xs) <- getArgs
  proxyAddr  <- lookupHost proxy (Just proxyport)
  msg <-parseMessage method xs
  bracket (socket AF_INET Stream defaultProtocol) close (\skt -> do
    connect skt (addrAddress proxyAddr)
    M.sendMessage skt msg
    (_,repl) <- M.expect skt M.reply Nothing
    print repl)

parseMessage "listen" [self,sport,proto,x] = do
  selfAddr   <- lookupHost self (Just sport)
  proto'     <- evaluate $ fromInteger $ read proto
  num        <- evaluate $ read x
  return (M.ListenOn proto' (addrAddress selfAddr) num)
parseMessage "connect" [self,sport,peer,peerport,proto] = do
  selfAddr   <- lookupHost self (Just sport)
  peerAddr   <- lookupHost peer (Just peerport)
  proto'     <- evaluate $ fromInteger $ read proto
  return (M.OpenConnection proto' (addrAddress selfAddr) (addrAddress peerAddr))
parseMessage mth _ = error $ "unknown method: " ++ mth