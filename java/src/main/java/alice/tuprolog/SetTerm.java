/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package alice.tuprolog;

import java.util.*;
import org.pcollections.*;

/**
 *  This class implements persistent finite sets as a tuProlog Term object.
 *
 *  NOTE! We assume throughout that every term inserted into a set is ground.
 *  This is OK for Suppl because we only compile mode-checked programs, but this
 *  assumption would fail for general Prolog programs.
 */
public class SetTerm extends Struct {
    MapPSet<TermKey> set;
    
    protected SetTerm( MapPSet<TermKey> set ) {
	super("edu.pdx.cecs.suppl.ContainersLibrary.SetTerm.#SET#");
	this.set = set;
    }

    public SetTerm ( Iterator<? extends Term> it ) {
	MapPSet<TermKey> s = HashTreePSet.empty();
	while( it.hasNext() ) {
	    Term t = it.next().getTerm();
	    s = s.plus( new TermKey(t) );
	}
	this.set = s;
    }

    public SetTerm() {
	super("edu.pdx.cecs.suppl.ContainersLibrary.SetTerm.#SET#");
	this.set = HashTreePSet.empty();
    }

    public SetTerm delete( Term v ) {
	return new SetTerm( this.set.minus( new TermKey(v) ) );
    }

    public SetTerm set_insert(Term v) {
	return new SetTerm( this.set.plus( new TermKey(v) ));
    }

    public SetTerm union( SetTerm st ) {
	return new SetTerm( this.set.plusAll(st.set) );
    }


    public boolean contains(Term v) {
	return set.contains(new TermKey(v));
    }

    public int size() {
	return set.size();
    }

    public Term termElementList() {
	Iterator<TermKey> it = set.iterator();
	return iterList(it);
    }

    private Struct iterList(Iterator<TermKey> it) {
	if( it.hasNext() ) {
	    TermKey tk = it.next();
	    Struct tail = iterList(it);
	    return new Struct( tk.getTerm(), tail );
	} else {
	    return new Struct();
	}
    }

    public Set<TermKey> elements() {
	return set;
    }

    @Override
    public boolean isEqual (Term t) {

	t = t.getTerm();
	if(t instanceof SetTerm) {
	    return( this.set.equals( ((SetTerm) t).set ) );
	}
	
	return false;
    }

    @Override
    public String toString() {
	return "SET" + set.toString();
    }

    @Override
    boolean unify(List<Var> vl1, List<Var> vl2, Term t) {

	t = t.getTerm();
	if(t instanceof SetTerm) {
	    SetTerm st = (SetTerm) t;
	    
	    // Here we rely on the assumption that all terms in the set are ground.
	    return( this.set.equals( ((SetTerm) t).set ) );
	    
	} else if (t instanceof Var) {
	    return t.unify(vl2, vl1, this);
	}
	
	return false;
    }
    
    public boolean isAtomic() { return false; }
    public boolean isCompound() { return true; }
    public boolean isStruct() { return true; }
    public boolean isAtom() { return false; }
    public boolean isList() { return false; }
    public boolean isEmptyList() { return false; }
    public boolean isGround() { return true; }
    
    public Term getTerm() { return this; }
    
    public boolean isGreater( Term t ) {
	// This is OK because Suppl does not allow comparison operations
	// for arbitrary terms.
	return false;
    }
    
}
