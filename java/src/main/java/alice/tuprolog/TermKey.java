/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package alice.tuprolog;

import java.util.*;

/** This class exists as a wrapper for terms.  We need it because the fine
    folks who wrote tuProlog didn't bother to provide sensible "hashCode()"
    implementations for Term objects.  So, if we use Terms as the keys
    of a Hashmap or put them in a Hashset we get the wrong answers.

    Instead, we use this wrapper which attempts to give sensible hashCode
    implementations for the Term subclasses it knows about and dumps
    everything it doesn't understand into bucket 0.  This is always safe
    to do, but is very bad for performance; so we try to do better for
    all the Terms we know about.
 */

public class TermKey {
    Term term;
    int hash;

    public TermKey(Term term) {
	this.term = term;
	hash = calcHash(term);
    }

    public Term getTerm() { return term; }

    int calcHash(Term t) {
	if( t == null ) {
	    return 0;
	} else if( t instanceof Number ) {

	    long v = java.lang.Double.doubleToLongBits( ((Number) t).doubleValue() );
	    return (int)(v^(v>>>32));

        } else if ( t instanceof SetTerm ) {

	    SetTerm st = (SetTerm) t;
	    int x = 0;

	    // addition is commutative, so this operation is order-invariant
	    for( TermKey v : st.elements() ) {
		x += v.hashCode();
	    }

	    return x;

	} else if ( t instanceof MapTerm ) {

	    MapTerm mt = (MapTerm) t;
	    int x = 0;

	    // addition is commutative, so this operation is order-invariant
	    for( Map.Entry<TermKey,Term> e : mt.entries() ) {
		x += e.getKey().hashCode();
		x += calcHash(e.getValue());
	    }

	    return x;

	} else if ( t instanceof Struct ) {

	    Struct s = (Struct) t;
	    int x = s.getName().hashCode();
	    for(int i=0; i<s.getArity(); i++) {
		x += calcHash(s.getArg(i));
	    }

	    return x;

	} else {
	    // default cop-out: if we don't know enough to
	    // get a good result, toss it into bucket 0
	    return 0;
	}
    }
    
    public boolean equals(Object o) { 
	if(o instanceof TermKey) {
	    return term.equals( ((TermKey) o).term );
	}
	
	return false;
    }

    public int hashCode() { return hash; }
    public String toString() { return term.toString(); }
}
