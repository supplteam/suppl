/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package alice.tuprolog;

import java.util.*;
import org.pcollections.*;

/**
 *  This class implements persistent finite maps as a tuProlog Term object.
 *
 *  NOTE! We assume throughout that every term inserted into a map is ground.
 *  This is OK for Suppl because we only compile mode-checked programs, but this
 *  assumption would fail for general Prolog programs.
 */
public class MapTerm extends Struct {
    HashPMap<TermKey, Term> map;
    
    protected MapTerm( HashPMap<TermKey, Term> map ) {
	super("edu.pdx.cecs.suppl.ContainersLibrary.MapTerm.#MAP#");
	this.map = map;
    }

    public MapTerm ( Iterator<? extends Term> it ) {
	HashPMap<TermKey, Term> m = HashTreePMap.empty();

	while( it.hasNext() ) {
	    Struct t = (Struct) it.next().getTerm();

            Iterator< ? extends Term > lit = t.listIterator();
            Term k = lit.next().getTerm();
            Term v = lit.next().getTerm();

	    m = m.plus( new TermKey(k), v );
	}
	this.map = m;
    }

    public MapTerm() {
	super("edu.pdx.cecs.suppl.ContainersLibrary.MapTerm.#MAP#");
	this.map = HashTreePMap.empty();
    }


    public int size() {
	return map.size();
    }

    public MapTerm delete( Term k ) {
	return new MapTerm( this.map.minus( new TermKey(k) ) );
    }

    public MapTerm insert(Term k, Term v) {
	return new MapTerm( this.map.plus( new TermKey(k), v ) );
    }

    public MapTerm merge( MapTerm mt ) {
	return new MapTerm( this.map.plusAll(mt.map) );
    }

    public Term get(Term k) {
	return map.get(new TermKey(k));
    }

    public Term termEntryList() {
	Iterator< Map.Entry<TermKey, Term>> it = map.entrySet().iterator();
	return iterList(it);
    }

    private Struct iterList(Iterator< Map.Entry<TermKey,Term>> it) {
	if( it.hasNext() ) {
            Map.Entry<TermKey,Term> e = it.next();
            Struct head = 
                new Struct( e.getKey().getTerm() ,
                            new Struct( e.getValue(), new Struct() ));
	    Struct tail = iterList(it);
	    return new Struct( head, tail );
	} else {
	    return new Struct();
	}
    }

    public Set< Map.Entry<TermKey, Term> > entries() {
	return map.entrySet();
    }

    @Override
    public boolean isEqual (Term t) {

	t = t.getTerm();
	if(t instanceof MapTerm) {
	    return( this.map.equals( ((MapTerm) t).map ) );
	}
	
	return false;
    }

    @Override
    public String toString() {
	return "MAP" + map.toString();
    }

    @Override
    boolean unify(List<Var> vl1, List<Var> vl2, Term t) {

	t = t.getTerm();
	if(t instanceof MapTerm) {
	    MapTerm mt = (MapTerm) t;
	    
	    // Here we rely on the assumption that all terms in the maps are ground.
	    return( this.map.equals( ((MapTerm) t).map ) );
	    
	} else if (t instanceof Var) {
	    return t.unify(vl2, vl1, this);
	}
	
	return false;
    }
    
    public boolean isAtomic() { return false; }
    public boolean isCompound() { return true; }
    public boolean isStruct() { return true; }
    public boolean isAtom() { return false; }
    public boolean isList() { return false; }
    public boolean isEmptyList() { return false; }
    public boolean isGround() { return true; }
    
    public Term getTerm() { return this; }
    
    public boolean isGreater( Term t ) {
	// This is OK because Suppl does not allow comparison operations
	// for arbitrary terms.
	return false;
    }
    
}
