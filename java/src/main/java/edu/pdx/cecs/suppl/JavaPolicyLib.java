/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;

import java.lang.reflect.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import java.security.*;
import javax.security.auth.Subject;

/** Implementations of primitive functions and predicates
 *  that are useful for expressing Java security policies.
 */
public class JavaPolicyLib extends Library {
    JavaLibrary jlib;

    public JavaPolicyLib( JavaLibrary jlib ) {
	this.jlib = jlib;
    }

    public Term principalSet_1( Term subjectTerm ) {
	try {
	    subjectTerm = subjectTerm.getTerm();
	    Subject subject = (Subject) jlib.getRegisteredDynamicObject((Struct) subjectTerm );
	    
	    Set<Principal> ps = subject.getPrincipals();
	    SetTerm st = new SetTerm();
	    for( Principal p : ps ) {
		Term t = jlib.registerDynamic( p );
		st = st.set_insert(t);
	    }
	    return st;
	} catch (Throwable ex) { ex.printStackTrace(); return null; }
    }

    public boolean princName_io_2( Term princTerm, Term nameTerm ) {
	try {
	    princTerm = princTerm.getTerm();
	    Principal princ = 
		(Principal) jlib.getRegisteredDynamicObject( (Struct) princTerm );
	    String name = princ.getName();
	    return engine.unify( nameTerm, new Struct(name) );
	} catch (Throwable ex) { ex.printStackTrace(); return false; }
    }

    public boolean princType_io_2( Term princTerm, Term nameTerm ) {
	try {
	    princTerm = princTerm.getTerm();
	    Principal princ = 
		(Principal) jlib.getRegisteredDynamicObject( (Struct) princTerm );
	    String classname = princ.getClass().getName();
	    return engine.unify( nameTerm, new Struct(classname) );
	} catch (Throwable ex) { ex.printStackTrace(); return false; }
    }


    public boolean permType_io_2( Term permTerm, Term nameTerm ) {
	try {
	    permTerm = permTerm.getTerm();
	    Permission perm = 
		(Permission) jlib.getRegisteredDynamicObject( (Struct) permTerm );
	    String classname = perm.getClass().getName();
	    return engine.unify( nameTerm, new Struct(classname) );
	} catch (Throwable ex) { ex.printStackTrace(); return false; }
    }

    public boolean permName_io_2( Term permTerm, Term nameTerm ) {
	try {
	    permTerm = permTerm.getTerm();
	    Permission perm = 
		(Permission) jlib.getRegisteredDynamicObject( (Struct) permTerm );
	    String name = perm.getName();
	    return engine.unify( nameTerm, new Struct(name) );
	} catch (Throwable ex) { ex.printStackTrace(); return false; }
    }

    public boolean permActions_io_2( Term permTerm, Term actsTerm ) {
	try {
	    permTerm = permTerm.getTerm();
	    Permission perm = 
		(Permission) jlib.getRegisteredDynamicObject( (Struct) permTerm );

	    String actions = perm.getActions();
	    Term actList = actionsToActionList(actions);
	    return engine.unify( actsTerm, actList );
	} catch (Throwable ex) { ex.printStackTrace(); return false; }
    }

    public Term loggingPerm_1( Term name ) {
	try {
	    Struct sname = (Struct) name.getTerm();
	    Permission p = new LoggingPermission( sname.getName(), null );
	    return jlib.registerDynamic( p );

	} catch (Throwable ex) { ex.printStackTrace(); return null; }
    }

    public Term readProperty_1( Term name ) {
	try {
	    final Struct sname = (Struct) name.getTerm();
	    String prop = 
		AccessController.doPrivileged(new PrivilegedAction<String>() {
			public String run() {
			    return System.getProperty(sname.getName());
			}
		    });

	    return new Struct(prop);
	} catch (Throwable ex) { ex.printStackTrace(); return null; }
    }

    public Term reflectPerm_1( Term name ) {
	try {
	    Struct sname = (Struct) name.getTerm();
	    Permission p = new ReflectPermission( sname.getName() );
	    return jlib.registerDynamic( p );

	} catch (Throwable ex) { ex.printStackTrace(); return null; }
    }

    Term actionsToActionList( String actions ) {
	System.out.println("parsing actions: "+actions);
	
	String[] actArray = actions.split(",");
	Term[] termArray = new Term[actArray.length];
	for( int i=0; i<actArray.length; i++ ) {
	    termArray[i] = new Struct(actArray[i]);
	}
	return new Struct( termArray );
    }

    String actionListToActions( Struct actList ) {
	StringBuilder sb = new StringBuilder();
	boolean first = true;

	Iterator it = actList.listIterator();
	while( it.hasNext () ) {
	    Struct act = (Struct) it.next();
	    if( first ) { 
		first = false; 
	    } else {
		sb.append(",");
	    }
	    sb.append( act.getName() );
	}

	return sb.toString();
    }

    public Term filePerm_2( Term name, Term acts ) {
	try {
	    Struct sname = (Struct) name.getTerm();
	    Struct sacts = (Struct) acts.getTerm();
	    String actions = actionListToActions( sacts );

	    Permission p = new FilePermission( sname.getName(), actions );
	    return jlib.registerDynamic( p );

	} catch (Throwable ex) { ex.printStackTrace(); return null; }
    }

    public Term runtimePerm_1( Term name ) {
	try {
	    Struct sname = (Struct) name.getTerm();
	    Permission p = new RuntimePermission( sname.getName() );
	    return jlib.registerDynamic( p );

	} catch (Throwable ex) { ex.printStackTrace(); return null; }
    }

    public Term propertyPerm_2( Term name, Term acts ) {
	try {
	    Struct sname = (Struct) name.getTerm();
	    Struct sacts = (Struct) acts.getTerm();
	    String actions = actionListToActions( sacts );

	    Permission p = new PropertyPermission( sname.getName(), actions );
	    return jlib.registerDynamic( p );

	} catch (Throwable ex) { ex.printStackTrace(); return null; }
    }

    public boolean permImplies_ii_2( Term p1, Term p2 ) {
	try {
	    p1 = p1.getTerm();
	    p2 = p2.getTerm();

	    Permission pm1 = (Permission) jlib.getRegisteredDynamicObject( (Struct) p1 );
	    Permission pm2 = (Permission) jlib.getRegisteredDynamicObject( (Struct) p2 );

	    return (pm1.implies( pm2 ));

	} catch (Throwable ex) { ex.printStackTrace(); return false; }
    }
}
