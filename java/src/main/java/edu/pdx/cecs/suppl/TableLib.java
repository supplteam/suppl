/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *  This library provides Prolog primitives that implement Suppl tables.
 *  
 *  There are two table implementations: first "Bag" tables, which are 
 *  implemented literally as a linked list of rows, and each row is implemented
 *  as an array of Terms.  The main way to access a table is to scan the entire table using
 *  a cursor, implemeneted as a list iterator.
 *
 *  "Key" tables have a primary key and are implemented via a LinkedHashMap.  Right now,
 *  the hashmap is used only to enforce the primary key property; it is not used for row
 *  lookups.
 *
 *  In the future, better table implementations, including indexes, are desirable.  However,
 *  this works for the time being.
 */
public class TableLib extends Library {
    JavaLibrary jlib;

    public TableLib( JavaLibrary jlib ) {
	this.jlib = jlib;
    }

    // Global mapping of table names to table objects.
    HashMap<String,Table> tableMap = new HashMap<String,Table>();

    /**
     *  Provides the Prolog predicate cursorFinished/1.
     *
     *  Given a cursor object, does it have additional values to yield?
     */
    public boolean cursorFinished_1( Term arg1 ) {
	try {
	    arg1 = arg1.getTerm();
	    Iterator it = (Iterator) jlib.getRegisteredDynamicObject( (Struct) arg1 );
	    return !it.hasNext();
	} catch (Throwable ex) { ex.printStackTrace(); return false; }
    }

    /**
     *  Provides the Prolog predicate nextRow/2.
     *
     *  Given a cursor object in argument 1, unify argument 2 with the next row, which is interpreted as a list of terms.
     */
    public boolean nextRow_2( Term arg1, Term arg2 ) {
	try {

	arg1 = arg1.getTerm();
	Iterator it = (Iterator) jlib.getRegisteredDynamicObject( (Struct) arg1 );
	Table.Row r;
	if( it.hasNext() ) {
	    r = (Table.Row) it.next();
	    return arg2.unify(getEngine(), new Struct(r.columns));
	} else {
	    return false;
	}
	} catch(Throwable ex) { ex.printStackTrace(); return false; }
    }

    /** Provides the Prolog predicate dumpTable/1
     *
     *  This is a debugging predicate that prints the contents of the named
     *  table onto the console.
     */

    public boolean dumpTable_1( Term arg1 ) throws PrologError {
	arg1 = arg1.getTerm();

	String tabName;

	if( arg1 instanceof Struct && ((Struct) arg1).isAtom() ) {
	    tabName = arg1.toString();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 1,
					 "atom",
					 arg1 );
	}

	Table tab = tableMap.get(tabName);

	if( tab == null ) {
	    throw PrologError.existence_error(getEngine().getEngineManager(),
					      1,
					      "table",
					      arg1,
					      new Struct("named table does not exist"));
	}

	tab.dumpTable();

	return true;
    }

    /**
     *  Provides the Prolog predicate tableCursor/2.
     *
     *  The first argument is interpreted as a string giving the name of the table.
     *  The second argument is unified with a table cursor object.
     */
    public boolean tableCursor_2( Term arg1, Term arg2 ) throws PrologError {
	arg1 = arg1.getTerm();
	arg2 = arg2.getTerm();

	String tabName;
	Var x;

	if( arg1 instanceof Struct && ((Struct) arg1).isAtom() ) {
	    tabName = arg1.toString();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 1,
					 "atom",
					 arg1 );
	}

	Table tab = tableMap.get(tabName);
	if( tab == null ) {
	    throw PrologError.existence_error(getEngine().getEngineManager(),
					      1,
					      "table",
					      arg1,
					      new Struct("named table does not exist"));
	}

	if( arg2 instanceof Var ) {
	    x = (Var) arg2;
	} else {
	    throw PrologError.instantiation_error(getEngine().getEngineManager(), 2);
	}
	
	Term t = jlib.registerDynamic(tab.getIterator());
	return arg2.unify( getEngine(), t );
    }

    // Given a Prolog term, interpretet as a string and look up the named table in the 
    // table map.
    Table getTable( Term tabTerm ) throws PrologError {
	String tabName;

	tabTerm = tabTerm.getTerm();

	if( tabTerm instanceof Struct && ((Struct) tabTerm).isAtom() ) {
	    tabName = tabTerm.toString();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 1,
					 "atom",
					 tabTerm );
	}

	Table tab = tableMap.get(tabName);
	if( tab == null ) {
	    throw PrologError.existence_error(getEngine().getEngineManager(),
					      1,
					      "table",
					      tabTerm,
					      new Struct("named table does not exist"));
	}

	return tab;
    }
    
    // Given a term, interpret is as a list of terms and generate a Row object.
    Table.Row getRow( Term rowTerm ) throws PrologError {
	Struct termList;

	rowTerm = rowTerm.getTerm();

	if( rowTerm instanceof Struct && ((Struct)rowTerm).isList() ) {
	    termList = (Struct) rowTerm;
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 2,
					 "list",
					 rowTerm );
	}

	Table.Row r = new Table.Row();
	r.columns = new Term[termList.listSize()];
	Iterator<? extends Term> it = termList.listIterator();
	for( int i = 0; i<termList.listSize(); i++ ) {
	    r.columns[i] = it.next();
	}
	
	return r;
    }

    /**
     *  Provides the Prolog predicate deleteRow/2
     *
     *  This predicate deletes a single row matching argument 2 from the table named in argument 1.
     */
    public boolean deleteRow_2( Term arg1, Term arg2 ) throws PrologError {
	try {
	    Table tab = getTable( arg1 );
	    Table.Row r = getRow( arg2 );

	    tab.removeRow(r);

	    return true;
	} catch(Throwable t) {
	    t.printStackTrace();
	    return false;
	}
    }

    /**
     *  Provides the Prolog predicate deleteAllRows/2
     *
     *  This predicate deletes all rows matching argument 2 from the table named in argument 1.
     */
    public boolean deleteAllRows_2( Term arg1, Term arg2 ) throws PrologError {
	try {
	    Table tab = getTable( arg1 );
	    Table.Row r = getRow( arg2 );

	    tab.removeAllRows(r);

	    return true;
	} catch(Throwable t) {
	    t.printStackTrace();
	    return false;
	}
    }

    /**
     *  Provides the Prolog predicate storeRow/2
     *
     *  Stores the row in argument 2 in the table named in argument 1
     */
    public boolean storeRow_2( Term arg1, Term arg2 ) throws PrologError {
	try {
	    Table tab = getTable( arg1 );
	    Table.Row r = getRow( arg2 );

	    if( tab.getTTL() > 0 ) {
		long now = (new Date()).getTime();
		r.timeout = new Date(now + tab.getTTL());
	    }

	    tab.storeRow( r );
	    return true;
	} catch(Throwable t) {
	    t.printStackTrace();
	    return false;
	}
    }

    /**
     *  Provides the prolog directive setupTable/3
     *
     *  Sets up a new bag-valued table with the name given in argument
     *  1, with the number of columns given in argument 2 and with row
     *  lifetime given in argument 3.  If the row lifetime is 0, rows
     *  do not time out, i.e., row liftime is indefinite.
     */
    public void setupTable_3( Term arg1, Term arg2, Term arg3 ) throws PrologError {
	arg1 = arg1.getTerm();
	arg2 = arg2.getTerm();
	arg3 = arg3.getTerm();

	String tabName;
	int cols;
	long ttl;

	if( arg1 instanceof Struct && ((Struct) arg1).isAtom() ) {
	    tabName = arg1.toString();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 1,
					 "atom",
					 arg1 );
	}

	if( arg2 instanceof alice.tuprolog.Number ) {
	    cols = ((alice.tuprolog.Number)arg2).intValue();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 2,
					 "number",
					 arg2 );
	}

	if( arg3 instanceof alice.tuprolog.Number ) {
	    ttl = ((alice.tuprolog.Number)arg3).longValue();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 3,
					 "number",
					 arg3);
	}

	System.out.println("Setting up table: "+tabName);
	
	tableMap.put( tabName, new BagTable((SupplEngine) engine, tabName, cols, ttl) );
    }

    /**
     *  Provides the prolog directive setupKeyTable/4
     *
     *  Sets up a new primary-key table with the name given in
     *  argument 1, with the number of columns given in argument 2 and
     *  with row lifetime given in argument 3.  If the row lifetime is
     *  0, rows do not time out, i.e., row liftime is indefinite.
     *  Argument 4 is a list of integer values indicating which columns
     *  are primary key columns.
     */
    public void setupKeyTable_4( Term arg1, Term arg2, Term arg3, Term arg4 ) throws PrologError {
	arg1 = arg1.getTerm();
	arg2 = arg2.getTerm();
	arg3 = arg3.getTerm();
	arg4 = arg4.getTerm();

	String tabName;
	int cols;
	long ttl;
	int[] keys;

	if( arg1 instanceof Struct && ((Struct) arg1).isAtom() ) {
	    tabName = arg1.toString();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 1,
					 "atom",
					 arg1 );
	}

	if( arg2 instanceof alice.tuprolog.Number ) {
	    cols = ((alice.tuprolog.Number)arg2).intValue();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 2,
					 "number",
					 arg2 );
	}

	if( arg3 instanceof alice.tuprolog.Number ) {
	    ttl = ((alice.tuprolog.Number)arg3).longValue();
	} else {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 3,
					 "number",
					 arg3);
	}

	try {
	    Struct l = (Struct) arg4;
	    int sz = l.listSize();
	    keys = new int[sz];
	    int i = 0;
	    Iterator it = ((Struct) arg4).listIterator();
	    
	    while(it.hasNext()) {
		keys[i] = ((alice.tuprolog.Number) it.next()).intValue();
		i++;
	    }
	} catch ( Exception ex ) {
	    throw PrologError.type_error(getEngine().getEngineManager(),
					 4,
					 "list number",
					 arg4);
	}

	System.out.println("Setting up primary key table: "+tabName);
	
	tableMap.put( tabName, new KeyTable((SupplEngine) engine, tabName, cols, ttl, keys) );
    }

}
