/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import java.util.*;

public abstract class Table {
    public abstract void removeRow( Row r );
    public abstract void removeAllRows( Row r );
    public abstract Iterator<Row> getIterator();
    public abstract Iterator<Row> getQueryIterator( Row r );
    public abstract long getTTL();
    public abstract void storeRow( Row r );
    public abstract void dumpTable();

    SupplEngine engine;

    public Table( SupplEngine engine ) {
	this.engine = engine;
    }

    Date getNow() {
	return new Date();
    }

    public static class Row {
	public Term[] columns;
	public Date timeout;

	public String toString() {
	    StringBuilder sb = new StringBuilder();
	    for(int i=0; i<columns.length; i++) {
		sb.append(columns[i].toString());
		sb.append(" ; ");
	    }

	    if( timeout != null ) {
		sb.append("(");
		sb.append(timeout.toString());
		sb.append(")");
	    }

	    return sb.toString();
	}

	public boolean matches(Row r) {
	    int len = columns.length;
	    if( r.columns.length != len ) { return false; }

	    for( int i=0; i < len; i++ ) {
		if( !(columns[i] instanceof Var) &&
		    !columns[i].equals(r.columns[i]) ) { return false; }
	    }

	    return true;
	}

	public boolean equals(Object x) {
	    if ( !(x instanceof Row) ) { return false; }

	    Row r = (Row) x;
	    int len = columns.length;
	    if( r.columns.length != len ) { return false; }

	    for( int i=0; i < len; i++ ) {
		if( !columns[i].equals(r.columns[i]) ) { return false; }
	    }

	    return true;
	}
    }

}
