/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *  This class provides Prolog primitives to implement the primitive Suppl container types,
 *  like set and map.
 */
public class ContainersLibrary extends Library {
    JavaLibrary jlib;

    public ContainersLibrary(JavaLibrary jlib) {
	this.jlib = jlib;
    }

    public Term set_empty_0() {
	return new SetTerm();
    }

    public Term set_size_1( Term set ) {
	set = set.getTerm();
	
	if( set instanceof SetTerm ) {
	    int x = ((SetTerm) set).size();
	    return new Int(x);
	}

	engine.warn("nonset passed to set_size");
	return null;
    }

    public Term set_insert_2(Term v, Term set) {
	v = v.getTerm();
	set = set.getTerm();

	if(set instanceof SetTerm) {
	    return ((SetTerm) set).set_insert(v);
	}

	engine.warn("nonset passed to set_insert");
	return null;
    }

    public Term set_delete_2(Term v, Term set) {
	v = v.getTerm();
	set = set.getTerm();

	if(set instanceof SetTerm) {
	    return ((SetTerm) set).delete(v);
	}

	engine.warn("nonset passed to set_delete");
	return null;
    }

    public Term set_union( Term set1, Term set2 ) {
	set1 = set1.getTerm();
	set2 = set2.getTerm();

	if( set1 instanceof SetTerm && set2 instanceof SetTerm ) {
	    return ((SetTerm) set1).union( (SetTerm) set2 );
	}

	engine.warn("nonset passed to set_union");
	return null;
    }

    public boolean set_member_ii_2( Term v, Term set ) {
	v = v.getTerm();
	set = set.getTerm();

	if( set instanceof SetTerm ) {
	    return ((SetTerm) set).contains(v);
	}

	engine.warn("nonset passed to set_member");
	return false;
    }

    public boolean set_elements_oi_2( Term l, Term set ) {
	l = l.getTerm();
	set = set.getTerm();

	if( set instanceof SetTerm ) {
	    SetTerm st = (SetTerm) set;
	    Term sl = st.termElementList();
	    return engine.unify( l, sl );
	}

	engine.warn("nonset passed to set_elements_oi");
	return false;
    }

    public boolean set_elements_ii_2( Term l, Term set ) {
	l = l.getTerm();
	set = set.getTerm();

	try {

	    Struct s = (Struct) l;
	    Iterator<? extends Term> it = s.listIterator();
	    SetTerm st = new SetTerm( it );
	    return st.equals( set );

	} catch (Throwable ex) {
	    ex.printStackTrace();
	}

	return false;
    }


    public boolean set_elements_io_2( Term l, Term set ) {
	l = l.getTerm();
	set = set.getTerm();

	try {

	    Struct s = (Struct) l;
	    Iterator<? extends Term> it = s.listIterator();
	    SetTerm st = new SetTerm( it );
	    return engine.unify( set, st );

	} catch (Throwable ex) {
	    ex.printStackTrace();
	}

	return false;
    }

    public boolean set_iterator_next_2( Term e, Term it ) {
	e = e.getTerm();
	it = it.getTerm();

	try {
	    Iterator<TermKey> iter =
		(Iterator<TermKey>) jlib.getRegisteredDynamicObject((Struct) it);

	    if( iter.hasNext() ) {
		Term e2 = iter.next().getTerm();
		return engine.unify( e, e2 );
	    }
	} catch (Throwable ex) {
	    ex.printStackTrace();
	}

	return false;
    }

    public Term set_iterator_1( Term set ) {
	set = set.getTerm();

	if( set instanceof SetTerm ) {
	    Iterator<TermKey> it = ((SetTerm) set).elements().iterator();
	    return jlib.registerDynamic(it);
	}

	engine.warn("nonset passed to set_iterator");
	return null;
    }

    public Term map_empty_0() {
	return new MapTerm();
    }

    public Term map_size_1( Term map ) {
	map = map.getTerm();

	if(map instanceof MapTerm) {
	    int x = ((MapTerm) map).size();
	    return new Int(x);
	}

	engine.warn("nonmap passed to map_size");
	return null;
    }

    public Term map_delete_2( Term k, Term map ) {
	k = k.getTerm();
	map = map.getTerm();
	
	if(map instanceof MapTerm) {
	    return ((MapTerm) map).delete(k);
	}

	engine.warn("nonmap passed to map_delete");
	return null;
    }

    public Term map_insert_3( Term k, Term v, Term map ) {
	k = k.getTerm();
	v = v.getTerm();
	map = map.getTerm();

	if(map instanceof MapTerm) {
	    return ((MapTerm) map).insert(k,v);
	}

	engine.warn("nonmap passed to map_insert");
	return null;
    }

    public Term map_merge_2( Term map1, Term map2 ) {
	map1 = map1.getTerm();
	map2 = map2.getTerm();

	if( map1 instanceof MapTerm && map2 instanceof MapTerm ) {
	    return ((MapTerm) map1).merge( (MapTerm) map2 );
	}

	engine.warn("nonmap passed to map_merge");
	return null;
    }

    public boolean map_member_ioi_3( Term k, Term v, Term map ) {
	k = k.getTerm();
	v = v.getTerm();
	map = map.getTerm();

	if(map instanceof MapTerm) {
	    MapTerm mt = (MapTerm) map;
	    Term el = mt.get(k);
	    if( el != null ) {
		return engine.unify( v, el );
	    } else {
		return false;
	    }
	}

	engine.warn("nonmap passed to map_member");
	return false;
    }

    public boolean map_iterator_next_3( Term k, Term v, Term it ) {
	k = k.getTerm();
	v = v.getTerm();
	it = it.getTerm();

	try {
	    Iterator< Map.Entry<TermKey,Term> > iter =
		(Iterator< Map.Entry<TermKey,Term> >)
		jlib.getRegisteredDynamicObject((Struct) it);

	    if( iter.hasNext() ) {
		Map.Entry<TermKey,Term> entry = iter.next();
		Term k2 = entry.getKey().getTerm();
		Term v2 = entry.getValue();
		return engine.unify( k, k2 ) && engine.unify( v, v2 );
	    }
	} catch (Throwable ex) {
	    ex.printStackTrace();
	}

	return false;
    }

    public Term map_iterator_1( Term map ) {
	map = map.getTerm();

	if( map instanceof MapTerm ) {
	    Iterator< Map.Entry<TermKey,Term> > it 
		= ((MapTerm) map).entries().iterator();
	    return jlib.registerDynamic(it);
	}

	engine.warn("nonmap passed to map_iterator");
	return null;
    }

    public boolean map_elements_io_2( Term l, Term map ) {
        l = l.getTerm();
        map = map.getTerm();
        
	try {

	    Struct s = (Struct) l;
	    Iterator<? extends Term> it = s.listIterator();
	    MapTerm mt = new MapTerm( it );

	    return engine.unify( map, mt );

	} catch (Throwable ex) {
	    ex.printStackTrace();
	}

        return false;
    }

    public boolean map_elements_oi_2( Term l, Term map ) {
        l = l.getTerm();
        map = map.getTerm();

	if( map instanceof MapTerm ) {
	    MapTerm mt = (MapTerm) map;
	    Term ml = mt.termEntryList();

	    return engine.unify( l, ml );
	}

	engine.warn("nonmap passed to map_elements_oi");
        return false;
    }

    public boolean map_elements_ii_2( Term l, Term map ) {
        l = l.getTerm();
        map = map.getTerm();

	try {

	    Struct s = (Struct) l;
	    Iterator<? extends Term> it = s.listIterator();
	    MapTerm mt = new MapTerm( it );
	    return mt.equals( map );

	} catch (Throwable ex) {
	    ex.printStackTrace();
	}

        return false;
    }
}
