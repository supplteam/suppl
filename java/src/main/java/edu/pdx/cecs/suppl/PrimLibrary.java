/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *  This library provides basic Suppl runtime servies.
 */
public class PrimLibrary extends Library {

    WeakHashMap<Object, Struct> objs;
    JavaLibrary jlib;
    long eventCounter;

    public PrimLibrary(JavaLibrary jlib) {
	this.jlib = jlib;
	this.objs = new WeakHashMap();
        this.eventCounter = 0;
    }

    /** 
     *  This method is executed whenever a new event is handled.  We use it to repopulate
     *  the tuProlog dynamic registered object pool given our own pool of registered objects.
     *
     *  Using a WeakHashMap allows us to repopulate dynamic object bindings
     *  on every new event for the previously-registered objects that are
     *  still live.  This is necessary because registered objects may be
     *  placed into tables and we need to restore their bindings on subsequent
     *  events.  The WeakHashMap allows dead objects to fall out of the map,
     *  keeping this scheme from holding onto memory forever (I hope).
     *
     *  @override
     */
    public void onSolveBegin(Term goal) {
	synchronized(objs) {
            eventCounter += 1;

	    for( Map.Entry<Object, Struct> e : objs.entrySet() ) {
		jlib.registerDynamic(e.getValue(), e.getKey() );
	    }
	}
    }

    /** 
     *  Register an object.
     *
     *  Given an object to register, first check if an equal object
     *  already exists in our WeakHashMap.  If so, reuse the bound identifier;
     *  if not, get a new one and push the object into the various places it needs to be.
     */
    public Struct registerObject( Object o ) {
     	Struct t = null;
     	synchronized(objs) {
     	    t = objs.get(o);
     	    if( t == null ) {
     		t = jlib.registerDynamic(o);
     		objs.put( o, t );
     		return t;
     	    } else {
     		return t;
     	    }
     	}
    }

    // Apply the escaping rules for string literals.
    // We do this here because tuProlog doesn't.
    private String scanString( String in ) {
        StringBuilder s = new StringBuilder();
        StringReader r = new StringReader(in);
        int c;

        try {
            while( (c = r.read()) >= 0 ) {
                if( c == '\\' ) {
                    c = r.read();
                    if( c < 0 ) { break; }

                    switch(c) {
                    case 'a'  : s.append( (char) '\u0007'); break;
                    case 'b'  : s.append( (char) '\b'); break;
                    case 'f'  : s.append( (char) '\f'); break;
                    case 'n'  : s.append( (char) '\n'); break;
                    case 'r'  : s.append( (char) '\r'); break;
                    case 't'  : s.append( (char) '\t'); break;
                    case 'u'  : s.append( (char) unicodeChar(r) ); break;
                    case 'v'  : s.append( (char) '\u000B'); break;
                    default   : s.append( (char) c); break;
                    }
                } else {
                    s.append((char) c);
                }
            }
        } catch (IOException ex) {}

        return s.toString();
    }

    private int unicodeChar( Reader r ) throws IOException {
        int u1 = hexValue(r.read());
        int u2 = hexValue(r.read());
        int u3 = hexValue(r.read());
        int u4 = hexValue(r.read());

        return ((u1 << 12) + (u2 << 8) + (u3 << 4) + u4);
    }

    private int hexValue( int x ) {
        switch(x) {
        case '0' : return 0;
        case '1' : return 1;
        case '2' : return 2;
        case '3' : return 3;
        case '4' : return 4;
        case '5' : return 5;
        case '6' : return 6;
        case '7' : return 7;
        case '8' : return 8;
        case '9' : return 9;

        case 'a' : return 10;
        case 'b' : return 11;
        case 'c' : return 12;
        case 'd' : return 13;
        case 'e' : return 14;
        case 'f' : return 15;

        case 'A' : return 10;
        case 'B' : return 11;
        case 'C' : return 12;
        case 'D' : return 13;
        case 'E' : return 14;
        case 'F' : return 15;
        }

        return 0;
    }

    /**
     * This method provides the Prolog evaluable functor "str_lit".
     *
     * Calls to "str_lit" are automatically produced by the Suppl compiler surrounding every
     * string literal.  This is necessary beacuase tuProlog itself has quite poor support for
     * string escape syntax.  The "str_lit" function takes a string contaning embedded escape
     * sequences and interprets the escapes, giving the intended raw string.
     */
    public Term str_lit_1( Term x ) {
        if( x instanceof Struct ) {
            String raw = ((Struct) x).getName();
            String out = scanString(raw);

            return new Struct(out);
        }

        engine.warn("nonstruct passed to str_lit");
        return null;
    }

    /**
     * This method provides thes Prolog evaluable functor "current_event."
     *
     * It provides access to the event ID of the current event.  This
     * is presently implemented as a "long" value which is incremented by
     * 1 every event.  Wraparound is technically possible, but would take
     * quite a lot of events for that to occur.
     */
    public Term current_event_0 () {
        return new alice.tuprolog.Long( eventCounter );
    }

    /**
     * This method provides the Prolog evaluable functor "show".
     *
     * "show" provides a way to get a string representation of an arbitrary term.
     * It is implemented by simply using the toString() method of Term objects.
     * This works fine for most things, but for strings it involves tuProlog's
     * crappy handling of string values.  Unfortunately, it is not entirely straightforward
     * to do better, so for now we simply get unfortunate behavor when strings are shown.
     */
    public Term show_1( Term x ) {
        x = x.getTerm();

        if( x instanceof Struct ) {
            try {

                Object o = jlib.getRegisteredDynamicObject((Struct) x);
                if( o != null ) {
                    Struct s = new Struct(o.toString());
                    //System.out.println("Show: "+s.toString());
                    return s;
                }

            } catch( alice.tuprolog.lib.InvalidObjectIdException ex ) {}
        }

        Struct s = new Struct(x.toString());
        //System.out.println("Show: "+s.toString());
        return s;
    }

}
