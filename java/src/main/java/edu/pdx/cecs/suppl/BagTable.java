/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import java.util.*;

public class BagTable extends Table {
    String name;
    int cols;
    LinkedList<Row> rows;
    long ttl;

    public BagTable(SupplEngine engine, String name, int cols, long ttl) {
	super(engine);
	this.name = name;
	this.cols = cols;
	this.ttl  = ttl;
	this.rows = new LinkedList<Row>();
    }

    public long getTTL() {
	return ttl;
    }

    public RowIterator getIterator() {
	return new RowIterator(rows.iterator());
    }

    public RowIterator getQueryIterator( Row r ) {
	return new RowIterator(rows.iterator(), r);
    }

    public void dumpTable() {
	System.out.println("TABLE: "+name);
	for( Row r : rows ) {
	    System.out.println( r );
	}
    }

    /**
     * The RowIterator class does little more than wrap an underlying
     * list iterator.  However, this iterator also takes care of "timing out"
     * rows that have expired their liftimes.
     */
    public class RowIterator implements Iterator<Row> {
	Iterator<Row> it;
	Row buffer;
	Date now;
	Row queryRow;

	public void remove() throws UnsupportedOperationException {
	    it.remove();
	}

	public RowIterator(Iterator<Row> it)
	{
	    this.it = it;
	    this.buffer = null;
	    now = getNow();
	}

	public RowIterator(Iterator<Row> it, Row queryRow)
	{
	    this(it);
	    this.queryRow = queryRow;
	}

	void fetchNext() {
	    while(buffer == null && it.hasNext()) {
		Row temp = it.next();
		if( temp.timeout == null || temp.timeout.after(now) ) {
                    if( queryRow == null || queryRow.matches(temp) ) {
			buffer = temp;
		    }
		} else {
		    //System.out.println("row expired");
		    try { it.remove(); } catch (UnsupportedOperationException ex) {}
		}
	    }
	}

	public boolean hasNext() {
	    fetchNext();
	    return buffer != null;
	}

	public Row next() throws NoSuchElementException {
	    fetchNext();
	    if( buffer == null ) {
		throw new NoSuchElementException();
	    } else {
		Row temp = buffer;
		buffer = null;
		return temp;
	    }
	}
    }


    public void storeRow( Row r ) {
	rows.add( r );
    }

    public void removeRow( Row r ) {
	Iterator it = rows.iterator();

	while(it.hasNext()) {
	    Object x = it.next();
	    if( r.equals(x) ) {
		it.remove();
		return;
	    }
	}
    }

    public void removeAllRows( Row r ) {
	Iterator it = rows.iterator();
	
	while(it.hasNext()) {
	    Object x = it.next();
	    if( r.equals(x) ) {
		it.remove();
	    }
	}
    }
}
