/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *  This class embodies the Suppl runtime system.  It is essentially
 *  the tuProlog "Prolog" class enhanced with some additional features.
 *  These features are largely implemented as additional libraries, so
 *  the main task of this class is to load these additional custom libraries.
 */
public class SupplEngine extends alice.tuprolog.Prolog {
    protected JavaLibrary jlib;
    protected TableLib tlib;
    protected ContainersLibrary clib;
    protected PrimLibrary plib;

    /**
     *  Get access to the loaded JavaLibrary.  This libraray is useful for
     *  getting access to dynamically loaded objects.
     */
    public JavaLibrary getJavaLib() {
        return jlib;
    }

    /**
     *  Set up a new Suppl engine.  This consists of loading custom libraries
     *  and making sure the primitive theory is loaded.  The primitive theory
     *  is a file of built-in Prolog code that provides some basic services.
     *  The primitive theory is embedded in the .jar as a resource file 
     *  named "primTheory.pl".
     */
    public SupplEngine( ) throws Exception {
	super();
	
	jlib = (JavaLibrary)
	    loadLibrary("alice.tuprolog.lib.JavaLibrary");

	plib = new PrimLibrary(jlib);
	loadLibrary(plib);

	tlib = new TableLib(jlib);
	loadLibrary(tlib);

	clib = new ContainersLibrary(jlib);
	loadLibrary(clib);

        InputStream primInStream = 
	    RunPolicy.class.getResourceAsStream("primTheory.pl");
        Theory primTheory = new Theory(primInStream);
        addTheory(primTheory);
    }

    /**
     * Register an object with the Suppl engine.
     *
     * To make arbitrary Java objects avaliable in policies, use this method
     * to register the object.  Each object will be given a canonical name to
     * represent it, which is the returned Struct.  Given such a struct, the
     * registered object can be retrieved by calling the getRegisteredObject()
     * method.
     *
     * @param o  The object to register
     * @return A canonical Prolog representation of the given Java object
     */
    public Struct registerObject(Object o) {
	return plib.registerObject(o);
    }

    /**
     * Look up a previously-registered object by its name.
     *
     * @param t  The canonical Prolog representation of a Java object
     * @return The bound Java object
     */
    public Object getRegisteredObject(Struct t) 
	throws alice.tuprolog.lib.InvalidObjectIdException
    {
	return jlib.getRegisteredDynamicObject(t);
    }
}
