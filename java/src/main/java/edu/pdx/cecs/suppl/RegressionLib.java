/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 *  This class provides runtime services that are useful for running regression tests.
 */
public class RegressionLib extends Library {
    JavaLibrary jlib;

    static final String libTheory = "";

    public String getTheory() { return libTheory; }
    
    static class RegressionTest {
        public int num;
        public Term location;
        public Term query;
    }

    static class RegressionHandler {
	public Term location;
	public Term event;
	public Term actions;
    }

    List tests;

    public RegressionLib( JavaLibrary jlib ) {
	this.jlib = jlib;
        this.tests = new LinkedList();
    }

    /**
     *  This provides the Prolog predicate run_handler_regression/3
     *
     *  @param arg1 source file location of the testing pragma
     *  @param arg2 the event to handle
     *  @param arg3 the list of expected actions
     */
    public boolean run_handler_regression_3( Term arg1, Term arg2, Term arg3 ) {
        arg1 = arg1.getTerm();
        arg2 = arg2.getTerm();
	arg3 = arg3.getTerm();

        System.out.println("Registering a handler regression: " +
                           arg1.toString() +
                           " " +
                           arg2.toString() +
                           " " +
                           arg3.toString());

	RegressionHandler t = new RegressionHandler();
	t.location = arg1;
	t.event = arg2;
	t.actions = arg3;

        tests.add(t);

	return true;
    }

    /**
     *  This provides the Prolog predicate run_query_regression/3
     *
     *  @param arg1 the number of expected solutions
     *  @param arg2 source file location of the testing pragma
     *  @param arg3 the goal to execute
     */
    public boolean run_query_regression_3( Term arg1, Term arg2, Term arg3 ) {
        arg1 = arg1.getTerm();
        arg2 = arg2.getTerm();
        arg3 = arg3.getTerm();

        System.out.println("Registering a query regression: " +
                           arg1.toString() +
                           " " +
                           arg2.toString() +
                           " " +
                           arg3.toString());

        RegressionTest t = new RegressionTest();
        t.num = ((alice.tuprolog.Number) arg1).intValue();
        t.location = arg2;
        t.query = arg3;

        tests.add(t);

	return true;
    }
    
    /**
     *  Run a query regression test.  Ensure the expected number of solutions
     *  matches the actual number of solutions found by the policy engine.
     */
    public void runQueryRegression( RegressionTest t ) throws Exception {
	int n = 0;
	SolveInfo info = engine.solve(t.query);
	while( info.isSuccess() ) {
	    n++;
                    
	    if(engine.hasOpenAlternatives()) {
		info = engine.solveNext();
	    } else {
		break;
	    }
	}
	    
	if( n == t.num ) {
	    System.out.println("test ok");
	} else {
	    System.err.println("expected: "+t.num+" got: "+ n);
	    System.err.println("  " + t.location.toString());
	    System.err.println("  " + t.query.toString());
	}
    }

    /**
     *  Run a handler regression test.  Ensure the actions returned by the policy
     *  match the expected list of actions indicated in the test.
     */
    public void runHandlerRegression( RegressionHandler t ) throws Exception {
	Term q = new Struct("executeHandlerList",t.event, new Var("ACTIONS"));

	SolveInfo info = engine.solve(q);
	if( info.isSuccess() ) {
	    Term actions = info.getTerm( "ACTIONS" );
	    if( engine.unify( actions, t.actions ) ) {
		System.out.println("handler test ok");
	    } else {
		System.err.println("handler regression failed");
		System.err.println("  " + t.location.toString());
		System.err.println("  " + t.event.toString());
		System.err.println("  expected: " + t.actions.toString());
		System.err.println("  got: "+actions.toString());
	    }
	} else {
	    System.err.println("handler regression failed");
	    System.err.println("  " + t.location.toString());
	    System.err.println("  " + q.toString());
	}
    }

    /**
     *  Query the policy to determine what regression tests to run by
     *  executing the "setup_regression" query.  Then enumerate and run the
     *  registered tests.
     */
    public void runRegressions() throws Exception {
	Struct q = new Struct("setup_regression");
	SolveInfo info = engine.solve(q);

	while(info.isSuccess()) {
	    if(engine.hasOpenAlternatives()) {
		info = engine.solveNext();
	    } else {
		break;
	    }
	}

	for (Object t : tests) {
	    try {
		if( t instanceof RegressionTest ) {
		    runQueryRegression( (RegressionTest) t );
		} else if (t instanceof RegressionHandler ) {
		    runHandlerRegression( (RegressionHandler) t );
		}
	    } catch (Exception ex) {
		ex.printStackTrace();
	    }
	}
    }

    /**
     *  Load the files named on the command line and execute any embedded 
     *  dynamic regression tests.
     */
    public static void main(String[] args) throws Exception {
	SupplEngine engine = RunPolicy.setupEngine();
        RegressionLib rlib = new RegressionLib(engine.jlib);
        engine.loadLibrary(rlib);

        RunPolicy.loadTheories(args, engine);

        rlib.runRegressions();
    }
}
