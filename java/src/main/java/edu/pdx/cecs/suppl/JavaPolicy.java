/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

import java.lang.reflect.*;
import java.security.*;
import java.net.*;
import java.io.*;
import java.util.*;
import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;

import alice.tuprolog.*;

import edu.pdx.cecs.suppl.*;

public class JavaPolicy extends Policy {
    
    SupplEngine engine;
    Subject subject;

    public JavaPolicy( SupplEngine engine, Subject subject ) {
        this.engine = engine;
        this.subject = subject;
    }

    public static void main( String args[] ) 
        throws Exception
    {
        if( args.length < 2 ) {
            System.err.println("specify a main class and policy file");
        }
        String mainClass = args[0];
        String policyFile = args[1];

        int i = 2;

        // Build up a list of URLs for getting code.  Stop
        // if we encounter "--" on the command line
        ArrayList<URL> urls = new ArrayList<URL>();
        while( i < args.length && !args[i].equals("--") ) {
            urls.add( new URL( args[i] ) );
            i++;
        }

        // Collect any remaining arguments after the "--" to pass
        // on to the loaded main
        ArrayList<String> remainingArgs = new ArrayList<String>();
        for( i++; i < args.length; i++ ) {
            remainingArgs.add(args[i]);
        }

        // Login the user, get the resulting subject object and set it to read only
        LoginContext loginContext = new LoginContext( "JavaSUPPLPolicyLogin" );
        loginContext.login();
        Subject subject = loginContext.getSubject();
        subject.setReadOnly();

        // Set up the SUPPL engine
        SupplEngine engine = RunPolicy.setupEngine();
        JavaPolicyLib jplib = new JavaPolicyLib( engine.getJavaLib() );
        engine.loadLibrary(jplib);
        engine.addTheory(new Theory(new FileInputStream(policyFile)));

        // Build a class loader for loading the application code
        ClassLoader cl = new URLClassLoader( urls.toArray( new URL[0] ) );

        // Build the policy object and set the policy
        JavaPolicy policy = new JavaPolicy( engine, subject );
        Policy.setPolicy(policy);

        // Install a security manager, if there is not one already
        if( System.getSecurityManager() == null ) {
            System.setSecurityManager(new SecurityManager());
        }

        // Launch the sub-task
        launch( subject, cl, mainClass, remainingArgs.toArray( new String[0] ) );
    }

    /** Given a logged-in subject and a classloader, launch the "main" method of
        the given class, with the given args.

        NOTE! The called code
        may eleveate its privilges again by using a "doAsPrivileged" call!
        In this case, that code will run in the protection
        domain assigned by the given classloader, which may have additional
        privileges.  Any permissions allowed by the classloader will
        automatically bypass the SUPPL policy!  Be aware!
     */
    public static void launch( final Subject subj, 
                               final ClassLoader cl,
                               final String className,
                               final String[] args )
    {
        try {
            // Set up a dummy restricted protection domain.  This domain has no
            // built-in permissions at all.
            final ProtectionDomain restrictedDomain =
                new ProtectionDomain( new CodeSource( new URL("file:restricted!"),
                                                      (CodeSigner []) null ),
                                      new Permissions(), null, null );

            // Use reflection to invoke the main method of the given class.
            // This method is executed in the dummy protection domain above,
            // which ensures that our custom policy is interrogated when
            // security-sensitive actions are taken (unless the client
            // elevates with a doAsPrivilged).
            Subject.doAsPrivileged( subj, new PrivilegedAction<Object>() {
                    public Object run() {
                        try {
                            Class<?> c = cl.loadClass(className);
                            Method m = c.getMethod("main", args.getClass() );
                            m.invoke( null, new Object[]{ args } );
                        } catch (Throwable th) { th.printStackTrace(); }
                        return null;
                    }
                },
                new AccessControlContext( new ProtectionDomain[]{ restrictedDomain } )
                );
        } catch( MalformedURLException ex ) {
            ex.printStackTrace();
            System.exit(1);
        }

    }


    /** @override */
    public boolean implies( final ProtectionDomain domain, final Permission perm ) {

        // Immediately allow any "base" permissions that were granted at classloading time.
        // NOTE! Base permissions like this bypass calling into the SUPPL policy!
        // This is important, because it prevents infinite-looping or deadlock when code that
        // implements the SUPPL engine itself performs security-sensitive calls.
        PermissionCollection perms = this.getPermissions( domain );
        if( perms != null && perms.implies(perm) ) { return true; }

        // Otherwise, raise privileges and evaluate the policy program
        Boolean allow =
            AccessController.doPrivileged(new PrivilegedAction<Boolean>() {
                    public Boolean run() {
                        boolean allow = false;

                        try {
                            allow = evaluatePolicy(domain,perm);
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }

                        return new Boolean(allow);
                    }
                });

        return allow.booleanValue();
    }


    /** Evaluate the SUPPL policy to determine if the requested action is allowed.
     */
    synchronized boolean evaluatePolicy( ProtectionDomain domain, Permission perm ) 
        throws Exception
    {
        Term domTerm = engine.registerObject( domain );
        Term permTerm = engine.registerObject( perm );
        Term subjTerm = engine.registerObject( subject );

        Struct q = new Struct("permRequest",domTerm,subjTerm,permTerm);

        List<Term> tms = RunPolicy.handleEvent( engine, q );

        for( Term t : tms ) {
            t = t.getTerm();
            if( t instanceof Struct ) {
                if( ((Struct) t).getName().equals("allow") ) {
                    return true;
                }
            }
        }

        System.out.println("Policy says no: "+perm.toString());
        return false;
    }
 
}

