/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

package edu.pdx.cecs.suppl;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

/** 
 *  This class provides easy access to the Suppl runtime system.
 */
public class RunPolicy {

    static class StdoutListener implements OutputListener {
	public void onOutput(OutputEvent e) {
	    System.out.print(e.getMsg());
	}
    }

    static class StderrWarnings implements WarningListener {
        public void onWarning(WarningEvent e) {
            System.err.println(e.getMsg());
        }
    }

    /**
     *  Load the given compiled policy files.
     *
     *  @param filenames   An array of file names to load
     *  @param engine      The Suppl runtime engine to load the files into
     */
    public static void loadTheories(String[] filenames, SupplEngine engine) throws Exception {
	for (String x : filenames) {
	    engine.addTheory(new Theory(new FileInputStream(x)));
	}
    }

    /**
     *  Build a Suppl engine and load the standard libraries.
     *
     *  @return A Suppl runtime engine with no policy loaded.
     */
    public static SupplEngine setupEngine() throws Exception {
	SupplEngine engine = new SupplEngine();

	engine.addOutputListener(new StdoutListener());

        // Uncomment to get warnings.  Turned off now because
        // it produces false positives for some reason and often
        // does not fire when I want it to.  Ugh... :(

        // engine.addWarningListener(new StderrWarnings());

	return engine;
    }

    /**
     *  Build a Suppl engine and load the standard libraries and load the
     *  named policy files.
     *
     *  @param filenames An array of file names to load
     *  @return A Suppl runtime engine with the given policy files loaded.
     */
    public static SupplEngine setupEngine(String[] filenames) throws Exception {
        SupplEngine engine = setupEngine();
        loadTheories(filenames, engine);
        return engine;
    }


    /**
     *  Invoke the policy on a given action.
     *
     *  @param engine  The Suppl runtime engine to use.
     *  @param action  A term representing the event to hand to the policy
     *  @return The list of actions produced by the policy
     */
    public static List<Term> handleEvent(SupplEngine engine, Term action) throws Exception {
	Struct query = new Struct("executeHandler", action, new Var("RESPONSE"));

	LinkedList<Term> rs = new LinkedList<Term>();

	SolveInfo info = engine.solve(query);

	while(info.isSuccess()) {
	    Term resp = info.getVarValue("RESPONSE");
	    if( resp != null && resp instanceof Struct ) {
	    	rs.add(resp);
	    }
	    if(engine.hasOpenAlternatives()) {
		info = engine.solveNext();
	    } else {
		break;
	    }
	}
	return rs;
    }

    /**
     *  Invoke the policy on a given action.
     *
     *  @param engine  The Suppl runtime engine to use.
     *  @param name    The name of the action to handle
     *  @param evArgs  An array of arguments for the action.  These strings will be parsed by the tuProlog
                       engine as Prolog terms.
     *  @return The list of actions produced by the policy
     */
    public static List<Term> handleEvent(SupplEngine engine, String name, String[] evArgs)
	throws Exception {
	Term[] tms = new Term[evArgs.length];
	for(int i=0; i<evArgs.length; i++) {
	    tms[i] = engine.toTerm(evArgs[i]);
	}
	
	Term action = new Struct(name, tms);
	return handleEvent(engine, action);
    }

    /**
     *  Invoke the policy on a given action.
     *
     *  @param engine  The Suppl runtime engine to use.
     *  @param str     The action to handle.  The string will be parsed by the tuProlog engine
                       as a Prolog term.
     *  @return The list of actions produced by the policy
     */
    public static List<Term> handleEvent(SupplEngine engine, String str)
	throws Exception {
	Term q = engine.toTerm(str);
	return handleEvent(engine, q);
    }

    /**
     *  Given a configured Suppl engine, continuously execute the policy on the given
     *  input and output streams.
     *
     *  This method will loop indefinately, reading input lines from "in" until it is closed.
     *  Each input line is expected to parse as a Prolog term representing an action to handle.
     *  The engine will be invoked on the given action string: any produced actions will be
     *  output on the "out" writer.
     *
     *  @param engine   The Suppl engine representing the policy.
     *  @param in       An input stream from which to read policy actions.
     *  @param out      An output stream where output actions will be written.
     */
    public static void runPolicy( SupplEngine engine, Reader in, Writer out )
	throws Exception {
	BufferedReader rin = new BufferedReader(in);
	BufferedWriter rout = new BufferedWriter(out);

	while(true) {
	    String input = rin.readLine();
	    if(input == null) { break; }
	    if(input.length() == 0) { continue; }
	    
	    List<Term> rs;
	    try {
		synchronized(engine) {
		    rs = handleEvent(engine, input);
		}

		String x = rs.toString();
		rout.write(x);
		rout.write('\n');
		rout.flush();

	    } catch (Exception ex) {
		ex.printStackTrace();
	    }
	}
    }

    static class ClientHandler extends Thread {
	Socket sock;
	SupplEngine engine;

	public ClientHandler( SupplEngine engine, Socket sock ) {
	    this.engine = engine;
	    this.sock = sock;
	}

	// Run the policy on the given connection socket
	public void run () {
	    try {

		runPolicy( engine,
			   new InputStreamReader(sock.getInputStream()),
			   new OutputStreamWriter(sock.getOutputStream()) );

	    } catch (Exception ex) {
		ex.printStackTrace();
	    } finally {
		try { sock.close(); } catch (Exception ex) { ex.printStackTrace(); }
	    }
	}
    }

    static class ServerHandler extends Thread {
	ServerSocket ssock;
	SupplEngine engine;

	public ServerHandler(SupplEngine engine, ServerSocket ssock) {
	    this.engine = engine;
	    this.ssock = ssock;
	}

	// Listen on the given server socket, spawing new handler threads for
	// each incomming connnection.
	public void run () {
	    try {
		while(true) {
		    Socket sock = ssock.accept();
		    ClientHandler chdl = new ClientHandler(engine, sock );
		    chdl.start();
		}
	    } catch( Exception ex ) {
		ex.printStackTrace();
	    } finally {
		try { ssock.close(); } catch (Exception ex) { ex.printStackTrace(); }
	    }
	}
    }

    /**
     *  Given a configured Suppl engine, continuously execute the policy on the
     *  given server socket.
     *
     *  This method waits for connections on the given streamsocket.  When a client connects,
     *  the stream socket connection is fed into the "runPolicy" method, which allows the
     *  connected client to directly produce actions and receive the produced events.
     *
     *  @param engine  The Suppl runtime engine to use.
     *  @param ssock   A server socket to listen for connections on
     */
    public static void runPolicyOnSocket(SupplEngine engine, ServerSocket ssock)
	throws Exception {

	ServerHandler shdl = new ServerHandler(engine, ssock);
	shdl.start();
    }

    /**
     *  Given a configured Suppl engine, continuously execute the policy on the
     *  terminal.
     *
     *  The standard in and standard out streams are directly connected to the policy
     *  engine, given a basic REPL access to the policy engine.
     *
     *  @param engine  The Suppl runtime engine to use.
     */
    public static void runPolicyOnTerminal(SupplEngine engine)
	throws Exception {

	runPolicy( engine,
		   new InputStreamReader(System.in),
		   new OutputStreamWriter(System.out) );
    }


    /**
     *  Main entry point.  This takes a list of command line arguments
     *  as policy files to load and execute the policy defined by those
     *  files on the terminal.
     */
    public static void main(String[] args) throws Exception {
	SupplEngine engine = setupEngine(args);
	runPolicyOnTerminal(engine);
    }
};
