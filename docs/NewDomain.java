import java.util.*;
import alice.tuprolog.*;
import edu.pdx.cecs.suppl.*;

public class NewDomain {

    public static void main( String[] args ) throws Exception {

	// Build the interpreter engine a

	SupplEngine engine = RunPolicy.setupEngine();

	// Set up and install our own domain-specific library into
        // the interpreter.
	NewLibrary lib = new NewLibrary(engine);
	engine.loadLibrary(lib);
  // Load the policy files names in 'args'
  RunPolicy.loadTheories(args, engine);

	System.out.println( "engine is setup!" );
	System.out.println();

	// a straightforward demonstaration of how
        // to use the policy API and the tuProlog Term API
	simpleEvent(engine);

	// a more sophistocated example demonstrating
        // passing Java objects into the policy space
        // as abstract data types
        complexEvent(engine);
    }

    static void simpleEvent(SupplEngine engine) throws Exception {

	// Construct an example event using the Term API
	Term[] evargs = new Term[] {
	    new Struct( "string literal" ),
	    new Int( 6 )
	};
	Struct event = new Struct( "notification", evargs );

	System.out.println( "handling a simple event:" );
	System.out.println( event.toString() );
	System.out.println();

	// Invoke the policy!

	// Taking the engine monitor is unnecessary here, but this
        //   demonstrates the suggested usage.  Likewise, any
        //   later modifications of the engine (e.g., loading new files)
        //   should be synchronized.
	List<Term> actions;
	synchronized(engine) {
	    actions = RunPolicy.handleEvent(engine, event);
	}

	// Now inspect the actions and do something with them
	System.out.println();
	System.out.println( "handling the policy results:" );

	for (Term t : actions) {
	    System.out.println();

	    // DON'T FORGET RULE #1
	    t = t.getTerm();

	    // Check if we got something that looks like an action
	    if( !(t.isCompound() || t.isAtom()) ) {
		System.err.println( "bizzare policy output! " + t.toString() );
		System.err.println( "this should never happen" );
		continue;
	    }

	    Struct s = (Struct) t;
	    String action_name = s.getName();
	    int arity = s.getArity();

	    System.out.println( "full term: " + s.toString() );
	    System.out.println( "action: " + action_name );
	    System.out.println( "arity:" + arity );

	    for(int i = 0; i < arity; i++ ) {
		Term x = s.getArg(i);
		// RULE #1
		x = x.getTerm();
		System.out.println( "  arg " + i + ": " + x.toString() );
	    }

	}
    }

    // I plan to add an additional example here demonatrating
    // passing Java objects in events and actions.
    static void complexEvent(SupplEngine engine) throws Exception {
    }

}
