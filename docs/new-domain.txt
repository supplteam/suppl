How to set up the policy language for use in a problem domain
=============================================================

There are two main ways the policy language interacts with its
environment: through the event handling/action returning mechanism and
via the use of primitive queries and functions.  The main task to be
completed when setting up a new domain is to decide what these
events/actions/primitives should be, write down their formal
definitions in the policy language and provide implementations for any
primitives.  Marshalling code may also need to be written to translate
events and actions from their external representation into the
internal represetnation.

The main API of the policy engine allows the client to present to the
policy module a prolog term representing an event to be handled.  The
policy will execute its handlers, run queries, etc. and will
eventually return a list (possibly empty) of actions to be undertaken
as a result of the event; these actions will also be represented as Prolog
terms.  The API for building and accessing these terms is defined by
the tuProlog library (see http://tuprolog.sourceforge.net/doc/api/).
The (fairly simple) way that policy language events and actions are
mapped onto prolog terms is described below.

The semantics of events and actions is essentially a black box to the policy
language; their meaning is defined by how the surrounding application translates
stimuli into event terms and how it interprets the returned action terms.

Likewise, the meaning of primitive predicates and functions is unknown
to the policy language, but is defined externally.  The implementation
of primitives may be given either by writing some prolog code and
loading it into the tuProlog engine before starting the policy; or by
writing Java code using the foreign-code mechanisim provided by
tuProlog; or by a combination of these two methods.  For a general description of
how the tuProlog foreign code mechanism works, consult chapter 7 of the
tuProlog guide; here I will only cover the highlights.

NOTE: the one main restriction on using Java to implement Prolog primitives
is that Java implemented predicates do not support backtracking, and thus must be
deterministic.  A nondeterministic language primtive must therefore be implemented
via Prolog code (possibly calling into Java code).

Example code
============

Example code is provided as a companion to this document.  The "new-domain.plc"
and "NewDomain.java" files in this directory implement the examples discussed
in this document and provide a hands-on way to understand the concepts discussed
here. The makefile in this directory should build and run the example, provided
the policy compiler and java library have previously been built.

Additionally, the "NewLibrary.java" file gives an exemplar of how to build
a new tuProlog extension libraray to implement language primitives.

Events and actions
===================

Events and actions are the main way the policy language interacts with its environment.
Events are declared in the policy language using "event" declarations, and actions
are declared using  "action" declarations.  For example:

event notification(string, number).
action lockdown.
action raise_alarm( number ).


This sets up an event called "notification" with two arguments: a string and a number.
It also sets up two actions: "lockdown", that takes no
arguments, and "raise_alarm", that takes a single numeric argument.

Concretely, instances of events and actions are represnted by Prolog terms
with corresponding shape.  For example, an instance of each of the above
events/actions looks like:

notification( 'asdf', 42 )
lockdown
raise_alarm( 10 )

Thus, the translation between the policy language and Prolog representation
of events and actions is nearly trivial.  The only wrinkle comes in when
we consider events or actions with _named_ arguments.  Prolog has no
syntax for named arguments, so named argument sets are translated into
positional arguments.  The arguments appear in the order of their
declaration.  For example:

event namedEvent{ firstArg : string, secondArg : string }.

Has two arguments, named suggestevly.  In instance of the event,
in the policy language, might look like:

namedEvent{ firstArg: "asdf", secondArg: "qwerty" }

This is exactly identical to:

namedEvent{ secondArg: "qwerty", firstArg: "asdf" }

When translated into Prolog, the arguments are rendered in the order
of their event declaration, so the Prolog version of this event is:

namedEvent( "qwerty", "asdf" )

The same scheme is used for actions/primitive procedures and also for
primitive predicates.

In Java-side code, Prolog terms may be manipulated via tuProlog API;
specifically the "Term" class and its subclass "Struct".  See below
for some comments about using the tuProlog API and some gotchas to avoid.

Using the RunPolicy API
=======================

The main entry point into the policy engine is the "RunPolicy"
class (see "java/RunPolicy.java").  The static method
"setupEngine" takes a list of file names and sets up a tuProlog
interpreter engine; the file names provided are loaded into the
engine, along with the basic libraries needed to run policies.
The provided files should be compiled policy files or Prolog
code (e.g., for implementing primitives).  Once the Prolog engine
is set up, the "handleEvent" method is the main way to interact
with the policy.  "handleEvent" takes the interpreter engine object
and a Term object representing the event to handle and returns
a list of Term objects, representing the actions returned by the policy
in response to the event.

In addition there are some utility methods for running policies
on input and output streams; this is useful mainly for debugging
purposes.  This mode parses Prolog terms from the input stream,
interpreting them as actions, and outputs the generated action
terms on the output stream.

Threading note: multithreaded apps should hold the monitor on the Prolog
engine object while calling the handleEvent method; the tuProlog interpreter
itself is not thread-safe.

The tuProlog Term API
======================

To interact with the policy language, the main thing you need to know about the
Term API is how to construct and how to examine Term objects.  The Term
class has three subclasses: Number, Struct and Var.  "Number" is an abstract
subclass representing all numeric data in tuProlog; it is pretty straightforward.
The "Var" class represents tuProlog variables.  For the policy language,
all events actions should be ground (i.e., not contain any variables) so we will
not be much interested in "Var".  The "Struct" subclass represents pretty much
everything else: atoms (identifiers, essentially), compound terms 
(identifiers applied to arguments) and lists.

--Some basic rules and tips for using this interface --

*** RULE #1 ***
ALWAYS CALL getTerm() BEFORE TRYING TO USE A TERM.  For example,
suppose I just retrieved "x" from a list iterator.  The next line of
code should be:

x = x.getTerm();

For internal implementation reasons, tuProlog does not always resolve references
to variables.  If you do not call getTerm(), you may have a variable object
instead of the expected term, even though policy computations are expected to
produce ground terms.  The getTerm() method resolves variables, getting
the actual term object you expect.  If the object is not a variable, it does nothing.
If you forget to do this, downcasts will fail in unexpected places, various other
things will break, and it will be hard to figure out why.  SO DON'T FORGET.


** To get the name of an atom or compound form (a compound is an identifier
applied to some arguments) use the getName() method

** Use the getArg() method to get the arguments of a compound form.

** Oddly, tuProlog treats atoms and string literals the same; thus to get the value
of a string literal, use the Term.getName() method.

** Lists are treated specially.  Use the listIterator() method to get an iterator
   for accessing the elements of a list.


Implementing language primitives
================================

The other major way the policy language interacts with its environment
is via primitive predicates.  Primitive predicates are declared
in the policy language with a "primitive predicate" declaration,
which behaves exactly like the regular "predicate" declaration;
furthermore "mode" declarations work pretty much the same way.

Providing an implemenation for these primitives boils down to
making sure the tuProlog interpreter knows what to do when
the named predicate is called.  Ususally this means loading a
custom Prolog library into the interpreter.  A custom library
is built by writing a Java class that extends the tuProlog
"Library" class.  Libraries may contain raw Prolog code
and/or specially-named Java functions that the interpreter will
automatically invoke when the named predicate is called.

Suppose our policy contains the following declarations:

data asdf ::= mkAsdf(string).
primitive predicate primOp( string, asdf ).
mode primOp(in,in).
mode primOp(in,out).

These lines declare a new type named "asdf" with a single
constructor function named "mkAsdf", which takes a string
argument.  Next we declare "primOp" as a primitive predicate
that relates a string and a value of type "asdf"; futher
we declare this operation to have the (in,in) and (in,out) modes.

To implement "primOp" as a Java function in a custom library,
the library class must contain methods with the following signatures:

  public boolean primOp_ii_2( Term, Term );
  public boolean primOp_io_2( Term, Term );

The main thing to note is that the name of the function is a slightly-mangled
version of the predicate name; the mangling indicates the mode of the
predicate and the number of arguments it takes. The "primOp_ii_2"
function implements the primOp predicate considered at the (in,in)
mode, whereas "primOp_io_2" represents the (in,out) mode.

The tuProlog interpreter will automatically examine custom library
classes when they are loaded, looking for methods that conform to this
naming scheme.  Any methods found are then made avaliable in the
interpreter.  If the naming scheme is too inflexible, there are other
ways to bind names to Java functions: see the tuProlog documentation
(section 7.3) for more info.

When a predicate is called from the Prolog program, the associated
Term arguments become arguments to the Java method; the return value
from the function indicates if the predicate succeeded or failed.

If we want to use primitive predicates to calculate some output values
from input values, the easiest way to accomplish this is the
Prolog.unify() method.  If 'x' and 'y' are terms and 'engine' is
the prolog engine, the call 'engine.unify(x, y)' will succeed
if x and y are terms such that the free variables occuring
in x and y can be filled in a way such that the resulting
expressions are equal.  If x and y have no variables, then
unify is essentially an equality test.  In the particular
case where y is simply a variable, "engine.unify(x, y)" will
succeed and have the effect of assigning the value of x to
y.  Unification is symmetric, so "engine.unify(x, y)" will
have the same practical effect as "engine.unify(y, x)".

Consider the following code:

    public boolean primOp_io_2( Term arg1, Term arg2 ) {
	arg1 = arg1.getTerm();
	Struct x = new Struct("mkAsdf", arg1);
	return this.engine.unify(x, arg2);
    }

This Java code will implement the "primOp" predicate from above.
It simply wraps the first argument in a "mkAsdf" constructor
and attempts to unify the resulting term with the second argument.
Thus, "primOp(X,Y)" has basically the same effect as "X = mkAsdf(Y)".

NOTE! Again, it is not directly possible to implement nondeterministic
predicates in Java.  Instead, some cooperation must be engineered
between Java and Prolog.

Name mangling details
=====================
Name mangling for primitive predicates occurs in two steps.
First the policy compiler appends the mode string; thus
in Prolog, the "primOp" predicate from above is split into
two different predicates based on mode: "primOp_ii" and "primOp_io".
Thus, if implementing primitives in Prolog, only mode mangling
should be considered.  Second, the number of argument suffix
is added automatically by the tuProlog Prolog-to-Java interface;
this is why the Java name for these predicates becomes
"primOp_ii_2" and "primOp_io_2".

There are three argument modes: in, out, and ignore.  The
in mode is mapped to the character "i", the out mode is
mapped to the character "o" and the ignore mode is mapped to
character "x".  So, for example, mode (in,in,ignore,out) is
mapped to suffix "_iixo".  Predicates with named arguments
are mangled in the order the arguments appear in their
predicate declaration.  NOTE this is not necessarily
the same order as the mode declaration!



Abstract Data Types (or: passing Java objects through tuProlog)
===============================================================

One might wish to expose some part of a Java class's behavior
to a policy program to facilitate code reuse or for performance
reasons, or for a number of other possible reasons.  The best
way to do this is to declare a "primitive type" in the policy
language; the "implemenation" of this primitive type will then
be a Java class, and the collection of primitive predicates
associated with that type will be thin wrappers around the
class's methods.  What makes all this work is tuProlog's ability
to pass around arbitrary Java object references and treat them
as ordinary Prolog values.  

Suppose we want to expose a type of complex numbers in the policy
language.  We might write down the following abstract API:


primitive type complex.
primitive predicate complex_coordinates( number, number, complex ).
mode complex_coordinates(in,in,out).

primitive predicate complex_add( complex, complex, complex ).
mode complex_add(in,in,out).

primitive predicate print_complex( complex ).
mode print_complex(in).


The predicate "complex_coordinates" allows us to construct a
complex number when given the real and imaginary coordinates;
the predicate "complex_add" adds two complex numbers (giving
their addition as the result).  Finally "print_complex" is
a predicate that always succeeds, but has the side effect
of printing the complex coordinates.

!!!NOTE!!! It is entirely possible to write predicates that perform
side-effects, e.g., print_complex causes output to appear.  THIS IS
DISCOURAGED IN THE STRONGEST POSSIBLE TERMS.  The policy language
compiler assumes that predicate calls are side-effect free and may
rearrange program code so that primitive predicates are called in a
different order or a different number of times than a direct reading
of the policy program would imply. SUBTLE BUGS AND VERY WEIRD BEHAVIOR
WILL RESULT IF PREDICATES CAUSE OR RELY ON SIDE EFFECTS.

The print_complex predicate above is intended for debugging purposes
and is largely harmeless, but serves to illustrate the point: general
purpose Java code may cause unlimited side-effects.  On the other
hand, primitive predicate code is assumed not to cause observable
side-effects by the compiler. Be aware of this potential problem.

To implement a Java-side ADT, first implement the desired behavior
as a Java class.  Then, the trick is to use the "registerObject()"
and "getRegisteredObject()" methods of the SupplEngine class.
The "registerObject()" method takes an arbitrary Java object
reference, generates a fresh identifier, and binds that Java object
reference to the identifier.  This identifier can now be passed around
as an ordinary Prolog term.  If the identifier is later passed into
a method implementing a primitive predicate, the associated object
can be retrieved via the "getRegisteredObject()" method.

All the details of how to make this work are spelled out in the
accompaning file "NewLibrary.java", found in this "docs" directory.
