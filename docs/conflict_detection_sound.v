(* Coq proof of conflict detection soundness *)

Require Import List.
Require Import ZArith.
Import ListNotations.

Definition ident := Z.

Inductive term :=
| var : ident -> term
| nlit : Z -> term.

Definition args := list term.

Inductive clause := 
| eq : term -> term -> clause
| pred : ident -> args -> clause
| and : clause -> clause -> clause
| neq : term -> term -> clause.

Inductive qterm :=
| bindvar : ident -> qterm
| refvar : ident -> qterm.

Definition qargs := list qterm.

Inductive qclause := 
| qeq : qterm -> qterm -> qclause
| qpred : ident -> qargs -> qclause
| qand : qclause -> qclause -> qclause
| qneq : qterm -> qterm -> qclause.

Inductive body :=
| action : ident -> args -> body
| seq : body -> body -> body
| foreach : qclause -> body -> body
| query : branch -> body
| skip : body
with branch :=
| querybranch : qclause -> body -> branch -> branch
| defaultbranch : body -> branch -> branch
| empty : branch.

Scheme indbody := Induction for body Sort Prop
with indbranch := Induction for branch Sort Prop.
Combined Scheme indbb from indbody, indbranch.

Inductive prog :=
| handle : ident -> qargs -> body -> prog.

Definition env := term -> option Z.

Inductive guard :=
| pguard : qclause -> guard
| nguard : qclause -> guard.

Definition path := list guard.

Inductive conflictdecl := 
| ConflictDecl : ident -> qargs -> ident -> qargs -> clause -> conflictdecl.
Inductive pconflict :=  
| Pconflict : (list guard) -> (list guard) -> (list guard) -> 
              args -> args -> conflictdecl -> pconflict.

(* Given actions and their arguments, tells if they are in conflict *)
Parameter is_conflict : (env * ident * args) -> (env * ident * args) -> bool.

(* Given actions, their args and a conflict decl, tells if they "conform" to the conflict decl *)
Parameter is_conflict' : (ident * args) -> (ident * args) -> conflictdecl -> bool.

(* Given actions and their args, returns whether they are in conflict, and such a conflict decl if they are *)
Parameter is_pconflict : (ident * args) -> (ident * args) -> option conflictdecl.

Axiom cc :
  forall a,
    is_conflict a a = false.
Axiom ccsym :
  forall a b,
    is_conflict a b = is_conflict b a.
Axiom cpc :
  forall s s' nm nm' arg arg',
    is_conflict (s, nm, arg) (s', nm', arg') = true ->
    exists c, is_pconflict (nm, arg) (nm', arg') = Some c.

(* list concatenation function *)
Definition concat {A : Type} (l : list (list A)) : list A :=
fold_right (@app A) [] l.

(* Combines two sets of path/actions to get conflicts *)
Definition h (cmn : list guard) (l : list (ident * args * path)) (l' : list (ident * args * path)) : list pconflict :=
  concat (map (fun z => 
                 match z with (a, b, c) => 
                   concat (map (fun y => 
                                    match y with (u, v, w) => 
                                         match is_pconflict (a, b) (u, v) with
                                         | Some cd => [Pconflict cmn c w b v cd] 
                                         | None => []
                                         end end) l') end) l).

(* Computes all path leading to actions *)
Function g (b: body) {struct b} : list (ident * args * path) := 
match b with
| action nm ar => [(nm, ar, [])]
| seq b1 b2 => g b1 ++ g b2
| foreach qcl b' => map (fun (z : ident * args * path) => 
                           match z with (xy, t) =>  (xy, pguard qcl::t) end) (g b')
| query qbrs => aux qbrs
| skip => []
end
with 
aux (br : branch) : list (ident * args * path) :=
match br with
| empty => []
| querybranch qcl b' qbrs => map (fun (z : ident * args * path) => 
                                    match z with (xy, t) =>  (xy, pguard qcl::t) end) (g b') ++ 
                                 map (fun z => match z with (xy, t) => (xy, nguard qcl::t) end) (aux qbrs)
| defaultbranch b' qbrs => g b'
end.

Function g' (cmn:path) (b:body) {struct b} : list (ident * args * path) :=
match b with
| action nm ar => [(nm, ar, cmn)]
| seq b1 b2 => g' cmn b1 ++ g' cmn b2
| foreach qcl b' => g' (cmn ++ [pguard qcl]) b' 
| query qbrs => aux' cmn qbrs
| skip => []
end
with 
aux' (cmn:path) (br : branch) {struct br} : list (ident * args * path) :=
match br with
| empty => []
| querybranch qcl b' qbrs => g' (cmn ++ [pguard qcl]) b' ++
                             aux' (cmn ++ [nguard qcl]) qbrs
| defaultbranch b' qbrs => g' cmn b'
end.

Lemma g'app: (forall b cmn1 cmn2,
        map (fun (z: ident * args * path) => match z with (xy,t) => (xy,cmn1 ++ t) end) (g' cmn2 b) = g' (cmn1 ++ cmn2) b)
        /\                                                                                                     
        forall br cmn1 cmn2,
        map (fun (z: ident * args * path) => match z with (xy,t) => (xy,cmn1 ++ t) end) (aux' cmn2 br) = aux' (cmn1 ++ cmn2) br.
Proof.
  apply indbb; intros; simpl; auto.  
  rewrite  map_app. rewrite H.  rewrite H0.  auto. 
  rewrite app_ass. auto.
  rewrite map_app. f_equal. rewrite app_ass. auto.
  rewrite app_ass. auto.
Qed.  

Lemma g'g': forall b cmn1 cmn2,
        map (fun (z: ident * args * path) => match z with (xy,t) => (xy,cmn1 ++ t) end) (g' cmn2 b) = g' (cmn1 ++ cmn2) b.
Proof. destruct g'app. auto.
Qed.

Lemma aux'aux': forall br cmn1 cmn2,
   map (fun (z: ident * args * path) => match z with (xy,t) => (xy,cmn1 ++ t) end) (aux' cmn2 br) = aux' (cmn1 ++ cmn2) br.
Proof. destruct g'app. auto. 
Qed.

Lemma gg'_aux : forall b cmn, 
                  map (fun (z: ident * args * path) => match z with (xy,t) => (xy,cmn ++ t) end) (g b) = g' cmn b. 
Proof.
  induction b using indbody 
             with (P0 := (fun br => forall cmn, map (fun (z:ident * args * path) => match z with (xy,t) => (xy,cmn ++ t) end) (aux br) = aux' cmn br)); intros; simpl; auto. 
  rewrite app_nil_r; auto. 
  rewrite map_app. rewrite IHb1. rewrite IHb2.  auto.
  replace 
     (fun z : ident * args * path =>
      let (p, t) := z in (p, pguard q :: t))
  with 
     (fun z : ident * args * path =>
      let (p, t) := z in (p, [pguard q] ++ t)) by auto. 
  rewrite IHb. apply g'g'. 
  rewrite map_app. f_equal. 
  replace 
     (fun z : ident * args * path =>
      let (p, t) := z in (p, pguard q :: t))
  with 
     (fun z : ident * args * path =>
      let (p, t) := z in (p, [pguard q] ++ t)) by auto. 
  rewrite IHb. apply g'g'.
  replace 
     (fun z : ident * args * list guard =>
      let (p0, t) := z in (p0, nguard q :: t))
  with 
     (fun z : ident * args * list guard => 
      let (p0, t) := z in (p0, [nguard q] ++ t)) by auto. 
  rewrite IHb0. apply aux'aux'. 
Qed.  

Lemma gg' : forall b, g b = g' [] b. 
Proof.
  intros. rewrite <- gg'_aux. simpl. 
  induction (g b).  auto. simpl. f_equal; auto. destruct a; auto.
Qed.

(* Computes all the potential conflicts in the body *)
Fixpoint f (cmn : list guard) (b : body) {struct b} : list pconflict := 
match b with
| action _ _ => []
| seq b1 b2 => f cmn b1 ++ f cmn b2 ++ h cmn (g b1) (g b2)
| foreach qcl b' => f (cmn ++ [pguard qcl]) b' ++ h cmn (g b) (g b)
| query qbrs => faux cmn qbrs
| skip => []
end
with faux (cmn : list guard) (br : branch) : list pconflict :=
match br with
| empty => []
| querybranch qcl b' qbrs => f (cmn ++ [pguard qcl]) b' ++ faux (cmn ++ [nguard qcl]) qbrs
| defaultbranch b' _ => f cmn b'
end.

Fixpoint count_none (l : list (option Z)) : nat :=
match l with
| [] => 0
| None::l' => S (count_none l')
| Some _::l' => (count_none l')
end.

(* Used for update_env *)
Definition eq_term (t t' : term) : bool :=
match (t, t') with
| (var s, var s') => Zeq_bool s s'
| (nlit n, nlit n') => Zeq_bool n n'
| (_, _) => false
end.

(* Variable assignment in an environment *)
Definition update_env (s : env) (t : term) (n : Z) : env :=
fun x => if (eq_term x t) then Some n else s x.

(* What it means to be an environment extension *)
Inductive extension : env -> env -> Prop :=
| ex1 : forall (s : env), extension s s
| ex2 : forall (s : env) (t : term) (n : Z), s t = None -> 
                                             extension s (update_env s t n)
| ex3 : forall (s s' s'' : env), extension s s' -> 
                                 extension s' s'' -> 
                                 extension s s''.

(* What it means to be a path extension *)
Inductive extension_path : path -> path -> Prop :=
| exp : forall p p', extension_path p (p ++ p').

(* Reflexivity *)
Lemma extension_path_nil : 
  forall p, extension_path p p.
Proof.
intros.
assert (extension_path p p <-> extension_path p (p ++ [])).
rewrite app_nil_r; auto.
split; auto.
apply H.
constructor.
Qed.

(* Transitivity *)
Lemma extension_path_transitive : 
  forall p p' p'', extension_path p p' -> 
                   extension_path p' p'' -> 
                   extension_path p p''.
Proof.
intros.
induction H0.
inversion H.
assert (p ++ (p'0 ++ p') = (p ++ p'0) ++ p').
apply app_assoc.
rewrite <- H2.
apply exp.
Qed.

Definition qtm2tm (q : qterm) : term :=
match q with
| bindvar v => var v
| refvar v => var v
end.

(* eval_predicate : env -> ident -> args -> option bool *)
Parameter eval_predicate : env -> ident -> args -> option bool.

(* eval_predicate = none -> some arguments are not instantiated *)
(* some true/false = value of the predicate *)
Axiom eval_pred_none : 
  forall s nm arg, 
    count_none (map s arg) > 0 <-> (eval_predicate s nm arg = None).

(* A predicate evaluated in an environment extension keeps its same value as before if
   all args were instantiated *)
Axiom extension_pred : 
  forall s s' nm arg v, extension s s' -> 
                        eval_predicate s nm arg = Some v -> 
                        eval_predicate s' nm arg = Some v.

Definition eq_opt (a b : option Z) : option bool :=
match (a, b) with
| (None, None) => None
| (None, Some _) => None
| (Some _, None) => None
| (Some v, Some v') => Some (Zeq_bool v v')
end.

(* A qcl is evaluated to true/false iff all its terms are instantiated *)
Fixpoint eval_qcl (s : env) (qcl : qclause) : option bool :=
match qcl with
| qeq qt qt' => (eq_opt (s (qtm2tm qt)) (s (qtm2tm qt')))
| qneq qt qt' => match (eq_opt (s (qtm2tm qt)) (s (qtm2tm qt'))) with
                   | None => None
                   | Some v => Some (negb v)
                 end
| qand qcl1 qcl2 => match (eval_qcl s qcl1, eval_qcl s qcl2) with
                      | (None, _) => None
                      | (_, None) => None
                      | (Some v, Some v') => Some (andb v v')
                    end
| qpred nm arg => eval_predicate s nm (map qtm2tm arg)
end.

Lemma eq_term_eval : 
  forall (s : env) t t', 
    eq_term t t' = true -> s t = s t'.
Proof.
intros.
case_eq t; intros.
case_eq t'; intros.
rewrite H0 in H; rewrite H1 in H.
unfold eq_term in H.
assert (i = i0).
apply Zeq_bool_eq; auto.
rewrite H2; auto.
rewrite H0 in H; rewrite H1 in H; unfold eq_term in H.
contradict H; discriminate.
unfold eq_term in H.
rewrite H0 in H.
case_eq t'; intros.
rewrite H1 in H.
contradict H; discriminate.
rewrite H1 in H.
assert (z = z0).
apply Zeq_bool_eq; auto.
rewrite H2; auto.
Qed.

Lemma extension_eval_qcl : 
  forall s s' qcl v, 
    extension s s' -> 
    eval_qcl s qcl = Some v -> 
    eval_qcl s' qcl = Some v.
Proof.
intros s s' qcl.
induction qcl.
induction 1; intros; auto.
unfold eval_qcl.
unfold eq_opt.
unfold update_env.
unfold eval_qcl in H0.
unfold eq_opt in H0.
case_eq (s (qtm2tm q)); intros.
case_eq (s (qtm2tm q0)); intros.
rewrite H1 in H0; rewrite H2 in H0.
case_eq (eq_term (qtm2tm q) t); intros.
assert (s (qtm2tm q) = s t).
apply eq_term_eval; auto.
rewrite H4 in H1; rewrite H1 in H.
contradict H; discriminate.
case_eq (eq_term (qtm2tm q0) t); intros.
assert (s (qtm2tm q0) = s t).
apply eq_term_eval; auto.
rewrite H5 in H2; rewrite H2 in H.
contradict H; discriminate.
auto.
rewrite H1 in H0; rewrite H2 in H0.
contradict H0; discriminate.
case_eq (s (qtm2tm q0)); intros.
rewrite H1 in H0; rewrite H2 in H0.
contradict H0; discriminate.
rewrite H1 in H0; rewrite H2 in H0.
contradict H0; discriminate.

intros.
unfold eval_qcl.
unfold eval_qcl in H0.
apply extension_pred with s; auto.

intros.
unfold eval_qcl; fold eval_qcl.
unfold eval_qcl in H0; fold eval_qcl in H0.
case_eq (eval_qcl s qcl1); intros.
case_eq (eval_qcl s qcl2); intros.
rewrite H1 in H0; rewrite H2 in H0.
assert (eval_qcl s' qcl1 = Some b).
apply IHqcl1; auto.
assert (eval_qcl s' qcl2 = Some b0).
apply IHqcl2; auto.
rewrite H3; rewrite H4.
auto.
rewrite H1 in H0; rewrite H2 in H0.
contradict H0; discriminate.
rewrite H1 in H0.
contradict H0; discriminate.

induction 1; intros; auto.
unfold eval_qcl.
unfold eq_opt.
unfold update_env.
unfold eval_qcl in H0.
unfold eq_opt in H0.
case_eq (s (qtm2tm q)); intros.
case_eq (s (qtm2tm q0)); intros.
rewrite H1 in H0; rewrite H2 in H0.
case_eq (eq_term (qtm2tm q) t); intros.
assert (s (qtm2tm q) = s t).
apply eq_term_eval; auto.
rewrite H4 in H1; rewrite H1 in H.
contradict H; discriminate.
case_eq (eq_term (qtm2tm q0) t); intros.
assert (s (qtm2tm q0) = s t).
apply eq_term_eval; auto.
rewrite H5 in H2; rewrite H2 in H.
contradict H; discriminate.
auto.
rewrite H1 in H0; rewrite H2 in H0.
contradict H0; discriminate.
case_eq (s (qtm2tm q0)); intros.
rewrite H1 in H0; rewrite H2 in H0.
contradict H0; discriminate.
rewrite H1 in H0; rewrite H2 in H0.
contradict H0; discriminate.
Qed.

Inductive models : env -> qclause -> env -> Prop :=
| models_intro s qcl s' : extension s s' -> eval_qcl s' qcl = Some true -> models s qcl s'. 

Inductive actions : env -> body -> list (env * ident * args) -> Prop :=
| actions_action s nm arg : actions s (action nm arg) [(s,nm,arg)]
| actions_seq s b1 b2 l1 l2: 
      actions s b1 l1 -> actions s b2 l2 -> actions s (seq b1 b2) (l1++l2)
| actions_foreachS s qcl b ll s' l' :   
      actions s (foreach qcl b) ll -> models s qcl s' -> actions s' b l' -> actions s (foreach qcl b) (l' ++ ll)
| actions_foreachN s qcl b : actions s (foreach qcl b) []
| actions_query s qbrs ll : qactions s qbrs ll -> actions s (query qbrs) ll
| actions_skip s : actions s skip []
with qactions : env -> branch -> list (env * ident * args) -> Prop :=
| qactions_empty s : qactions s empty []
| qactions_queryF s qcl br brs ll : (forall s', ~ models s qcl s') -> 
                               qactions s brs ll ->
                               qactions s (querybranch qcl br brs) ll
| qactions_queryT s qcl br brs ll s' : models s qcl s' -> actions s' br ll -> qactions s (querybranch qcl br brs) ll
| qactions_default s br brs ll : actions s br ll -> qactions s (defaultbranch br brs) ll
.

Scheme indactions := Induction for actions Sort Prop
with indqactions := Induction for qactions Sort Prop.
Combined Scheme indact from indactions, indqactions.

Lemma actions_extension : 
  forall s b l,
    actions s b l -> forall s' nm arg, In (s', nm, arg) l -> extension s s'.
Proof.
induction 1 using indactions with (P := fun s b l H => (forall s' nm arg, In (s', nm, arg) l -> extension s s')) (P0 := fun s b l H => forall s' nm arg, In (s', nm, arg) l -> extension s s'); intros.
assert ((s, nm, arg) = (s', nm0, arg0) \/ In (s', nm0, arg0) []).
apply in_inv; auto.
destruct H0.
assert (s = s').
congruence.
rewrite H1.
constructor.
contradict H0.
assert (In (s', nm, arg) l1 \/ In (s', nm, arg) l2).
apply in_app_or; auto.
destruct H2; auto.
apply IHactions1 with nm arg; auto.
apply IHactions2 with nm arg; auto.
assert (In (s'0, nm, arg) l' \/ In (s'0, nm, arg) ll).
apply in_app_or; auto.
destruct H2; auto.
apply ex3 with s'.
inversion m; auto.
apply IHactions2 with nm arg; auto.
apply IHactions1 with nm arg; auto.
contradict H.
apply IHactions with nm arg; auto.
contradict H.
contradict H.
apply IHactions with nm arg; auto.
apply ex3 with s'.
inversion m; auto.
apply IHactions with nm arg; auto.
apply IHactions with nm arg; auto.
Qed.

Lemma qactions_extension :
  forall s b l,
    qactions s b l -> forall s' nm arg, In (s', nm, arg) l -> extension s s'.
Proof.
induction 1; intros.
contradict H.
apply IHqactions with nm arg; auto.
apply ex3 with s'; auto.
inversion H; auto.
apply actions_extension with br ll nm arg; auto.
apply actions_extension with br ll nm arg; auto.
Qed.

(* Environment consistent with a path *)
Inductive consistentenvpath : env -> path -> Prop :=
| ep1 : forall s, consistentenvpath s []
| ep2 : forall s qcl p, eval_qcl s qcl = Some true -> consistentenvpath s p -> consistentenvpath s (pguard qcl::p)
| ep3 : forall s qcl p, (forall s', ~ models s qcl s') -> consistentenvpath s p -> consistentenvpath s (nguard qcl::p)
| epapp : forall s p p', consistentenvpath s p -> consistentenvpath s p' -> consistentenvpath s (p ++ p').

(* An environment extension stays consistent *)
Lemma extension_consistent_path : 
  forall s s' p, 
    extension s s' -> 
    consistentenvpath s p -> 
    consistentenvpath s' p.
Proof.
intros.
induction H0.
constructor; auto.
constructor; auto.
apply extension_eval_qcl with s; auto.
constructor; auto.
intros.
contradict H0.
assert (models s qcl s'0).
constructor; auto.
apply ex3 with s'; auto.
inversion H0; auto.
inversion H0; auto.
intro.
assert (~ models s qcl s'0).
auto.
contradiction.
constructor; auto.
Qed.

Lemma exists_path : 
  forall b s w, 
    actions s b w -> 
    forall s' a arg, 
      In (s', a, arg) w -> 
      exists p, 
        consistentenvpath s' p /\ 
        In (a, arg, p) (g b).
Proof.
induction 1 using indactions with (P := fun s b w H => forall (s' : env) (a : ident) (arg : args),
  In (s', a, arg) w ->
  exists p : path, consistentenvpath s' p /\ In (a, arg, p) (g b)) 
                                    (P0 := fun s b w H => forall (s' : env) (a : ident) (arg : args),
  In (s', a, arg) w ->
  exists p : path, consistentenvpath s' p /\ In (a, arg, p) (g (query b))); intros.
exists []; auto.
split.
constructor.
unfold g.
assert ((s, nm, arg) = (s', a, arg0) \/ In (s', a, arg0) []).
apply in_inv; auto.
destruct H0.
assert (a = nm).
congruence.
assert (arg0 = arg).
congruence.
rewrite H1; rewrite H2.
apply in_eq; auto.
contradict H0; auto.
unfold g; fold g.
assert (In (s', a, arg) l1 \/ In (s', a, arg) l2).
apply in_app_or; auto.
destruct H2.
assert (exists p : path, consistentenvpath s' p /\ In (a, arg, p) (g b1)); auto.
destruct H3 as [x [H3 H4]].
exists x.
split; auto.
apply in_or_app; auto.
assert (exists p : path, consistentenvpath s' p /\ In (a, arg, p) (g b2)); auto.
destruct H3 as [x [H3 H4]].
exists x.
split; auto.
apply in_or_app; auto.

unfold g; fold g.
assert (In (s'0, a, arg) l' \/ In (s'0, a, arg) ll).
apply in_app_or; auto.
destruct H2.
assert (exists p : path, consistentenvpath s'0 p /\ In (a, arg, p) (g b)); auto.
destruct H3 as [p [H3 H4]].
exists (pguard qcl :: p).
split; auto.
constructor.
apply extension_eval_qcl with s'.
apply actions_extension with b l' a arg; auto.
inversion m; auto.
auto.
apply in_map_iff.
exists (a, arg, p); auto.
assert (exists p : path,
                 consistentenvpath s'0 p /\ In (a, arg, p) (g (foreach qcl b))); auto.
contradict H.

apply IHactions; auto.
contradict H.
contradict H.
assert (exists p : path,
                consistentenvpath s' p /\ In (a, arg, p) (g (query brs))).
apply IHactions; auto.
destruct H0 as [p [H0 H1]].
exists (nguard qcl :: p).
split; auto.
constructor.
intros.
contradict n.
assert (models s qcl s'0).
constructor.
apply ex3 with s'.
apply qactions_extension with brs ll a arg; auto.
inversion n; auto.
inversion n; auto.
intro.
assert (~ models s qcl s'0); auto.
auto.
unfold g; fold aux; fold g.
apply in_or_app.
right.
apply in_map_iff.
exists (a, arg, p); auto.
assert (extension s' s'0).
apply actions_extension with br ll a arg; auto.
assert (exists p : path,
                consistentenvpath s'0 p /\ In (a, arg, p) (g br)).
apply IHactions; auto.
destruct H2 as [p [H2 H3]].
exists (pguard qcl :: p); auto.
split; auto.
constructor; auto.
inversion m.
apply extension_eval_qcl with s'; auto.
unfold g; fold g; fold aux.
apply in_or_app.
left.
apply in_map_iff.
exists (a, arg, p); auto.
unfold g; fold g.
auto.
Qed.

(* Like in_map_iff but for concat *)
Lemma in_concat : 
  forall A l (x:A), 
    In x (concat l) <-> 
    exists u, 
      In u l /\ In x u. 
Proof.
intros.
split; intros.
induction l.
unfold concat in H.
unfold fold_right in H.
contradict H.
unfold concat in H.
unfold fold_right in H.
assert (In x a \/ In x (concat l)).
apply in_app_or; auto.
destruct H0.
exists a.
split; auto.
apply in_eq.
assert (exists u, In u l /\ In x u).
apply IHl; auto.
destruct H1 as [u [H1 H2]].
exists u; split; auto.
apply in_cons; auto.

unfold concat.
destruct H as [u [H H0]].
induction l.
contradict H.
assert (a = u \/ In u l).
apply in_inv; auto.
destruct H1.
unfold fold_right.
apply in_or_app.
left.
rewrite H1; auto.
unfold fold_right.
apply in_or_app.
right.
apply IHl; auto.
Qed.

(* Main theorem stating that a satisfiable Pconflict is computed
   for each actual conflict *)
Theorem main :
  forall s0 b w,
    actions s0 b w ->
    forall cp0 s1 a1 arg1 s2 a2 arg2,
      consistentenvpath s0 cp0 -> 
      In (s1, a1, arg1) w ->
      In (s2, a2, arg2) w ->
      is_conflict (s1, a1, arg1) (s2, a2, arg2) = true ->
      exists cp d1 d2 c,
        (In (Pconflict cp d1 d2 arg1 arg2 c) (f cp0 b) \/
         In (Pconflict cp d2 d1 arg2 arg1 c) (f cp0 b)) /\
        extension_path cp0 cp /\
        exists s,
          extension s0 s /\
          consistentenvpath s cp /\
          extension s s1 /\
          extension s s2 /\
          consistentenvpath s1 d1 /\
          consistentenvpath s2 d2.
Proof.
induction 1 using indactions with (P := fun s0 b w H => forall cp0 s1 a1 arg1 s2 a2 arg2,
      consistentenvpath s0 cp0 -> 
      In (s1, a1, arg1) w ->
      In (s2, a2, arg2) w ->
      is_conflict (s1, a1, arg1) (s2, a2, arg2) = true ->
      exists cp d1 d2 c,
        (In (Pconflict cp d1 d2 arg1 arg2 c) (f cp0 b) \/
         In (Pconflict cp d2 d1 arg2 arg1 c) (f cp0 b)) /\
        extension_path cp0 cp /\
        exists s,
          extension s0 s /\
          consistentenvpath s cp /\
          extension s s1 /\
          extension s s2 /\
          consistentenvpath s1 d1 /\
          consistentenvpath s2 d2)
                                    (P0 := fun s0 b w H => forall cp0 s1 a1 arg1 s2 a2 arg2,
      consistentenvpath s0 cp0 -> 
      In (s1, a1, arg1) w ->
      In (s2, a2, arg2) w ->
      is_conflict (s1, a1, arg1) (s2, a2, arg2) = true ->
      exists cp d1 d2 c,
        (In (Pconflict cp d1 d2 arg1 arg2 c) (f cp0 (query b)) \/
         In (Pconflict cp d2 d1 arg2 arg1 c) (f cp0 (query b))) /\
        extension_path cp0 cp /\
        exists s,
          extension s0 s /\
          consistentenvpath s cp /\
          extension s s1 /\
          extension s s2 /\
          consistentenvpath s1 d1 /\
          consistentenvpath s2 d2); intros.
assert ((s, nm, arg) = (s1, a1, arg1) \/ In (s1, a1, arg1) []).
apply in_inv; auto.
assert ((s, nm, arg) = (s2, a2, arg2) \/ In (s2, a2, arg2) []).
apply in_inv; auto.
destruct H3; try contradiction.
destruct H4; try contradiction.
rewrite <- H3 in H2.
rewrite <- H4 in H2.
contradict H2.
rewrite cc.
auto.

assert (In (s1, a1, arg1) l1 \/ In (s1, a1, arg1) l2).
apply in_app_or; auto.
assert (In (s2, a2, arg2) l1 \/ In (s2, a2, arg2) l2).
apply in_app_or; auto.
destruct H5; destruct H6.
assert (exists (cp d1 d2 : list guard) (c : conflictdecl),
                 (In (Pconflict cp d1 d2 arg1 arg2 c) (f cp0 b1) \/
                  In (Pconflict cp d2 d1 arg2 arg1 c) (f cp0 b1)) /\
                 extension_path cp0 cp /\
                 (exists s0 : env,
                    extension s s0 /\
                    consistentenvpath s0 cp /\
                    extension s0 s1 /\
                    extension s0 s2 /\
                    consistentenvpath s1 d1 /\ consistentenvpath s2 d2)).
apply IHactions1 with a1 a2; auto.
destruct H7 as [cp [d1 [d2 [c [H7 H8]]]]].
destruct H8 as [H8 [s0 [H9 [H10 [H11 [H12 [H13 H14]]]]]]].
exists cp; exists d1; exists d2; exists c.
repeat split; auto.
destruct H7.
left.
unfold f; fold f.
apply in_or_app.
left; auto.
right.
unfold f; fold f.
apply in_or_app.
left; auto.
exists s0.
repeat split; auto.

assert (exists d1, consistentenvpath s1 d1 /\ In (a1, arg1, d1) (g b1)).
apply exists_path with s l1; auto.
assert (exists d2, consistentenvpath s2 d2 /\ In (a2, arg2, d2) (g b2)).
apply exists_path with s l2; auto.
assert (exists c : conflictdecl, is_pconflict (a1, arg1) (a2, arg2) = Some c).
apply cpc with s1 s2; auto.
destruct H9 as [c H11].
destruct H8 as [d2 [H9 H10]].
destruct H7 as [d1 [H7 H8]].
exists cp0; exists d1; exists d2; exists c.
repeat split; auto.
left.
unfold f; fold f.
apply in_or_app.
right.
apply in_or_app.
right.
unfold h; fold h.
apply in_concat.
eexists.
split.
apply in_map_iff.
exists (a1, arg1, d1); auto.
apply in_concat.
eexists.
split.
apply in_map_iff.
exists (a2, arg2, d2); auto.
rewrite H11.
apply in_eq.
apply extension_path_nil.
exists s; repeat split; try constructor; auto.
apply actions_extension with b1 l1 a1 arg1; auto.
apply actions_extension with b2 l2 a2 arg2; auto.
assert (exists d1, consistentenvpath s1 d1 /\ In (a1, arg1, d1) (g b2)).
apply exists_path with s l2; auto.
assert (exists d2, consistentenvpath s2 d2 /\ In (a2, arg2, d2) (g b1)).
apply exists_path with s l1; auto.
rewrite ccsym in H4.
assert (exists c : conflictdecl, is_pconflict (a2, arg2) (a1, arg1) = Some c).
apply cpc with s2 s1; auto.
destruct H9 as [c H11].
destruct H8 as [d2 [H9 H10]].
destruct H7 as [d1 [H7 H8]].
exists cp0; exists d1; exists d2; exists c.
repeat split.
right.
unfold f; fold f.
apply in_or_app.
right.
apply in_or_app.
right.
unfold h; fold h.
apply in_concat.
eexists.
split.
apply in_map_iff.
exists (a2, arg2, d2).
split; auto.
apply in_concat.
eexists.
split.
apply in_map_iff.
exists (a1, arg1, d1).
split; auto.
rewrite H11.
apply in_eq.
apply extension_path_nil.
exists s.
repeat split; try constructor; auto.
apply actions_extension with b2 l2 a1 arg1; auto.
apply actions_extension with b1 l1 a2 arg2; auto.
assert (exists (cp d1 d2 : list guard) (c : conflictdecl),
                 (In (Pconflict cp d1 d2 arg1 arg2 c) (f cp0 b2) \/
                  In (Pconflict cp d2 d1 arg2 arg1 c) (f cp0 b2)) /\
                 extension_path cp0 cp /\
                 (exists s0 : env,
                    extension s s0 /\
                    consistentenvpath s0 cp /\
                    extension s0 s1 /\
                    extension s0 s2 /\
                    consistentenvpath s1 d1 /\ consistentenvpath s2 d2)).
apply IHactions2 with a1 a2; auto.
destruct H7 as [cp [d1 [d2 [c [H7 H8]]]]].
destruct H8 as [H8 [s0 [H9 [H10 [H11 [H12 [H13 H14]]]]]]].
exists cp; exists d1; exists d2; exists c.
repeat split; auto.
destruct H7.
left.
unfold f; fold f.
apply in_or_app.
right.
apply in_or_app.
left.
auto.
right.
unfold f; fold f.
apply in_or_app.
right.
apply in_or_app.
left.
auto.
exists s0.
repeat split; auto.

assert (In (s1, a1, arg1) l' \/ In (s1, a1, arg1) ll).
apply in_app_or; auto.
assert (In (s2, a2, arg2) l' \/ In (s2, a2, arg2) ll).
apply in_app_or; auto.
destruct H5; destruct H6.
assert (exists (cp d1 d2 : list guard) (c : conflictdecl),
                 (In (Pconflict cp d1 d2 arg1 arg2 c) (f (cp0 ++ [pguard qcl]) b) \/
                  In (Pconflict cp d2 d1 arg2 arg1 c) (f (cp0 ++ [pguard qcl]) b)) /\
                 extension_path (cp0 ++ [pguard qcl]) cp /\
                 (exists s : env,
                    extension s' s /\
                    consistentenvpath s cp /\
                    extension s s1 /\
                    extension s s2 /\
                    consistentenvpath s1 d1 /\ consistentenvpath s2 d2)).
apply IHactions2 with a1 a2; auto.
constructor.
apply extension_consistent_path with s; auto.
inversion m; auto.
constructor; auto.
inversion m; auto.
constructor.
destruct H7 as [cp [d1 [d2 [c [H7 H8]]]]].
destruct H8 as [H8 [s0 [H9 [H10 [H11 [H12 [H13 H14]]]]]]].
exists cp; exists d1; exists d2; exists c.
repeat split; auto.
destruct H7.
left.
unfold f; fold f.
apply in_or_app; auto.
right.
unfold f; fold f.
apply in_or_app; auto.
inversion H8; auto.
rewrite <- app_assoc.
constructor.
exists s0.
repeat split; try constructor; auto.
apply ex3 with s'; auto.
inversion m; auto.

assert (exists c, is_pconflict (a1, arg1) (a2, arg2) = Some c).
apply cpc with s1 s2; auto.
destruct H7 as [c H7].
assert (exists d1, consistentenvpath s1 d1 /\ In (a1, arg1, d1) (g b)).
apply exists_path with s' l'; auto.
assert (exists d2, consistentenvpath s2 d2 /\ In (a2, arg2, d2) (g (foreach qcl b))).
apply exists_path with s ll; auto.
destruct H9 as [d2 [H10 H11]].
destruct H8 as [d1 [H8 H9]].
assert (In (a1, arg1, pguard qcl :: d1) (g (foreach qcl b))).
unfold g; fold g.
apply in_map_iff.
exists (a1, arg1, d1); auto.
exists cp0; exists (pguard qcl :: d1); exists d2; exists c.
repeat split; auto.
left.
unfold f; fold f.
apply in_or_app.
right.
unfold h; fold h.
apply in_concat.
eexists.
split; auto.
apply in_map_iff.
exists (a1, arg1, pguard qcl :: d1).
split; auto.
apply in_concat.
eexists.
split; auto.
apply in_map_iff.
exists (a2, arg2, d2).
split; auto.
rewrite H7.
apply in_eq; auto.
apply extension_path_nil.
exists s; repeat split; try constructor; auto.
apply ex3 with s'; auto.
inversion m; auto.
apply actions_extension with b l' a1 arg1; auto.
apply actions_extension with (foreach qcl b) ll a2 arg2; auto.
apply extension_eval_qcl with s'.
apply actions_extension with b l' a1 arg1; auto.
inversion m; auto.

assert (exists c, is_pconflict (a1, arg1) (a2, arg2) = Some c).
apply cpc with s1 s2; auto.
destruct H7 as [c H7].
assert (exists d1, consistentenvpath s1 d1 /\ In (a1, arg1, d1) (g (foreach qcl b))).
apply exists_path with s ll; auto.
assert (exists d2, consistentenvpath s2 d2 /\ In (a2, arg2, d2) (g b)).
apply exists_path with s' l'; auto.
destruct H9 as [d2 [H10 H11]].
destruct H8 as [d1 [H8 H9]].
assert (In (a2, arg2, pguard qcl :: d2) (g (foreach qcl b))).
unfold g; fold g.
apply in_map_iff.
exists (a2, arg2, d2); auto.
exists cp0; exists d1; exists (pguard qcl :: d2); exists c.
repeat split; auto.
left.
unfold f; fold f.
apply in_or_app.
right.
unfold h; fold h.
apply in_concat.
eexists.
split.
apply in_map_iff.
exists (a1, arg1, d1).
split; auto.
apply in_concat.
eexists.
split; auto.
apply in_map_iff.
exists (a2, arg2, pguard qcl :: d2).
rewrite H7.
split; auto.
apply in_eq.
apply extension_path_nil.
exists s; repeat split; try constructor; auto.
apply actions_extension with (foreach qcl b) ll a1 arg1; auto.
apply ex3 with s'; auto.
inversion m; auto.
apply actions_extension with b l' a2 arg2; auto.
apply extension_eval_qcl with s'; auto.
apply actions_extension with b l' a2 arg2; auto.
inversion m; auto.
apply IHactions1 with a1 a2; auto.
contradiction.
apply IHactions with a1 a2; auto.
contradiction.
contradiction.

assert (exists (cp d1 d2 : list guard) (c : conflictdecl),
                (In (Pconflict cp d1 d2 arg1 arg2 c) (f (cp0 ++ [nguard qcl]) (query brs)) \/
                 In (Pconflict cp d2 d1 arg2 arg1 c) (f (cp0 ++ [nguard qcl]) (query brs))) /\
                extension_path (cp0 ++ [nguard qcl]) cp /\
                (exists s0 : env,
                   extension s s0 /\
                   consistentenvpath s0 cp /\
                   extension s0 s1 /\
                   extension s0 s2 /\
                   consistentenvpath s1 d1 /\ consistentenvpath s2 d2)).
apply IHactions with a1 a2; auto.
constructor; auto.
constructor; auto.
constructor.
destruct H3 as [cp [d1 [d2 [c [H3 H4]]]]].
destruct H4 as [H4 [s0 [H5 [H6 [H7 [H8 [H9 H10]]]]]]].
exists cp; exists d1; exists d2; exists c.
repeat split; auto.
destruct H3.
left.
unfold f; fold f; fold faux.
unfold f in H3; fold f in H3; fold faux in H3.
apply in_or_app; auto.
right.
unfold f; fold f; fold faux.
unfold f in H3; fold f in H3; fold faux in H3.
apply in_or_app; auto.
inversion H4; auto.
rewrite <- app_assoc.
constructor.
exists s0; repeat split; try constructor; auto.

assert (exists (cp d1 d2 : list guard) (c : conflictdecl),
                (In (Pconflict cp d1 d2 arg1 arg2 c) (f (cp0 ++ [pguard qcl]) br) \/
                 In (Pconflict cp d2 d1 arg2 arg1 c) (f (cp0 ++ [pguard qcl]) br)) /\
                extension_path (cp0 ++ [pguard qcl]) cp /\
                (exists s : env,
                   extension s' s /\
                   consistentenvpath s cp /\
                   extension s s1 /\
                   extension s s2 /\
                   consistentenvpath s1 d1 /\ consistentenvpath s2 d2)).
apply IHactions with a1 a2; auto.
constructor.
apply extension_consistent_path with s; auto.
inversion m; auto.
constructor.
inversion m; auto.
constructor.
destruct H4 as [cp [d1 [d2 [c [H4 H5]]]]].
destruct H5 as [H5 [s0 [H6 [H7 [H8 [H9 [H10 H11]]]]]]].
exists cp; exists d1; exists d2; exists c.
repeat split; auto.
destruct H4.
left.
unfold f; fold faux; fold f.
unfold f in H4; fold faux in H4; fold f in H4.
apply in_or_app; auto.
right.
unfold f; fold faux; fold f.
unfold f in H4; fold faux in H4; fold f in H4.
apply in_or_app; auto.
inversion H5.
rewrite <- app_assoc.
constructor.
exists s0; repeat split; try constructor; auto.
apply ex3 with s'; auto.
inversion m; auto.

assert (exists (cp d1 d2 : list guard) (c : conflictdecl),
                (In (Pconflict cp d1 d2 arg1 arg2 c) (f cp0 br) \/
                 In (Pconflict cp d2 d1 arg2 arg1 c) (f cp0 br)) /\
                extension_path cp0 cp /\
                (exists s0 : env,
                   extension s s0 /\
                   consistentenvpath s0 cp /\
                   extension s0 s1 /\
                   extension s0 s2 /\
                   consistentenvpath s1 d1 /\ consistentenvpath s2 d2)).
apply IHactions with a1 a2; auto.
destruct H4 as [cp [d1 [d2 [c [H4 H5]]]]].
destruct H5 as [H5 [s0 [H6 [H7 [H8 [H9 [H10 H11]]]]]]].
exists cp; exists d1; exists d2; exists c.
repeat split; auto.
exists s0; auto.
repeat split; try constructor; auto.
Qed.

(* What it means for a pconflict to be satisfiable *)
Inductive sat : pconflict -> Prop :=
| sat1 : 
    forall cp d1 d2 ar1 ar2 c, 
      (exists s s1 s2, consistentenvpath s cp -> 
                       (consistentenvpath s1 d1 /\ 
                        consistentenvpath s2 d2 /\ 
                        extension s s1 /\ 
                        extension s s2)) -> 
      sat (Pconflict cp d1 d2 ar1 ar2 c).

Definition init_env := fun (t : term) => (None : option Z).

(* What's an actual conflict *)
Inductive actual_conflicts : prog -> (env * ident * args) -> (env * ident * args) -> Prop :=
| actual_conflicts_intro s s' nm nm' arg arg' w ev eargs b: is_conflict (s, nm, arg) (s', nm', arg') = true -> actions init_env b w -> In (s, nm, arg) w -> In (s', nm', arg') w -> actual_conflicts (handle ev eargs b) (s, nm, arg) (s', nm', arg').

(* An actual conflict is always satisfiable *)
Corollary actual_conflicts_sat :
  forall s s' nm nm' arg arg' ev eargs b,
    actual_conflicts (handle ev eargs b) (s, nm, arg) (s', nm', arg') ->
    exists cp d1 d2 c, ((In (Pconflict cp d1 d2 arg arg' c) (f [] b) /\ 
                         sat (Pconflict cp d1 d2 arg arg' c)) 
                        \/ 
                        (In (Pconflict cp d2 d1 arg' arg c) (f [] b) /\ 
                         sat (Pconflict cp d2 d1 arg' arg c))).
Proof.
intros.
inversion H.
assert (exists cp d1 d2 c,
        (In (Pconflict cp d1 d2 arg arg' c) (f [] b) \/
         In (Pconflict cp d2 d1 arg' arg c) (f [] b)) /\
        extension_path [] cp /\
        exists s0,
          extension init_env s0 /\
          consistentenvpath s0 cp /\
          extension s0 s /\
          extension s0 s' /\
          consistentenvpath s d1 /\
          consistentenvpath s' d2).
apply main with w nm nm'; try constructor; auto.
destruct H13 as [cp [d1 [d2 [c [H13 H14]]]]].
destruct H14 as [H14 [ss [H15 [H16 [H17 [H18 [H19 H20]]]]]]].
destruct H13.
exists cp; exists d1; exists d2; exists c; auto.
left.
split; auto.
constructor.
exists ss; exists s; exists s'; auto.
exists cp; exists d1; exists d2; exists c; auto.
right.
split; auto.
constructor.
exists ss; exists s'; exists s; auto.
Qed.
