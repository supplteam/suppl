prog ::= <empty>
       | decl '.' prog

decl ::= rule
       | handler
       | preddecl
       | modedecl
       | tabledecl
       | indexdecl
       | typedecl
       | proceduredecl
       | functiondecl
       | eventdecl
       | datadecl
       | 'import' <string literal>
       | pragma
       | proceduredefn

rule ::- goal ':-' clauses
       | goal ':-'
       | goal

goal ::= ident args
       | ident

args ::= '(' terms ')'
       | '{' namedtermlist '}'

namedtermlist
       ::= ident ':' term
         | ident ':' term ',' namedtermlist

clauses :: clause
         | clause ',' clauses

clause ::= '(' clauses ')'
         | 'true'
         | 'false'
         | term '=' term
         | term '<>' term
         | clause '|' clause
         | 'not' clause
         | ident args?
         | term '<' term
         | term '<=' term
         | term '=<' term
         | term '>' term
         | term '>=' term
         | 'findall' '(' term ',' goal ',' var ')'

terms ::= term
        | term ',' terms

term ::= var
       | ident args?
       | '[' term '|' term ']'
       | '[' terms ']'
       | '[' ']'
       | '(' terms ')'
       | <numeric literal>
       | <string literal>
       | '_'       
       | term '++' term
       | term '+' term
       | term '-' term
       | term '*' term
       | term '/' term
       | '~' term

qterms ::= qterm
         | qterm ',' qterms

qterm ::= var
       | '?' var
       | ident qargs?
       | '[' qterm '|' qterm ']'
       | '[' qterms ']'
       | '[' ']'
       |  '(' qterms ')'
       | <numeric literal>
       | <string literal>
       | '_'       
       | qterm '++' qterm
       | qterm '+' qterm
       | qterm '-' qterm
       | qterm '*' qterm
       | qterm '/' qterm
       | '~' qterm

qvars ::= '?' var
        | '?' var ',' qvars

qterms ::= qterm
         | qterm ',' qterms

namedqterms ::= ident ':' qterm
              | ident ':' qterm ',' namedqterms

evvar ::= '?' var
        | '_' 

evvars ::= evvar
        | evvar ',' evvvars

namedevvars ::= ident ':' evvar
              | ident ':' evvar ',' namedevvars

handler ::= 'handle' ident '(' evvars ')' '=>' body ';' 'end'
          | 'handle' ident '{' namedevvars '}' '=>' body ';' 'end'
          | 'handle' ident '=>' body ';' 'end'

body ::= ident args?
       | 'skip'
       | body ';' body
       | 'query' branches 'end'
       | 'queue' 'insert' '(' terms ')' 'into' ident 
       | 'queue' 'insert' '{' namedtermlist '}' 'into' ident
       | 'queue' 'delete' '(' terms ')' 'from' ident
       | 'queue' 'delete' '{' namedtermlist '}' 'from' ident
       | 'foreach' ident '(' qterms ')' '=>' body ';' 'end'
       | 'foreach' ident '{' namedqterms '}' '=>' body ';' 'end'

branches ::= branch
           | branch '|' branches

branch ::= ident '=>' body ';'
         | ident '(' qterms ')' '=>' body
         | ident '{' namedqterms '}' '=>' body
         | qterm '=' qterm '=>' body
         | '_' '=>' body

proceduredefn ::= 'define' 'procedure' ident ':=' body ';' end
                | 'define' 'procedure' ident '(' evvars ')' ':=' body ';' end
                | 'define' 'procedure' ident '{' namedevvars '}'
                      ':=' body ';' end

pragma ::=
   | 'pragma' 'emit' <string literal>
   | 'pragma' 'expect' 'error' '(' <number literal> ',' <number literal> ')'
   | 'pragma' 'expect' 'error' '(' <number literal> ',' <number literal> ',' <string literal> ')'
   | 'pragma' 'expect' 'warning' '(' <number literal> ',' <number literal> ')'
   | 'pragma' 'expect' 'warning' '(' <number literal> ',' <number literal> ',' <string literal> ')'
   | 'pragma' 'debug'
   | 'pragma' 'query' <number literal> goal
   | 'pragma' 'handle' term 'yields' '[' terms? ']'

============= static declarations =================

type ::= ident
       | var
       | type '*' type
       | 'list' '(' type ')'
       | 'map' '(' type ',' type ')'
       | 'set' '(' type ')'
       | 'number'
       | 'string'
       | '(' type ')'

mode ::= 'in'
       | 'out'
       | 'ignore'     

types ::= type
        | type ',' types

namedtypes ::= ident ':' type
             | ident ':' type ',' namedtypes

typeargs ::= '(' types ')'
           | '{' namedtypes '}'

preddecl ::= 'primitive'? 'predicate' ident typeargs?

proceduredecl ::= 'primitive'? 'procedure' ident typeargs?

functiondecl ::= 'primitive' 'function' ident typeargs? 'yields' type

typedecl ::= 'primitive' 'type' ident
           | 'type' ident ':=' type

datadecl ::= 'data' ident '::=' databranches

databranches ::= databranch
               | databranch '|' databranches

databranch ::= ident typeargs?
               
eventdecl ::= 'event' ident typeargs?


modes ::= mode
        | mode ',' modes

namedmodes ::= ident ':' mode
             | ident ':' mode ',' namedmodes

modeargs ::= '(' modes ')'
           | '{' namedmodes '}'

modedecl ::= 'mode' ident modeargs

tabledecl ::= 'table' ident typeargs
            | 'table' ident typeargs 'lifetime' <number literal>

indexdecl ::= 'index' ident modeargs

