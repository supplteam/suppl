#!/usr/bin/env perl

my $client = 'mosh-client';
my $predict = 'adaptive'

my ( $ip, $port, $key );
while ( <> ) {
    chomp;
    if ( m{^host: } ) {
      ( $ip ) = m{^host:\s*(\S+)\s*$} or die "Bad MOSH IP string: $_\n";
    } elsif ( m{^MOSH CONNECT } ) {
      if ( ( $port, $key ) = m{^MOSH CONNECT (\d+?) ([A-Za-z0-9/+]{22})\s*$} ) {
      } else {
	die "Bad MOSH CONNECT string: $_\n";
      }
    } else {
      print "$_\n";
    }
  }

  if ( not defined $ip ) {
      die "$0: Did not find remote IP address (is SSH ProxyCommand disabled?).\n";
  }

  if ( not defined $key or not defined $port ) {
    die "$0: Did not find mosh server startup message.\n";
  }

  # Now start real mosh client
  $ENV{ 'MOSH_KEY' } = $key;
  $ENV{ 'MOSH_PREDICTION_DISPLAY' } = $predict;
#  exec {$client} ($client, $ip, $port);
