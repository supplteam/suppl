import java.util.*;
import java.util.jar.*;
import java.io.*;


public class GetMainClass {
    public static void main(String args[]) throws Exception {
	JarFile j = new JarFile(new File(args[0]));
	System.out.println(j.getManifest().getMainAttributes().getValue("Main-class"));
    }
}