# turn on ipv4 forwarding
sysctl -w net.ipv4.ip_forward=1

# turn on per-connection statistics tracking
sysctl -w net.netfilter.nf_conntrack_acct=1

# turn on ipv6 forwarding
#sysctl -w net.ipv6.conf.all.forwarding=1


# Flush the FORWARD chain.
iptables -t filter -F FORWARD

# Set default deny policy.
iptables -t filter -P FORWARD DROP

## rules

# First, reject any non-SYN TCP packets in the NEW state with a reset.
# Resets are rate limited; if the rate is exceeded, the packet is just dropped.
iptables -t filter -A FORWARD -m conntrack --ctstate NEW -p tcp ! --syn -m limit --limit 1/minute --limit-burst 20 -j REJECT --reject-with tcp-reset
iptables -t filter -A FORWARD -m conntrack --ctstate NEW -p tcp ! --syn -j DROP

# Send new unmarked packets to a user-land queue; the queue will either drop the packet
# directly, or mark it and send it back to the chain.  We then save the mark
# written by the queue handler into the connection mark.  We then drop all
# packets that are part of an unmarked flow.
iptables -t filter -A FORWARD -m mark --mark 0 -m conntrack --ctstate NEW -j NFQUEUE --queue-num 1
iptables -t filter -A FORWARD -m conntrack --ctstate NEW -j CONNMARK --save-mark
iptables -t filter -A FORWARD -m connmark --mark 0 -j DROP

# Accept packets if the byte count on their traffic flow is less than 10000 bytes
iptables -t filter -A FORWARD -m conntrack --ctstate NEW,ESTABLISHED,RELATED -m connbytes --connbytes 0:10000 --connbytes-dir both --connbytes-mode bytes -j ACCEPT

# any remaining packets (including those not attached to a flow) are dropped by policy
