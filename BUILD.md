# Building SUPPL #

The overall SUPPL system is written in a number of different programming languages
and thus requires some fairly hefty setup to satisfy the external dependencies.
The main SUPPL compiler (directory `frontend`) is written in Haskell, and uses
the cabal build system.  The Java code (directory `java` and `javafilterlib`)
use Maven for building.  The OCaml Why3 shim (directory `ocaml`) uses
ocamlfind to find its dependicies; the easiest way to set up an OCaml environment
is to use the `opam` system.  Finally, the C netfilter library can be built using
a standard C compiler setup, provided the right development libraries for netfilter
are installed.

Detailed instructions for setting up all these systems are found below.
A cheet sheet for installation is also included for Debian-based Linux systems
and for Mac OS X.

## Haskell environment ##

The SUPPL compiler is written in Haskell, and uses cabal-install to 
automatically find its dependencies and install itself.  If you do not
already have a Haskell environment installed, the easiest way to get
one is to install the [Haskell Platform]; follow the directions
for your OS to install the system.

[Haskell Platform]: <http://www.haskell.org/platform/>

Once the Haskell platform is installed, execute

    cabal update

to fetch the current list of avaliable Haskell packages.
Now, if all goes well, the frontend compiler should automatically
find and install all its dependencies.  The makefile for `frontend`
simply executes:

    cabal install

If problems arise, you can attempt to perform the steps in a more manual
way, e.g.,

    cd frontend
    cabal configure
    cabal build
    cabal install

The [cabal user's guide] may be helpful reading if you run into problems.

[cabal user's guide]: <http://www.haskell.org/cabal/users-guide/>

### Common Problems ###

  * Make sure you add the cabal binary path to your system path. Typically,
    on *NIX systems, this directory is:

        $HOME/.cabal/bin/

  * Alex version is too old.  We need at least version 3.0.5.

  * Dependencies will not build.  This, unfortunately, happens with
    depressing frequency.  A farily common problem is that older versions
    of packages can conflict with new packages you want to install.
    One way to solve this is to remove old versions of packages and try to build again.
    A more heavyweight solution is to use a [cabal sandbox]; this will give you a fresh,
    isolated, environment for building.

[cabal sandbox]: <http://www.haskell.org/cabal/users-guide/installing-packages.html#developing-with-sandboxes>


## Java environment ##

You will need to install a fairly recent Java JDK; Java SE versions 1.6 or later should work.
The official [Oracle JDK] is probably most reliable, but alternative Java implementations
may also work.

[Oracle JDK]: <http://www.oracle.com/technetwork/java/javase/downloads/index.html>

In addition, you will need the [Maven] software project management/build tool, which is
used to build the Java runtime system.

[Maven]: <http://maven.apache.org/>

Once you have installed Maven, a configuration step must be performed before the system will build.
Execute the following from the top-level directory of the SUPPL code base:

    mvn install:install-file -Dfile=tuprolog/tuprolog.jar -DgroupId=it.unibo.alice.tuprolog -DartifactId=tuprolog -Dversion=2.7.2 -Dpackaging=jar

This will inform Maven about the tuProlog system; it needs this to build the runtime.

## OCaml environment and Why3 ##

To build the Why3 system and interaction shim, you will need to install an [OCaml environment].
Your best bet (on *NIX) is to install the [opam] system, which automatically manages package
dependencies and installation.

[Ocaml environment]: <http://ocaml.org/docs/install.html>
[opam]: <http://opam.ocaml.org>

NOTE: if you use opam, make sure you set up your environment by setting up
your .bashrc file or by executing the following.

    opam init
    eval $(opam config env)


With OCaml and opam installed, you need to install the Why3 system:

    opam install why3

The Why3 system pulls in dependencies on a number of other things, including GTK2+,
lablgtk and gtksourceview, which you will also need to install separately.

As an alternative, one can build why3 manually and avoid building the Why3 IDE; this
avoids pulling in the GTK dependencies.

    curl -O https://gforge.inria.fr/frs/download.php/33490/why3-0.83.tar.gz
    tar xfz why3-0.83.tar.gz
    cd why3-0.83
    ./configure --disable-ide --prefix=/usr/local
    make
    make byte
    make install
    make install-lib

In addition you will need to install at least one prover to discharge the
conflict detection problems.  [CVC4] and [AltErgo] seem to do well on
these problems.  After installing Why3 you will need to configure
any provers you have installed.  If installed in standard locations,
these can usually be found automatically by the `why3config` tool:

    why3config --detect-provers

If this doesn't work provers may also be added manually:

    why3config --add-prover cvc4 <location of cvc4 binary>
    why3config --add-prover z3 <location of z3 binary>

The currently-configured provers can be listed via:

    why3 --list-provers


[CVC4]: <http://cvc4.cs.nyu.edu/web/>
[AltErgo]: <http://alt-ergo.lri.fr>

### Common problems ###

  * `opam` and `ocamlfind` rely on certain environment variables being set to correctly to locate
    installed packages and binaries.  Be sure to:

        eval $(opam config env)

    to set up your environment.  Alternately:

        opam init --auto-setup

    will edit your `.bash_profile` to set up your environment on shell startup.


## Linux/C Environment ##

To build the netfilter firewall prototype, you will need a standard C compiler
toolchain (e.g., gcc) as well as the development libraries for netfilter.  These are:

    libnetfilter-conntrack
    libnetfilter-queue
    libnfnetlink

Obviously, you will also need to be running a Linux kernel with the appropriate
netfilter modules avaliable.

In addition, to build the Java JNI bindings to this library, you will need
the SWIG tool for building C bindings.

### Common problems ###
  * Missing include files, e.g., `libnetfilter_conntrack/libnetfilter_conntrack.h`.  If you
    are on a Debian-based Linux, make sure you install the development versions of the required
    packages.  For example:

        sudo apt-get install libnetfilter-conntrack3
        sudo apt-get install libnetfilter-conntrack-dev
        sudo apt-get install libnetfilter-queue1
        sudo apt-get install libnetfilter-queue-dev
        sudo apt-get install libnfnetlink0
        sudo apt-get install libnfnetlink-dev

  * Missing JNI include files, e.g., `jni.h`.  Edit the `Makefile` in `javafilterlib`
    to include the correct JDK include file directory for your system.

  * Link errors when building in `filterlib`.  This is probably caused by an outdated
     libnetfilter-queue1 package.  You need version 1.0.0 or later: Debian stable and
     Ubuntu 12.04 LTS currently have an older version of this package.  Find the right
     packages somewhere on the Internet and execute:

         sudo dpkg -i libnetfilter-queue1_1.0.0-1_i386.deb
         sudo dpkg -i libnetfilter-queue-dev_1.0.0-1_i386.deb

     Edit as necessary for your system.

# Cheet sheets #

## Windows systems ##

There are binary installers for Java, Haskell and OCaml that work on
Windows.  I have no experince with them.

Your best bet might well be to build using a Linux virtual machine.
You might also try [Cygwin].

Good luck!

[Cygwin]: <https://www.cygwin.com>


## Mac OS X system ##

I recommend using [homebrew] to build the necessary system-level dependencies on OS X.
Follow the directions on the homebrew homepage to set it up on your system.

[homebrew]: <http://brew.sh>

For Haskell dependencies:

    brew install ghc
    brew install haskell-platform
    export PATH=$HOME/.cabal/bin
    cabal update
    cabal install alex
    cabal install happy

    make frontend

For OCaml and Why3 dependencies:

    brew install ocaml opam
    opam install ocamlfind
    opam install alt-ergo
    
    curl -O https://gforge.inria.fr/frs/download.php/33490/why3-0.83.tar.gz
    tar xfz why3-0.83.tar.gz
    cd why3-0.83
    ./configure --disable-ide --prefix=/usr/local
    make
    make byte
    make install # may need sudo
    make install-lib # may need sudo
    cd ..

    why3config --detect-provers
    make ocaml

For Java dependencies:

    brew install maven
    mvn install:install-file -Dfile=tuprolog/tuprolog.jar -DgroupId=it.unibo.alice.tuprolog -DartifactId=tuprolog -Dversion=2.7.2 -Dpackaging=jar

    (cd java; mvn package)


For C/Netfilter dependencies: no can do.  This code will only work on a Linux system.


## Debian-based Linux systems ##

For Haskell dependencies:

    sudo apt-get install ghc
    sudo apt-get install cabal-install
    sudo apt-get install zlib1g-dev
    sudo apt-get install libreadline-dev

    cabal update
    export PATH=$HOME/.cabal.bin:$PATH
    # Now rebuild a new cabal-install from sources
    # This may not be necessary, depending on how recent your distro is
    cabal install cabal-install
    sudo apt-get remove cabal-install    
    rehash -r

    cabal install alex
    cabal install happy

    make frontend

For OCaml and Why3 dependencies:

    sudo apt-get install ocamal
    sudo apt-get install ocamal-findlib
    sudo apt-get install alt-ergo
    sudo apt-get install cvc3 # optional
    sudo apt-get install cvc4 # optional
    
    curl -O https://gforge.inria.fr/frs/download.php/33490/why3-0.83.tar.gz
    tar xfz why3-0.83.tar.gz
    cd why3-0.83
    ./configure --disable-ide --prefix=/usr/local
    make
    make byte
    make install # may need sudo, depending on install loc
    make install-lib # may need sudo, depending on install loc
    cd ..

    why3config --detect-provers
    make ocaml

For Java dependencies:

    sudo apt-get install openjdk-7-jdk
    # OR install Oracle JDK from their website
    sudo apt-get install maven
    mvn install:install-file -Dfile=tuprolog/tuprolog.jar -DgroupId=it.unibo.alice.tuprolog -DartifactId=tuprolog -Dversion=2.7.2 -Dpackaging=jar

    (cd java; mvn package)


For C/Netfilter dependencies:

    sudo apt-get install libnetfilter-conntrack3
    sudo apt-get install libnetfilter-conntrack-dev
    sudo apt-get install libnetfilter-queue1
    sudo apt-get install libnetfilter-queue-dev
    sudo apt-get install libnfnetlink0
    sudo apt-get install libnfnetlink-dev
    
    make filterlib

For Java/Netfilter dependencies:

    sudo apt-get install swig
    $EDITOR javafilterlib/Makefile  # set location of JDK include files
    
    make javafilterlib
