0.4
---

* "findall now returns a set of solutions instead of a list

* "findall" now requires "?" variable binding forms in its goal for output mode
   variables that appear in the term pattern.

* Syntax: actions are now introduced via the "action" keyword
           instead of "primitive procedure"

* Feature: tables now take a mandatory primary key.  Tables now have
    set-valued semantics instead of bag-valued semantics, and the columns in the
    primary key uniquely determine the rest of the row.  The "queue delete all"
    command is no longer necessary and is removed.  Mode checking for deletes
    now only requires the key columns to be specified.

* Feature: Expanded the allowed clause forms for query branches
    and "foreach" constructs.  They may now have a
    comma-separated sequence of predicates, equalities and
    binary comparisons.  Variables are bound from left-to-right in the
    comma-sequence.

* Feature: Conflict analysis phase.  Conflicts may be declared with syntax like:
       conflict action1( ?X, ?Y ), action2( ?Z ) =>
            ... some clause mentioning X, Y and Z ....

    In conflict analysis mode, the handlers in a policy will be analyzed to find
    all potential conflicts.  Every potential conflict generates a problem
    to be passed off to an SMT solver.  The solver tries to prove that the
    potential conflict cannot actually happen.

* BUGFIX: modechecking gets wrong answers with disjunctions
* BUGFIX: peephole optimizer gets wrong answers with disjunctions

0.3
---

* Syntax: type definitions now use ':=' symbol rather than '='.

                type asdf := list(number).
   
          instead of:
 
                type asdf = list(number).


* Syntax: '=<' is now accepted as a less-than-or-equal operator
          in addition to '<='

* Syntax: C-style escape charaters now allowed in string literals

* Syntax: The "list" type now requires parens around its argument.
            list(X)
          instead of
            list X

* UI: Improved error messages from the parsing phase.

* Feature: Tuple types and terms are now accepted by the system.

* Feature: '++' is now an operator for string and list concatenation

* Feature: there is now an implicit "prelude" module that contains lots
    of primitive functions and predicates provided by the
    underlying Prolog implementation.  See 'frontend/prelude.plc'

* Feature: finite sets and maps
    Implement persistent finite maps and sets by using the pcollections
    library and piping them through the tuProlog term API.  Types 'map(K,V)'
    and 'set(V)' are now avaliable.

* API: Some minor changes to the runtime API.  Main change: The Prolog
    engine used by the RunPolicy class is now 'edu.pdx.cecs.suppl.SupplEngine' rather
    than 'alice.tuprolog.Prolog'.

* Build system: The supplc compiler now wants to be installed in order to have
    access to associated data files.  By default, supplc will install in the
    user's "$HOME/.cabal/bin" directory.

* Bugfix : implement a workaround for the fact that tuProlog has crappy
    handling of string escape sequences.

* Documentation : module documentation for the compiler source.
    Run "make docs" in directory "frontend" to build.

* Documentation : module documentation for the Java runtime source.
    Run "make docs" in directory "java" to build.

* Documentation : "docs/compilerInternals.html" contains some high-level
    design documentation for the compiler.

* Documentation: updated "docs/grammar.txt" to latest syntactic updates

* Internals : overhaul the parsing phase.  We should now get better
    error messages in some instances, and the parser grammar is simpler
    because the term, qterm and clause syntactic categories are collapsed
    into a single "preterm" category.  The three syntactic sorts are
    then distinguished in a separate postprocessing phase.

0.2
----
* Syntax: predicates may now be declared with a mode.  For example:
     predicate asdf(number in, string out).
  may be used instead of
     predicate asdf(number, string).
     mode asdf(in,out).
  Additional modes may be declared as usual with additional mode statements.

* Syntax: rules with no body can now be written without a turnstile:
       fact(10).
   instead of
       fact(10) :- .
   The old style is still accepted

* Syntax: "return" is no longer a keyword

* IR: a brand-new intermediate representation

* Modechecking: the modechecker is entirely overhauled

* Bugfix: lots of corner cases in the old modechecker are now fixed

* Command line: the supplc executable now accepts command line options
          run "supplc -h" for a usage listing

0.1
----
* Initial implementation
