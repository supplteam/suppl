/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

#include <stdio.h>
#include <jni.h>
#include "../filterlib/api.h"

JavaVM* g_vm = NULL;

jint JNI_OnLoad(JavaVM* vm, void* x )
{
  g_vm = vm;
  init_connection_library();
  return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM* vm, void* x )
{
  teardown_connection_library();
}


struct connectionCallbackInfo {
  jmethodID install_method;
  jmethodID destroy_method;
  jobject conn_obj;
};

int connection_free_callback( void* arg ) {
  struct connectionCallbackInfo* cbi = (struct connectionCallbackInfo*) arg;
  
  JNIEnv* env;
  if( (*g_vm)->AttachCurrentThreadAsDaemon( g_vm, &env, NULL ) != 0 ) {
    fprintf(stderr, "failed to attach free callback thread!\n");
    return -1;
  }
  
  fprintf(stderr, "freeing global ref\n");
  (*env)->DeleteGlobalRef(env, cbi->conn_obj);
  free(cbi);

  return 0;
}

int connection_destroy_callback( const struct connection_desc* desc
		       , const struct connection_stats* stats
		       , void* arg ) {
  struct connectionCallbackInfo* cbi = (struct connectionCallbackInfo*) arg;
  
  JNIEnv* env;
  if( (*g_vm)->AttachCurrentThreadAsDaemon( g_vm, &env, NULL ) != 0 ) {
    fprintf(stderr, "failed to attach free callback thread!\n");
    return -1;
  }

  jlong jdesc;
  jlong jstats;

  *(struct connection_desc **)&jdesc = desc;
  *(struct connection_stats **)&jstats = stats;

  (*env)->CallVoidMethod( env,
		  cbi->conn_obj,
		  cbi->destroy_method,
		  jdesc,
		  jstats
		  );

  if( (*env)->ExceptionCheck(env) ) {
    (*env)->ExceptionDescribe(env);
    (*env)->ExceptionClear(env);
    return -1;
  }

  return 0;
}

int connection_install_callback( const struct connection_desc* desc
		       , const struct connection_stats* stats
		       , void* arg ) {
  struct connectionCallbackInfo* cbi = (struct connectionCallbackInfo*) arg;
  
  JNIEnv* env;
  if( (*g_vm)->AttachCurrentThreadAsDaemon( g_vm, &env, NULL ) != 0 ) {
    fprintf(stderr, "failed to attach free callback thread!\n");
    return -1;
  }
  
  jlong jdesc;
  *(struct connection_desc **)&jdesc = desc;

  (*env)->CallVoidMethod( env,
		  cbi->conn_obj,
		  cbi->install_method,
		  jdesc
		  );

  if( (*env)->ExceptionCheck(env) ) {
    (*env)->ExceptionDescribe(env);
    (*env)->ExceptionClear(env);
    return -1;
  }

  return 0;
}

JNIEXPORT void JNICALL Java_edu_pdx_cecs_suppl_conn_JavaApi_connectionGetCounters
( JNIEnv* env
, jclass jcls
, jlong conn_hdl
) {
  conn_t conn = *((conn_t *) &conn_hdl);
  struct connection_stats* stats =
    (struct connection_stats*) malloc(sizeof(struct connection_stats));

  connection_get_counters(conn, stats);

  jlong jresult;
  *(struct connection_stats **)&jresult = stats; 
  return jresult;
}

JNIEXPORT jlong JNICALL Java_edu_pdx_cecs_suppl_conn_JavaApi_installConnection
( JNIEnv* env
, jclass jcls
, jlong descl
, jobject connObj
) {
  struct connection_description* desc = *((struct connection_desc**) &descl);

  struct connectionCallbackInfo* cbi =
    (struct connectionCallbackInfo*) malloc(sizeof(struct connectionCallbackInfo));

  jclass dumpCls = (*env)->GetObjectClass(env, connObj);
  if( dumpCls == NULL ) {
    fprintf(stderr, "Failed to get object class!\n");
    return 0;
  }

  cbi-> install_method = (*env)->GetMethodID(env, dumpCls, "invokeInstallCallback", "(J)V");
  if( cbi->install_method == NULL ) {
    fprintf(stderr, "Failed to get callback method!\n");
    free(cbi);
    return 0;
  }

  cbi-> destroy_method = (*env)->GetMethodID(env, dumpCls, "invokeDestroyCallback", "(JJ)V");
  if( cbi->destroy_method == NULL ) {
    fprintf(stderr, "Failed to get callback method!\n");
    free(cbi);
    return 0;
  }

  cbi->conn_obj = (*env)->NewGlobalRef(env, connObj);

  conn_t hdl = install_connection(
		     desc, 
		     &connection_install_callback,
		     &connection_destroy_callback,
		     &connection_free_callback,
		     cbi );

  conn_t jresult;
  *(conn_t *)&jresult = hdl; 
  return jresult;
}

JNIEXPORT jlong JNICALL Java_edu_pdx_cecs_suppl_conn_JavaApi_freshReference
( JNIEnv* env
, jclass jcls
, jlong jconn_handle
) {
  conn_t conn_handle = *((conn_t *) &jconn_handle);
  conn_t fresh = fresh_reference(conn_handle);
  conn_t jresult;
  *(conn_t *)&jresult = fresh; 
  return jresult;
}

JNIEXPORT jlong JNICALL Java_edu_pdx_cecs_suppl_conn_JavaApi_freeConnection
( JNIEnv* env
, jclass jcls
, jlong jconn_handle
) {
  conn_t conn_handle = *((conn_t *) &jconn_handle);
  free_connection(conn_handle);
}

JNIEXPORT jint JNICALL Java_edu_pdx_cecs_suppl_conn_JavaApi_teardownConnection
( JNIEnv* env
, jclass jcls
, jlong jconn_handle
) {
  conn_t conn_handle = *((conn_t *) &jconn_handle);
  return (jint) teardown_connection(conn_handle);
}

/***** Dump connections ******/

struct dumpCallbackInfo {
  jmethodID dump_method;
  jobject dump_ob;
  JNIEnv* dump_env;
};

int invoke_callback(
   conn_t conn_handle,
   const struct connection_desc* desc,
   const struct connection_stats* stats,
   void* arg ) {

  struct dumpCallbackInfo* cdi = (struct dumpCallbackInfo*) arg;
  JNIEnv* env = cdi->dump_env;
  
  jlong jconn_handle;
  jlong jdesc;
  jlong jstats;

  *(conn_t *)&jconn_handle = conn_handle;
  *(struct connection_desc **)&jdesc = desc;
  *(struct connection_stats **)&jstats = stats;

  (*env)->CallVoidMethod( env,
		  cdi->dump_ob,
		  cdi->dump_method,
		  jconn_handle,
		  jdesc,
		  jstats
		  );

  if( (*env)->ExceptionCheck(env) ) {
    return -1;
  }
    
  return 0;
}


JNIEXPORT void JNICALL Java_edu_pdx_cecs_suppl_conn_JavaApi_dumpConnections
( JNIEnv* env
, jclass jcls
, jobject cb_ob
) {

  jclass dumpCls = (*env)->GetObjectClass(env, cb_ob);
  if( dumpCls == NULL ) {
    fprintf(stderr, "Failed to get object class!\n");
    return;
  }

  jmethodID mid = (*env)->GetMethodID(env, dumpCls, "invokeCallback", "(JJJ)V");
  if( mid == NULL ) {
    fprintf(stderr, "Failed to get callback method!\n");
    return;
  }

  struct dumpCallbackInfo cdi;
  cdi.dump_ob = cb_ob;
  cdi.dump_method = mid;
  cdi.dump_env = env;

  dump_connections( &invoke_callback, (void *) &cdi );
}
