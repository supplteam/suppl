// Copyright (c) 2013 SOUND Project, Portland State University.

%{
#include <netinet/in.h>
#include <sys/socket.h>
%}

%typemap(jni) NET_SHORT "jint"
%typemap(jtype) NET_SHORT "int"
%typemap(jstype) NET_SHORT "int"
%typemap(javain) NET_SHORT "$javainput"
%typemap(javaout) NET_SHORT { return $jnicall; }
%typemap(in) NET_SHORT { $1 = htons((uint16_t) $input); }
%typemap(out) NET_SHORT { $result = (jint) ntohs($1); }

%typemap(jni) NET_LONG "jlong"
%typemap(jtype) NET_LONG "long"
%typemap(jstype) NET_LONG "long"
%typemap(javain) NET_LONG "$javainput"
%typemap(javaout) NET_LONG { return $jnicall; }
%typemap(in) NET_LONG { $1 = htonl((uint32_t) $input); }
%typemap(out) NET_LONG { $result = (jlong) ntohl($1); }


%typemap(jni) INET4_ADDR "jbyteArray"
%typemap(jtype) INET4_ADDR "byte[]"
%typemap(jstype) INET4_ADDR "java.net.InetAddress"

%typemap(javain) INET4_ADDR "$javainput.getAddress()"
%typemap(javaout, throws="java.net.UnknownHostException") INET4_ADDR
  { return java.net.InetAddress.getByAddress($jnicall); }

%typemap(in) INET4_ADDR {
  int i;
  int sz;
  jbyte* arr;
  jbyteArray jarr = $input;
  signed char* carr = (signed char*) &($1);

  if (!jarr) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null array");
    $cleanup
    return $null;
  }
  sz = (*jenv)->GetArrayLength(jenv, jarr);
  if(sz != 4) {
    SWIG_JavaThrowException(jenv, SWIG_JavaIllegalArgumentException, "expected array of size 4");
    $cleanup
    return $null;
  }
  arr = (*jenv)->GetByteArrayElements(jenv, jarr, 0);
  if(!arr) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null array contents");
    $cleanup
    return $null;
  }
  for (i=0; i<4; i++) carr[i] = arr[i];
  (*jenv)->ReleaseByteArrayElements(jenv, jarr, arr, JNI_ABORT);
}

%typemap(out) INET4_ADDR {
  jbyte* arr;
  int i;
  signed char* carr = (signed char*) &($1);

  $result = (*jenv)->NewByteArray(jenv, 4);
  if(! $result) {
    SWIG_JavaThrowException(jenv, SWIG_JavaOutOfMemoryError, "OOM");
    $cleanup
    return $null;
  }
  arr = (*jenv)->GetByteArrayElements(jenv, $result, 0);
  if(!arr) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null array contents");
    $cleanup
    return $null;
  }
  for(i=0; i<4; i++) arr[i] = carr[i];
  (*jenv)->ReleaseByteArrayElements(jenv, $result, arr, 0);
}



%typemap(jni) INET6_ADDR "jbyteArray"
%typemap(jtype) INET6_ADDR "byte[]"
%typemap(jstype) INET6_ADDR "java.net.InetAddress"

%typemap(javain) INET6_ADDR "$javainput.getAddress()"
%typemap(javaout, throws="java.net.UnknownHostException") INET6_ADDR
  { return java.net.InetAddress.getByAddress($jnicall); }

%typemap(in) INET6_ADDR {
  int i;
  int sz;
  jbyte* arr;
  jbyteArray jarr = $input;
  signed char* carr = (signed char*) &($1);

  if (!jarr) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null array");
    $cleanup
    return $null;
  }
  sz = (*jenv)->GetArrayLength(jenv, jarr);
  if(sz != 16) {
    SWIG_JavaThrowException(jenv, SWIG_JavaIllegalArgumentException, "expected array of size 16");
    $cleanup
    return $null;
  }
  arr = (*jenv)->GetByteArrayElements(jenv, jarr, 0);
  if(!arr) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null array contents");
    $cleanup
    return $null;
  }
  for (i=0; i<16; i++) carr[i] = arr[i];
  (*jenv)->ReleaseByteArrayElements(jenv, jarr, arr, JNI_ABORT);
}

%typemap(out) INET6_ADDR {
  jbyte* arr;
  int i;
  signed char* carr = (signed char*) &($1);

  $result = (*jenv)->NewByteArray(jenv, 16);
  if(! $result) {
    SWIG_JavaThrowException(jenv, SWIG_JavaOutOfMemoryError, "OOM");
    $cleanup
    return $null;
  }
  arr = (*jenv)->GetByteArrayElements(jenv, $result, 0);
  if(!arr) {
    SWIG_JavaThrowException(jenv, SWIG_JavaNullPointerException, "null array contents");
    $cleanup
    return $null;
  }
  for(i=0; i<16; i++) arr[i] = carr[i];
  (*jenv)->ReleaseByteArrayElements(jenv, $result, arr, 0);
}

%apply NET_SHORT { in_port_t }
%apply NET_LONG  { uint32_t sin6_flowinfo }
%apply NET_LONG  { uint32_t sin6_scope_id } 

%apply INET4_ADDR { struct in_addr, in_addr_t }
%apply INET6_ADDR { struct in6_addr }

typedef unsigned short sa_family_t;

struct sockaddr_in {
  sa_family_t sin_family;
  in_port_t sin_port;
  struct in_addr sin_addr;
};

struct sockaddr_in6 {
  sa_family_t     sin6_family;
  in_port_t       sin6_port;
  uint32_t        sin6_flowinfo;
  struct in6_addr sin6_addr;
  uint32_t        sin6_scope_id;
};


%javaconst(1);

enum
  {
    IPPROTO_IP = 0,	   /* Dummy protocol for TCP.  */
    IPPROTO_HOPOPTS = 0,   /* IPv6 Hop-by-Hop options.  */
    IPPROTO_ICMP = 1,	   /* Internet Control Message Protocol.  */
    IPPROTO_IGMP = 2,	   /* Internet Group Management Protocol. */
    IPPROTO_IPIP = 4,	   /* IPIP tunnels (older KA9Q tunnels use 94).  */
    IPPROTO_TCP = 6,	   /* Transmission Control Protocol.  */
    IPPROTO_EGP = 8,	   /* Exterior Gateway Protocol.  */
    IPPROTO_PUP = 12,	   /* PUP protocol.  */
    IPPROTO_UDP = 17,	   /* User Datagram Protocol.  */
    IPPROTO_IDP = 22,	   /* XNS IDP protocol.  */
    IPPROTO_TP = 29,	   /* SO Transport Protocol Class 4.  */
    IPPROTO_IPV6 = 41,     /* IPv6 header.  */
    IPPROTO_ROUTING = 43,  /* IPv6 routing header.  */
    IPPROTO_FRAGMENT = 44, /* IPv6 fragmentation header.  */
    IPPROTO_RSVP = 46,	   /* Reservation Protocol.  */
    IPPROTO_GRE = 47,	   /* General Routing Encapsulation.  */
    IPPROTO_ESP = 50,      /* encapsulating security payload.  */
    IPPROTO_AH = 51,       /* authentication header.  */
    IPPROTO_ICMPV6 = 58,   /* ICMPv6.  */
    IPPROTO_NONE = 59,     /* IPv6 no next header.  */
    IPPROTO_DSTOPTS = 60,  /* IPv6 destination options.  */
    IPPROTO_MTP = 92,	   /* Multicast Transport Protocol.  */
    IPPROTO_ENCAP = 98,	   /* Encapsulation Header.  */
    IPPROTO_PIM = 103,	   /* Protocol Independent Multicast.  */
    IPPROTO_COMP = 108,	   /* Compression Header Protocol.  */
    IPPROTO_SCTP = 132,	   /* Stream Control Transmission Protocol.  */
    IPPROTO_RAW = 255,	   /* Raw IP packets.  */
    IPPROTO_MAX
  };

enum
  {
    IPPORT_ECHO = 7,		/* Echo service.  */
    IPPORT_DISCARD = 9,		/* Discard transmissions service.  */
    IPPORT_SYSTAT = 11,		/* System status service.  */
    IPPORT_DAYTIME = 13,	/* Time of day service.  */
    IPPORT_NETSTAT = 15,	/* Network status service.  */
    IPPORT_FTP = 21,		/* File Transfer Protocol.  */
    IPPORT_TELNET = 23,		/* Telnet protocol.  */
    IPPORT_SMTP = 25,		/* Simple Mail Transfer Protocol.  */
    IPPORT_TIMESERVER = 37,	/* Timeserver service.  */
    IPPORT_NAMESERVER = 42,	/* Domain Name Service.  */
    IPPORT_WHOIS = 43,		/* Internet Whois service.  */
    IPPORT_MTP = 57,

    IPPORT_TFTP = 69,		/* Trivial File Transfer Protocol.  */
    IPPORT_RJE = 77,
    IPPORT_FINGER = 79,		/* Finger service.  */
    IPPORT_TTYLINK = 87,
    IPPORT_SUPDUP = 95,		/* SUPDUP protocol.  */


    IPPORT_EXECSERVER = 512,	/* execd service.  */
    IPPORT_LOGINSERVER = 513,	/* rlogind service.  */
    IPPORT_CMDSERVER = 514,
    IPPORT_EFSSERVER = 520,

    /* UDP ports.  */
    IPPORT_BIFFUDP = 512,
    IPPORT_WHOSERVER = 513,
    IPPORT_ROUTESERVER = 520,

    /* Ports less than this value are reserved for privileged processes.  */
    IPPORT_RESERVED = 1024,

    /* Ports greater this value are reserved for (non-privileged) servers.  */
    IPPORT_USERRESERVED = 5000
  };
