// Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu.

package edu.pdx.cecs.suppl.conn;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class ConnectionLib extends Library {
    JavaLibrary jlib;

    public ConnectionLib(JavaLibrary jlib) { this.jlib = jlib; }

    public static class MyDumpCallback extends DumpCallback {
	LinkedList<Term> ls;
	JavaLibrary jlib;

	public MyDumpCallback(JavaLibrary jlib) {
	    ls = new LinkedList<Term>();
	    this.jlib = jlib;
	}

	protected void callback(long conn_handle, connection_desc desc, connection_stats stats) {
	    Term dterm = jlib.registerDynamic( desc );
	    Term sterm = jlib.registerDynamic( stats );
	    ls.add( new Struct( dterm, sterm ) );
	}

	public Term getResult() {
	    return new Struct( ls.toArray(new Term[0]) );
	}
    }

    public static class MyConnection extends Connection {
	public MyConnection(connection_desc desc) {
	    super(desc);
	}

	protected void installCallback( connection_desc desc ) {
	    System.out.println("MyConnection install callback!");
	}

	protected void destroyCallback( connection_desc desc, 
					connection_stats stats ) {
	    System.out.println("MyConnection destroy callback!");
	}
    }

    public static class ProtoPort {
	public int proto;
	public int port;

	public String toString() {
	    return (proto + " " + port);
	}
    }

    public boolean dumpConnections_1( Term x ) throws Exception {
	MyDumpCallback dcb = new MyDumpCallback( jlib );
	JavaApi.dumpConnections( dcb );
	Term cs = dcb.getResult();
	return x.unify(getEngine(), cs);
    }

    public Term installConnection_4( Term src,
				     Term sport,
				     Term dest,
				     Term dport )
    {
	Term ret = null;

	System.out.println("installing!");

	try {
	    Struct evSrc = (Struct) evalExpression(src);
	    Struct evDest = (Struct) evalExpression(dest);
	    Struct evSport = (Struct) evalExpression(sport);
	    Struct evDport = (Struct) evalExpression(dport);

	    InetAddress srcAddr  = (InetAddress) jlib.getRegisteredDynamicObject(evSrc);
	    InetAddress destAddr = (InetAddress) jlib.getRegisteredDynamicObject(evDest);
	    ProtoPort srcPort    = (ProtoPort)   jlib.getRegisteredDynamicObject(evSport);
	    ProtoPort destPort   = (ProtoPort)   jlib.getRegisteredDynamicObject(evDport);

	    if( srcPort.proto != destPort.proto ) {
		System.err.println("protocol mismatch!");
		return null;
	    }

	    connection_desc d = new connection_desc();
	    d.setSrc_addr( srcAddr );
	    d.setDest_addr( destAddr );
	    d.setProtocol( (short) srcPort.proto );
	    d.setSrc_port( srcPort.port );
	    d.setDest_port( destPort.port );

	    System.out.println("installing connection!");
	    MyConnection c = new MyConnection(d);
	    ret = jlib.registerDynamic( c );
	} catch (Throwable ex) {
            ex.printStackTrace();
	}

	return ret;
    }

    public boolean printobj_1( Term x ) {
	boolean ret = false;
	x = x.getTerm();

	if(x instanceof Struct) {
	    try {
		Object obj = jlib.getRegisteredDynamicObject((Struct) x);
		if(obj != null) {
		    getEngine().stdOutput(obj.toString());
		    ret = true;
		}
	    } catch (InvalidObjectIdException ex) {
	    }
	} if (x instanceof alice.tuprolog.Number) {
	    getEngine().stdOutput(x.toString());
	    ret = true;
	}

	return ret;
    }

    public Term tcp_0() {
	ProtoPort p = new ProtoPort();
	p.proto = JavaApi.IPPROTO_TCP;
	p.port  = 0;
	return jlib.registerDynamic( p );
    }

    public Term tcp_1( alice.tuprolog.Number x ) {
	ProtoPort p = new ProtoPort();
	p.proto = JavaApi.IPPROTO_TCP;
	p.port  = x.intValue();
	return jlib.registerDynamic( p );
    }

    public Term udp_0() {
	ProtoPort p = new ProtoPort();
	p.proto = JavaApi.IPPROTO_UDP;
	p.port  = 0;
	return jlib.registerDynamic( p );
    }

    public Term udp_1( alice.tuprolog.Number x ) {
	ProtoPort p = new ProtoPort();
	p.proto = JavaApi.IPPROTO_UDP;
	p.port  = x.intValue();
	return jlib.registerDynamic( p );
    }

    public Term icmp_0() {
	ProtoPort p = new ProtoPort();
	p.proto = JavaApi.IPPROTO_ICMP;
	p.port  = 0;
	return jlib.registerDynamic( p );
    }

    public Term ip_4( alice.tuprolog.Number x,
		      alice.tuprolog.Number y,
		      alice.tuprolog.Number z,
		      alice.tuprolog.Number w) {

	byte[] bs = new byte[] { (byte) x.intValue(),
				 (byte) y.intValue(),
				 (byte) z.intValue(),
				 (byte) w.intValue() };

	Term result = null;
	    
	try {
	    InetAddress addr = null;
	    addr = InetAddress.getByAddress( bs );
	    result = jlib.registerDynamic( addr );
	} catch ( Throwable ex ) {
	    ex.printStackTrace();
	}

	return result;
    }
}
