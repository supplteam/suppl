// Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu.

package edu.pdx.cecs.suppl.conn;

import java.lang.System;

public class JavaApi implements SwigApiConstants {
    static {
	try {
	    System.loadLibrary("javaapi"); 
	} catch (UnsatisfiedLinkError e) {
	    System.err.println("Native code library failed to load. \n" + e); 
	    System.exit(1); 
	}
    }

    public static native void dumpConnections(DumpCallback cb) throws Exception;
    public static native long installConnection(long desc, Connection conn);
    public static native long freshReference(long conn_handle);
    public static native void freeConnection(long conn_handle);
    public static native int teardownConnection(long conn_handle);
    public static native long connectionGetCounters( long conn_handle );

    /*****************************/

    static class MyDumpCallback extends DumpCallback {
	protected void callback( long conn,
				 connection_desc desc,
				 connection_stats stats )
	    throws Exception {

	    System.out.print( desc.getSrc_addr() );
	    System.out.print( ":" );
	    System.out.print( desc.getSrc_port() );
	    System.out.print( "  " );
	    System.out.print( desc.getDest_addr() );
	    System.out.print( ":" );
	    System.out.print( desc.getDest_port() );
	    System.out.print( "  " );
	    System.out.print( stats.getOrig_bytes() );
	    System.out.print( "  " );
	    System.out.println( stats.getRepl_bytes() );
	}
    }

    static void work() throws Exception {
	connection_desc d = new connection_desc();
	d.setSrc_addr (java.net.InetAddress.getByName("192.168.101.2"));
	d.setDest_addr(java.net.InetAddress.getByName("192.168.102.2"));
	d.setProtocol((short) IPPROTO_TCP);
	d.setSrc_port(0);
	d.setDest_port(22);

	Connection c = new Connection(d);

	MyDumpCallback dcb = new MyDumpCallback();
	for(int i =0; i<10; i++) {
	    try {
		java.lang.Thread.sleep(2 * 1000);
	    } catch (Exception ex) {
	    }

	    dumpConnections( dcb );
	}

	c.teardown();
	c.delete();
    }

    public static void main(String[] args) throws Exception {
	work();

	try {
	    java.lang.Thread.sleep(1000);
	} catch (Exception ex) {
	}
    }
};
