// Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu.

package edu.pdx.cecs.suppl.conn;

public class Connection {
    private long conn_handle;
	
    public Connection(connection_desc desc)
    {
	conn_handle = JavaApi.installConnection( connection_desc.getCPtr(desc), this );
	if(conn_handle == 0 ) {
	    throw new Error("Failed to install connection\n");
	}
    }

    public Connection(long hdl) {
	this.conn_handle = JavaApi.freshReference(hdl);
    }
    
    public synchronized final void delete() {
	if(conn_handle != 0) {
	    JavaApi.freeConnection(conn_handle);
	    conn_handle = 0;
	}
    }

    public final void teardown() {
	if( conn_handle != 0 ) {
	    JavaApi.teardownConnection(conn_handle);
	}
    }

    protected void finalize() {
	delete();
    }

    private void invokeInstallCallback( long ldesc ) {
	connection_desc desc = null;
	if(ldesc != 0) {
	    desc = new connection_desc( ldesc, false );
	}
	installCallback( desc );
    }
    
    private void invokeDestroyCallback( long ldesc, long lstats ) {
	connection_desc desc = null;
	if(ldesc != 0) {
	    desc = new connection_desc( ldesc, false );
	}
	connection_stats stats = null;
	if(lstats != 0) {
	    stats = new connection_stats( lstats, false );
	}
	
	destroyCallback( desc, stats );
    }

    public final connection_stats getCounters() {
	if( conn_handle != 0 ) {
	    long lstats = JavaApi.connectionGetCounters(conn_handle);
	    return new connection_stats(lstats, true);
	} else {
	    return null;
	}
    }
    
    protected void installCallback( connection_desc desc ) {
	System.out.println("Java-side install callback!");
    }

    protected void destroyCallback( connection_desc desc, 
				    connection_stats stats ) {
	System.out.println("Java-side destroy callback!");
    }
}

