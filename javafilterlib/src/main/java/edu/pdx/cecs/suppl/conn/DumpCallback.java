// Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu.

package edu.pdx.cecs.suppl.conn;

public abstract class DumpCallback {
    abstract protected void callback(long conn_handle, connection_desc desc, connection_stats stats)
	throws Exception;

    private void invokeCallback(long conn_handle, long ldesc, long lstats) throws Exception {
	connection_desc desc = null;
	if(ldesc != 0) {
	    desc = new connection_desc( ldesc, false );
	}
	connection_stats stats = null;
	if(lstats != 0) {
	    stats = new connection_stats( lstats, false );
	}
	    
	callback( conn_handle, desc, stats );
    }
};
