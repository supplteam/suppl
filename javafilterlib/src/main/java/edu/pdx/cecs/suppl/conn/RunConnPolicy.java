// Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu.

package edu.pdx.cecs.suppl.conn;

import alice.tuprolog.*;
import alice.tuprolog.event.*;
import alice.tuprolog.lib.*;
import java.io.*;
import java.net.*;
import java.util.*;

import edu.pdx.cecs.suppl.RunPolicy;
import edu.pdx.cecs.suppl.SupplEngine;

public class RunConnPolicy {

    static SupplEngine setupEngine(String[] filenames) throws Exception {
        SupplEngine engine = RunPolicy.setupEngine();
        JavaLibrary jlib = engine.getJavaLib();
	ConnectionLib clib = new ConnectionLib(jlib);
	engine.loadLibrary(clib);

        InputStream connInStream = 
	    RunConnPolicy.class.getResourceAsStream("connTheory.pl");
        Theory connTheory = new Theory(connInStream);
        engine.addTheory(connTheory);

        RunPolicy.loadTheories(filenames, engine);

	return engine;
    }

    public static void main(String[] args) throws Exception {
	SupplEngine engine = setupEngine(args);
	
	ServerSocket ssock1 = new ServerSocket( 2222, 0, InetAddress.getByName("192.168.101.1") );
	ServerSocket ssock2 = new ServerSocket( 2223, 0, InetAddress.getByName("192.168.102.1") );

	RunPolicy.runPolicyOnSocket(engine, ssock1);
	RunPolicy.runPolicyOnSocket(engine, ssock2);
	RunPolicy.runPolicyOnTerminal(engine);
	System.exit(0);
    }
};
