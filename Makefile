SUBDIRS = frontend java filterlib javafilterlib haskell ocaml policies tests

nofilter: frontend ocaml policies toplevel
	cd java; mvn package

all : $(SUBDIRS) toplevel
	mvn package

docs :
	make -C frontend docs
	make -C java docs

toplevel :
	pandoc -f markdown README.md > README.html || echo "install pandoc"
	pandoc -f markdown BUILD.md > BUILD.html || echo "install pandoc"
	pandoc -f markdown CHANGES.md > CHANGES.html || echo "install pandoc"

javafilterlib : filterlib
haskell : filterlib
policies : frontend ocaml
tests : java frontend

clean :
	for d in $(SUBDIRS); do make -C $$d clean; done
	-rm *.html
	-rm *~

$(SUBDIRS) :
	$(MAKE) -C $@

.PHONY : $(SUBDIRS) all clean docs toplevel
