{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

module PConflict where

import AST
import Tokens (Pn(..))
import Data.List
import qualified Data.Map as M
import SymbolTable
import qualified Modecheck as MC

data PConflict =
        PConflict
        { eventName :: Ident
        , common :: [Guards] {- Common guards in the path/control flow -}
        , conflicts :: [ConflictDecl (Clause (Pn, IType))] {- Which conflicts it may cause -}
        , div1 :: [Guards]
        , div2 :: [Guards]
                  -- ^ Works backwards, means the first guard encountered is actually at the tail of the list
        , act1 :: Ident
        , act2 :: Ident
        , args1 :: Args (Term (Pn, IType)) -- Arguments of first action
        , args2 :: Args (Term (Pn, IType)) -- Arguments of second action
        }

instance Show PConflict where
        show (PConflict nm cmn conflicts dvg1 dvg2 act1 act2 args1 args2) =
                "eventName : " ++ (getIdent nm) ++ "\n" ++
                "common : " ++ (show cmn) ++ "\n" ++
                "div1 : " ++ (show dvg1) ++ "\n" ++
                "div2 : " ++ (show dvg2) ++ "\n" ++
                "act1 : " ++ getIdent act1 ++ (showargs args1) ++ "\n" ++
                "act2 : " ++ getIdent act2 ++ (showargs args2) ++ "\n" ++
                "conflicts : \n" ++ (concatMap showConflict conflicts) ++ "\n\n"

showConflict (ConflictDecl (nm1,ag1) (nm2,ag2) cl) =
  getIdent nm1 ++ "(" ++ showargs ag1 ++ ") " ++
  getIdent nm2 ++ "(" ++ showargs ag2 ++ ") " ++
  show cl ++ "\n"

showConflict (ForbidDecl (nm1,ag1) cl) =
  getIdent nm1 ++ "(" ++ showargs ag1 ++ ") " ++
  show cl ++ "\n"

data Guards =
          EvGuards Ident (Args (Maybe Var))
        | PGuards Pn (QClause (Pn, IType))
        | NGuards Pn (QClause (Pn, IType))
        | FEGuards Pn (QClause (Pn, IType))
        | DefGuards Pn -- "Default" guard.  This exists only to keep track of the control flow path

instance Show Guards where
        show (EvGuards id args) = "EvGuard : "++ getIdent id ++ show args ++ "\n"
        show (PGuards pn qcl) = "PGuard : " ++ showqcl qcl ++ "\n"
        show (NGuards pn qcl) = "NGuard : " ++ showqcl qcl ++ "\n"
        show (FEGuards pn qcl) = "FEGuard : " ++ showqcl qcl ++ "\n"
        show (DefGuards pn) = "DefGuard \n"


showqcl (QCPred pn nm args) = getIdent nm ++ "(" ++ showargs args ++ ")"
showqcl (QCAnd qcl1 qcl2) = showqcl qcl1 ++ "/\\" ++ showqcl qcl2
showqcl qcl = show qcl

showargs (PositionalArgs l) = concat $ map (\qtm -> show (snd qtm)) l
showargs args = show args

{- Compute the list of PConflict in the file -}
getPConflicts :: SymbolTable -> Prog (Pn, IType) -> [PConflict]
getPConflicts st prog =
        let conflicts = getConflicts' prog in
        let (handlers, procedures) = findInProg prog in
        let handlers' = inlineHandlers st handlers procedures in
        let partitions = partitionHandlers handlers' in
        processHandlers partitions conflicts

{- Gets the list of conflict declaration in the file -}
getConflicts' :: Prog (Pn,IType) -> [ConflictDecl (Clause (Pn,IType))]
getConflicts' [] = []
getConflicts' ((_,_,DConflict d) : xs) = d : getConflicts' xs
getConflicts' ((pn,_,DTable (TableDecl nm targs margs _)) : xs) =
  let modes = extractArgs $ MC.expandNamedModes targs margs
      args1 = map (\ (x, n) -> (pn, Just ("_x" ++ show n))) $ zip modes [0 ..]
      args2 = map (\ (y, n) -> (pn, Just ("_y" ++ show n))) $ zip modes [0 ..]
      args_var1 = map (\ (ty, n) -> TVar (pn, type2itype ty) ("_x" ++ show n)) (zip (extractArgs targs) [(0::Integer) ..])
      args_var2 = map (\ (ty, n) -> TVar (pn, type2itype ty) ("_y" ++ show n)) (zip (extractArgs targs) [(0::Integer) ..])
      eqlist = map fst $ filter eq (zip (zip args_var1 args_var2) modes)
      neqlist = map fst $ filter (not . eq) (zip (zip args_var1 args_var2) modes)
      f = foldr (\ (x,y) c -> Clause pn (CAnd (Clause pn (CEq x y)) c)) (Clause pn CEmpty)
      g = foldr (\ (x,y) c -> Clause pn (COr (Clause pn (CNeq x y)) c)) (Clause pn (CNot (Clause pn CEmpty)))
      cl = (Clause pn (CAnd (f eqlist) (g neqlist)))
-- d' is the conflict declaration for insert/delete
      cl' = (f eqlist)
      d = ConflictDecl (Ident pn (".insert_" ++ getIdent nm), PositionalArgs args1) (Ident pn (".insert_" ++ getIdent nm), PositionalArgs args2) cl
      d' = ConflictDecl (Ident pn (".insert_" ++ getIdent nm), PositionalArgs args1) (Ident pn (".delete_" ++ getIdent nm), PositionalArgs args2) cl'
   in d : d' : getConflicts' xs
   where
     type2itype ty =
       case ty of
         Type _ (TypeName id l) -> IType (TypeName id (map type2itype l))
         Type _ (TypeList ty') -> IType (TypeList (type2itype ty'))
         Type _ (TypeMap ty1 ty2) -> IType (TypeMap (type2itype ty1) (type2itype ty2))
         Type _ (TypeSet ty') -> IType (TypeSet (type2itype ty'))
         Type _ (TypeTuple l) -> IType (TypeTuple (map type2itype l))
         Type _ TypeNumber -> IType TypeNumber
         Type _ TypeString -> IType TypeString
         Type _ (TypeVar v) -> error "table declarations should not have type variables"
     eq x =
       case x of
         (_, ModeIn) -> True
         _ -> False
getConflicts' (_ : xs) = getConflicts' xs

{- Returns  the list of all handlers and all procedures -}
findInProg :: Prog (Pn, IType) -> ([Handler (Pn, IType)], [ProcDefn (Pn, IType)])
findInProg ((pn, _, DHandler (ename, eargs, ebody)):xs) =
        let (a, b) = findInProg xs in
        (( (ename, eargs, ebody):a), b)
findInProg ((pn, _, DProcDefn (x, args, body)):xs) =
        let (a, b) = findInProg xs in
        (a, ( (x, args, body):b))
findInProg (x:xs) = findInProg xs
findInProg [] = ([], [])

{- Partition the handlers list by which event they handle -}
partitionHandlers :: [Handler (Pn, IType)] -> [[Handler (Pn, IType)]]
partitionHandlers l = partitionHandlers' ([], l)
   where
       partitionHandlers' (a, (b@(ename, _, _)):c) =
               let (x, y) = partition (f ename) c in
               partitionHandlers' (((b:x):a), y)
       partitionHandlers' (a, []) = a
       f nm = \ a@(ename, _, _) -> (getIdent nm) == (getIdent ename)

{- Given
                - a list of handlers
                - a list of procedures
         Returns
          - the list of handlers
                        where the procedures
                        are inlined -}
inlineHandlers :: SymbolTable -> [Handler (Pn, IType)] -> [ProcDefn (Pn, IType)] -> [Handler (Pn, IType)]
inlineHandlers st a b = map (inlineHandlers' st b) a
        where
                inlineHandlers' :: SymbolTable -> [ProcDefn (Pn, IType)] -> Handler (Pn, IType) -> Handler (Pn, IType)
                inlineHandlers' st procs (ename, eargs, ebody) =
                        (ename, eargs, inlineBody st procs ebody)
                inlineBody st procs ebody@(HBody pn (HProcedure x args)) =
                        case st_lookup x st of
                                Nothing -> error (unwords [qt x," not in scope"])
                                Just (Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ _))) ->
                                        case find (\ z@(nm, _, _) -> (getIdent nm) == (getIdent x)) procs of
                                          Just y -> inlineBody st procs (inlineCall y pn x args) -- loop ?
                                          Nothing -> error "inlineBody : Procedure called not found"
                                Just (Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl pid _))) -> ebody
                                Just i -> error (unwords ["expected",qt x,"to be a procedure, but it is a",namesort (sym_info i)])
                inlineBody st procs (HBody pn (HSeq b1 b2)) = HBody pn (HSeq (inlineBody st procs b1) (inlineBody st procs b2))
                inlineBody st procs (HBody pn (HForeach qcl b)) = HBody pn (HForeach qcl (inlineBody st procs b))
                inlineBody st procs (HBody pn (HQuery l)) = HBody pn (HQuery (map (inlineQuery procs) l))
                inlineBody st procs ebody = ebody
                inlineQuery procs (pn, QueryBranch qcl b) = (pn, QueryBranch qcl (inlineBody st procs b))
                inlineQuery procs (pn, DefaultBranch b) = (pn, DefaultBranch (inlineBody st procs b))
                inlineCall (pnm, pargs, HBody pn' z) pn nm args =
                        HBody pn' (substArgs (makeMap (extractArgs pargs) (extractArgs args)) z)
                makeMap (Just v:params') (t:actuals') = M.insert v t (makeMap params' actuals')
                makeMap (Nothing:params') (_:actuals') = makeMap params' actuals'
                makeMap [] [] = M.empty
                makeMap _ _ = error "makeMap : impossible"
                substArgs m (HProcedure nm args) = HProcedure nm (mapArgs (substTerm m) args)
                substArgs m (HSeq (HBody pn b1) (HBody pn' b2)) = HSeq (HBody pn (substArgs m b1)) (HBody pn' (substArgs m b2))
                substArgs m (HForeach qcl (HBody pn z)) = HForeach (substQueryHead (substQTerm m) qcl) (HBody pn (substArgs m z))
                substArgs m (HQuery l) = HQuery (map (f m) l)
                substArgs m (HInsert args nm) = HInsert (mapArgs (substTerm m) args) nm
                substArgs m (HDelete args nm) = HDelete (mapArgs (substTerm m) args) nm
                substArgs m b = b
                f m (pn, QueryBranch qcl (HBody pn' b)) = (pn, QueryBranch (substQueryHead (substQTerm m) qcl) (HBody pn' (substArgs m b)))
                f m (pn, DefaultBranch (HBody pn' b)) = (pn, DefaultBranch (HBody pn' (substArgs m b)))
                substTerm m (TTerm a t) = TTerm a (substTerm' (substTerm m) t)
                substTerm m (TVar a v) =
                        case M.lookup v m of
                                Just t -> t
                                Nothing -> TVar a v
                substTerm' st (TFunc x args) = TFunc x (mapArgs st args)
                substTerm' st (TCons t1 t2) = TCons (st t1) (st t2)
                substTerm' st (TList ts) = TList (map st ts)
                substTerm' st (TOp op t1 t2) = TOp op (st t1) (st t2)
                substTerm' st (TNegative t) = TNegative (st t)
                substTerm' _ t = t
                substQTerm m (QBindVar a v) = QBindVar a v
                substQTerm m (QRefVar a v) =
                        case M.lookup v m of
                                Just t -> term2qterm t
                                Nothing -> QRefVar a v
                substQTerm m (QTerm a t) = QTerm a (substTerm' (substQTerm m) t)
                substQueryHead sqt (QCEq pn t1 t2) = QCEq pn (sqt t1) (sqt t2)
                substQueryHead sqt (QCPred pn x args) = QCPred pn x (mapArgs sqt args)
                substQueryHead sqt (QCAnd c1 c2) = QCAnd (substQueryHead sqt c1) (substQueryHead sqt c2)
                substQueryHead sqt (QCNeq pn t1 t2) = QCNeq pn (sqt t1) (sqt t2)
                substQueryHead sqt (QCComp pn op t1 t2) = QCComp pn op (sqt t1) (sqt t2)
                term2qterm (TVar a v) = QRefVar a v
                term2qterm (TTerm a t) = QTerm a (fmap term2qterm t)




{- Given a list of inlined handlers
         grouped by the event they handle.
         Returns a list of potential conflict -}
processHandlers :: [[Handler (Pn, IType)]] -> [ConflictDecl (Clause (Pn, IType))] -> [PConflict]
processHandlers l conflicts = concatMap (processHandlers' conflicts) l

choosePair :: [a] -> [(a,a)]
choosePair [] = []
choosePair (x:xs) = map (\y -> (x,y)) xs ++ choosePair xs

processHandlers'
   :: [ConflictDecl (Clause (Pn, IType))]
   -> [Handler (Pn, IType)]
   -> [PConflict]
processHandlers' conflicts l =
   concatMap (processHandler conflicts) l ++
   do ((ename1,args1,b1),(ename2,args2,b2)) <- choosePair l
      let ps1 = bodyPaths b1 [EvGuards ename1 args1]
      let ps2 = bodyPaths b2 [EvGuards ename2 args2]
      (makePCon conflicts ename1 [] ps1 ps2 ++
       makePCon conflicts ename1 [] ps2 ps1)


{- Find the conflicts within one handler -}
processHandler :: [ConflictDecl (Clause (Pn, IType))] -> Handler (Pn, IType) -> [PConflict]
processHandler conflicts (ename, eargs, body) =
    findBodyConflicts conflicts ename body [EvGuards ename eargs]

-- Use this function to search for conflicts between two actions
findConflicts
    :: [ConflictDecl (Clause (Pn, IType))]
    -> (Ident, Args (Term (Pn, IType)))
    -> (Ident, Args (Term (Pn, IType)))
    -> Maybe [ConflictDecl (Clause (Pn, IType))]
findConflicts conflicts (nm, args) (nm', args') =
   let matchingConflict (ConflictDecl (nm1,_) (nm2,_) _) = nm == nm1 && nm' == nm2
       matchingConflict (ForbidDecl _ _) = False
       conflicts' = filter matchingConflict conflicts
    in if null conflicts'
          then Nothing
          else Just conflicts'

-- Use this function to search for self conflicting actions
findConflicts'
    :: [ConflictDecl (Clause (Pn, IType))]
    -> (Ident, Args (Term (Pn, IType)))
    -> Maybe [ConflictDecl (Clause (Pn, IType))]
findConflicts' conflicts (nm, args) =
   let matchingConflict (ConflictDecl _ _ _) = False
       matchingConflict (ForbidDecl (nm',_) _) = nm == nm'
       conflicts' = filter matchingConflict conflicts
    in if null conflicts'
          then Nothing
          else Just conflicts'



makePCon :: [ConflictDecl (Clause (Pn, IType))]
         -> Ident
         -> [Guards]
         -> [(Ident,Args (Term (Pn, IType)),[Guards])]
         -> [(Ident,Args (Term (Pn, IType)),[Guards])]
         -> [PConflict]
makePCon conflicts ename cmn ps1 ps2 =
   do (nm,args,path) <- ps1
      (nm',args',path') <- ps2
      case findConflicts conflicts (nm,args) (nm',args') of
        Nothing -> []
        Just cs -> return PConflict
                          { eventName = ename
                          , common = cmn
                          , div1 = path
                          , div2 = path'
                          , act1 = nm
                          , act2 = nm'
                          , args1 = args
                          , args2 = args'
                          , conflicts = cs
                          }


findBodyConflicts
   :: [ConflictDecl (Clause (Pn, IType))]
   -> Ident
   -> HBody (Pn,IType)
   -> [Guards]
   -> [PConflict]
findBodyConflicts conflicts ename = aux
 where
   aux ebody common =
     case ebody of
           HBody pn (HProcedure nm args) ->
             case findConflicts' conflicts (nm, args) of
               Nothing -> []
               Just cs -> [PConflict
                           { eventName = ename
                           , common = common
                           , div1 = []
                           , div2 = []
                           , act1 = nm
                           , act2 = nm
                           , args1 = args
                           , args2 = args
                           , conflicts = cs
                           }]
           HBody pn (HSeq bd bd') -> -- does the work of the h function
                         let l  = bodyPaths bd [] in
                         let l' = bodyPaths bd' [] in
                         concat [ makePCon conflicts ename common l l'
                                , makePCon conflicts ename common l' l
                                , aux bd common
                                , aux bd' common
                                ]
           HBody pn (HForeach qcl bd) -> -- h function here same
                    let l = bodyPaths bd [FEGuards pn qcl] in
                    -- Passing into 'foreach' constructs by extending the common path
                    -- is subsumed by two identical divergent paths, so we comment
                    -- out the following line.  An alternate design decision is
                    -- to arrange it so that a divergent control-flow path is never
                    -- paired with itself, and considering the common case instead.
                    -- The soundness of the alternate design is not immediately apparent.
                    -- aux bd ((FEGuards pn qcl):common) ++
                    makePCon conflicts ename common l l
           HBody pn (HQuery br) ->
                  case br of
                     [] -> []
                     ((x@(pn', QueryBranch qcl body)):xs) ->
                                (aux body ((PGuards pn' qcl):common)) ++
                                (aux (HBody pn (HQuery xs)) ((NGuards pn' qcl):common))
                     ((x@(pn', DefaultBranch body)):xs) ->
                                aux body common
                                -- We know that we will never execute anything beyond the
                                -- default branch, thus we don't care about the possible
                                -- conflicts there.
           _ -> []


bodyPaths
  :: HBody (Pn, IType)
  -> [Guards]
  -> [(Ident, Args (Term (Pn, IType)), [Guards])]
bodyPaths bd path =
        case bd of
                HBody pn (HProcedure nm args) -> [(nm, args, path)]
                HBody pn (HSeq bd bd') ->
                        let l = bodyPaths bd path in
                        let l' = bodyPaths bd' path in
                        l ++ l'
                HBody pn (HForeach qcl bd) -> bodyPaths bd ((FEGuards pn qcl):path)
                HBody pn (HQuery br) ->
                        case br of
                                [] -> []
                                ((x@(pn, QueryBranch qcl body)):xs) ->
                                      (bodyPaths body ((PGuards pn qcl):path))
                                   ++ (bodyPaths (HBody pn (HQuery xs)) ((NGuards pn qcl):path))
                                ((x@(pn, DefaultBranch body)):xs) ->
                                        bodyPaths body ((DefGuards pn):path)
                                        -- The actions beyond the default branch can't ever
                                        -- be executed, and thus can't produce conflicts.
                HBody pn (HInsert args nm) -> [(Ident pn (".insert_" ++ getIdent nm), args, path)]
                HBody pn (HDelete args nm) -> [(Ident pn (".delete_" ++ getIdent nm), args, path)]
                _ -> []
