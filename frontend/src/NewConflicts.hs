{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

module NewConflicts where

import Data.List
import qualified Data.Map as M
import Data.Map (Map)
import qualified Text.PrettyPrint as PP

import Tokens (Pn(..), displayPn)
import AST
import SymbolTable
import Compile
import Background
import PConflict
import qualified SMT as S
import Rules
import MinBG

why3pconflicts :: SymbolTable -> Prog (Pn, IType) -> Comp String
why3pconflicts st prog = runM (do
        let pconflicts = getPConflicts st prog
        let presumes = getPresume prog
        let presumesgroup = groupBy (\ (PresumeDecl (nm,_) _) (PresumeDecl (nm',_) _) -> nm == nm') presumes
        presumesprobs <- mapM (presumeDeclsProbs st) $ zip [0..] presumesgroup
        bg <- backgroundProg prog
        problems <- mapM (\ (i, x) -> pconflict2smt st presumes i x) (zip [0..] pconflicts)
        sm <- getStringMap
        let strdecls = map (S.why3cmd . processString) $ M.assocs sm
        let bg' =
             [ PP.text "theory Background"
             , PP.text "  use export suppl.SupplPrelude"
             ]
             ++ map (PP.nest 2) strdecls
             ++ map (PP.nest 2 . S.why3cmd) bg ++
             [ PP.text "end" ]
        let false =
             [ PP.text "theory False"
             , PP.text "  use import Background"
             , PP.text "  goal False : false"
             , PP.text "end" ]
        let prog' = bg' ++ (if containsAL bg then false else []) ++ map S.why3cmd presumesprobs ++ map S.why3cmd problems
        return $ PP.render $ PP.vcat $ prog'
        )
        where
        -- we only need to add to check for inconsistency if the user added an axiom or a lemma
          containsAL (x:xs) =
            case x of
            S.Axiom s _ -> if isPrefixOf "axiom" s then True else containsAL xs
            S.Lemma _ _ -> True
            _ -> containsAL xs
          containsAL [] = False

-- isfalse <-> false has been proved
-- we then look for a set of axioms that is inconsistent
-- cause we know all conflicts will be solved because of the inconsistency anyway
why3pconflicts' :: SymbolTable -> [(Integer, PConflict)] -> Prog (Pn, IType) -> Bool -> Comp String
why3pconflicts' st pconflicts prog isfalse = do
  if isfalse
    then do
      let m = falses prog
      Comp (\cst ->
          let (cst',_,_,_,_,mx) = unM m (cst,M.empty,0,[],[])
          in (cst', mx))
    else do
      let m = mapM (\(i, p) -> why3pconflict st prog i p) pconflicts
      Comp (\cst ->
          let (cst',_,_,_,_,mx) = unM m (cst,M.empty,0,[],[])
          in (cst', Just (concat $ f mx)))
  where
    f (Just x) = x
    f (Nothing) = []

falses :: Prog (Pn, IType) -> M String
falses prog = do
  let bg = filter eq prog
  let axioms = map lemma2axiom $ filter eq' prog
  let axioms' = map (\x -> bg ++ x) $ choose axioms
  let res = mapM f $ zip [0..] (map backgroundProg axioms')
  res' <- res
  return $ concat res'
  where
    eq (_,_,DHandler _) = False
    eq (_,_,DAssert _) = False
    eq _ = True
    eq' (_,_,DAssert (LemmaDecl _)) = True
    eq' (_,_,DAssert (AxiomDecl _)) = True
    eq' _ = False
    lemma2axiom (a,b,DAssert (LemmaDecl cl)) = (a,b,DAssert (AxiomDecl cl))
    lemma2axiom d = d
    f (n, x) = do
      bg <- x
      sm <- getStringMap
      let strdecls = map (S.why3cmd . processString) $ M.assocs sm
      let bg' =
            [ PP.text ("theory False" ++ show n)
            , PP.text "  use export suppl.SupplPrelude"
            ]
            ++ map (PP.nest 2) strdecls
            ++ map (PP.nest 2 . S.why3cmd) bg
            ++ [ PP.text "  goal False : false", PP.text "end\n"]
      return $ PP.render $ PP.vcat $ bg'

why3pconflict :: SymbolTable -> Prog (Pn, IType) -> Integer -> PConflict -> M String
why3pconflict st prog n pconflict = do
  let (mainbg, bgs) = getBackgrounds prog pconflict
  let bg = map backgroundProg (map (\x -> minBG pconflict $ mainbg ++ x) bgs)
  let res = mapM f $ zip (zip bg bgs) [0..]
  res' <- res
  return $ concat res'
  where
    presumes = getPresume prog
    f ((x, rules), i) = do
      bg <- x
      problem <- pconflict2smt st presumes 0 pconflict
      sm <- getStringMap
      let strdecls = map (S.why3cmd . processString) $ M.assocs sm
      let bg' =
            [ PP.text ("theory Conflict" ++ show n ++ "_" ++ show i)
            , PP.text "  use export suppl.SupplPrelude"
            ]
            ++ map (PP.nest 2) strdecls
            ++ map (PP.nest 2 . S.why3cmd) bg
            ++ [PP.text "\n"]
            ++ map g rules
            ++ [PP.text "\n",
            S.why3cmd' problem,
            PP.text "end\n" ]
      return $ PP.render $ PP.vcat $ bg'
    g (pn, _, DRule r@((_, nm, _), _)) = PP.text ("  (* rule " ++ getIdent nm ++ " @ " ++ displayPn pn ++ "*)")
    g _ = error "Rule expected"

{- Returns True iff no pragma conflicts were found
         or they the conflicts were found.
         Returns False and a error message describing which
         conflicts were found but werent given in a pragma
         and which conflicts were given but not found -}
testpconflicts :: SymbolTable -> Prog (Pn, IType) -> (Bool, String)
testpconflicts st prog =
        let pconflicts = getPConflicts st prog in
        let pragmas = pragmapconflict prog in
        let conflicts = nubBy eq $ map f pconflicts in
        let (a, b) = foldl (\ (x, y) z -> let y' = deleteBy eq z y in if length y' == length y then (z:x, y) else (x, y')) ([], conflicts) pragmas in
        case (a,b) of
          ([],[]) -> (True, "")
          ([], l) -> (False, "Potential conflicts not expected, namely : \n" ++
                                concat (map (\ (Ident pn act, Ident pn' act') -> act ++ "@" ++ displayPn pn ++ act' ++ "@" ++ displayPn pn' ++ "\n") l))
          (l, []) -> (False, "Potential conflicts not found, namely : \n" ++
                                concat (map (\ (Ident pn act, Ident pn' act') -> act ++ "@" ++ displayPn pn ++ act' ++ "@" ++ displayPn pn' ++ "\n") l))
          (l, l') -> (False, "Potential conflicts not expected, namely : \n" ++
                                concat (map (\ (Ident pn act, Ident pn' act') -> act ++ "@" ++ displayPn pn ++ act' ++ "@" ++ displayPn pn' ++ "\n") l') ++
                             "Potential conflicts not found, namely : \n" ++
                                concat (map (\ (Ident pn act, Ident pn' act') -> act ++ "@" ++ displayPn pn ++ act' ++ "@" ++ displayPn pn' ++ "\n") l))
        where
          eq (Ident pnx x, Ident pnx' x') (Ident pny y, Ident pny' y') =
            (pnx == pny && x == y && pnx' == pny' && x' == y')
          f (PConflict { act1 = id1, act2 = id2 }) =
            if loc id1 <= loc id2 then (id1, id2) else (id2,id1)

{- Looks for the pragma expect pconflict in the AST-}
pragmapconflict :: Prog (Pn, IType) -> [(Ident, Ident)]
pragmapconflict ((_, BuiltinCode, _):xs) = pragmapconflict xs
pragmapconflict ((Pn f _ _, _,
                 DPragma (GenericPragma (Ident _ "expect")
                           (Just (PTApplied _ (Ident _ "pconflict")
                              (PositionalArgs [(_, PTStringLit _ act1)
                                              , (_, PTNumLit _ l1)
                                              , (_, PTNumLit _ c1)
                                              , (_, PTStringLit _ act2)
                                              , (_, PTNumLit _ l2)
                                              , (_, PTNumLit _ c2)]))))):xs) =
        (Ident (Pn f (read l1) (read c1)) act1, Ident (Pn f (read l2) (read c2)) act2):(pragmapconflict xs)
pragmapconflict (x:xs) = pragmapconflict xs
pragmapconflict [] = []


pconflict2smt :: SymbolTable -> [AssertDecl (Clause (Pn,IType))] -> Integer -> PConflict -> M S.Cmd
pconflict2smt st presumes i pc@(PConflict ename common conflicts@((ConflictDecl _ _ (Clause pn _)):xs) div1 div2 (Ident pn1 nm1) (Ident pn2 nm2) args1 args2) =
     do let desc = (concat $ map (\ x@(ConflictDecl _ _ (Clause pn _)) -> "conflict condition @ " ++ displayPn pn ++ "\n") conflicts) ++
                      "for event : " ++ getIdent ename ++
                      "\naction1 : " ++ nm1 ++ " @ " ++ displayPn pn1 ++
                                                                "\naction2 : " ++ nm2 ++ " @ " ++ displayPn pn2
        let targs = case st_lookup ename st of
                        Nothing -> error (unwords [qt ename," not in scope"])
                        Just (Symbol _ _ _ (EventSymbol (EventDecl _ targs))) -> extractArgs targs
                        Just i -> error (unwords ["expected",qt ename,"to be an event, but it is a",namesort (sym_info i)])
        let presumes' = findPresume presumes ename
        eventParams <- mapM (\ _ -> getFresh) targs
        targs' <- mapM type2smt targs
        let decls = zipWith (\ a t -> S.DeclareFun a [] t) eventParams targs'
        let evdecl = S.DefineFun (S.IdName "event") [] (S.TApp "event" [])
                                 (S.App (S.IdName (getIdent ename)) (map S.EVar eventParams))
        prdecls <- presumeDecls presumes' eventParams
        expr <- guards2smt M.empty eventParams (reverse common) (reverse div1) (reverse div2) args1 args2 conflicts

        return $ (S.SubProblem ("Conflict" ++ show i) ([S.Information desc] ++ decls ++ [evdecl] ++ prdecls ++ [S.Axiom "conflict" expr, S.Goal "impossible" S.False]))

pconflict2smt st presumes i pc@(PConflict ename common conflicts@((ForbidDecl _ (Clause pn _)):xs) div1 div2 (Ident pn1 nm1) (Ident pn2 nm2) args1 args2) =
     do let desc = (concat $ map (\ x@(ForbidDecl _ (Clause pn _)) -> "forbidden action @ " ++ displayPn pn ++ "\n") conflicts) ++
                      "for event : " ++ getIdent ename ++
                      "\naction : " ++ nm1 ++ " @ " ++ displayPn pn1
        let targs = case st_lookup ename st of
                        Nothing -> error (unwords [qt ename," not in scope"])
                        Just (Symbol _ _ _ (EventSymbol (EventDecl _ targs))) -> extractArgs targs
                        Just i -> error (unwords ["expected",qt ename,"to be an event, but it is a",namesort (sym_info i)])
        let presumes' = findPresume presumes ename
        eventParams <- mapM (\ _ -> getFresh) targs
        targs' <- mapM type2smt targs
        let decls = zipWith (\ a t -> S.DeclareFun a [] t) eventParams targs'
        let evdecl = S.DefineFun (S.IdName "event") [] (S.TApp "event" [])
                                 (S.App (S.IdName (getIdent ename)) (map S.EVar eventParams))
        prdecls <- presumeDecls presumes' eventParams
        expr <- guards2smt M.empty eventParams (reverse common) [] [] args1 args2 conflicts

        return $ (S.SubProblem ("Conflict" ++ show i) ([S.Information desc] ++ decls ++ [evdecl] ++ prdecls ++ [S.Axiom "conflict" expr, S.Goal "impossible" S.False]))

pconflict2smt st presumes i pc@(PConflict ename common [] div1 div2 (Ident pn1 nm1) (Ident pn2 nm2) args1 args2) =
     error "No conflict"

guards2smt
   :: Map Var S.Ident
   -> [S.Ident]
   -> [Guards]
   -> [Guards]
   -> [Guards]
   -> Args (Term (Pn, IType))
   -> Args (Term (Pn, IType))
   -> [ConflictDecl (Clause (Pn, IType))]
   -> M (S.Formula)
guards2smt env eventParams (PGuards _ qcl:xs) diverge1 diverge2 args1 args2 conflicts = do
        (env', e) <- qc2smt env qcl
        (vs, bs) <- getBinders
        e' <- guards2smt env' eventParams xs diverge1 diverge2 args1 args2 conflicts
        return (S.exists_ bs (S.and [e, e']))

guards2smt env eventParams (NGuards _ qcl:xs) diverge1 diverge2 args1 args2 conflicts = do
        (env', e) <- qc2smt env qcl
        (vs, bs) <- getBinders
        e' <- guards2smt env' eventParams xs diverge1 diverge2 args1 args2 conflicts
        return (S.and [S.Not (S.exists_ bs e), e'])

guards2smt env eventParams (FEGuards _ qcl:xs) diverge1 diverge2 args1 args2 conflicts = do
        (env', e) <- qc2smt env qcl
        (vs, bs) <- getBinders
        e' <- guards2smt env' eventParams xs diverge1 diverge2 args1 args2 conflicts
        return (S.exists_ bs (S.and [e, e']))

guards2smt env eventParams (DefGuards _ :xs) diverge1 diverge2 args1 args2 conflicts =
        guards2smt env eventParams xs diverge1 diverge2 args1 args2 conflicts

guards2smt env eventParams (EvGuards ident eargs : xs) diverge1 diverge2 args1 args2 conflicts =
    let env' = extend env (extractArgs eargs) eventParams
     in guards2smt env' eventParams xs diverge1 diverge2 args1 args2 conflicts

guards2smt env eventParams [] diverge1 diverge2 args1 args2 conflicts = do
      (expr1,action_args1) <- diverge2smt eventParams env diverge1 args1
      (expr2,action_args2) <- diverge2smt eventParams env diverge2 args2
      let pnty1 = map getAnnot $ extractArgs args1
      let pnty2 = map getAnnot $ extractArgs args2
      conflictFormula <- conflicts2smt pnty1 action_args1 pnty2 action_args2 conflicts
      return (expr1 $ expr2 $ conflictFormula)


conflicts2smt :: [(Pn,IType)] -> [S.Expr] -> [(Pn,IType)] -> [S.Expr] -> [ConflictDecl (Clause (Pn, IType))] -> M S.Formula
conflicts2smt pnty1 args1 pnty2 args2 conflicts =
    fmap S.or $ mapM (conflict2smt pnty1 args1 pnty2 args2) conflicts


formalsEnv :: [(Pn,IType)] -> [Maybe Var] -> [S.Expr] -> Map Var S.Ident -> M ([S.Formula],Map Var S.Ident)
formalsEnv [] [] [] env = return ([],env)
formalsEnv ((pn,ty):pnty) (Nothing : vs) (x:xs) env = formalsEnv pnty vs xs env
formalsEnv ((pn,ty):pnty) (Just v  : vs) (x:xs) env =
    do (v',env') <- case M.lookup v env of
                     Just v' -> return (v',env)
                     Nothing -> do
                        v' <- getFresh
                        ty' <- gtype2smt ty
                        addBinder (Just v) (S.Bind v' ty')
                        let env' = M.insert v v' env
                        return (v',env')
       (cs,env'') <- formalsEnv pnty vs xs env'
       let c = S.Eq (S.EVar v') x
       return (c:cs, env'')

formalsEnv a b c _ = error $ "impossible! action/conflict mismatch!"


conflict2smt :: [(Pn,IType)] -> [S.Expr] -> [(Pn,IType)] -> [S.Expr] -> ConflictDecl (Clause (Pn, IType)) -> M S.Formula
conflict2smt pnty1 args1 pnty2 args2 (ConflictDecl (_,formals1) (_,formals2) cl) =
   do (args_cs,env) <- formalsEnv (pnty1 ++ pnty2) (extractArgs formals1 ++ extractArgs formals2) (args1 ++ args2) M.empty
      cl' <- clause2smt env cl
      (_,bs) <- getBinders
      return (S.exists_ bs $ S.and (args_cs ++ [cl']))

conflict2smt pnty1 args1 pnty2 args2 (ForbidDecl (_,formals1) cl) =
  do (args_cs,env) <- formalsEnv (pnty1) (extractArgs formals1) (args1) M.empty
     cl' <- clause2smt env cl
     (_,bs) <- getBinders
     return (S.exists_ bs $ S.and (args_cs ++ [cl']))



diverge2smt :: [S.Ident] -> Map Var S.Ident -> [Guards] -> Args (Term (Pn, IType)) -> M (S.Formula -> S.Formula, [S.Expr])
diverge2smt eventParams env [] args = do
        args' <- mapM (term2smt env) (extractArgs args)
        return (\k -> k, args')
diverge2smt eventParams env (PGuards _ qcl:xs) args = do
        (env', e) <- qc2smt env qcl
        (vs, bs) <- getBinders
        (e', ts) <- diverge2smt eventParams env' xs args
        return (\k -> S.exists_ bs (S.and [e, e' k]), ts)
diverge2smt eventParams env (NGuards _ qcl:xs) args = do
        (env', e) <- qc2smt env qcl
        (vs, bs) <- getBinders
        (e', ts) <- diverge2smt eventParams env' xs args
        return (\k -> S.and [S.Not (S.exists_ bs e), e' k], ts)
diverge2smt eventParams env (DefGuards _ : xs) args =
        diverge2smt eventParams env xs args
diverge2smt eventParams env (EvGuards ident eargs : xs) args =
    let env' = extend env (extractArgs eargs) eventParams
     in diverge2smt eventParams env' xs args
diverge2smt eventParams env (FEGuards _ qcl:xs) args = do
        (env', e) <- qc2smt env qcl
        (vs, bs) <- getBinders
        (e', ts) <- diverge2smt eventParams env' xs args
        return (\k -> S.exists_ bs (S.and [e, e' k]), ts)

getPresume :: Prog (Pn, IType) -> [AssertDecl (Clause (Pn,IType))]
getPresume ((_, _, DAssert assert@(PresumeDecl _ _)):xs) = assert:(getPresume xs)
getPresume (x:xs) = getPresume xs
getPresume [] = []

findPresume :: [AssertDecl (Clause (Pn,IType))] -> Ident -> [AssertDecl (Clause (Pn,IType))]
findPresume ((p@(PresumeDecl (nm,_) _)):xs) enm =
  if nm == enm then
    p:(findPresume xs enm)
  else
    findPresume xs enm
findPresume [] _ = []
findPresume (x:xs) enm = error "Expected PresumeDecl"

presumeDecls :: [AssertDecl (Clause (Pn,IType))] -> [S.Ident] -> M [S.Cmd]
presumeDecls l args = do
  mapM f $ zip [0..] l
  where
    f (n, PresumeDecl (nm, args') cl) = do
      let env = makeEnv args args'
      x <- clause2smt env cl
      return $ S.Axiom ("presume" ++ show n) x
    f _ = error "expected PresumeDecl"
    makeEnv args args' =
      foldr g M.empty (zip (extractArgs args') args)
    g (Just v, v') = M.insert v v'
    g (Nothing, _) = id

presumeDeclsProbs :: SymbolTable -> (Int, [AssertDecl (Clause (Pn,IType))]) -> M S.Cmd
presumeDeclsProbs st (n,l) = do
  let (PresumeDecl (ename,_) _):xs = l
  let targs = case st_lookup ename st of
                  Nothing -> error (unwords [qt ename," not in scope"])
                  Just (Symbol _ _ _ (EventSymbol (EventDecl _ targs))) -> extractArgs targs
                  Just i -> error (unwords ["expected",qt ename,"to be an event, but it is a",namesort (sym_info i)])
  args <- mapM (\ _ -> getFresh) targs
  targs' <- mapM type2smt targs
  let decls = zipWith (\ a t -> S.DeclareFun a [] t) args targs'
  axioms <- presumeDecls l args
  return $ S.SubProblem ("Presume" ++ show n) (decls ++ axioms ++ [S.Goal "impossible" S.False])

choose :: [a] -> [[a]]
choose [] = [[]]
choose (x:xs) = (map (\l -> x:l) (choose xs)) ++ (choose xs)
