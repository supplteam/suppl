{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module represents the \"code generation\" phase of the compiler.
--   As we are targeting Prolog, a high-level language, this pass is fairly
--   straightforward.
module EmitIR 
( emitProg
) where

import Data.List ( nub, sortBy )
import Data.Ratio
import qualified Data.Map as M
import Data.Map ( Map )
import Control.Monad
import Data.Maybe
import Data.Either

import Tokens (Pn(..))
import AST (Mode(..), Ident(..), Var(..), getIdent, NumericLiteral(..), showRational )
import SupplIR

import SymbolTable
import qualified Prolog as P
import Compile

type Emit a = Comp a

emitFreshVar :: Emit Int
emitFreshVar = compFreshVar

getST :: Emit SymbolTable
getST = compSymbolTable

-- | Transform a program in SupplIR into Prolog abstract syntax.
emitProg :: [Decl] -> Comp [P.Prolog]
emitProg ds = do
   ds' <- mapM emitDecl ds
   let ds'' = map snd $ sortBy (\ (n,_) (m,_) -> compare n m) $ ds'
   return ds''

emitDecl :: Decl -> Emit (String,P.Prolog)
emitDecl (DRule r) = emitRuleDefn r
emitDecl (DHandler h) = emitHandleDefn h
emitDecl (DProcDefn p) = emitProcDefn p
emitDecl (DRegressionQuery pn n b) = emitRegressionQuery pn n b
emitDecl (DRegressionHandle pn bs ev acts) = emitRegressionHandle pn bs ev acts
emitDecl (DEmit s) = return ("", P.Emit s)

emitRuleDefn :: RuleDefn -> Emit (String, P.Prolog)
emitRuleDefn (ident,mds,body) = do
   let body' = emitPredBodies body
   let ident' = (getIdent ident ++ modeSuffix mds)
   return ( ident'
          , P.Rule 
             (P.Application ident'
                            (map (P.Var . ("Arg"++) . show . snd)
                                 (zip mds [0..])))
             body'
          )

emitRegressionHandle
   :: Pn
   -> [PredBody]
   -> (Ident,[RValue]) 
   -> [(Ident,[RValue])] 
   -> Emit (String, P.Prolog)
emitRegressionHandle (Pn file ln col) bs ev acts = do
   let bs' = emitPredBodies bs
   ev' <- emitBasic ev
   acts' <- mapM emitBasic acts
   let loc = P.Application "location"
               [ P.StringLit file, P.Number (show ln), P.Number (show col) ]
   let run = P.Application "run_handler_regression" [ loc, ev', P.List acts' ]
   return ("setup_regression", P.Rule (P.Atom "setup_regression") (bs'++[run]) )

emitBasic :: (Ident,[RValue]) -> Emit P.Prolog
emitBasic (x,args) = do
   let args' = map emitRValue args
   return (P.Application (getIdent x) args')


emitRegressionQuery :: Pn -> String -> [PredBody] -> Emit (String, P.Prolog)
emitRegressionQuery (Pn file ln col) n [b@(Instr (CallPred md ident args))] = do
   let b' = emitPredBody b
   let loc = P.Application "location"
               [ P.StringLit file, P.Number (show ln), P.Number (show col) ]
   let dir = P.Application "run_query_regression" [ P.Number n, loc, b' ]
   return ( "setup_regression", P.Rule (P.Atom "setup_regression") [dir] )

emitRegressionQuery (Pn file ln col) n [b@(Instr (QueryTable md ident args))] = do
   let b' = emitPredBody b
   let loc = P.Application "location"
               [ P.StringLit file, P.Number (show ln), P.Number (show col) ]
   let dir = P.Application "run_query_regression" [ P.Number n, loc, b' ]
   return ( "setup_regression", P.Rule (P.Atom "setup_regression") [dir] )

emitRegressionQuery _ _ _ = fail "unlifted regression query"



emitHandleDefn :: HandleDefn -> Emit (String, P.Prolog)
emitHandleDefn (ident, args, body) = do
   eff <- emitFreshVar >>= return . ("EFF"++) . show
   body' <- emitHandleBodies eff (P.List []) body
   return ( "handle"
          , P.Rule 
             (P.Application "handle"
               [ P.Var eff
               , P.Application (getIdent ident)
                   (map (P.Var . ("Arg"++) . show) [0..args-1])
               ])
             body'
          )

emitProcDefn :: ProcDefn -> Emit (String, P.Prolog)
emitProcDefn (ident,args,body) = do
   eff <- emitFreshVar >>= return . ("EFF"++) . show
   effend <- emitFreshVar >>= return . ("EFF"++) . show
   body' <- emitHandleBodies eff (P.Var effend) body
   return ( getIdent ident
          , P.Rule
              (P.Application (getIdent ident)
                 ([ P.Var eff
                  , P.Var effend
                  ] ++ map (P.Var . ("Arg"++) . show) [0..args-1])
              )
              body' 
          )

emitLValue :: LValue -> P.Prolog
emitLValue (LArg i) = P.Var ("Arg"++show i)
emitLValue (LVar n) = P.Var ("Var"++n)
emitLValue Ignore  = P.Underscore
emitLValue (LCons h t) = P.Cons (emitLValue h) (emitLValue t)
emitLValue (LList xs) = P.List (map emitLValue xs)
emitLValue (LTuple xs) = P.Tuple (map emitLValue xs)
emitLValue (LData ident xs) = P.Application (getIdent ident) (map emitLValue xs)
emitLValue (LRVal _ r) = emitRValue r

emitRValue :: RValue -> P.Prolog
emitRValue (RArg i) = P.Var ("Arg"++show i)
emitRValue (RVar n) = P.Var ("Var"++n)
emitRValue (RCons h t) = P.Cons (emitRValue h) (emitRValue t)
emitRValue (RList xs) = P.List (map emitRValue xs)
emitRValue (RTuple xs) = P.Tuple (map emitRValue xs)
emitRValue (RData id xs) = P.Application (getIdent id) (map emitRValue xs)
emitRValue (RStringLit s) = P.StringLit s
emitRValue (REval e) = error "Evaluable inside RValue in emitRValue!"

emitNumericLiteral:: NumericLiteral -> String
emitNumericLiteral (LitInteger n) = show n
emitNumericLiteral (LitRational r) = showRational r


emitEvaluable :: Evaluable -> P.Prolog
emitEvaluable (EVal r) = emitRValue r

emitEvaluable (ENeg x) = P.Negative (emitEvaluable x)

emitEvaluable (EOp op x y) =
   P.BinOp (emitOp op) (emitEvaluable x) (emitEvaluable y)

emitEvaluable (Funcall ident evs) =
   P.Application (getIdent ident) (map emitEvaluable evs)

emitEvaluable (EVar v) = P.Var ("Var"++v)
emitEvaluable (EStringLit s) = P.StringLit s
emitEvaluable (ENumLit n) = P.Number (emitNumericLiteral n)


emitOp :: OP -> String
emitOp OpStrConcat = error "string concatenation must be removed prior to IR emit"
emitOp OpListAppend = error "list append must be removed prior to IR emit"
emitOp OpPlus = "+"
emitOp OpMinus = "-"
emitOp OpTimes = "*"
emitOp OpDiv = "/"


emitPredBodies :: [PredBody] -> [P.Prolog]
emitPredBodies xs = map emitPredBody xs

emitPredBody :: PredBody -> P.Prolog
emitPredBody (Instr i) = emitPredInstr i
emitPredBody (Disj xss) =
   let xss' = map emitPredBodies xss
    in P.Disjunction (map P.Conjunction xss')
emitPredBody (Not xs) =
   let xs' = emitPredBodies xs
    in P.Not (P.Conjunction xs')


emitPredInstrs :: [PredInstr] -> P.Prolog
emitPredInstrs xs = P.Conjunction (map emitPredInstr xs)

emitPredInstr :: PredInstr -> P.Prolog

emitPredInstr (CallPred mds id args) = 
   P.Application (getIdent id++modeSuffix mds) 
                 (map (either emitLValue emitRValue) args)

emitPredInstr (QueryTable mds id args) =
   -- FIXME? table queries currently do not make use of mode information
   P.Application (getIdent id)
                 (map (either emitLValue emitRValue) args)

emitPredInstr Fail = P.Atom "false"

emitPredInstr (Match l1 l2) = 
   P.Equal (emitLValue l1) (emitRValue l2)

emitPredInstr (Compare _ Equal l1 l2) =
   P.Equal (emitRValue l1) (emitRValue l2)

emitPredInstr (Compare _ NotEqual l1 l2) =
   P.Not (P.Equal (emitRValue l1) (emitRValue l2))

emitPredInstr (Compare TypeNumber LessThan l1 l2) =
   P.BinOp "<" (emitRValue l1) (emitRValue l2)

emitPredInstr (Compare TypeNumber LessEqual l1 l2) =
   P.BinOp "=<" (emitRValue l1) (emitRValue l2)

emitPredInstr (Compare t LessThan _ _) =
   error "impossible: comparison at non-numeric types"

emitPredInstr (Compare t LessEqual _ _) =
   error "impossible: comparison at non-numeric types"

emitPredInstr (Eval v e) = do
   P.Is (P.Var ("Var"++v)) (emitEvaluable e)

emitPredInstr (Findall v@(RVar _) [Instr (CallPred mds id args)] l) =
   P.Conjunction
     [ P.Application "findall"
             [ emitRValue v
             , P.Application
                     (getIdent id++modeSuffix mds) 
                     (map (either emitLValue emitRValue) args)
             , P.Var ("Tmp"++l)
             ]
     , P.Application "set_elements_io"
             [ P.Var ("Tmp"++l)
             , P.Var ("Var"++l)
             ]
     ]

emitPredInstr p@(Findall _ _ _) =
   error $ "improper findall in EmitIR: "++show p


modeSuffix :: [Mode] -> String
modeSuffix [] = ""
modeSuffix mds = "_"++map modeChar mds

modeChar :: Mode -> Char
modeChar ModeIn     = 'i'
modeChar ModeOut    = 'o'
modeChar ModeIgnore = 'x'


emitHandleBodies :: Var -> P.Prolog -> [HandleBody] -> Emit [P.Prolog]
emitHandleBodies eff effend [] = return [P.Equal (P.Var eff) effend]
emitHandleBodies eff effend (HInstr b:bs) = do
   (eff', b') <- emitHandleInstr eff b
   bs' <- emitHandleBodies eff' effend bs
   return (b' ++ bs')
emitHandleBodies eff effend (HBranch _ : bs) 
   = fail "internal error: branch construct at emit phase"

emitHandleBodies eff effend (HForeach [] [HInstr (HCallProc ident args)] : bs) = do
   eff1 <- emitFreshVar >>= return . ("EFF"++) . show
   eff2 <- emitFreshVar >>= return . ("EFF"++) . show
   eff3 <- emitFreshVar >>= return . ("EFF"++) . show
   let b1 = P.Application "findall"
              [ P.Var eff1
              , P.Application (getIdent ident) 
                   ([P.Var eff1, P.List []] ++ map emitRValue args)
              , P.Var eff2
              ]
   let b2 = P.Application "concat_onto" [P.Var eff2, P.Var eff3, P.Var eff]
   bs' <- emitHandleBodies eff3 effend bs
   return (b1:b2:bs')

emitHandleBodies eff effend (x@(HForeach _ _) : _) =
   fail $ "improper foreach in EmitIR: "++show x



emitHandleInstr :: Var -> HandleInstr -> Emit (Var, [P.Prolog])
emitHandleInstr eff (HPred body) = do
   let body' = emitPredBodies body
   return (eff, body')

emitHandleInstr eff (HCallProc ident args) = do
   eff' <- emitFreshVar >>= return . ("EFF"++) . show
   return ( eff'
          , [P.Application "once"
              [P.Application (getIdent ident)
                ([P.Var eff, P.Var eff'] ++ map emitRValue args)]]
          )

emitHandleInstr eff (HAction ident args) = do
   eff' <- emitFreshVar >>= return . ("EFF"++) . show
   let act = P.Application "action" [P.Application (getIdent ident) (map emitRValue args)]
   return ( eff'
          , [P.Equal (P.Var eff) (P.Cons act (P.Var eff'))]
          )

emitHandleInstr eff (HInsert ident args) = do
   eff' <- emitFreshVar >>= return . ("EFF"++) . show
   let act = P.Application "insertIntoTable" 
                 [P.Atom (getIdent ident), P.List (map emitRValue args)]
   return ( eff'
          , [P.Equal (P.Var eff) (P.Cons act (P.Var eff'))]
          )

emitHandleInstr eff (HDelete ident args) = do
   eff' <- emitFreshVar >>= return . ("EFF"++) . show
   let act = P.Application "deleteFromTable" 
                 [P.Atom (getIdent ident), P.List (map (maybe P.Underscore emitRValue) args)]
   return ( eff'
          , [P.Equal (P.Var eff) (P.Cons act (P.Var eff'))]
          )

