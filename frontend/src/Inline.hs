{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

module Inline where

import Data.List
import qualified Data.Map as M
import Data.Map (Map)

import AST
import SymbolTable
import Tokens( Pn(..), Located(..) )
import PConflict

{- Main function -}
inlineProg :: SymbolTable -> Prog (Pn, IType) -> Prog (Pn, IType)
inlineProg st prog =
        let rules = rulesProg prog in
        (map (inlineDecl rules) prog) ++ (createBackground st prog)

{- Modify handlers to inline the rules -}
inlineDecl :: Map Ident [Rule (Pn, IType)]
           -> (Pn, CodeOrigin, Decl (Pn, IType))
           -> (Pn, CodeOrigin, Decl (Pn, IType))
inlineDecl rules (pn, c, DHandler (ev, args, body)) = (pn, c, DHandler (ev, args, inlineBody rules body))
inlineDecl _ d = d

inlineBody :: Map Ident [Rule (Pn, IType)]
           -> HBody (Pn, IType)
           -> HBody (Pn, IType)
inlineBody rules (HBody pn (HSeq x y)) =
        (HBody pn (HSeq (inlineBody rules x) (inlineBody rules y)))

inlineBody rules (HBody pn (HQuery xs)) =
        (HBody pn (HQuery (inlineQuery rules xs)))

inlineBody _ bd = bd

inlineQuery :: Map Ident [Rule (Pn, IType)]
            -> [(Pn, QBranch (Pn, IType))]
            -> [(Pn, QBranch (Pn, IType))]
inlineQuery rules ((x@(pn, QueryBranch qcl body)):xs) =
        (inlineQuery' qcl (inlineBody rules body)):(inlineQuery rules xs)
        where
                inlineQuery' qcl@(QCPred pn nm args) body =
                        (pn, QueryBranch qcl (HBody pn (HQuery ll)))
                        where ll = (map (\ n -> (pn, QueryBranch (QCPred pn (Ident pn ((getIdent nm) ++ "rule" ++ show n)) (q args)) body)) [1, 2..length $ z]) ++ [(pn, DefaultBranch body)]
                                where
                                        q (PositionalArgs l) = PositionalArgs (map (\(pn, x) -> (pn, forceref x)) l)
                                        q (NamedArgs l) = NamedArgs (map (\(pn, s, x) -> (pn, s, forceref x)) l)
                                        z = case M.lookup nm rules of
                                                Nothing -> [] -- error ("looking for" ++ (getIdent nm))
                                                Just y -> y
                inlineQuery' qcl@(QCAnd qcl1 qcl2) body =
                        head $ inlineQuery rules [(pn, QueryBranch qcl1 (HBody pn (HQuery [(pn, QueryBranch qcl2 body)])))]
                inlineQuery' qcl body = (pn, QueryBranch qcl body)

inlineQuery rules (x:xs) =
        x:(inlineQuery rules xs)
inlineQuery _ [] = []

{- Given :
     - a list of PConflict
     - the AST
     - the symboltable
   Returns :
     - the new background
     - the list of handlers concerned
       by each PConflict where rules are
       inlined and irrelevant control path
       flows are replace by 'skip'
-}
inlineProgPConflict :: SymbolTable -> [PConflict] -> Prog (Pn, IType) -> (Prog (Pn, IType), [Prog (Pn, IType)])
inlineProgPConflict st pconflicts prog =
  let rules = rulesProg prog
      (handlers, background) = partition eq prog
   in (background ++ createBackground st prog, map (\pconflict -> concat $ map (inlineDeclPConflict rules pconflict) prog) pconflicts)
   where
     eq (_, _, DHandler _) = True
     eq _ = False

inlineDeclPConflict :: Map Ident [Rule (Pn, IType)]
                    -> PConflict
                    -> (Pn, CodeOrigin, Decl (Pn, IType))
                    -> [(Pn, CodeOrigin, Decl (Pn, IType))]
inlineDeclPConflict rules (PConflict enm [] _ d1 d2 _ _ _ _) d@(pnn, c, DHandler (Ident pn'' nm, args, body)) =
  let EvGuards (Ident pn _) _ : xs = reverse d1 -- non exhaustive on purpose
      EvGuards (Ident pn' _) _ : xs' = reverse d2
   in if pn == pn'' then [(pnn, c, DHandler(Ident pn'' nm, args, inlineBodyPConflict rules xs [] [] body))] else
      if pn' == pn'' then [(pnn, c, DHandler(Ident pn'' nm, args, inlineBodyPConflict rules xs' [] [] body))] else
      []

inlineDeclPConflict rules (PConflict enm cmn _ d1 d2 _ _ _ _) d@(pnn, c, DHandler (Ident pn' nm, args, body)) =
  let EvGuards (Ident pn _) _ : xs = reverse cmn in -- non exhaustive on purpose
  if pn == pn' then [(pnn, c, DHandler (Ident pn' nm, args, inlineBodyPConflict rules xs (reverse d1) (reverse d2) body))] else []

inlineDeclPConflict _ _ d =
  [d]

inlineBodyPConflict :: Map Ident [Rule (Pn, IType)]
                    -> [Guards]
                    -> [Guards]
                    -> [Guards]
                    -> HBody (Pn, IType)
                    -> HBody (Pn, IType)
inlineBodyPConflict rules ((EvGuards pn qcl):xs) d1 d2 bd =
  error "There shouldn't be an EvGuards here"

inlineBodyPConflict rules cmn d1 d2 (HBody pn (HQuery qbrs)) =
  HBody pn (HQuery (inlineQueryPConflict rules cmn d1 d2 qbrs))

inlineBodyPConflict rules [] [] [] bd =
  bd

inlineBodyPConflict rules [] d1 d2 (HBody pn (HSeq bd1 bd2)) =
  HBody pn (HSeq (inlineBodyPConflict rules d1 [] [] bd1) (inlineBodyPConflict rules d2 [] [] bd2))

inlineBodyPConflict rules [] ((FEGuards pn1 qc1l):xs) ((FEGuards pn2 qcl2):ys) (HBody pn (HForeach qcl bd)) =
  if pn1 == pn2 && pn1 == pn
    then
      let (cmn, d1, d2) = regroup xs ys in
      HBody pn (HForeach qcl (inlineBodyPConflict rules cmn d1 d2 bd))
    else
      HBody pn HSkip
  where
    regroup [] l' = ([], [], l')
    regroup l [] = ([], l, [])
    regroup (x:xs) (y: ys) =
      if eq x y then
        let (cmn, d1, d2) = regroup xs ys in
        (x:cmn, d1, d2)
      else
        ([], (x:xs), (y: ys))
    eq (PGuards pn _) (PGuards pn' _) = pn == pn'
    eq (NGuards pn _) (NGuards pn' _) = pn == pn'
    eq (DefGuards pn) (DefGuards pn') = pn == pn'
    eq (FEGuards pn _ ) (FEGuards pn' _) = pn == pn'
    eq _ _ = False

inlineBodyPConflict rules [] d1 d2 bd =
  bd -- HBody (Pn "" 0 0) HSkip

inlineBodyPConflict rules cmn d1 d2 (HBody pn' (HSeq bd1 bd2)) =
  HBody pn' (HSeq (inlineBodyPConflict rules cmn d1 d2 bd1) (inlineBodyPConflict rules cmn d1 d2 bd2))

inlineBodyPConflict rules ((FEGuards pn qcl):xs) d1 d2 bd@(HBody pn' (HForeach qcl' bd')) =
  if pn == pn'
    then
      HBody pn' (HForeach qcl' (inlineBodyPConflict rules xs d1 d2 bd'))
    else
      HBody pn HSkip

inlineBodyPConflict rules ((PGuards pn qcl):xs) d1 d2 bd =
  HBody pn HSkip

inlineBodyPConflict rules ((NGuards pn qcl):xs) d1 d2 bd =
  HBody pn HSkip

inlineBodyPConflict rules ((FEGuards pn qcl):xs) d1 d2 bd =
  HBody pn HSkip

inlineBodyPConflict rules ((DefGuards pn):xs) d1 d2 bd =
  HBody pn HSkip

inlineQueryPConflict :: Map Ident [Rule (Pn, IType)]
                     -> [Guards]
                     -> [Guards]
                     -> [Guards]
                     -> [(Pn, QBranch (Pn, IType))]
                     -> [(Pn, QBranch (Pn, IType))]
inlineQueryPConflict rules ((PGuards pn qcl):xs) d1 d2 qbrs@((pn'', QueryBranch qcl' bd):xss) =
  if pn == pn''
  then
    (inlineQueryPConflict' qcl' (inlineBodyPConflict rules xs d1 d2 bd)):(allskip xss)
  else qbrs -- wrong query
  where
    inlineQueryPConflict' qcl@(QCPred pn nm args) body =
      (pn, QueryBranch qcl (HBody pn (HQuery ll)))
      where
        ll = (map (\ n -> (pn, QueryBranch (QCPred pn (Ident pn ((getIdent nm) ++ "rule" ++ show n)) (q args)) body)) [1, 2..length $ z]) ++ [(pn, DefaultBranch body)]
        q (PositionalArgs l) = PositionalArgs (map (\(pn, x) -> (pn, forceref x)) l)
        q (NamedArgs l) = NamedArgs (map (\(pn, s, x) -> (pn, s, forceref x)) l)
        z = case M.lookup nm rules of
          Nothing -> [] -- error ("looking for" ++ (getIdent nm))
          Just y -> y
    inlineQueryPConflict' qcl body = (pn, QueryBranch qcl body)
    allskip ((pn, QueryBranch qcl bd):xs) =
      (pn, QueryBranch qcl (HBody pn HSkip)) : (allskip xs)
    allskip ((pn, DefaultBranch bd):xs) =
      [(pn, DefaultBranch (HBody pn HSkip))]
    allskip [] = []

inlineQueryPConflict rules ((NGuards pn qcl):xs) d1 d2 qbrs@((pn'', QueryBranch qcl' bd):xss) =
  if pn == pn''
  then
    (pn'', QueryBranch qcl' (HBody pn HSkip)) : (inlineQueryPConflict rules xs d1 d2 xss)
  else qbrs -- wrong query

inlineQueryPConflict rules ((DefGuards pn):xs) d1 d2 qbrs@((pn'', DefaultBranch bd):xss) =
  if pn == pn''
  then
    [(pn'', DefaultBranch (inlineBodyPConflict rules xs d1 d2 bd))]
  else qbrs -- wrong query

inlineQueryPConflict _ _ _ _ qbrs =
  qbrs

{- Change binding variables to RefVariables
         used when calling the new predicate/rules
         query
         | P(?X) ...
         becomes
         query
         | P(?X) =>
           query
                 | P1(X) => ...
-}
forceref :: QTerm (Pn, IType) -> QTerm (Pn, IType)
forceref (QBindVar a var) = QRefVar a var
forceref q = q


{- Given the symbol table,
         the program AST,
         gives back the necessary background
         for the inlining -}
createBackground :: SymbolTable -> Prog (Pn, IType) -> Prog (Pn, IType)
createBackground st prog =
        let rules = rulesProg prog in
        (concat $ map (createPred st rules) $ M.keys rules) ++ createRules rules

{- Find all the rule declarations from the program. -}
rulesProg :: Prog (Pn, IType) -> Map Ident [Rule (Pn, IType)]
rulesProg prog = foldl rulesDecl M.empty prog

rulesDecl :: Map Ident [Rule (Pn, IType)]
          -> (Pn, CodeOrigin, Decl (Pn, IType))
          -> Map Ident [Rule (Pn, IType)]
rulesDecl rules (pn, _, DRule r@((_, nm, _), _)) =
        let res = case containsFA r of
                True -> []
                False -> [r] in
        M.insertWith (\x y -> x ++ y) nm res rules

rulesDecl rules _ = rules

{- Create all the new rules for the new predicates
to be added to the program AST created for inlining -}
createRules :: Map Ident [Rule (Pn, IType)]
            -> [(Pn, CodeOrigin, Decl (Pn, IType))]
createRules rules =
        concat $ map (createRules' rules) $ M.keys rules

createRules' :: Map Ident [Rule (Pn, IType)]
             -> Ident
             -> [(Pn, CodeOrigin, Decl (Pn, IType))]
createRules' rules k =
        map (\ r@(((pn, nm, args), cl), n) -> (pn, UserCode, DRule ((pn, Ident pn ((getIdent nm)++"rule"++show n),args), cl))) $ zip (rules M.! k) [1, 2..]

{- Create the predicate declarations for the new predicates -}
createPred :: SymbolTable
           -> Map Ident [Rule (Pn, IType)]
           -> Ident
           -> [(Pn, CodeOrigin, Decl (Pn, IType))]
createPred st rules k =
        let pn = Pn "" 0 0 in
        let args = case st_lookup k st of
                                Nothing -> error "expected Predicate symbol, but got nothing."
                                Just s  ->
                                        case sym_info s of
                                        PredSymbol (PIDecl (PredDecl nm ar)) l -> (forceIn ar)
                                        _ -> error "expected Predicate symbol."
                                in
                                map (\n -> (pn, UserCode, DPred(PredDecl (Ident pn (getIdent k++"rule"++show n)) args))) [1, 2..length (rules M.! k)]

{- Force ModeIn for args -}
forceIn :: Args (a, Maybe Mode) -> Args (a, Maybe Mode)
forceIn (PositionalArgs xs) = PositionalArgs (map (\x@(pn, (a, mode)) -> (pn, (a, Just ModeIn))) xs)
forceIn (NamedArgs xs) = NamedArgs (map (\x@(pn, nm, (a, mode)) -> (pn, nm, (a, Just ModeIn))) xs)

{- Predicate indicating if the rule contains a findall -}
containsFA :: Rule (Pn, IType) -> Bool
containsFA r@(_, cl) =
        containsFA' cl
        where
                containsFA' :: Clause (Pn, IType) -> Bool
                containsFA' (Clause _ (CAnd cl cl')) = (containsFA' cl) || (containsFA' cl')
                containsFA' (Clause _ (COr cl cl')) = (containsFA' cl) || (containsFA' cl')
                containsFA' (Clause _ (CNot cl)) = containsFA' cl
                containsFA' (Clause _ (CFindall _ _ _)) = True
                containsFA' _ = False
