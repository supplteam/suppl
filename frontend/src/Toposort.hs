{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module collects together utility functions that help with
--   the common task of searching for cycles in a graph of references.
module Toposort where

import Data.Graph
import qualified Data.Map as M
import Data.Map ( Map )
import Data.List

import Tokens
import AST
import SymbolTable
import Compile

-- | Calculate the names mentioned by a type
referencedTypes :: Type -> [String]
referencedTypes (Type _ (TypeName x args)) = [getIdent x] ++ (concatMap referencedTypes args)
referencedTypes (Type _ (TypeTuple xs)) = concatMap referencedTypes xs
referencedTypes (Type _ (TypeVar _))  = []
referencedTypes (Type _ (TypeList t)) = referencedTypes t
referencedTypes (Type _ (TypeMap t1 t2)) = referencedTypes t1 ++ referencedTypes t2
referencedTypes (Type _ (TypeSet t)) = referencedTypes t
referencedTypes (Type _ TypeNumber) = []
referencedTypes (Type _ TypeString) = []

-- | Calculate the types mentioned by a type symbol, not passing through
--   data declarations.
typeDefRefs :: Symbol TypeSymbolInfo -> (Symbol TypeSymbolInfo, String, [String])
typeDefRefs s@(Symbol _ _ _ (TypeSymbol (PrimTypeDecl n)))  = (s,getIdent n,[])
typeDefRefs s@(Symbol _ _ _ (TypeSymbol (TypeDecl n _ t)))  = (s,getIdent n,referencedTypes t)
typeDefRefs s@(Symbol _ _ _ (DataSymbol (DataDecl n _ _)))  = (s,getIdent n,[])

-- | Calculate the types mentioned by a type symbol, passing through
--   data declarations.
typeDefRefsDeep :: Symbol TypeSymbolInfo -> (Symbol TypeSymbolInfo, String, [String])
typeDefRefsDeep s@(Symbol _ _ _ (TypeSymbol (PrimTypeDecl n)))  = (s,getIdent n,[])
typeDefRefsDeep s@(Symbol _ _ _ (TypeSymbol (TypeDecl n _ t)))  = (s,getIdent n,referencedTypes t)
typeDefRefsDeep s@(Symbol _ _ _ (DataSymbol (DataDecl n _ bs))) = (s,getIdent n,concatMap f bs)
   where f (_,xs) = concatMap referencedTypes (extractArgs xs)

typeDefToposort :: TypeSymbolTable -> [SCC (Symbol TypeSymbolInfo)]
typeDefToposort = stronglyConnComp . map typeDefRefs . M.elems

typeDefDeepToposort :: TypeSymbolTable -> [SCC (Symbol TypeSymbolInfo)]
typeDefDeepToposort = stronglyConnComp . map typeDefRefsDeep . M.elems

typeDeclRefs :: TypeDecl -> (TypeDecl, String, [String])
typeDeclRefs d@(TypeDecl n args t) = (d, getIdent n, referencedTypes t)
typeDeclRefs d@(PrimTypeDecl n) = (d, getIdent n, [])

typeDeclsToposort :: [TypeDecl] -> [SCC TypeDecl]
typeDeclsToposort = stronglyConnComp . map typeDeclRefs

ruleRefs :: Rule ta -> [String]
ruleRefs (gl, cl) = clauseRefs cl

argsRefs :: (a -> [String]) -> Args a -> [String]
argsRefs f = concatMap f . extractArgs

clauseRefs :: Clause ta -> [String]
clauseRefs (Clause _ CFalse)        = []
clauseRefs (Clause _ CTrue)         = []
clauseRefs (Clause _ (CEq t1 t2))   = []
clauseRefs (Clause _ (CNeq t1 t2))  = []
clauseRefs (Clause _ (CPred x _))   = [getIdent x]
clauseRefs (Clause _ (CAnd c1 c2))  = clauseRefs c1 ++ clauseRefs c2
clauseRefs (Clause _ (COr c1 c2))   = clauseRefs c1 ++ clauseRefs c2
clauseRefs (Clause _ (CNot c))      = clauseRefs c
clauseRefs (Clause _ CEmpty)        = []
clauseRefs (Clause _ (CComp _ _ _)) = []
clauseRefs (Clause _ (CFindall _ (_,x,_) _)) = [getIdent x]

referencedProc :: HBody a -> [String]
referencedProc (HBody _ (HProcedure x _)) = [getIdent x]
referencedProc (HBody _ (HSeq h1 h2)) = referencedProc h1 ++ referencedProc h2
referencedProc (HBody _ (HForeach _ h)) = referencedProc h
referencedProc (HBody _ (HQuery bs)) = concatMap f bs
  where f (_,QueryBranch _ h) = referencedProc h
        f (_,DefaultBranch h) = referencedProc h
referencedProc (HBody _ (HInsert _ _)) = []
referencedProc (HBody _ (HDelete _ _)) = []
referencedProc (HBody _ HSkip) = []


toposortMap :: Map Ident ([String],a) -> [SCC (Ident,a)]
toposortMap = stronglyConnComp . map f . M.assocs
  where f (id,(xs,a)) = ((id,a),getIdent id, xs)


referencedTypeVars :: IType -> [Integer]
referencedTypeVars (IType (TypeName _ args)) = concatMap referencedTypeVars args
referencedTypeVars (IType (TypeVar x)) = [x]
referencedTypeVars (IType (TypeList t)) = referencedTypeVars t
referencedTypeVars (IType (TypeSet t))  = referencedTypeVars t
referencedTypeVars (IType (TypeTuple ts)) = concatMap referencedTypeVars ts
referencedTypeVars (IType (TypeMap t1 t2)) = referencedTypeVars t1 ++ referencedTypeVars t2
referencedTypeVars (IType TypeNumber) = []
referencedTypeVars (IType TypeString) = []

typeMapRefs :: (Integer, (Pn, TypeMapEntry))
            -> ((Pn,Integer), Integer, [Integer])
typeMapRefs (x,(pn,UnboundTyVar)) = ((pn,x),x,[])
typeMapRefs (x,(pn,RigidTyVar _)) = ((pn,x),x,[])
typeMapRefs (x,(pn,BoundTyVar t)) = ((pn,x),x,referencedTypeVars t)

occursError :: SCC (Pn,Integer) -> [(Pn,String)]
occursError (AcyclicSCC x) = []
occursError (CyclicSCC []) = []
occursError (CyclicSCC ((pn,_):_)) =
    [(pn, unwords $ ["occurs check failed: cannot construct infinite type"])]

occursCheck :: TypeMap
            -> Maybe (Pn, String)
occursCheck tymap =
      let graph = map typeMapRefs $ M.assocs tymap
          sccs  = stronglyConnComp graph
          errs  = concatMap occursError sccs
      in case errs of
           [] -> Nothing
           (pn,msg):_ -> Just (pn,msg)

