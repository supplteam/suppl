{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

{- Given a PConflict,
   we can get all the predicates
   inside of a PGuards in its paths.
   Thus, for this particular PConflict,
   we can build multiple backgrounds
   and find out which rule causes the
   conflict. -}

module Rules where

import qualified Data.Map as M
import Data.Map (Map)
import Data.List
import AST
import PConflict
import Tokens (Pn (..))
import MinBG

rulesProg :: Prog (Pn, IType) -> Map Ident [(Pn, CodeOrigin, Decl (Pn, IType))]
rulesProg prog = foldl rulesDecl M.empty prog

rulesDecl :: Map Ident [(Pn, CodeOrigin, Decl (Pn, IType))]
          -> (Pn, CodeOrigin, Decl (Pn, IType))
          -> Map Ident [(Pn, CodeOrigin, Decl (Pn, IType))]
rulesDecl rules (pn, b, DRule r@((_, nm, _), _)) =
        --let res = case containsFA r of
        --        True -> []
        --        False -> [(pn, b, DRule r)] in
        M.insertWith (\x y -> x ++ y) nm [(pn, b, DRule r)] rules

rulesDecl rules _ = rules

containsFA :: Rule (Pn, IType) -> Bool
containsFA r@(_, cl) =
        containsFA' cl
        where
                containsFA' :: Clause (Pn, IType) -> Bool
                containsFA' (Clause _ (CAnd cl cl')) = (containsFA' cl) || (containsFA' cl')
                containsFA' (Clause _ (COr cl cl')) = (containsFA' cl) || (containsFA' cl')
                containsFA' (Clause _ (CNot cl)) = containsFA' cl
                containsFA' (Clause _ (CFindall _ _ _)) = True
                containsFA' _ = False

predsProg :: Prog (Pn, IType) -> [Ident]
predsProg [] = []
predsProg ((_, _, DPred (PredDecl nm _)):xs) = nm:predsProg xs
predsProg (x:xs) = predsProg xs

getBackgrounds :: Prog (Pn, IType) -> PConflict -> (Prog (Pn, IType), [Prog (Pn, IType)])
getBackgrounds prog pconflict =
  let bg = filter eq prog
      rules = rulesProg prog
      preds = getPredicates pconflict
      preds' = predsProg prog
      lists = map (\x -> M.lookup x rules) preds
      llists = concat $ map f lists
      rules' = M.toList rules
      rulesremoved = concat $ map snd $ foldl g rules' preds
      bg' = bg ++ rulesremoved --bg' is the common background
   in (bg', choices llists)
  where
    eq (_, _, DHandler _) = False
    eq (_, _, DRule r) = False
    eq _ = True
    choices [] = [[]]
    choices [x] = map (\y -> [y]) x
    choices (x:xs) =
      case x of
      [] -> choices xs
      [y] -> map (\z -> y:z) (choices xs)
      (y:ys) -> map (\z -> y:z) (choices xs) ++ choices (ys:xs)
    f (Just x) = if null x then [] else [x]
    f Nothing = []
    g ((a, b):xs) z = if a == z then xs else (a, b):(g xs z)
    g [] z = []

-- Does the "all deepening"
getBackgrounds2 :: Prog (Pn, IType) -> PConflict -> (Prog (Pn, IType), [Prog (Pn, IType)])
getBackgrounds2 prog pconflict =
  let prog' = minBG pconflict prog
      bg = filter eq prog'
      rules = rulesProg prog'
      preds' = predsProg prog'
      lists = map (\x -> M.lookup x rules) preds'
      llists = concat $ map f lists
      rules' = M.toList rules
      rulesremoved = concat $ map snd $ foldl g rules' preds'
      bg' = bg ++ rulesremoved --bg' is the common background
   in (bg', choices llists)
  where
    eq (_, _, DHandler _) = False
    eq (_, _, DRule r) = False
    eq _ = True
    choices [] = [[]]
    choices [x] = map (\y -> [y]) x
    choices (x:xs) =
      case x of
      [] -> choices xs
      [y] -> map (\z -> y:z) (choices xs)
      (y:ys) -> map (\z -> y:z) (choices xs) ++ choices (ys:xs)
    f (Just x) = if null x then [] else [x]
    f Nothing = []
    g ((a, b):xs) z = if a == z then xs else (a, b):(g xs z)
    g [] z = []

getPredicates :: PConflict -> [Ident]
getPredicates (PConflict _ cmn _ d1 d2 _ _ _ _) =
  nub $ getPredicates' cmn ++ getPredicates' d1 ++ getPredicates' d2
  where
    getPredicates' [] = []
    getPredicates' ((EvGuards _ _):xs) = getPredicates' xs
    getPredicates' ((FEGuards _ _):xs) = getPredicates' xs
    getPredicates' ((NGuards _ _):xs) = getPredicates' xs
    getPredicates' ((DefGuards _):xs) = getPredicates' xs
    getPredicates' ((PGuards pn qcl):xs) = getPred qcl ++ getPredicates' xs
    getPred (QCPred _ nm _) = [nm]
    getPred (QCAnd qcl1 qcl2) = getPred qcl1 ++ getPred qcl2
    getPred _ = []
