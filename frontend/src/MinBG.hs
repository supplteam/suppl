{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

{-
   Given a PConflict, try to build a minimal
   background.
     - Handlers, procedures, actions removed
     - Conflict declarations removed
     - Forbid declarations removed
     - Irrelevant predicates removed
     - Irrelevant axioms, lemmas, presumes removed
     - Irrelevant events removed
     - Irrelevant functions removed
     - Irrelevant table declarations removed
-}

module MinBG where

import AST
import PConflict
import Tokens (Pn(..))
import Data.Set (Set)
import qualified Data.Set as Set

-- The main function
minBG :: PConflict -> Prog (Pn, IType) -> Prog (Pn, IType)
minBG (PConflict enm cmn cdecl div1 div2 _ _ args1 args2) prog =
  let fst_set = stringSetOfGuards (cmn ++ div1 ++ div2) `Set.union` (Set.unions $ map stringSetOfTerms $ extractArgs args1 ++ extractArgs args2)
      snd_set = fst_set `Set.union` (Set.unions $ map (stringSetOfClause . getClause) cdecl)
      -- snd_set has all that is relevant in the pconflict
      -- we now need to get all relevant stuff from the ast
   in (\(_,x,_) -> x) $ whilesimplify (snd_set, [], prog) -- do a while to keep adding stuff until we can't
  where
    whilesimplify (a,b,c) =
      let (d,e,f) = simplify (a,b,c) in
      if Set.size d > Set.size a then
        whilesimplify (d,e,f)
      else (d,e,f)

{- The tuple corresponds to
   - a set of relevant stuff (their ident name)
   - relevant declarations in the ast
   - declarations to examine and see if relevant -}
simplify :: (Set String, Prog (Pn, IType), Prog (Pn, IType)) -> (Set String, Prog (Pn, IType), Prog (Pn, IType))
simplify (sset, r, (_,BuiltinCode,_):ir) = simplify (sset, r, ir)
simplify (sset, r, d@(_,_,DEvent (EventDecl nm args)):ir) =
  if Set.member (getIdent nm) sset then
    simplify (sset, d:r, ir) -- we keep all type declarations anyway, so no need to get more informations from args
  else
    simplify (sset, r, ir) -- we don't need to add back the event decl because we know it's useless
simplify (sset, r, d@(_,_,DType _):ir) = simplify (sset, d:r, ir)
simplify (sset, r, d@(_,_,DData _):ir) = simplify (sset, d:r, ir)
simplify (sset, r, d@(_,_,DAssert (PresumeDecl (nm,_) cl)):ir) =
   if Set.member (getIdent nm) sset then
     simplify (stringSetOfClause cl `Set.union` sset, d:r, ir)
   else
     simplify (sset, r, ir)
simplify (sset, r, d@(_,_,DAssert (AxiomDecl cl)):ir) =
   if Set.null $ stringSetOfClause cl `Set.intersection` sset then
     let (a,b,c) = simplify (sset, r, ir) in
     (a,b,d:c)
   else
     simplify (stringSetOfClause cl `Set.union` sset, d:r, ir)
simplify (sset, r, d@(_,_,DAssert (LemmaDecl cl)):ir) =
   if Set.null $ stringSetOfClause cl `Set.intersection` sset then
     let (a,b,c) = simplify (sset, r, ir) in
     (a,b,d:c)
   else
     simplify (stringSetOfClause cl `Set.union` sset, d:r, ir)
simplify (sset, r, d@(_,_,DTable (TableDecl nm _ _ _)):ir) =
  if Set.member (getIdent nm) sset then
    simplify (sset, d:r, ir)
  else
    let (a,b,c) = simplify (sset, r, ir) in
    (a,b,d:c)
simplify (sset, r, d@(_,_,DIndex (IndexDecl nm _)):ir) =
  if Set.member (getIdent nm) sset then
    simplify (sset, d:r, ir)
  else
    let (a,b,c) = simplify (sset, r, ir) in
    (a,b,d:c)
simplify (sset, r, d@(_,_,DRule ((_,nm,args), cl)):ir) =
  if Set.member (getIdent nm) sset then
    simplify ((Set.unions $ map stringSetOfTerms $ extractArgs args) `Set.union` sset `Set.union` (stringSetOfClause cl), d:r, ir)
  else
    let (a,b,c) = simplify (sset, r, ir) in
    (a,b,d:c)
simplify (sset, r, d@(_,_,DPred (PredDecl nm _)):ir) =
  if Set.member (getIdent nm) sset then
    simplify (sset, d:r, ir)
  else
    let (a,b,c) = simplify (sset, r, ir) in
    (a,b,d:c)
simplify (sset, r, d@(_,_,DPred (PrimPredDecl nm _)):ir) =
  if Set.member (getIdent nm) sset then
    simplify (sset, d:r, ir)
  else
    let (a,b,c) = simplify (sset, r, ir) in
    (a,b,d:c)
simplify (sset, r, d@(_,_,DMode (ModeDecl nm _)):ir) =
  if Set.member (getIdent nm) sset then
    simplify (sset, d:r, ir)
  else
    let (a,b,c) = simplify (sset, r, ir) in
    (a,b,d:c)
simplify (sset, r, d@(_,_,DFunction (PrimFunctionDecl nm _ _)):ir) =
  if Set.member (getIdent nm) sset then
    simplify (sset, d:r, ir)
  else
    let (a,b,c) = simplify (sset, r, ir) in
    (a,b,d:c)
simplify (sset, r, d@(_,_,DPragma _):ir) =
  simplify (sset, d:r, ir)
simplify (sset, r, _:ir) = simplify (sset, r, ir)
simplify (sset, r, []) = (sset, r, [])

stringSetOfGuards :: [Guards] -> Set String
stringSetOfGuards [] = Set.empty
stringSetOfGuards ((EvGuards nm _):xs) = Set.insert (getIdent nm) (stringSetOfGuards xs)
stringSetOfGuards ((PGuards _ qcl):xs) = stringSetOfQClause qcl `Set.union` stringSetOfGuards xs
stringSetOfGuards ((NGuards _ qcl):xs) = stringSetOfQClause qcl `Set.union` stringSetOfGuards xs
stringSetOfGuards ((FEGuards _ qcl):xs) = stringSetOfQClause qcl `Set.union` stringSetOfGuards xs
stringSetOfGuards ((DefGuards _):xs) = stringSetOfGuards xs

stringSetOfQClause :: QClause (Pn, IType) -> Set String
stringSetOfQClause (QCPred _ nm args) = (Set.singleton $ getIdent nm) `Set.union` (Set.unions $ map stringSetOfQTerms (extractArgs args))
stringSetOfQClause (QCAnd qcl qcl') = stringSetOfQClause qcl `Set.union` stringSetOfQClause qcl'
stringSetOfQClause (QCEq _ tm tm') = stringSetOfQTerms tm `Set.union` stringSetOfQTerms tm'
stringSetOfQClause (QCNeq _ tm tm') = stringSetOfQTerms tm `Set.union` stringSetOfQTerms tm'
stringSetOfQClause (QCComp _ _ tm tm') = stringSetOfQTerms tm `Set.union` stringSetOfQTerms tm'

stringSetOfClause :: Clause (Pn, IType) -> Set String
stringSetOfClause (Clause _ (CPred nm args)) = (Set.singleton $ getIdent nm) `Set.union` (Set.unions $ map stringSetOfTerms (extractArgs args))
stringSetOfClause (Clause _ (CAnd qcl qcl')) = stringSetOfClause qcl `Set.union` stringSetOfClause qcl'
stringSetOfClause (Clause _ (COr qcl qcl')) = stringSetOfClause qcl `Set.union` stringSetOfClause qcl'
stringSetOfClause (Clause _ (CEq tm tm')) = stringSetOfTerms tm `Set.union` stringSetOfTerms tm'
stringSetOfClause (Clause _ (CNeq tm tm')) = stringSetOfTerms tm `Set.union` stringSetOfTerms tm'
stringSetOfClause (Clause _ (CComp _ tm tm')) = stringSetOfTerms tm `Set.union` stringSetOfTerms tm'
stringSetOfClause (Clause _ (CNot cl)) = stringSetOfClause cl
stringSetOfClause (Clause _ (CFindall _ (_,nm,args) _)) = Set.singleton (getIdent nm) `Set.union` (Set.unions $ map stringSetOfQTerms $ extractArgs args)
stringSetOfClause _ = Set.empty

stringSetOfQTerms :: QTerm (Pn, IType) -> Set String
stringSetOfQTerms (QTerm _ (TFunc nm args)) = Set.insert (getIdent nm) (Set.unions $ map stringSetOfQTerms $ extractArgs args)
stringSetOfQTerms (QTerm _ (TCons tm tm')) = stringSetOfQTerms tm `Set.union` stringSetOfQTerms tm'
stringSetOfQTerms (QTerm _ (TOp _ tm tm')) = stringSetOfQTerms tm `Set.union` stringSetOfQTerms tm'
stringSetOfQTerms (QTerm _ (TNegative tm)) = stringSetOfQTerms tm
stringSetOfQTerms (QTerm _ (TList tm)) = Set.unions $ map stringSetOfQTerms tm
stringSetOfQTerms (QTerm _ (TTuple tm)) = Set.unions $ map stringSetOfQTerms tm
stringSetOfQTerms _ = Set.empty

stringSetOfTerms :: Term (Pn, IType) -> Set String
stringSetOfTerms (TTerm _ (TFunc nm args)) = Set.insert (getIdent nm) (Set.unions $ map stringSetOfTerms $ extractArgs args)
stringSetOfTerms (TTerm _ (TCons tm tm')) = stringSetOfTerms tm `Set.union` stringSetOfTerms tm'
stringSetOfTerms (TTerm _ (TOp _ tm tm')) = stringSetOfTerms tm `Set.union` stringSetOfTerms tm'
stringSetOfTerms (TTerm _ (TNegative tm)) = stringSetOfTerms tm
stringSetOfTerms (TTerm _ (TList tm)) = Set.unions $ map stringSetOfTerms tm
stringSetOfTerms (TTerm _ (TTuple tm)) = Set.unions $ map stringSetOfTerms tm
stringSetOfTerms _ = Set.empty

getClause :: ConflictDecl (Clause (Pn, IType)) -> Clause (Pn, IType)
getClause (ConflictDecl _ _ cl) = cl
getClause (ForbidDecl _ cl) = cl
