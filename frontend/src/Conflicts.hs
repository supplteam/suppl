{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements the heart of the conflict analysis.
--   It both produces the background theory for the logic-programming part of
--   a Suppl policy, and also examines the set of slices of a program to
--   define all the conflict formulae that will be sent to the SMT solvers.
--
module Conflicts where

import Data.List
import Data.Graph
import Control.Monad
import qualified Data.Set as Set
import Data.Set ( Set )
import qualified Data.Map as M
import Data.Map ( Map )
import qualified Text.PrettyPrint as PP
import Control.Monad.Trans.State
import Control.Monad.Trans.Class


import Tokens( Pn(..), displayPn, Located(..) )
import AST
import SymbolTable
import Slice
import Compile
import qualified SMT as S
import Background

cvc4prelude :: [PP.Doc]
cvc4prelude =
  [ PP.text "(set-logic ALL_SUPPORTED)"
  , PP.text "(define-sort Set (X) (Array X Bool))"
  , PP.text "(define-sort Map (X Y) (Array X Y))"
  , PP.text "(declare-sort action 0)"
  , PP.text "(declare-sort event 0)"
  , PP.text "(declare-datatypes (A) ((List (nil) (insert (List$hd A) (List$tl List)))))"
  ]

getConflicts :: Prog (Pn,IType) -> [ConflictDecl (Clause (Pn,IType))]
getConflicts [] = []
getConflicts ((_,_,DConflict d) : xs) = d : getConflicts xs
getConflicts (_ : xs) = getConflicts xs

cvc4conflicts :: SymbolTable -> Prog (Pn,IType) -> Comp String
cvc4conflicts st prog = runM (do
  let slices = Slice.findInProg st prog
  bg <- backgroundProg prog
  let conflicts = getConflicts prog
  let consistentCheck = S.SubProblem "ConsistencyCheck" [S.Goal "consistent" S.False]
  problems <- processSlices slices conflicts
  sm <- getStringMap
  let strdecls = map processString $ M.assocs sm
  let prog = map S.cvc4cmd (strdecls++bg++consistentCheck : problems)
  return $ PP.render $ PP.vcat $ (cvc4prelude ++ prog))

why3conflicts :: SymbolTable -> Prog (Pn,IType) -> Comp String
why3conflicts st prog = runM (do
  let slices = Slice.findInProg st prog
  let conflicts = getConflicts prog
  bg <- backgroundProg prog
  problems <- processSlices slices conflicts
  sm <- getStringMap

  let strdecls = map (S.why3cmd . processString) $ M.assocs sm
  let bg' =
       [ PP.text "theory Background"
       , PP.text "  use export suppl.SupplPrelude"
       ]
       ++ map (PP.nest 2) strdecls
       ++ map (PP.nest 2 . S.why3cmd) bg ++
       [ PP.text "end" ]
  --let consistentCheck = S.why3cmd $ S.SubProblem "ConsistencyCheck" [S.Goal "consistent" S.False]
  let prog = bg' ++ map S.why3cmd problems
  return $ PP.render $ PP.vcat $ prog)


{- Build the sat problems corresponding to the slices of a program.
   Includes pairwise and foreach-induced conflicts. -}
processSlices :: [Slice] -> [ConflictDecl (Clause (Pn,IType))] -> M [S.Cmd]
processSlices ss conflicts = do
  let (ss',ps) = splitSlices ss
  let ss'' = inlineSlices ps ss'

  -- FIXME
  let gs = groupEASlices ss''
  let pairs = concatMap allpairs gs
  pair_conflicts <- mapM (uncurry slicepair2smt) pairs
  feslices <- mapM sliceFE2smt ss''
  let foreach_conflicts = [c | cs <- feslices, c <- cs]
  let conflicts = map (\ (i,x) -> S.SubProblem ("Conflict"++show i) x)
                    $ zip [0..] (pair_conflicts++foreach_conflicts)
  return conflicts
  where
   allpairs (x:xs) = [(x,y) | y <- xs] ++ allpairs xs
   allpairs [] = []



{- Build the sat problems corresponding to a conflict between two slices. -}
slicepair2smt :: Slice -> Slice -> M [S.Cmd]
slicepair2smt ps1 ps2 =
  do let PAction pna1 xa _:_ = ps1
     let PAction pna2 _ _:_ = ps2
     let PEvent pn1 x args1 tys1:ps1' = reverse ps1
     let PEvent pn2 _ args2 _:ps2' = reverse ps2
     let desc = "conflict condition for event: " ++ getIdent x ++ " @ " ++
                displayPn pn1 ++ " and " ++ displayPn pn2 ++ "\naction: " ++
                getIdent xa ++ " @ " ++ displayPn pna1 ++ " and " ++ displayPn pna2
     eventParams <- mapM (\ _ -> getFresh) tys1
     tys1' <- mapM type2smt tys1
     let decls = zipWith (\ a t -> S.DeclareFun a [] t) eventParams tys1'
     let evdecl = S.DefineFun (S.IdName "event") [] (S.TApp "event" [])
                    (S.App (S.IdName (getIdent x)) (map S.EVar eventParams))
     let env1 = extend M.empty (extractArgs args1) eventParams
     let env2 = extend M.empty (extractArgs args2) eventParams
     (expr1,action_args1) <- slice2smt env1 ps1'
     (expr2,action_args2) <- slice2smt env2 ps2'
     let diseqs = foldr (\ (a1,a2) e ->
                           S.Or [S.Not (S.Eq a1 a2), e])
                  S.False
                  (zip action_args1 action_args2)

     let decls' = decls ++ [evdecl]
     return $ ([S.Information desc] ++ decls' ++
               [S.Axiom "conflict" (expr1 $ expr2 $ diseqs), S.Goal "impossible" S.False])

{- Given:
     - renaming environment
     - a reversed slice (without PEvent)
   Returns:
     - an expression with a hole whose satisfiability implies a conflict
     - a list of action arguments
-}

slice2smt :: Map Var S.Ident -> Slice -> M (S.Formula -> S.Formula,[S.Expr])
slice2smt env (PAction _ x args:[]) =
  do args' <- mapM (term2smt env) (extractArgs args)
     return (\k -> k,args')
slice2smt env (PGuard _ qh:ps') =
  do (env',e) <- qc2smt env qh
     (vs,bs) <- getBinders
     (e',ts) <- slice2smt env' ps'
     return (\k -> S.exists_ bs (S.and [e, e' k]),ts)
slice2smt env (PNGuard _ qh:ps') =
  do (env',e) <- qc2smt env qh
     (vs,bs) <- getBinders
     (e',ts) <- slice2smt env' ps'
     return (\k -> S.and [S.Not (S.exists_ bs e), e' k],ts)
slice2smt env (PEGuard _ qh:ps') =
  do (env',e) <- qc2smt env qh
     (vs,bs) <- getBinders
     (e',ts) <- slice2smt env' ps'
     return (\k -> S.exists_ bs (S.and [e,e' k]),ts)

slice2smt env s = error $ "unexpected slice form: "++show s


{- Build sat problems for the for-each conflicts in a single slice. -}
sliceFE2smt :: Slice -> M [[S.Cmd]]
sliceFE2smt ps =
  do let PAction pna xa _:_ = ps
     let PEvent pn x args tys:ps' = reverse ps
     let desc = "foreach conflict condition for event: \n" ++ getIdent x ++ " @ " ++ displayPn pn ++
                "\naction: " ++ getIdent xa ++ " @ " ++ displayPn pna
     eventParams <- mapM (\ _ -> getFresh) tys
     tys' <- mapM type2smt tys
     let decls = zipWith (\ a t -> S.DeclareFun a [] t) eventParams tys'
     let env = extend M.empty (extractArgs args) eventParams
     exprs <- sliceFE2smtl' env ps'

     let evdecl = S.DefineFun (S.IdName "event") [] (S.TApp "event" [])
                    (S.App (S.IdName (getIdent x)) (map S.EVar eventParams))
     let decls' = decls ++ [evdecl]

     let scripts = map (\e -> ([S.Information desc] ++ decls
                               ++ [S.Axiom "conflict" e, S.Goal "impossible" S.False]))
                   exprs
     return scripts

{- Given:
     - renaming environment
     - a reversed slice (without PEvent)
   Returns:
     - a list of expressions whose satisfiability implies a conflict
-}
sliceFE2smtl' :: Map Var S.Ident -> Slice -> M [S.Formula]
sliceFE2smtl' env (PAction _ _ _:[]) = return []
sliceFE2smtl' env (PGuard _ qh:ps') =
  do (env',e) <- qc2smt env qh
     (vs,bs) <- getBinders
     --let env' = extend env vs (map S.bindVar bs)
     es' <- sliceFE2smtl' env' ps'
     return (map (\ e' -> S.exists_ bs (S.and [e, e'])) es')
sliceFE2smtl' env (PNGuard _ qh:ps') =
  do (env',e) <- qc2smt env qh
     (vs,bs) <- getBinders
     --let env' = extend env vs (map S.bindVar bs)
     es' <- sliceFE2smtl' env'  ps'
     return (map (\ e' -> S.and [S.Not (S.exists_ bs e), e']) es')
sliceFE2smtl' env (ps@(PEGuard _ qh:ps')) =
  do (expr1,action_args1) <- slice2smt env ps
     (expr2,action_args2) <- slice2smt env ps
     let diseqs = foldr (\ (a1,a2) e ->
                            S.Or [S.Not (S.Eq a1 a2), e])
                  S.False
                  (zip action_args1 action_args2)
     (env',e) <- qc2smt env qh
     (vs,bs) <- getBinders
     --let env' = extend env vs (map S.bindVar bs)
     es' <- sliceFE2smtl' env' ps'
     return ((expr1 $expr2 $ diseqs) : map (\ e' -> S.exists_ bs (S.and [e, e'])) es')

sliceFE2smtl' env s = error $ "unexpected slice form "++show s


{- Machinery for munging slices. -}

{- Split set of slices into action-terminated and call-terminated groups -}
splitSlices :: [Slice] -> ([Slice],[Slice])
splitSlices ss =  partition f ss
  where f (PAction _ _ _:_) = True
        f _ = False

{- Inline procedure slices into action-terminated ones
   this makes bogus assumption that procedures are not recursive
   and it may be a stupid approach anyhow (requires substitution, blows up slice sizes).
   NB: Doesn't rename bound variables when inlining, so shadowing can occur. This seems
   to be harmless for now, but bears closer inspection.-}
inlineSlices :: [Slice] -> [Slice] -> [Slice]
inlineSlices procss = g
   where g (ps:ss) =
            case last ps of
              PProc _ p params _ -> g (ss' ++ ss)
                where ss' =  map f (filter h procss)
                      f (PPcall _ actuals:ps') = substSlice substMap (init ps) ++ ps'
                         where substMap = mkSubstMap (extractArgs params)
                                                     (extractArgs actuals)
                      f _ = error "impossible"
    	              h (PPcall p' _:_) | p == p' = True
                      h _ = False
              _ -> ps:(g ss)
         g [] = []

mkSubstMap :: [Maybe Var] -> [a] -> Map Var a
mkSubstMap (Just v:params') (t:actuals') = M.insert v t (mkSubstMap params' actuals')
mkSubstMap (Nothing:params') (_:actuals') = mkSubstMap params' actuals'
mkSubstMap [] [] = M.empty
mkSubstMap _ _ = error "impossible"

substSlice :: Map Var (Term (Pn,IType)) -> Slice -> Slice
substSlice m ps = map (substPPoint m) ps

substPPoint :: Map Var (Term (Pn,IType)) -> PPoint -> PPoint
substPPoint m (PAction pn x args) = PAction pn x (mapArgs (substTerm m) args)
substPPoint m (PPcall x args) = PPcall x (mapArgs (substTerm m) args)
substPPoint m (PGuard pn qh) = PGuard pn (substQueryHead (substQTerm m) qh)
substPPoint m (PNGuard pn qh) = PNGuard pn (substQueryHead (substQTerm m) qh)
substPPoint m (PEGuard pn qh) = PEGuard pn (substQueryHead (substQTerm m) qh)
substPPoint m (PEvent pn x args targs) = PEvent pn x args targs
substPPoint m (PProc pn x args targs) = PProc pn x args targs

substTerm :: Show a => Map Var (Term a) -> Term a -> Term a
substTerm m (TTerm a t) = TTerm a (substTerm' (substTerm m) t)
substTerm m (TVar a v) =
   case M.lookup v m of
     Just t -> t
     Nothing -> TVar a v

substQTerm :: Show a => Map Var (Term a) -> QTerm a -> QTerm a
substQTerm m (QBindVar a v) = QBindVar a v
substQTerm m (QRefVar a v) =
   case M.lookup v m of
     Just t -> term2qterm t
     Nothing -> QRefVar a v
substQTerm m (QTerm a t) = QTerm a (substTerm' (substQTerm m) t)

substTerm' :: Show tm => (tm -> tm) -> Term' tm -> Term' tm
substTerm' st (TFunc x args) = TFunc x (mapArgs st args)
substTerm' st (TCons t1 t2) = TCons (st t1) (st t2)
substTerm' st (TList ts) = TList (map st ts)
substTerm' st (TOp op t1 t2) = TOp op (st t1) (st t2)
substTerm' st (TNegative t) = TNegative (st t)
substTerm' _ t = t

substQueryHead :: (QTerm a -> QTerm a) -> QClause a -> QClause a
substQueryHead sqt (QCEq pn t1 t2) = QCEq pn (sqt t1) (sqt t2)
substQueryHead sqt (QCPred pn x args) = QCPred pn x (mapArgs sqt args)
substQueryHead sqt (QCAnd c1 c2) = QCAnd (substQueryHead sqt c1) (substQueryHead sqt c2)
substQueryHead sqt (QCNeq pn t1 t2) = QCNeq pn (sqt t1) (sqt t2)
substQueryHead sqt (QCComp pn op t1 t2) = QCComp pn op (sqt t1) (sqt t2)

term2qterm :: Term a -> QTerm a
term2qterm (TVar a v) = QRefVar a v
term2qterm (TTerm a t) = QTerm a (fmap term2qterm t)

{- Given set of event-to-action slices, group into sets with
   the same event and action -}
groupEASlices :: [Slice] -> [[Slice]]
groupEASlices ss =  groupBy (\ ps1 ps2 -> compareSlice ps1 ps2 == EQ) (sortBy compareSlice ss)

compareSlice :: Slice -> Slice -> Ordering -- deliberately partial
compareSlice ps1 ps2  = compare (head ps1,last ps1) (head ps2, last ps2)

comparePPoint :: PPoint -> PPoint -> Ordering
comparePPoint (PAction _ id1 _) (PAction _ id2 _) = compareIdent id1 id2
comparePPoint (PEvent _ id1 _ _) (PEvent _ id2 _ _) = compareIdent id1 id2
comparePPoint _ _ = undefined -- deliberately partial

instance Ord PPoint where
  compare = comparePPoint

compareIdent :: Ident -> Ident -> Ordering
compareIdent id1 id2 = compare (getIdent id1) (getIdent id2)
