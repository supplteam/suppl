{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements a simple peephole optimizer that removes
--   certain kinds of redundant match and evaluate instructions
--   from an IR program.  The "LiftAssigns" and "LiftBranches" passes
--   tend to introduce a lot of unnecessary fresh variables, so here
--   we remove as many as can easily be found.
--
--   We could probably do a better job by performing a def/use analysis,
--   but this seems sufficent to remove the worst of the redundant instructions.
--
module Peephole
( peepholeProg
) where

import Control.Monad
import qualified Data.Map as M
import Data.Map ( Map )
import Data.List

import Compile
import Tokens (Pn(..))
import AST (Mode(..), Ident(..), Var(..), OP(..), getIdent )
import SupplIR

-- | Run the peephole optimizer on an IR program to remove redundant
--   match and eval instructions.
peepholeProg :: [Decl] -> [Decl]
peepholeProg = map peepholeDecl

peepholeDecl :: Decl -> Decl
peepholeDecl (DRule (ident,md,body)) =
  let body' = peepholeBodies body
   in DRule (ident,md,body')

peepholeDecl (DHandler (ident, n, bs)) =
  let bs' = peepholeHBodies bs
   in DHandler (ident, n, bs')

peepholeDecl (DProcDefn (ident, n, bs)) =
  let bs' = peepholeHBodies bs
   in DProcDefn (ident, n, bs')

peepholeDecl (DRegressionQuery pn nm body) =
  let body' = peepholeBodies body
   in DRegressionQuery pn nm body'

peepholeDecl (DRegressionHandle pn ps ev acts) =
   DRegressionHandle pn ps ev acts

peepholeDecl (DEmit s) = DEmit s


peepholeBodies :: [PredBody] -> [PredBody]
peepholeBodies bs = peephole M.empty bs (\_ bs' -> bs')

peepholeHBodies :: [HandleBody] -> [HandleBody]
peepholeHBodies bs = hpeephole M.empty bs

hpeephole :: Map Var RValue -> [HandleBody] -> [HandleBody]
hpeephole m [] = []
hpeephole m (HInstr (HPred ps) : bs) = 
   peephole m ps (\m' ps' ->
        (if null ps' then [] else [HInstr (HPred ps')]) ++ hpeephole m' bs)
hpeephole m (HInstr (HCallProc ident rvs) : bs) =
   HInstr (HCallProc ident (map (substVarsRValue m) rvs)) : hpeephole m bs
hpeephole m (HInstr (HAction ident rvs) : bs) =
   HInstr (HAction ident (map (substVarsRValue m) rvs)) : hpeephole m bs
hpeephole m (HInstr (HInsert ident rvs) : bs) =
   HInstr (HInsert ident (map (substVarsRValue m) rvs)) : hpeephole m bs
hpeephole m (HInstr (HDelete ident rvs) : bs) =
   HInstr (HDelete ident (map (fmap $ substVarsRValue m) rvs)) : hpeephole m bs
hpeephole m (HBranch xs : bs) =
   HBranch (map (peepholeBranch m) xs) : hpeephole m bs
hpeephole m (HForeach ps hs : bs) =
   let (ps',hs') = peepholeBranch m (ps,hs)
    in HForeach ps' hs' : hpeephole m bs

peepholeBranch :: Map Var RValue -> ([PredBody], [HandleBody]) -> ([PredBody],[HandleBody])
peepholeBranch m (ps,hs) = peephole m ps (\m' ps' -> (ps', hpeephole m' hs))

peephole :: Map Var RValue -> [PredBody] -> (Map Var RValue -> [PredBody] -> a) -> a
peephole m [] k = k m []
peephole m (Instr (Match Ignore _) : bs) k = peephole m bs k

peephole m (Instr (Match (LVar v) (RVar v')) : bs) k =
   peephole (M.insert v (RVar v') m) bs k

peephole m (Instr (Match (LVar v) (RArg i)) : bs) k =
   peephole (M.insert v (RArg i) m) bs k

peephole m (Instr (Eval v (EVal r)) : bs) k =
   peephole m (Instr (Match (LVar v) r) : bs) k

peephole m (Instr (Eval v (EVar v')) : bs) k =
   peephole (M.insert v (RVar v') m) bs k

peephole m (Instr (Findall r ps v) : bs) k =
   peephole m ps  (\m_ps ps' ->
   let r' = substVarsRValue m_ps r in
   peephole m bs (\m' bs' ->
   k m' (Instr (Findall r' ps' v) : bs')))

peephole m (Instr i : bs) k =
   let i' = substVars m i
    in peephole m bs (\m' bs' -> k m' (Instr i' : bs'))

peephole m (Not xs : bs) k = 
   peephole m xs (\_ xs' ->
   peephole m bs (\m' bs' ->
     k m' (Not xs' : bs')))

peephole m (Disj xss : bs) k =
   let xss' = map (\xs -> peephole m xs
                      (\m' xs' -> xs' ++ disjBranchFixup m m' bs))
                  xss
    in peephole m bs (\m' bs' -> k m' (Disj xss' : bs'))


-- To make the peephole optimizer work with disjunctions
--   we need to readd equations to the end of disjunction
--   branch for variables in the new map that is not in the old
--   map, provided the variable appears somewhere in the
--   remainder of the body list.
--
-- This function calculates the equations that need to be added.
disjBranchFixup :: Map Var RValue -> Map Var RValue -> [PredBody] -> [PredBody]
disjBranchFixup m m' bs = 
   concatMap (\ (v,rv) ->
        case M.lookup v m of
           Just _  -> []
           Nothing ->
             if v `elem` freePBodies bs
               then [Instr $ Match (LVar v) rv]
               else []
      )
      (M.assocs m')

substVars :: Map Var RValue -> PredInstr -> PredInstr
substVars m (CallPred mds ident args) =
   let args' = substVarsArgs m args
    in (CallPred mds ident args')
substVars m (QueryTable mds ident args) =
   let args' = substVarsArgs m args
    in (QueryTable mds ident args')
substVars m (Match l r) =
   let l' = substVarsLValue m l
       r' = substVarsRValue m r
    in (Match l' r')
substVars m Fail = Fail
substVars m (Compare ty op x y) =
   let x' = substVarsRValue m x
       y' = substVarsRValue m y
    in (Compare ty op x' y')
substVars m (Eval v e) = 
   let e' = substVarsEval m e
     in(Eval v e')
substVars m (Findall r bs v) = (Findall r bs v)

substVarsArgs :: Map Var RValue -> [Either LValue RValue] -> [Either LValue RValue]
substVarsArgs m = map (substVarsArg m)

substVarsArg :: Map Var RValue -> Either LValue RValue -> (Either LValue RValue)
substVarsArg m (Left l) = Left $ substVarsLValue m l
substVarsArg m (Right r) = Right $ substVarsRValue m r

substVarsLValue :: Map Var RValue -> LValue -> LValue
substVarsLValue m (LArg i) = (LArg i)
substVarsLValue m (LVar v) = (LVar v)
substVarsLValue m Ignore = Ignore
substVarsLValue m (LCons h t) =
   let h' = substVarsLValue m h
       t' = substVarsLValue m t
     in (LCons h' t')
substVarsLValue m (LList xs) =
   let xs' = map (substVarsLValue m) xs
    in (LList xs')
substVarsLValue m (LTuple xs) =
   let xs' = map (substVarsLValue m) xs
    in (LTuple xs')
substVarsLValue m (LData ident xs) =
   let xs' = map (substVarsLValue m) xs
    in (LData ident xs')
substVarsLValue m (LRVal ty r) =
   let r' = substVarsRValue m r
    in (LRVal ty r')

substVarsRValue :: Map Var RValue -> RValue -> RValue
substVarsRValue m (RArg i) = (RArg i)
substVarsRValue m (RVar v) = unwindVar m v
substVarsRValue m (RCons h t) =
   let h' = substVarsRValue m h
       t' = substVarsRValue m t
    in (RCons h' t')
substVarsRValue m (RList xs) =
   let xs' = map (substVarsRValue m) xs
    in (RList xs')
substVarsRValue m (RTuple xs) =
   let xs' = map (substVarsRValue m) xs
    in (RTuple xs')
substVarsRValue m (RData ident xs) =
   let xs' = map (substVarsRValue m) xs
     in (RData ident xs')
substVarsRValue m (RStringLit s) = (RStringLit s)
substVarsRValue m (REval e) = REval $ substVarsEval m e

substVarsEval :: Map Var RValue -> Evaluable -> Evaluable
substVarsEval m (EVal r) = EVal $ substVarsRValue m r
substVarsEval m (EVar v) = 
   let x = unwindVar m v
    in case x of
          RVar v' -> (EVar v')
          _ -> (EVal x)
substVarsEval m (EOp op x y) =
   let x' = substVarsEval m x
       y' = substVarsEval m y
    in (EOp op x' y')
substVarsEval m (ENeg e) = ENeg $ substVarsEval m e
substVarsEval m (Funcall ident xs) =
   let xs' = map (substVarsEval m) xs
    in (Funcall ident xs')
substVarsEval m (EStringLit s) = (EStringLit s)
substVarsEval m (ENumLit n) = (ENumLit n)

unwindVar :: Map Var RValue -> Var -> RValue
unwindVar m v =
   case M.lookup v m of
      Nothing -> (RVar v)
      Just (RVar v') -> unwindVar m v'
      Just x -> x
