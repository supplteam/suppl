{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module defines the intermediate representation used in the
--   Suppl compiler, and a pretty printer for the IR.
module SupplIR where

import qualified Data.Map as M
import Data.Map ( Map )
import Data.List
import qualified Text.PrettyPrint as PP
import Text.PrettyPrint (($$), (<>), (<+>))

import Tokens (Pn(..), displayPn )
import AST (Mode(..), Ident(..), Var(..), getIdent, NumericLiteral(..) )

data Type
   = TypeName Ident [Type]
   | TypeList Type
   | TypeMap Type Type
   | TypeSet Type
   | TypeNumber
   | TypeString
   | TypeTuple [Type]
   | TypeRigidVar String
  deriving (Eq, Show)

data OP = OpPlus | OpMinus | OpTimes | OpDiv | OpStrConcat | OpListAppend
  deriving (Eq, Show)

data LValue
  = LArg Int
  | LVar Var
  | Ignore
  | LCons LValue LValue
  | LList [LValue]
  | LTuple [LValue]
  | LData Ident [LValue]
  | LRVal Type RValue
 deriving (Eq,Show)

data RValue
  = RArg Int
  | RVar Var
  | RCons RValue RValue
  | RList [RValue]
  | RTuple [RValue]
  | RData Ident [RValue]
  | RStringLit String
  | REval Evaluable
 deriving (Eq,Show)

data Evaluable
  = EVal RValue
  | EVar Var
  | EOp OP Evaluable Evaluable
  | ENeg Evaluable
  | Funcall Ident [Evaluable]
  | EStringLit String
  | ENumLit NumericLiteral
 deriving (Eq,Show)

data CmpOp
  = Equal
  | NotEqual
  | LessThan
  | LessEqual
 deriving (Eq,Show)

data PredInstr
  = CallPred [Mode] Ident [Either LValue RValue]
  | QueryTable [Mode] Ident [Either LValue RValue]
  | Match LValue RValue
  | Fail
  | Compare Type CmpOp RValue RValue
  | Eval Var Evaluable
  | Findall RValue [PredBody] Var
 deriving (Eq,Show)

data PredBody
  = Instr PredInstr
  | Disj [[PredBody]]
  | Not [PredBody]
 deriving (Eq,Show)

data HandleInstr
  = HPred [PredBody]
  | HCallProc Ident [RValue]
  | HAction Ident [RValue]
  | HInsert Ident [RValue]
  | HDelete Ident [Maybe RValue]
 deriving (Eq,Show)

data HandleBody
  = HInstr HandleInstr
  | HBranch [([PredBody],[HandleBody])]
  | HForeach [PredBody] [HandleBody]
 deriving (Eq,Show)

type RuleDefn = (Ident, [Mode], [PredBody])
type HandleDefn = (Ident, Int, [HandleBody])
type ProcDefn = (Ident, Int, [HandleBody])

data Decl
  = DRule RuleDefn
  | DHandler HandleDefn
  | DProcDefn ProcDefn
  | DRegressionQuery Pn String [PredBody]
  | DRegressionHandle Pn [PredBody] (Ident,[RValue]) [(Ident,[RValue])]
  | DEmit String
 deriving (Eq,Show)


-- | Pretty-print a sequence of IR declarations
renderDecls :: [Decl] -> String
renderDecls = PP.render . printDecls

printDecls :: [Decl] -> PP.Doc
printDecls = PP.vcat . intersperse (PP.text "") . map printDecl

printDecl :: Decl -> PP.Doc
printDecl (DRule r) = printRule r
printDecl (DHandler h) = printHandler h
printDecl (DProcDefn p) = printProcDefn p
printDecl (DRegressionQuery pn n b) = printRegressionQuery pn n b
printDecl (DRegressionHandle pn bs ev acts) = printRegressionHandle pn bs ev acts
printDecl (DEmit s) = PP.text "Emit:" $$ PP.nest 2 (PP.text s)

printRegressionHandle
   :: Pn -> [PredBody] 
   -> (Ident,[RValue]) -> [(Ident,[RValue])] -> PP.Doc
printRegressionHandle pn bs ev acts =
  PP.text "Regression Handler:" <+> printBasic ev
  $$
  PP.nest 2 (PP.vcat $ map printPBody bs)
  $$
  PP.nest 2 (PP.vcat $ map printBasic acts)

printBasic :: (Ident,[RValue]) -> PP.Doc
printBasic (x,args) =
  PP.text (getIdent x) <>
  PP.parens (PP.cat $ PP.punctuate PP.comma $ map printRValue args)

printRegressionQuery :: Pn -> String -> [PredBody] -> PP.Doc
printRegressionQuery pn n body = 
  PP.text "Regression Query:" <+>
  PP.text n <+>
  PP.text (displayPn pn)
  $$
  PP.nest 2 (printPBodies body)

printRule :: RuleDefn -> PP.Doc
printRule (ident,mode,body) = 
  PP.text "Rule:" <+> 
  PP.text (getIdent ident) <+>
  printMode mode
  $$
  PP.nest 2 (printPBodies body)

printMode :: [Mode] -> PP.Doc
printMode mds = PP.brackets (PP.hsep $ PP.punctuate PP.comma (map printOneMode mds))

printOneMode :: Mode -> PP.Doc
printOneMode ModeIn     = PP.text "in"
printOneMode ModeOut    = PP.text "out"
printOneMode ModeIgnore = PP.text "ignore"

printPBodies :: [PredBody] -> PP.Doc
printPBodies = PP.vcat . map printPBody

printPBody :: PredBody -> PP.Doc
printPBody (Instr i) = printPInstr i
printPBody (Disj bs) =
  PP.text "DISJ" $$ (PP.vcat (map printOneDisj bs))
printPBody (Not bs) =
  PP.text "NOT" $$ PP.nest 2 (printPBodies bs)

printOneDisj :: [PredBody] -> PP.Doc
printOneDisj bs = PP.text "|" $$ (PP.nest 2 (printPBodies bs))

printPInstr :: PredInstr -> PP.Doc
printPInstr (CallPred mds ident args) = 
  PP.text "CALL" <> printMode mds <+>
  PP.text (getIdent ident) <>
  printArgs args

printPInstr (QueryTable mds ident args) =
  PP.text "QUERYTABLE" <> printMode mds <+>
  PP.text (getIdent ident) <> printArgs args

printPInstr (Match l r) =
  PP.text "MATCH" <+>
  printLValue l <+>
  PP.text ":=" <+>
  printRValue r

printPInstr Fail = PP.text "FAIL"

printPInstr (Compare ty op x y) =
  PP.text "COMPARE" <> (PP.parens (printType ty)) <+>
  printRValue x <+>
  printCmpOp op <+>
  printRValue y
  
printPInstr (Eval v ev) =
  PP.text "EVAL" <+> PP.text "?" <> PP.text v <+> PP.text ":=" <+> printEval ev

printPInstr (Findall r body l) =
  PP.text "FINDALL" <+> printRValue r <+>
  PP.text "INTO" <+> PP.text "?" <> PP.text l
  $$
  PP.nest 2 (printPBodies body)

printType :: Type -> PP.Doc
printType (TypeName ident []) = PP.text (getIdent ident)
printType (TypeName ident args) = PP.text (getIdent ident) <> PP.parens (PP.cat $ PP.punctuate (PP.text ", ") $ map printType args)
printType (TypeList ty) = PP.text "list" <> PP.parens (printType ty)
printType (TypeSet ty) = PP.text "set" <> PP.parens (printType ty)
printType (TypeMap ty1 ty2) = 
   PP.text "map" <> PP.parens (printType ty1 <> PP.comma <+> printType ty2)
printType (TypeNumber) = PP.text "number"
printType (TypeString) = PP.text "string"
printType (TypeRigidVar s) = PP.text s
printType (TypeTuple ts) = 
   PP.parens (PP.cat $ PP.punctuate (PP.text "*") $ map printType ts)

printCmpOp :: CmpOp -> PP.Doc
printCmpOp Equal = PP.text "=="
printCmpOp NotEqual = PP.text "<>"
printCmpOp LessThan = PP.text "<"
printCmpOp LessEqual = PP.text "<="

printArgs :: [Either LValue RValue] -> PP.Doc
printArgs xs = PP.parens $ PP.sep $ PP.punctuate PP.comma (map printArg xs)

printArg :: Either LValue RValue -> PP.Doc
printArg (Left l) = printLValue l
printArg (Right r) = printRValue r

printLValue :: LValue -> PP.Doc
printLValue (LArg i) = PP.text "?ARG#" <> PP.int i
printLValue (LVar v) = PP.text "?" <> PP.text v
printLValue Ignore = PP.text "_"
printLValue (LCons h t) = PP.brackets (printLValue h <+> PP.text "|" <+> printLValue t)
printLValue (LList xs) = 
  PP.brackets (PP.sep $ PP.punctuate PP.comma $ map printLValue xs)
printLValue (LTuple xs) =
  PP.parens (PP.sep $ PP.punctuate PP.comma $ map printLValue xs)
printLValue (LData ident xs) =
  PP.text (getIdent ident) <>
  PP.parens (PP.sep $ PP.punctuate PP.comma $ map printLValue xs)
printLValue (LRVal _ r) = printRValue r

printRValue :: RValue -> PP.Doc
printRValue (RArg i) = PP.text "ARG#" <> PP.int i
printRValue (RVar v) = PP.text v
printRValue (RCons h t) = PP.brackets (printRValue h <+> PP.text "|" <+> printRValue t)
printRValue (RList xs) = 
  PP.brackets (PP.sep $ PP.punctuate PP.comma $ map printRValue xs)
printRValue (RTuple xs) = 
  PP.parens (PP.sep $ PP.punctuate PP.comma $ map printRValue xs)
printRValue (RData ident xs) =
  PP.text (getIdent ident) <>
  PP.parens (PP.sep $ PP.punctuate PP.comma $ map printRValue xs)
printRValue (RStringLit s) = PP.text (show s)
printRValue (REval ev) = printEval ev

printEval :: Evaluable -> PP.Doc
printEval (EVal r) = printRValue r
printEval (EStringLit s) = PP.text (show s)
printEval (ENumLit n) = PP.text (show n)
printEval (EVar v) = PP.text v
printEval (EOp op x y) = PP.parens (printEval x <+> printOp op <+> printEval y)
printEval (ENeg x) = PP.parens (PP.text "~" <> printEval x)
printEval (Funcall ident xs) =
  PP.text (getIdent ident) <>
  PP.parens (PP.sep $ PP.punctuate PP.comma $ map printEval xs)

printOp :: OP -> PP.Doc
printOp OpStrConcat = PP.text "++"
printOp OpListAppend = PP.text "++l"
printOp OpPlus = PP.text "+"
printOp OpMinus = PP.text "-"
printOp OpTimes = PP.text "*"
printOp OpDiv = PP.text "/" 

printProcDefn :: ProcDefn -> PP.Doc
printProcDefn (ident,i,body) =
  PP.text "Procedure:" <+> PP.text (getIdent ident) <+> PP.int i
  $$
  PP.nest 2 (printHBodies body)

printHandler :: HandleDefn -> PP.Doc
printHandler (ident,i,body) =
  PP.text "Handler:" <+> PP.text (getIdent ident) <+> PP.int i
  $$
  PP.nest 2 (printHBodies body)

printHBodies :: [HandleBody] -> PP.Doc
printHBodies = PP.vcat . map printHBody

printHBody :: HandleBody -> PP.Doc
printHBody (HInstr i) = printHInstr i
printHBody (HBranch xs) =
  PP.text "BRANCH" $$ (PP.vcat (map printOneBranch xs))
printHBody (HForeach qs bs) =
  PP.text "FOREACH" 
  $$ (PP.nest 2 (
    printPBodies qs
    $$
    PP.text "=>"
    $$
    printHBodies bs))

printOneBranch :: ([PredBody],[HandleBody]) -> PP.Doc
printOneBranch (qs,bs) =
   PP.text "|" 
   $$ (PP.nest 2 (
    printPBodies qs
    $$
    PP.text "=>"
    $$
    printHBodies bs))


printHInstr :: HandleInstr -> PP.Doc
printHInstr (HPred bs) = printPBodies bs
printHInstr (HCallProc ident xs) =
  PP.text "CALLPROC" <+> PP.text (getIdent ident) <>
  PP.parens (PP.cat $ PP.punctuate PP.comma $ map printRValue xs)
printHInstr (HAction ident xs) =
  PP.text "ACTION" <+> PP.text (getIdent ident) <>
  PP.parens (PP.cat $ PP.punctuate PP.comma $ map printRValue xs)
printHInstr (HInsert ident xs) =
  PP.text "INSERT" <+> PP.text (getIdent ident) <>
  PP.parens (PP.cat $ PP.punctuate PP.comma $ map printRValue xs)
printHInstr (HDelete ident xs) =
  PP.text "DELETE" <+> PP.text (getIdent ident) <>
  PP.parens (PP.cat $ PP.punctuate PP.comma $ map (maybe (PP.text "_") printRValue) xs)



freePBodies :: [PredBody] -> [Var]
freePBodies = snd . boundFreePBodies

boundFreePBodies :: [PredBody] -> ([Var],[Var])
boundFreePBodies [] = ([],[])
boundFreePBodies (p:ps) = 
    let (bnd1,free1) = boundFreePBody p
        (bnd2,free2) = boundFreePBodies ps
     in (bnd1++bnd2, free1 ++ (filter (\x -> not (x `elem` bnd1)) free2))

boundFreePBody :: PredBody -> ([Var],[Var])
boundFreePBody (Instr i) = boundFreeInstr i
boundFreePBody (Not ps) =
    let (_,free) = boundFreePBodies ps
     in ([],free)
boundFreePBody (Disj xss) =
    let (bnds,frees) = unzip $ map boundFreePBodies xss
     in (intersectMany bnds, concat frees)

boundFreeInstr :: PredInstr -> ([Var],[Var])
boundFreeInstr Fail = ([],[])
boundFreeInstr (Compare _ _ x y) = ([], freeRValue x ++ freeRValue y)
boundFreeInstr (Match l r) =
   let (lbnd, lfree) = boundFreeLValue l
       rfree = freeRValue r
    in (lbnd, lfree++rfree)
boundFreeInstr (Eval v e) = ([v], freeEval e)
boundFreeInstr (Findall r ps l) = 
   let (_,free) = boundFreePBodies (ps ++ [Instr (Match (LVar l) r)])
    in ([l], free)
boundFreeInstr (CallPred _ _ args) = boundFreeArgs args
boundFreeInstr (QueryTable _ _ args) = boundFreeArgs args

boundFreeArgs :: [Either LValue RValue] -> ([Var],[Var])
boundFreeArgs [] = ([],[])
boundFreeArgs (Right r:xs) =
    let (bnd,free) = boundFreeArgs xs
        free' = freeRValue r
     in (bnd,free'++free)
boundFreeArgs (Left l:xs) =
    let (bnd,free) = boundFreeArgs xs
        (bnd',free') = boundFreeLValue l
     in (bnd'++bnd,free'++free)

freeRValue :: RValue -> [Var]
freeRValue (RArg i) = []
  -- error "unexpected argument value inside branch during branch lifting!"
freeRValue (RVar v) = [v]
freeRValue (RCons h t) = freeRValue h ++ freeRValue t
freeRValue (RList xs) = concatMap freeRValue xs
freeRValue (RTuple xs) = concatMap freeRValue xs 
freeRValue (RData ident xs) = concatMap freeRValue xs
freeRValue (RStringLit _) = []
freeRValue (REval e) = freeEval e


freeEval :: Evaluable -> [Var]
freeEval (EVal r) = freeRValue r
freeEval (EVar v) = [v]
freeEval (EOp op x y) = freeEval x ++ freeEval y
freeEval (ENeg x) = freeEval x
freeEval (Funcall ident xs) = concatMap freeEval xs
freeEval (EStringLit _) = []
freeEval (ENumLit _) = []

boundFreeLValue :: LValue -> ([Var],[Var])
boundFreeLValue (LArg i) = ([],[])
   -- error "unexpected argument value inside branch during branch lifting!"
boundFreeLValue (LVar v) = ([v], [])
boundFreeLValue Ignore = ([],[])
boundFreeLValue (LCons x y) =
    let (bnd1,free1) = boundFreeLValue x
        (bnd2,free2) = boundFreeLValue y
     in (bnd1++bnd2, free1++free2)
boundFreeLValue (LList xs) =
    let (bnds, frees) = unzip $ map boundFreeLValue xs
     in (concat bnds, concat frees)
boundFreeLValue (LTuple xs) =
    let (bnds, frees) = unzip $ map boundFreeLValue xs
     in (concat bnds, concat frees)
boundFreeLValue (LData ident xs) =
    let (bnds, frees) = unzip $ map boundFreeLValue xs
     in (concat bnds, concat frees)
boundFreeLValue (LRVal _ r) = ([], freeRValue r)


freeHBodies :: [HandleBody] -> [Var]
freeHBodies = snd . boundFreeHBodies

boundFreeHBodies :: [HandleBody] -> ([Var],[Var])
boundFreeHBodies [] = ([],[])
boundFreeHBodies (h:hs) = 
    let (bnd1,free1) = boundFreeHBody h
        (bnd2,free2) = boundFreeHBodies hs
     in (bnd1++bnd2, free1 ++ (filter (\x -> not (x `elem` bnd1)) free2))

boundFreeHBody :: HandleBody -> ([Var],[Var])
boundFreeHBody (HInstr i) = boundFreeHInstr i
boundFreeHBody (HBranch xss) =
    let (_,frees) = unzip $ map (\ (ps,bs) -> boundFreeHBodies ((HInstr (HPred ps)) : bs)) xss
     in ([], concat frees)
boundFreeHBody (HForeach ps bs) =
    let (_,free) = boundFreeHBodies (HInstr (HPred ps) : bs)
     in ([], free)

boundFreeHInstr :: HandleInstr -> ([Var],[Var])
boundFreeHInstr (HPred ps) = boundFreePBodies ps
boundFreeHInstr (HCallProc _ rs) = ([], concatMap freeRValue rs)
boundFreeHInstr (HAction _ rs) = ([], concatMap freeRValue rs)
boundFreeHInstr (HInsert _ rs) = ([], concatMap freeRValue rs)
boundFreeHInstr (HDelete _ rs) = ([], concatMap (maybe [] freeRValue) rs)

intersectMany :: Eq a => [[a]] -> [a]
intersectMany [] = []
intersectMany (x:xs) = foldr intersect x xs
