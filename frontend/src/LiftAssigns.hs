{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements an IR-to-IR pass that decompose complex
--   term expressions into a sequence of match statements
--   involving simpler terms.  The transformation is similar to,
--   but less strict than, a transformation to A-normal form.
--
--   The main reason to do this is so that we can force Prolog
--   to evaluate callable functions and operators.  Prolog has
--   a weak kind of evaluation semantics where evaluation only
--   occurs in very specific contexts.  In order to implement
--   the call-by-value semantics of Suppl, we have to decompose
--   terms in this way so we can insert @is@ operators to
--   force evaluation.
--
module LiftAssigns
( liftProg
) where

import Control.Monad
import qualified Data.Map as M
import Data.Map ( Map )
import Data.List

import Compile
import Tokens (Pn(..))
import AST (Mode(..), Ident(..), Var(..), getIdent )
import SupplIR

-- | Decompose match statements into A-normalish form.
liftProg :: [Decl] -> Comp [Decl]
liftProg = mapM liftDecl

liftDecl :: Decl -> Comp Decl
liftDecl (DRule (ident,md,body)) = do
  body' <- liftPBodies body
  return (DRule (ident,md,body'))

liftDecl (DHandler (ident,i,body)) = do
  body' <- liftHBodies body
  return (DHandler (ident,i,body'))

liftDecl (DProcDefn (ident,i,body)) = do
  body' <- liftHBodies body
  return (DProcDefn (ident,i,body'))

liftDecl (DRegressionQuery pn n body) = do
  body' <- liftPBodies body
  return (DRegressionQuery pn n body')
  
liftDecl (DRegressionHandle pn bs ev acts) = do
  (bs1,ev') <- liftBasic ev
  (bs2,acts') <- fmap unzip $ mapM liftBasic acts
  return (DRegressionHandle pn (bs ++ bs1 ++ concat bs2) ev' acts')

liftDecl (DEmit s) = return (DEmit s)

liftBasic :: (Ident,[RValue]) -> Comp ([PredBody],(Ident,[RValue]))
liftBasic (nm,rvs) = do
    (ps, rvs') <- fmap unzip $ mapM liftRValue rvs
    return (map Instr $ concat ps, (nm,rvs'))

liftHBodies :: [HandleBody] -> Comp [HandleBody]
liftHBodies = fmap concat . mapM liftHBody

liftHBody :: HandleBody -> Comp [HandleBody]
liftHBody (HInstr i) = fmap (map HInstr) (liftHInstr i)
liftHBody (HBranch xss) = fmap ((:[]) . HBranch) (mapM liftOneBranch xss)
liftHBody (HForeach ps bs) = do
     ps' <- liftPBodies ps
     bs' <- liftHBodies bs
     return [HForeach ps' bs']

liftOneBranch :: ([PredBody],[HandleBody]) -> Comp ([PredBody],[HandleBody])
liftOneBranch (ps,bs) = do
     ps' <- liftPBodies ps
     bs' <- liftHBodies bs
     return (ps', bs')

liftHInstr :: HandleInstr -> Comp [HandleInstr]
liftHInstr (HPred ps) = fmap ((:[]) . HPred) (liftPBodies ps)
liftHInstr (HCallProc ident rs) = do
    (pre,rs') <- fmap unzip $ mapM liftRValue rs
    return [HPred (map Instr (concat pre)), HCallProc ident rs']
liftHInstr (HAction ident rs) = do
    (pre,rs') <- fmap unzip $ mapM liftRValue rs
    return [HPred (map Instr (concat pre)), HAction ident rs']
liftHInstr (HInsert ident rs) = do
    (pre,rs') <- fmap unzip $ mapM liftRValue rs
    return [HPred (map Instr (concat pre)), HInsert ident rs']
liftHInstr (HDelete ident rs) = do
    (pre,rs') <- fmap unzip $ mapM liftMRValue rs
    return [HPred (map Instr (concat pre)), HDelete ident rs']


liftPredInstr :: PredInstr -> Comp [PredInstr]

liftPredInstr Fail = return [Fail]

liftPredInstr (Compare ty op r1 r2) = do
   (pre1,r1') <- liftRValue r1
   (pre2,r2') <- liftRValue r2
   return (pre1++pre2++[Compare ty op r1' r2'])

liftPredInstr (Match l r) = do
   (pre,r') <- liftRValue r
   (l',post) <- liftLValue l
   return (pre++[Match l' r']++post)

liftPredInstr (Eval v e) = do
   (pre,e') <- liftEval e
   return (pre++[Eval v e'])

liftPredInstr (CallPred mds ident args) = do
   xss <- mapM liftArg args
   let (pre, args', post) = unzip3 xss
   return (concat pre ++ [CallPred mds ident args'] ++ concat post)

liftPredInstr (QueryTable mds ident args) = do
   xss <- mapM liftArg args
   let (pre, args', post) = unzip3 xss
   return (concat pre ++ [QueryTable mds ident args'] ++ concat post)

liftPredInstr (Findall r bs l) = do
   (pre,r') <- liftRValue r
   bs' <- liftPBodies bs
   return ([Findall r' (bs'++map Instr pre) l])

liftPBodies :: [PredBody] -> Comp [PredBody]
liftPBodies = fmap concat . mapM liftPBody

liftPBody :: PredBody -> Comp [PredBody]
liftPBody (Instr i) = fmap (map Instr) $ liftPredInstr i
liftPBody (Not xs)  = 
  do xs' <- liftPBodies xs
     return [Not xs']
liftPBody (Disj xss) = 
  do xss' <- mapM liftPBodies xss
     return [Disj xss']

liftArg :: Either LValue RValue 
         -> Comp ([PredInstr],Either LValue RValue,[PredInstr])
liftArg (Left l) = do 
   (l',post) <- liftLValue l
   return ([],Left l',post)
liftArg (Right r) = do 
   (pre,r') <- liftRValue r
   return (pre,Right r',[])

liftLValue :: LValue -> Comp (LValue,[PredInstr])
liftLValue (LArg i) = return (LArg i, [])
liftLValue (LVar v) = return (LVar v, [])
liftLValue Ignore   = return (Ignore, [])

liftLValue (LCons h t) = do
   (h', post1) <- liftLValue h
   (t', post2) <- liftLValue t
   return (LCons h' t', post1++post2)   

liftLValue (LList xs) = do
   xss <- mapM liftLValue xs
   let (xs', post) = unzip xss
   return (LList xs', concat post)

liftLValue (LTuple xs) = do
   xss <- mapM liftLValue xs
   let (xs', post) = unzip xss
   return (LTuple xs', concat post)

liftLValue (LData ident xs) = do
   xss <- mapM liftLValue xs
   let (xs', post) = unzip xss
   return (LData ident xs', concat post)

-- When an RValue occurs in an LValue context, introduce
-- a new fresh variable instead to accept the outcomming
-- value.  Then, insert a comparison operation afterwards.
liftLValue (LRVal ty r) = do
   x <- compFreshVar >>= (return . ("Fresh"++) . show)
   (pre, r') <- liftRValue r
   return (LVar x, pre++[Compare ty Equal (RVar x) r'])

liftMRValue :: Maybe RValue -> Comp ([PredInstr],Maybe RValue)
liftMRValue Nothing  = return ([],Nothing)
liftMRValue (Just r) =
   do (ls,r') <- liftRValue r
      return (ls,Just r')

liftRValue :: RValue -> Comp ([PredInstr],RValue)
liftRValue (RArg i) = return ([],RArg i)
liftRValue (RVar v) = return ([],RVar v)

liftRValue (RCons h t) = do
   (pre1, h') <- liftRValue h
   (pre2, t') <- liftRValue t
   return (pre1++pre2, RCons h' t')

liftRValue (RList xs) = do
   xss <- mapM liftRValue xs
   let (pre, xs') = unzip xss
   return (concat pre, RList xs')

liftRValue (RTuple xs) = do
   xss <- mapM liftRValue xs
   let (pre, xs') = unzip xss
   return (concat pre, RTuple xs')

liftRValue (RData ident xs) = do
   xss <- mapM liftRValue xs
   let (pre, xs') = unzip xss
   return (concat pre, RData ident xs')

-- Oddly enough, string literals need to be evaluated.
-- This is because tuProlog has very poor support for
-- string literal escapes, so we are forced to implement
-- our own.
liftRValue (RStringLit s) = do
   x <- compFreshVar >>= (return . ("Fresh"++) . show)
   return ([Eval x (EStringLit s)], RVar x)

-- When an evaluable occurs inside an RValue, lift it
-- out by evalauting it into a fresh variable.
liftRValue (REval e) = do
   x <- compFreshVar >>= (return . ("Fresh"++) . show)
   (pre, e') <- liftEval e
   return (pre++[Eval x e'], RVar x)


liftEval :: Evaluable -> Comp ([PredInstr], Evaluable)

liftEval (EVar v) = return ([], EVar v)


-- Oddly enough, string literals need to be evaluated.
-- This is because tuProlog has very poor support for
-- string literal escapes, so we are forced to implement
-- our own.
liftEval (EStringLit s) = do 
   x <- compFreshVar >>= (return . ("Fresh"++) . show)
   return ([Eval x (EStringLit s)], EVar x)

liftEval (ENumLit n) = return ([], ENumLit n)

-- Pull out RValues occuring inside evaluables.
liftEval (EVal r) = do
   x <- compFreshVar >>= (return . ("Fresh"++) . show)
   (pre, e') <- liftRValue r
   return (pre++[Match (LVar x) e'], EVar x)

-- Implmenet the string concatenation operator via the
-- @str_concat_iio@ predicate defined in the  primitive theory.
liftEval (EOp OpStrConcat x y) = do
   (pre1,x') <- liftEval x
   (pre2,y') <- liftEval y
   fv <- compFreshVar >>= (return . ("Fresh"++) . show)
   let i = CallPred [ModeIn,ModeIn,ModeOut]
                    (Ident (Pn "" 0 0) "str_concat")
                    [Right (REval x'), Right (REval y'), Left (LVar fv)]
   pre3 <- liftPredInstr i
   return (pre1++pre2++pre3, EVar fv)

-- Implmenet the list concatenation operator via the
-- native Prolog @append@ predicate.
liftEval (EOp OpListAppend x y) = do
   (pre1,x') <- liftEval x
   (pre2,y') <- liftEval y
   fv <- compFreshVar >>= (return . ("Fresh"++) . show)
   let i = CallPred [ModeIn,ModeIn,ModeOut]
                    (Ident (Pn "" 0 0) "append")
                    [Right (REval x'), Right (REval y'), Left (LVar fv)]
   pre3 <- liftPredInstr i
   return (pre1++pre2++pre3, EVar fv)

liftEval (EOp op x y) = do
   (pre1,x') <- liftEval x
   (pre2,y') <- liftEval y
   fv <- compFreshVar >>= (return . ("Fresh"++) . show)
   let i = Eval fv (EOp op x' y')
   return (pre1 ++ pre2 ++ [i], EVar fv)

liftEval (ENeg x) = do
   (pre, x') <- liftEval x
   fv <- compFreshVar >>= (return . ("Fresh"++) . show)
   let i = Eval fv (ENeg x')
   return (pre ++ [i], EVar fv)

liftEval (Funcall ident xs) = do
   xss <- mapM liftEval xs
   let (pre, xs') = unzip xss
   x <- compFreshVar >>= (return . ("Fresh"++) . show)
   let i = Eval x (Funcall ident xs')
   return (concat pre ++ [i], EVar x)
