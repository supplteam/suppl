{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

import System.Environment
import CmdLine

main = getArgs >>= CmdLine.runCompiler
