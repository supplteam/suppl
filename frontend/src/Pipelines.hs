{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module defines the various compiler pipelines that can be
--   invoked from the command line.
module Pipelines where

import qualified Data.Map as M

import Tokens( Pn(..) )
import AST
import Compile

import qualified SymbolTable
import qualified Scopecheck
import qualified SupplIR as IR
import qualified EmitIR
import qualified Typecheck as TC
import qualified Modecheck as MC
import qualified LiftAssigns
import qualified LiftBranches
import qualified Prolog
import qualified Schema
import qualified Peephole
import qualified Docs
import qualified Conflicts
import qualified Inline
import qualified PConflict
import qualified NewConflicts

-- | Scan the given program and produce symbol tables.  Check the symbol tables
--   for sanity.  If they pass, stash them in the 'Comp' monad for later use
--   by the rest of the compiler phases.
buildSymbolTables :: [FileDecl] -> Comp ()
buildSymbolTables decls =
    let (msgs, tst, st) = SymbolTable.processDecls decls
        msgs' = map (\ (pn,x) -> Err (Just pn) x) msgs
     in do addErrs msgs'
           Scopecheck.checkDeclScopes tst st
           Scopecheck.checkProcScopes decls st
           compUpdateState (\x -> x{ comp_tst = tst, comp_st = st })

-- | This pipeline runs the frontend and static analysis phases of the compiler.
--   It emits the IR immediately after modechecking.
irCompilerPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
irCompilerPipeline files prog0 docs = do
      processPragmas prog0
      buildSymbolTables prog0
      phaseFail
      prog1 <- TC.typecheck prog0
      prog2 <- MC.modecheck prog1
      phaseFail

      return (IR.renderDecls prog2)

-- | This pipeline runs the frontend, static analysis, and middle phases of the compiler.
--   It emits the IR produced after all IR transformation passes, immediately before
--   it is transformed into Prolog.
ir2CompilerPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
ir2CompilerPipeline files prog0 docs = do
      processPragmas prog0
      buildSymbolTables prog0
      phaseFail
      prog1 <- TC.typecheck prog0
      prog2 <- MC.modecheck prog1
      phaseFail
      prog3 <- LiftAssigns.liftProg prog2
      prog4 <- LiftBranches.liftProg prog3
      let prog5 = Peephole.peepholeProg prog4
      return (IR.renderDecls prog5)


-- | This pipeline runs the frontend and static analysis phases of the compiler.
--   It also inlines predicates into handler bodies.
--   It emits the IR immediately after modechecking.
ir3CompilerPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
ir3CompilerPipeline files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog1 <- TC.typecheck prog
      phaseFail
      st <- compSymbolTable
      let prog' = Inline.inlineProg st prog1
      let (msgs, tst1, st1) = SymbolTable.processDecls prog'
      compUpdateState (\x -> x{ comp_tst = tst1, comp_st = st1 })
      st' <- compSymbolTable
      prog'' <- MC.modecheck prog'
      phaseFail
      return (IR.renderDecls prog'')

-- | The main compiler pipeline.  It runs all phases of the compiler, including
--   table schema compilation.  It outputs the final executable Prolog policy program.
mainPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
mainPipeline files prog0 docs = do
      processPragmas prog0
      buildSymbolTables prog0
      phaseFail
      prog1 <- TC.typecheck prog0
      prog2 <- MC.modecheck prog1
      phaseFail
      prog3 <- LiftAssigns.liftProg prog2
      prog4 <- LiftBranches.liftProg prog3
      let prog5 = Peephole.peepholeProg prog4
      prog6 <- EmitIR.emitProg prog5
      schemaProg <- Schema.compileSchema
      let prog7 = schemaProg ++ prog6
      return (Prolog.renderProlog prog7)

-- | This pipeline is planned to eventually produce module documentation.
--   It is currently an unimplemented stub.
docsCompilerPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
docsCompilerPipeline files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      Docs.buildDocs files prog docs

-- | Produce just the Prolog code corresponding to the table schema.
schemaCompilerPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
schemaCompilerPipeline files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog' <- TC.typecheck prog
      phaseFail
      out <- Schema.compileSchema
      return (Prolog.renderProlog out)

-- | Produce Why3 code for checking policy conflicts
conflictsCompilerPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
conflictsCompilerPipeline files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog1 <- TC.typecheck prog
      phaseFail
      MC.modecheck prog1
      phaseFail
      st <- compSymbolTable
      Conflicts.why3conflicts st prog1

-- | Produce Why3 code checking policy conflicts; new algorithm
conflictsCompilerPipeline2 :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
conflictsCompilerPipeline2 files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog1 <- TC.typecheck prog
      phaseFail
      MC.modecheck prog1
      phaseFail
      st <- compSymbolTable
      NewConflicts.why3pconflicts st prog1

-- | Produce Why3 code with inlined rules for checking policy conflicts; new algorithm
conflictsCompilerPipeline2inline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
conflictsCompilerPipeline2inline files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog1 <- TC.typecheck prog
      phaseFail
      st <- compSymbolTable
      let prog' = Inline.inlineProg st prog1
      let (msgs, tst1, st1) = SymbolTable.processDecls prog'
      compUpdateState (\x -> x{ comp_tst = tst1, comp_st = st1 })
      st' <- compSymbolTable
      MC.modecheck prog'
      phaseFail
      NewConflicts.why3pconflicts st prog'

-- | Pipeline used to pretty-print a policy with potential conflict information highlighed
displayConflictsPipeline
   :: String -> [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
displayConflictsPipeline results files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog1 <- TC.typecheck prog
      phaseFail
      MC.modecheck prog1
      phaseFail
      st <- compSymbolTable
      let conflicts = PConflict.getPConflicts st prog1
      Docs.buildConflictDocs results conflicts files prog docs

-- | Pipeline used for regression testing of conflict generation
testPConflictPipeline :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
testPConflictPipeline files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog1 <- TC.typecheck prog
      phaseFail
      MC.modecheck prog1
      phaseFail
      st <- compSymbolTable
      let (a, b) = NewConflicts.testpconflicts st prog1 in
        if a then
          return ""
          else
            return b

-- | Pipeline used for making case disjunction on rules for unsolved conflicts
deepConflictsPipeline :: String -> [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
deepConflictsPipeline results files prog docs = do
  processPragmas prog
  buildSymbolTables prog
  phaseFail
  prog1 <- TC.typecheck prog
  phaseFail
  MC.modecheck prog1
  phaseFail
  st <- compSymbolTable
  let conflicts = PConflict.getPConflicts st prog1
  let (conflicts', isfalse) = Docs.getUnsolvedConflicts results conflicts
  if (null conflicts' && not isfalse)
    then
      return ""
    else
      NewConflicts.why3pconflicts' st conflicts' prog1 isfalse

displayDeepConflictsPipeline
   :: String -> [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
displayDeepConflictsPipeline results files prog docs = do
      processPragmas prog
      buildSymbolTables prog
      phaseFail
      prog1 <- TC.typecheck prog
      phaseFail
      MC.modecheck prog1
      phaseFail
      st <- compSymbolTable
      let conflicts = PConflict.getPConflicts st prog1
      Docs.buildConflictDocs2 results conflicts files prog docs prog1
