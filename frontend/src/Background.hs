{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

module Background where

import qualified Data.Map as M
import Data.Map ( Map )
import Data.Graph
import Control.Monad
import Control.Monad.Trans.State
import Control.Monad.Trans.Class
import Data.List

import Tokens( Pn(..), displayPn, Located(..) )
import AST
import SymbolTable
import Compile
import Toposort
import qualified SMT as S
import qualified Modecheck as MC


{- Machinery for generating logical sat expressions from terms and types. -}
newtype M a = M {unM ::
      (CompSt,Map String Int, Int,[Maybe Var],[S.Binder]) ->
      (CompSt,Map String Int, Int,[Maybe Var],[S.Binder],Maybe a)}

instance Monad M where
   return x = M (\ (cst,sm,c,ns,bs) -> (cst,sm,c,ns,bs,Just x))
   m >>= f = M (\ (cst,sm,c,ns,bs) ->
                    let (cst',sm',c',ns',bs',mx) = unM m (cst,sm,c,ns,bs)
                     in case mx of
                           Nothing -> (cst',sm',c',ns',bs',Nothing)
                           Just x  -> unM (f x) (cst',sm',c',ns',bs'))

instance Functor M where
   fmap f m = m >>= return . f

liftComp :: Comp a -> M a
liftComp x = M (\ (cst,sm,c,ns,bs) ->
   let (cst',z) = unComp x cst
    in case z of
         Nothing -> (cst',sm,c,ns,bs,Nothing)
         Just z' -> (cst',sm,c,ns,bs,Just z')
   )

runM :: M a -> Comp a
runM m = Comp (\cst ->
   let (cst',_,_,_,_,mx) = unM m (cst,M.empty,0,[],[])
    in (cst', mx))

addBinder :: Maybe Var -> S.Binder -> M ()
addBinder n b = M (\ (cst,sm,c,ns,bs) -> (cst,sm,c,n:ns,b:bs,Just ()))

getBinders :: M ([Maybe Var],[S.Binder])
getBinders = M (\ (cst,sm,c,ns,bs) -> (cst,sm,c,[],[],Just (ns,bs)))

setBinders :: ([Maybe Var],[S.Binder]) -> M ()
setBinders (ns,bs) = M (\ (cst,sm,c,_,_) -> (cst,sm,c,ns,bs,Just ()))

getFresh :: M S.Ident
getFresh = M (\ (cst,sm,c,ns,bs) -> (cst,sm,c+1,ns,bs,Just (S.IdFresh c)))

getFreshInt :: M Int
getFreshInt = M (\ (cst,sm,c,ns,bs) -> (cst,sm,c+1,ns,bs,Just c))

mkStrLit :: String -> M Int
mkStrLit s = M (\ (cst,sm,c,ns,bs) ->
  case M.lookup s sm of
    Nothing -> (cst,M.insert s c sm,c+1,ns,bs,Just c)
    Just x  -> (cst,sm,c,ns,bs,Just x))

getStringMap :: M (Map String Int)
getStringMap = M (\ (cst,sm,c,ns,bs) -> (cst,sm,c,ns,bs,Just sm))


extend :: Map Var S.Ident -> [Maybe Var] -> [S.Ident] -> Map Var S.Ident
extend env vs bs = foldr extend1 env (zip vs bs)
  where extend1 (Just v,n) = M.insert v n
        extend1 (Nothing,_) = id


{- Generate the background declarations for a program. -}
processString :: (String,Int) -> S.Cmd
processString (s,i) = S.DeclareString i s

backgroundTypes :: TypeSymbolTable -> M [S.Cmd]
backgroundTypes tst = fmap concat $ mapM emitTypesScc $ typeDefDeepToposort tst

emitTypesScc :: SCC (Symbol TypeSymbolInfo) -> M [S.Cmd]
emitTypesScc (AcyclicSCC x) = emitType x
emitTypesScc (CyclicSCC xs) = emitTypes xs

emitType :: Symbol TypeSymbolInfo -> M [S.Cmd]
emitType (Symbol _ BuiltinCode _ _) = return [] -- ignore builtin symbols
emitType (Symbol _ _ _ (DataSymbol dd)) =
    do dd' <- emitDataDecl dd
       return [S.DeclareData [dd']]
emitType (Symbol _ _ _ (TypeSymbol td)) = fmap (:[]) $ emitTyDecl td

emitTypes :: [Symbol TypeSymbolInfo] -> M [S.Cmd]
emitTypes xs = f [] xs
  where f dds [] =
             do let dta = S.DeclareData dds
                return [dta]
        f dds (Symbol _ BuiltinCode _ _ : xs) = f dds xs -- ignore builtin symbols
        f dds (Symbol _ _ _ (DataSymbol dd) : xs) =
             do dd' <- emitDataDecl dd
                f (dd':dds) xs
        f dds (Symbol _ _ _ (TypeSymbol td) : xs) =
             case td of
                PrimTypeDecl id -> error "prim type decls cannot be recursive with data declarations"
                TypeDecl id args ty ->
                   do ty' <- type2smt ty
                      let dd' = S.DataTypeDecl (getIdent id) (map snd args) ty'
                      f (dd':dds) xs

emitTyDecl :: TypeDecl -> M S.Cmd
emitTyDecl (PrimTypeDecl id) = return (S.DeclareType (getIdent id) [])
emitTyDecl (TypeDecl id args ty) = do
     ty' <- type2smt ty
     return (S.DefineType (getIdent id) (map snd args) ty')

backgroundProg :: Prog (Pn,IType) -> M [S.Cmd]
backgroundProg prog =
   let f (rs, opq, xs, ys) x =
           do (rs', opq', xs', ys') <- backgroundDecl rs x
              return (rs', M.unionWith predSortCombine opq opq', xs++xs', ys++ys')
    in do (rules, opq, predecls, asserts) <- foldM f (M.empty, M.empty, [], []) prog
          tst <- liftComp (compTypeSymbolTable)
          tys <- backgroundTypes tst
          rules' <- emitPredMap opq rules
          asserts' <- mapM (uncurry backgroundAssert) $ zip [0..] asserts
          return (tys ++ predecls ++ rules' ++ asserts')

emitDataDecl :: DataDecl -> M S.DataDecl
emitDataDecl (DataDecl x vs bs) =
    do bs' <- mapM emitDataBranch bs
       return (S.DataDecl (getIdent x) (map snd vs) bs')


emitDataBranch :: (Ident, Args Type) -> M (String,[S.Type])
emitDataBranch (cx, tys) =
    do tys' <- mapM (emitDataBranchTy cx) (extractArgs tys)
       return (getIdent cx, tys')

emitDataBranchTy :: Ident -> Type -> M S.Type
emitDataBranchTy cx ty = type2smt ty

emitPredMap :: Map Ident PredicateSort -> Map Ident ([String],[Rule (Pn,IType)]) -> M [S.Cmd]
emitPredMap opq rules = fmap concat $ mapM (emitPredScc opq) $ toposortMap rules

emitPredScc :: Map Ident PredicateSort -> SCC (Ident,[Rule (Pn, IType)]) -> M [S.Cmd]
emitPredScc opq (AcyclicSCC x) = emitPred opq x
emitPredScc opq (CyclicSCC xs) = do
    (decls,rss) <- fmap unzip $ mapM (emitRecPred opq) xs
    return (decls ++ concat rss)

emitPred :: Map Ident PredicateSort -> (Ident,[Rule (Pn,IType)]) -> M [S.Cmd]
emitPred opq p@(nm,rs) =
   case M.lookup nm opq of
     Nothing -> fmap (:[]) $ emitTransparentPred p
     Just OpaquePredicate -> fmap (:[]) $ emitOpaquePred p
     Just OpenPredicate -> emitOpenPred p >>= \ (decl,rs) -> return (decl:rs)

emitRecPred :: Map Ident PredicateSort -> (Ident,[Rule (Pn,IType)]) -> M (S.Cmd,[S.Cmd])
emitRecPred opq p@(nm,rs) =
   case M.lookup nm opq of
     Nothing -> do
       (x,y) <- emitTransparentRecPred p
       (z,t) <- emitOpenPred p
       return (x, y++t)
     Just OpaquePredicate -> fmap (\x -> (x,[])) $ emitOpaquePred p
     Just OpenPredicate -> emitOpenPred p

emitOpaquePred :: (Ident,[Rule (Pn,IType)]) -> M S.Cmd
emitOpaquePred (nm,rs) = do
   symbol <- liftComp (lookupTermIdent nm)
   case sym_info symbol of
      PredSymbol (PIDecl (PredDecl _ tys)) _ -> do
        let numArgs = length (extractArgs tys)
        tys' <- mapM (type2smt . fst) (extractArgs tys)
        return (S.DeclarePred (S.IdName (getIdent nm)) tys')

      _ -> liftComp (errMsg (loc nm) $ unwords ["expected",qt nm,"to be a defined predicate"])

emitOpenPred :: (Ident,[Rule (Pn,IType)]) -> M (S.Cmd,[S.Cmd])
emitOpenPred (nm,rs) = do
   bs <- mapM rule2smt rs
   symbol <- liftComp (lookupTermIdent nm)
   case sym_info symbol of
      PredSymbol (PIDecl (PredDecl _ tys)) _ -> do
        let numArgs = length (extractArgs tys)
        tys' <- mapM (type2smt . fst) (extractArgs tys)
        let args = map S.IdArg [0 .. numArgs-1]
        let binders = map (uncurry S.Bind) (zip args tys')
        let decl = S.DeclarePred (S.IdName (getIdent nm)) tys'
        let head = S.Pred (S.IdName (getIdent nm)) (map S.EVar args)
        rs' <- mapM (openRule2smt head binders) rs
        return (decl,rs')

      _ -> liftComp (errMsg (loc nm) $ unwords ["expected",qt nm,"to be a defined predicate"])

openRule2smt :: S.Formula -> [S.Binder] -> Rule (a,IType) -> M S.Cmd
openRule2smt head argbs ((pn,id,gl), Clause _ CEmpty) = do
    --Need to modify it so that we don't have redundant equalities in
    --the output, need to wait for a fix of bug in rose.plc
    env1 <- goalEnv M.empty gl
    gl' <- goal2smt env1 gl
    (_,bs) <- getBinders
    i <- getFreshInt
    let nm = getIdent id++"_rule"++show i
    return (S.Axiom nm (S.forall_ (argbs++bs) (S.Implies (S.and (gl')) head)))
openRule2smt head argbs ((pn,nm,args), cl) = do
    env <- clauseEnv M.empty (Clause pn (COr (Clause pn (CNot cl)) (Clause pn (CPred nm args))))
    cl' <- clause2smt env (Clause pn (COr (Clause pn (CNot cl)) (Clause pn (CPred nm args))))
    (_,bs) <- getBinders
    i <- getFreshInt
    return (S.Axiom (getIdent nm++"_rule"++show i) (S.forall_ bs cl'))

emitTransparentPred :: (Ident,[Rule (Pn,IType)]) -> M S.Cmd
emitTransparentPred (nm,rs) = do
   bs <- mapM rule2smt rs
   symbol <- liftComp (lookupTermIdent nm)
   case sym_info symbol of
      PredSymbol (PIDecl (PredDecl _ tys)) _ -> do
        let numArgs = length (extractArgs tys)
        tys' <- mapM (type2smt . fst) (extractArgs tys)
        let args = map S.IdArg [0 .. numArgs-1]
        let binders = map (uncurry S.Bind) (zip args tys')
        return (S.DefinePred (S.IdName (getIdent nm)) binders
                (case bs of
                    [] -> S.False
                    [x] -> x
                    _ -> S.Or bs))

      _ -> liftComp (errMsg (loc nm) $ unwords ["expected",qt nm,"to be a defined predicate"])

emitTransparentRecPred :: (Ident,[Rule (Pn,IType)]) -> M (S.Cmd,[S.Cmd])
emitTransparentRecPred (nm,rs) = do
   {-bs <- mapM rule2smt rs
   symbol <- liftComp (lookupTermIdent nm)
   case sym_info symbol of
      PredSymbol (PIDecl (PredDecl _ tys)) _ -> do
        let numArgs = length (extractArgs tys)
        tys' <- mapM (type2smt . fst) (extractArgs tys)
        let args = map S.IdArg [0 .. numArgs-1]
        let binders = map (uncurry S.Bind) (zip args tys')
        let decl = (S.DeclarePred (S.IdName (getIdent nm)) tys')
        let defn = (S.Axiom (getIdent nm++"_defn")
                    (S.forall_ binders
                     (S.Iff (S.Pred (S.IdName (getIdent nm)) (map S.EVar args))
                            (S.or bs))))
        return (decl,[defn])

      _ -> liftComp (errMsg (loc nm) $ unwords ["expected",qt nm,"to be a defined predicate"])-}
   symbol <- liftComp (lookupTermIdent nm)
   case sym_info symbol of
     PredSymbol (PIDecl (PredDecl _ tys)) _ -> do
       tys' <- mapM (type2smt . fst) (extractArgs tys)
       let decl = (S.DeclarePred (S.IdName (getIdent nm)) tys')
       bs <- mapM rule2smt rs
       let numArgs = length (extractArgs tys)
       let args = map S.IdArg [0 .. numArgs -1]
       let binders = map (uncurry S.Bind) (zip args tys')
       let defn = (S.Axiom (getIdent nm++"_defn")
                   (S.forall_ binders
                   (S.Implies (S.Pred (S.IdName (getIdent nm)) (map S.EVar args))
                          (S.or bs))))
       return (decl, defn:[])
     _ -> liftComp (errMsg (loc nm) $ unwords ["expected",qt nm,"to be a defined predicate"])

data PredicateSort
 = OpaquePredicate | OpenPredicate
 deriving (Eq,Show)

predSortCombine OpaquePredicate _ = OpaquePredicate
predSortCombine _ OpaquePredicate = OpaquePredicate
predSortCombine OpenPredicate OpenPredicate = OpenPredicate

type2itype :: Type -> IType
type2itype ty =
  case ty of
    Type _ (TypeName id l) -> IType (TypeName id (map type2itype l))
    Type _ (TypeList ty') -> IType (TypeList (type2itype ty'))
    Type _ (TypeMap ty1 ty2) -> IType (TypeMap (type2itype ty1) (type2itype ty2))
    Type _ (TypeSet ty') -> IType (TypeSet (type2itype ty'))
    Type _ (TypeTuple l) -> IType (TypeTuple (map type2itype l))
    Type _ TypeNumber -> IType TypeNumber
    Type _ TypeString -> IType TypeString
    Type _ (TypeVar v) -> error "declarations should not have type variables"

backgroundDecl :: Map Ident ([String],[Rule (Pn,IType)])
               -> (Pn, CodeOrigin, Decl (Pn,IType))
               -> M (Map Ident ([String],[Rule (Pn,IType)])
                    , Map Ident PredicateSort
                    , [S.Cmd]
                    , [AssertDecl (Clause (Pn,IType))]  -- [S.Cmd]
                    )

backgroundDecl rules (pn, BuiltinCode, _) =
     return (rules, M.empty, [], []) -- ignore all builtin declarations

backgroundDecl rules (pn, _, DEvent (EventDecl x args)) =
     do tys <- mapM type2smt (extractArgs args)
        return (rules, M.empty, [S.DeclareEvent (getIdent x) tys], [])

backgroundDecl rules (pn, _, DProcedure (PrimProcedureDecl x args)) =
     do tys <- mapM type2smt (extractArgs args)
        return (rules, M.empty, [S.DeclareAction (getIdent x) tys], [])

backgroundDecl rules (pn, _, DTable (TableDecl nm args m _)) =
     do tys <- mapM type2smt (extractArgs args)
        let modes = extractArgs $ MC.expandNamedModes args m
        let cl = clause_of_table nm  (extractArgs args) modes
        res <- axiom cl
        return (rules, M.empty, [S.DeclarePred (S.IdName (getIdent nm)) tys, res], [])
     where
       clause_of_table ident args modes =
         let args1 = map (\ (ty, n) -> (pn, TVar (pn, type2itype ty) ("_x" ++ show n))) (zip args [(0::Integer) ..])
             args2 = map (\ (ty, n) -> (pn, TVar (pn, type2itype ty) ("_y" ++ show n))) (zip args [(0::Integer) ..])
             decl1 = (Clause pn (CPred ident (PositionalArgs args1)))
             decl2 = (Clause pn (CPred ident (PositionalArgs args2)))
             eqlist = map fst (filter eq (zip (zip (map snd args1) (map snd args2)) modes))
             neqlist = map fst (filter (not . eq) (zip (zip (map snd args1) (map snd args2)) modes))
             f = foldr (\ (x,y) c -> Clause pn (CAnd (Clause pn (CEq x y)) c)) (Clause pn CEmpty)
             cl = rightarrow decl1 (rightarrow decl2 (rightarrow (f eqlist) (f neqlist)))
          in cl
       rightarrow cl cl' =
         Clause pn (COr (Clause pn (CNot cl)) cl')
       eq x =
         case x of
           (_, ModeIn) -> True
           _ -> False
       axiom cl =
         do env <- clauseEnv M.empty cl
            cl' <- clause2smt env cl
            (_,bs) <- getBinders
            return (S.Axiom ("axiom_table_"++ getIdent nm) (S.forall_ bs cl'))

backgroundDecl rules (pn, _, DPred (PredDecl nm args)) =
     -- This wierd insert here makes sure that a declared predicate with no rules
     -- gets emitted as a predicate defined to be empty.
     do let rules' = M.insertWith (\ (x1,x2) (y1,y2) -> (x1++y1,x2++y2))
                                  nm ([], []) rules
        return (rules', M.empty, [], [])

backgroundDecl rules (pn, _, DPred (PrimPredDecl x args)) =
     do tys <- mapM (type2smt . fst) (extractArgs args)
        return (rules, M.empty, [S.DeclarePred (S.IdName (getIdent x)) tys], [])

backgroundDecl rules (pn, _, DRule r@((_,nm,_),_)) =
     do let rules' = M.insertWith (\ (x1,x2) (y1,y2) -> (x1++y1,x2++y2))
                                  nm (ruleRefs r, [r]) rules
        return (rules', M.empty, [], [])

backgroundDecl rules (pn, _, DFunction (PrimFunctionDecl x args ret)) =
     do tys <- mapM type2smt (extractArgs args)
        ret' <- type2smt ret
        return (rules, M.empty, [S.DeclareFun (S.IdName (getIdent x)) tys ret'], [])

backgroundDecl rules (pn, _, DAssert assert) = return (rules, M.empty, [], [assert])

backgroundDecl rules (pn, _, DPragma (GenericPragma pragid (Just (PTApplied _ id _)))) =
    case getIdent pragid of
       "opaquePredicate" -> return (rules, M.singleton id OpaquePredicate, [], [])
       "openPredicate"   -> return (rules, M.singleton id OpenPredicate, [], [])
       _ -> return (rules, M.empty, [], [])

backgroundDecl rules _ = return (rules, M.empty, [],[])

backgroundAssert :: Int -> AssertDecl (Clause (Pn,IType)) -> M S.Cmd
backgroundAssert n (AxiomDecl cl) =
     do env <- clauseEnv M.empty cl
        cl' <- clause2smt env cl
        (_,bs) <- getBinders
        let nm = "axiom"++show n
        let x = S.Axiom nm (S.forall_ bs cl')
        return x
backgroundAssert n (LemmaDecl cl) =
     do env <- clauseEnv M.empty cl
        cl' <- clause2smt env cl
        (_,bs) <- getBinders
        let nm = "lemma"++show n
        let x = S.Lemma nm (S.forall_ bs cl')
        return x
backgroundAssert n (PresumeDecl (nm, arg) cl) =
    do return $ S.Information ""
    {-do sinfo <- liftComp (lookupTermIdent nm)
       let EventSymbol (EventDecl _ targs) = sym_info sinfo
       let args = unifytypes targs arg
       let newcl = (Clause dummy_pn (COr (Clause dummy_pn (CNot (Clause dummy_pn (CPred nm args)))) cl))
       env <- clauseEnv M.empty newcl
       cl' <- clause2smt env newcl
       (_,bs) <- getBinders
       let id = "presume"++show n
       let x = S.Axiom id (S.forall_ bs cl')
       return x
    where
      dummy_pn = Pn "" 0 0
      unifytypes targs@(PositionalArgs _) args@(PositionalArgs _) =
        PositionalArgs $ map aux $ zip [0..] $ zip (map type2itype $ extractArgs targs) (extractArgs args)
      unifytypes targs@(NamedArgs _) args@(PositionalArgs _) =
        PositionalArgs $ map aux $ zip [0..] $ zip (map type2itype $ extractArgs targs) (extractArgs args)
      unifytypes (NamedArgs l) (NamedArgs l') =
        PositionalArgs $ map (aux' l') $ zip [0..] l
      unifytypes (PositionalArgs _) (NamedArgs _) = error "event declared with positional args, but attempt to use with named args"
      aux (n, (t, Just s)) = (dummy_pn, TVar (dummy_pn, t) s)
      aux (n, (t, Nothing)) = (dummy_pn, TVar (dummy_pn, t) $ ".x"++show n)
      aux' l (n, (_,s, t)) =
        case find (\ (_,s',_) -> s == s') l of
          Just (_,_,Just x) -> (dummy_pn, TVar (dummy_pn, type2itype t) x)
          Just (_,_,Nothing) -> (dummy_pn, TVar (dummy_pn, type2itype t) $ ".x"++show n)
          Nothing -> error "shouldn't happen"-}

gentype2smt :: (ty -> M S.Type) -> (a -> M S.Type) -> Type' ty a -> M S.Type
gentype2smt g f (TypeName x args) =
     do args' <- mapM g args
        return (S.TApp (getIdent x) args')

gentype2smt g f TypeNumber = return S.Real
gentype2smt g f TypeString = return S.String
gentype2smt g f (TypeList t) =
     do t' <- g t
        return (S.List t')
gentype2smt g f (TypeSet t) =
     do t' <- g t
        return (S.Set t')
gentype2smt g f (TypeMap t1 t2) =
     do t1' <- g t1
        t2' <- g t2
        return (S.Map t1' t2')
gentype2smt g f (TypeTuple xs) = fmap S.Tuple $ mapM g xs
gentype2smt g f (TypeVar v) = f v

gtype2smt :: IType -> M S.Type
gtype2smt (IType t) = gentype2smt gtype2smt (\v ->
   do (pn,x) <- liftComp (lookupTypeVar v)
      case x of
         UnboundTyVar -> liftComp (errMsg pn $ "unbound type variable")
         RigidTyVar x -> return $ S.TVar x
         BoundTyVar ty -> gtype2smt ty) t

type2smt :: Type -> M S.Type
type2smt (Type _ t) = gentype2smt type2smt (return . S.TVar) t

clause2smt :: Map Var S.Ident -> Clause (a, IType) -> M S.Formula
clause2smt env (Clause _ (CEq t1 t2)) = do
    t1' <- term2smt env t1
    t2' <- term2smt env t2
    return $ S.Eq t1' t2'
clause2smt env (Clause _ (CNeq t1 t2)) = do
    t1' <- term2smt env t1
    t2' <- term2smt env t2
    return $ S.Not (S.Eq t1' t2')
clause2smt env (Clause _ (CAnd c1 c2)) = do
    c1' <- clause2smt env c1
    c2' <- clause2smt env c2
    return $ S.and [c1',c2']
clause2smt env (Clause _ (COr c1 c2)) = do
    c1' <- clause2smt env c1
    c2' <- clause2smt env c2
    return $ S.or [c1',c2']
clause2smt env (Clause _ (CNot c)) = do
    c' <- clause2smt env c
    return $ S.Not c'
clause2smt env (Clause _ (CEmpty)) = do
    return $ S.True
clause2smt env (Clause _ (CTrue)) = do
    return $ S.True
clause2smt env (Clause _ (CFalse)) = do
    return $ S.False
clause2smt env (Clause _ (CComp op t1 t2)) = do
    t1' <- term2smt env t1
    t2' <- term2smt env t2
    return $ S.Comp op t1' t2'
clause2smt env (Clause _ (CPred id args)) = do
    args' <- mapM (term2smt env) (extractArgs args)
    return $ S.Pred (S.IdName (getIdent id)) args'

clause2smt env (Clause _ (CFindall tm (pn,x,args) v)) = do
    (oldvs,oldbs) <- getBinders

    let (a,ty) = getAnnot tm
    ty' <- gtype2smt ty
    fr <- getFresh

    args' <- qgoal2smt env args
    (vs,bs) <- getBinders
    let env' = extend env vs (map S.bindVar bs)

    tm' <- term2smt env' tm

    setBinders (oldvs,oldbs)

    case M.lookup v env of
      Nothing -> error $ "missing variable! " ++ v
      Just v' -> return
        (S.Forall [S.Bind fr ty']
           (S.Iff
           (S.Pred (S.IdName "set_member") [S.EVar fr, S.EVar v'])
           (S.exists_ bs
               (S.And [ S.Eq (S.EVar fr) tm'
                      , S.Pred (S.IdName (getIdent x)) args'
                      ]
           )))
        )

qgoal2smt :: Map Var S.Ident -> Args (QTerm (a,IType)) -> M [S.Expr]
qgoal2smt env args = mapM f (extractArgs args)
  where f qtm = fmap snd $ qterm2smt env qtm

term2smt :: Map Var S.Ident -> Term (a,IType) -> M S.Expr
term2smt env (TVar _ v) =
     case M.lookup v env of
       Just n -> return $ S.EVar n
       Nothing -> error $ "FIXME: term2smt var " ++ v ++ " not in environment"
term2smt env (TTerm (_,ty) tm) =
   fmap fst $ runStateT (term'2smt (\t -> StateT $ \_ -> fmap (\x -> (x,())) $ term2smt env t) ty tm) ()

qterm2smt :: Map Var S.Ident -> QTerm (a,IType) -> M (Map Var S.Ident, S.Expr)
qterm2smt env (QBindVar (x,t) v) =
        do fr <- getFresh
           t' <- gtype2smt t
	   addBinder (Just v) (S.Bind fr t')
           let env' = M.insert v fr env
	   return $ (env', S.EVar fr)
qterm2smt env (QRefVar _ v) =
    case M.lookup v env of
       Just n -> return $ (env, S.EVar n)
       Nothing -> error $ "FIXME: var " ++ v ++ " not in environment"
qterm2smt env (QTerm (_,ty) tm) =
     do (tm',env') <- runStateT (term'2smt (\t -> StateT $ \e -> fmap (\(x,y) -> (y,x)) $ qterm2smt e t) ty tm) env
        return (env',tm')

term'2smt :: (tm -> StateT a M S.Expr) -> IType -> Term' tm -> StateT a M S.Expr
term'2smt tm2smt ty TAnonVar = lift $
        do fr <- getFresh
           ty' <- gtype2smt ty
	   addBinder Nothing (S.Bind fr ty')
	   return $ S.EVar fr
term'2smt tm2smt _ (TFunc x args) =
	do args' <- mapM tm2smt $ extractArgs args
           sym <- lift $ liftComp $ lookupTermIdent x
           case sym_info sym of
              FuncSymbol (FIConstructor _ _ _ _ _) ->
                   return $ S.DataApp (getIdent x) args'
              FuncSymbol _ ->
                   return $ S.App (S.IdName (getIdent x)) args'
              _ -> error $ "expected a function symbol: "++(getIdent x)

term'2smt tm2smt _ (TCons t1 t2) =
        do t1' <- tm2smt t1
           t2' <- tm2smt t2
           return $ S.Cons t1' t2'
term'2smt tm2smt _ (TTuple xs) =
        do xs' <- mapM tm2smt xs
           return $ S.ETuple xs'
term'2smt tm2smt _ (TList xs) = f xs
    where f [] = return $ S.Nil
          f (x:xs) = do x'  <- tm2smt x
                        xs' <- f xs
                        return $ S.Cons x' xs'
term'2smt tm2smt ty (TNegative t) =
        do t' <- tm2smt t
           return $ S.Neg t'
term'2smt _ _ (TNum _ s) = return $ S.NumLit s
term'2smt _ _ (TString s) = do
    i <- lift $ mkStrLit s
    return (S.EString i)

term'2smt tm2smt ty (TOp op t1 t2) = do
    t1' <- tm2smt t1
    t2' <- tm2smt t2
    return $ S.Op op t1' t2'


qc2smt :: Map Var S.Ident -> QClause (a,IType) -> M (Map Var S.Ident, S.Formula)
qc2smt env (QCPred _ x args) =
	do let f (e,xs) x = do (e',x') <- qterm2smt e x; return (e',xs++[x'])
           (env',args') <- foldM f (env,[]) $ extractArgs args
	   return $ (env',S.Pred (S.IdName (getIdent x)) args')
qc2smt env (QCEq _ t1 t2) =
	do (env1,e1) <- qterm2smt env t1
	   (env2,e2) <- qterm2smt env1 t2
   	   return $ (env2,S.Eq e1 e2)
qc2smt env (QCNeq _ t1 t2) =
	do (_,e1) <- qterm2smt env t1
	   (_,e2) <- qterm2smt env t2
   	   return $ (env,S.Not (S.Eq e1 e2))
qc2smt env (QCAnd qc1 qc2) =
        do (env1,qc1') <- qc2smt env qc1
           (env2,qc2') <- qc2smt env1 qc2
           return $ (env2,S.and [qc1',qc2'])
qc2smt env (QCComp _ op t1 t2) =
	do (_,e1) <- qterm2smt env t1
	   (_,e2) <- qterm2smt env t2
   	   return $ (env,S.Comp op e1 e2)


rule2smt :: Rule (a,IType) -> M S.Formula
rule2smt ((pn,id,gl),Clause _ CEmpty) = do
    env1 <- goalEnv M.empty gl
    gl' <- goal2smt env1 gl
    (_,bs) <- getBinders
    return (S.exists_ bs (S.and (gl')))
rule2smt ((pn,id,gl),cl) = do
    env1 <- goalEnv M.empty gl
    env  <- clauseEnv env1 cl
    gl' <- goal2smt env gl
    cl' <- clause2smt env cl
    (_,bs) <- getBinders
    return (S.exists_ bs (S.and (gl'++[cl'])))

goal2smt :: Map Var S.Ident -> Args (Term (a,IType)) -> M [S.Formula]
goal2smt env args = do
    args' <- mapM (term2smt env) (extractArgs args)
    let l = [(a,i) | (a, i) <- zip args' [0..], a /= S.EVar (S.IdArg i)]
    let xs = map (\ (a,i) -> S.Eq (S.EVar (S.IdArg i)) a) l -- (zip args' [0..])
    return xs

goalEnv :: Map Var S.Ident -> Args (Term (a,IType)) -> M (Map Var S.Ident)
goalEnv env gl = foldM goalEnv' env (zip [0..] $ extractArgs gl)
  where
    goalEnv' env (i, TVar (_,ty) v) =
        case M.lookup v env of
           Just _ -> return env
           Nothing -> return $ M.insert v (S.IdArg i) env
    goalEnv' env (i, t) = termEnv env t

clauseEnv :: Map Var S.Ident -> Clause (a,IType) -> M (Map Var S.Ident)
clauseEnv env (Clause _ (CEq t1 t2)) = termEnv env t1 >>= \env' -> termEnv env' t2
clauseEnv env (Clause _ (CNeq t1 t2)) = termEnv env t1 >>= \env' -> termEnv env' t2
clauseEnv env (Clause _ (CPred _ args)) = foldM termEnv env (extractArgs args)
clauseEnv env (Clause _ (CAnd c1 c2)) = clauseEnv env c1 >>= \env' -> clauseEnv env' c2
clauseEnv env (Clause _ (COr c1 c2)) = clauseEnv env c1 >>= \env' -> clauseEnv env' c2
clauseEnv env (Clause _ (CNot c)) = clauseEnv env c
clauseEnv env (Clause _ CEmpty) = return env
clauseEnv env (Clause _ CTrue) = return env
clauseEnv env (Clause _ CFalse) = return env
clauseEnv env (Clause _ (CComp _ t1 t2)) = termEnv env t1 >>= \env' -> termEnv env' t2
clauseEnv env (Clause _ (CFindall tm (_,x,args) v)) =
         do let (a,ty) = getAnnot tm
            env' <- termEnv env (TVar (a,IType (TypeSet ty)) v)
            return env'

termEnv :: Map Var S.Ident -> Term (a,IType) -> M (Map Var S.Ident)
termEnv env (TVar (_,ty) v) =
    case M.lookup v env of
       Just _ -> return env
       Nothing -> do
          v' <- getFresh
          ty' <- gtype2smt ty
          addBinder (Just v) (S.Bind v' ty')
          let env' = M.insert v v' env
          return env'
termEnv env (TTerm _ TAnonVar) = return env
termEnv env (TTerm _ (TFunc _ args)) = foldM termEnv env (extractArgs args)
termEnv env (TTerm _ (TCons hd tl)) = termEnv env hd >>= \env' -> termEnv env' tl
termEnv env (TTerm _ (TList xs)) = foldM termEnv env xs
termEnv env (TTerm _ (TTuple xs)) = foldM termEnv env xs
termEnv env (TTerm _ (TNum _ _)) = return env
termEnv env (TTerm _ (TString _)) = return env
termEnv env (TTerm _ (TOp _ t1 t2)) = termEnv env t1 >>= \env' -> termEnv env' t2
termEnv env (TTerm _ (TNegative t)) = termEnv env t
