{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements the logic for loading and parsing files
--   and handling import statements.
--
module FileLoad where

import System.IO

import qualified Parser
import Tokens( Pn(..) )
import AST
import Compile


-- | Parse the named file, unless it has previously been visited.
chaseImports :: String
             -> CodeOrigin
             -> [String]
	     -> IO ([String],[(Pn,String)],[FileDecl],[(Pn,CodeOrigin,String)])
chaseImports f orig visited =
    if (f `elem` visited)
       then return (visited,[],[],[])
       else do
           hdl <- openFile f ReadMode
           str <- hGetContents hdl
           let (errs,decls,docs) = Parser.runProgramParser f orig str
           (visited', errs2, decls', docs') <- scanDecls (f:visited) decls
           return (visited, errs++errs2, decls', docs++docs')


-- | Scan a list of parsed declarations, looking for import statments.
--   Chase down any named import files.
scanDecls :: [String] -> [FileDecl] 
          -> IO ([String],[(Pn,String)],[FileDecl],[(Pn,CodeOrigin,String)])
scanDecls visited [] = return (visited,[],[],[])
scanDecls visited ((_,orig,DImport x):decls) = do
    (visited',errs1,xs,docs1) <- chaseImports x orig visited
    (visited'',errs2,ys,docs2) <- scanDecls visited' decls
    return (visited'', errs1++errs2, xs ++ ys, docs1++docs2)
scanDecls visited (d:decls) = do
    (visited',errs,xs,docs) <- scanDecls visited decls
    return (visited', errs, d:xs, docs)

-- | Parse a list of input file names, keeping track of which files
--   have been previously visited so as not to parse them again.
chaseImportList :: [(CodeOrigin,String)] -> [String]
                -> IO ([String],[(Pn,String)],[FileDecl],[(Pn,CodeOrigin,String)])
chaseImportList [] visited = return (visited,[],[],[])
chaseImportList ((orig,f):fs) visited = do
   (visited',errs1,xs,docs1) <- chaseImports f orig visited
   (visited'',errs2,ys,docs2) <- chaseImportList fs visited'
   return (visited'',errs1++errs2, xs++ys, docs1++docs2)


-- | Given a list of inputs files, parse the input files,
--   chasing down any import files.  Return any parse errors, the list of
--   parsed declarations, and the list of documentation lines.
parseFiles :: [(CodeOrigin,String)] -> IO ([(Pn,String)], [FileDecl], [(Pn,CodeOrigin,String)])
parseFiles fs = do
   (_,errs,decls,docs) <- chaseImportList fs []
   return (errs,decls,docs)

-- | Parse the named file, and any referenced includes,
--   returning any error messages and the parsed declarations.
loadFile :: String -> IO ([(Pn,String)], [FileDecl])
loadFile nm = 
  do (err,ds,_) <- parseFiles [(UserCode,nm)] 
     return (err,ds)


parseFilesNonrecursive :: [(CodeOrigin,String)] -> IO ([(Pn,String)], [FileDecl], [(Pn,CodeOrigin,String)])
parseFilesNonrecursive [] = return ([],[],[])
parseFilesNonrecursive ((orig,f):fs) =
   do hdl <- openFile f ReadMode
      str <- hGetContents hdl
      let (errs,decls,docs) = Parser.runProgramParser f orig str
      (errs',decls',docs') <- parseFilesNonrecursive fs
      return (errs++errs', decls++decls', docs++docs')
