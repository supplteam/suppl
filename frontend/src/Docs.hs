{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module produces a very basic code documentation view of a Suppl program as HTML.
--   It does some basic hyperlinking and code highlighting. However, it doesn't do a great
--   job of pretty-printing code.  Probably we could do a better job of printing the code
--   if we worked with either the token stream instead of the AST, or with the raw source text.
--
--   Anyway, it is not immediately clear what we want from module documentation for these
--   types of programs, but here is a proof-of-concept.
module Docs where

import Control.Monad
import Data.List ( null, sortBy, intersect, intersperse, nub, findIndices, find, (\\), groupBy, isPrefixOf )
import Data.String
import Data.Monoid
import qualified Data.Map as M
import Data.Map ( Map )

import qualified Text.Parsec as P
import Text.Parsec ( (<|>) )

import Text.Markdown

import Blaze ( (!), Html, toHtml )
import qualified Blaze as B
import qualified Text.Blaze.Internal as BI
import qualified Text.Blaze.Html5 as H
import qualified Data.Text.Lazy as T

import Tokens( Pn(..) )
import AST
import Compile
import SymbolTable
import PConflict
import Rules
import NewConflicts ( choose )

type LocMap = Map Pn [String]

buildLocMap :: [PConflict] -> [(Pn,AssertDecl (Clause Pn))] -> LocMap
buildLocMap cs as =
   let m1 = foldr extendLocMap M.empty $ zip [0..] cs
       m2 = foldr extendLocMapAssert m1 $ zip [0..] as
    in m2
{-
buildLocMap2 :: [PConflict] -> [[Prog (Pn, IType)]] -> LocMap
buildLocMap2 cs rs =
   let m1 = foldr extendLocMap M.empty $ zip [0..] cs
       m2 = foldr extendLocMapRules m1 $ zip [0..] rs
    in m2
-}
buildLocMap2 :: [PConflict] -> [(Int, [(Int, Prog (Pn, IType))])] -> LocMap
buildLocMap2 cs rs =
  let m1 = foldr extendLocMap M.empty $ zip [0..] cs
      m2 = foldr aux m1 rs
   in m2
  where
    aux (n, l) m = foldr (aux' n) m $ concatMap (\(x, l) -> map (\z -> (x, z)) l) l
    aux' n (p, (_, _, DRule r@((pn, _, _), _))) = M.insertWith (++) pn ["rule"++show n++"_"++show p]
    aux' _ _ = id

extendLocMapRules :: (Int, [Prog (Pn, IType)]) -> LocMap -> LocMap
extendLocMapRules (n, l) m = foldr (aux n) m $ concatMap (\ (x, l) -> map (\z -> (x, z)) l) $ zip [0..] l
  where
    aux n (p, (_, _, DRule r@((pn, _, _), _))) = M.insertWith (++) pn ["rule"++show n++"_"++show p]
    aux _ _ = id

extendLocMapAssert :: (Int,(Pn,AssertDecl (Clause Pn))) -> LocMap -> LocMap
extendLocMapAssert (n,(pn,AxiomDecl _)) = id
extendLocMapAssert (n,(pn,PresumeDecl _ _)) = id
extendLocMapAssert (n,(pn,LemmaDecl _)) = M.insertWith (++) pn ["lemma"++show n]

extendLocMap :: (Int,PConflict) -> LocMap -> LocMap
extendLocMap (n,PConflict{ eventName = enm, act1 = a1, act2 = a2, common = cmn, div1 = d1, div2 = d2}) m =
    M.insertWith (++) (loc enm) ["conflict-common-"++show n] $
    M.insertWith (++) (loc a1)  ["conflict-path1-"++show n] $
    M.insertWith (++) (loc a2)  ["conflict-path2-"++show n] $
    extendLMPath ("conflict-common-"++show n) cmn $
    extendLMPath ("conflict-path1-"++show n) d1 $
    extendLMPath ("conflict-path2-"++show n) d2 $
    m

extendLMPath :: String -> [Guards] -> LocMap -> LocMap
extendLMPath clazz gs m = foldr (extendLMGuard clazz) m gs

extendLMGuard :: String -> Guards -> LocMap -> LocMap
extendLMGuard clazz g =
  case g of
    PGuards pn _ -> M.insertWith (++) pn [clazz]
    NGuards _ _ -> id
    FEGuards pn _ -> M.insertWith (++) pn [clazz]
    DefGuards pn -> M.insertWith (++) pn [clazz]
    EvGuards ident args -> M.insertWith (++) (loc ident) [clazz]

    --NGuards pn _ -> M.insertWith (++) pn [clazz]

data ConflictResult
  = Valid
  | Invalid
  | Timeout
  | Unknown String
  | Failure String
  | HighFailure
 deriving (Eq,Show)

proofResultsParser :: P.Parsec String (Map String ConflictResult) ()
proofResultsParser =
   do P.skipMany proofResult
      P.skipMany P.newline
      P.eof

proofResult :: P.Parsec String (Map String ConflictResult) ()
proofResult =
  do thy <- P.many (P.alphaNum <|> P.char '_')
     P.char '.'
     goal <- P.many P.alphaNum
     let nm = thy ++ "." ++ goal
     P.skipMany P.space
     P.char ':'
     P.skipMany P.space
     res <- parseResult
     P.skipMany $ P.noneOf "\n"
     P.newline
     P.modifyState (M.insertWith combineResult nm res)

parseResult :: P.Parsec String u ConflictResult
parseResult = P.choice
   [ P.string "Valid"   >> return Valid
   , P.string "Invalid" >> return Invalid
   , P.string "Timeout" >> return Timeout
   , P.string "Unknown" >> P.skipMany P.space >> P.between (P.char '(') (P.char ')') (fmap Unknown $ P.many $ P.noneOf ")\n")
   , P.string "Failure" >> P.skipMany P.space >> P.between (P.char '(') (P.char ')') (fmap Failure $ P.many $ P.noneOf ")\n")
   , P.string "HighFailure" >> return HighFailure
   ]

combineResult :: ConflictResult -> ConflictResult -> ConflictResult
combineResult Valid _ = Valid
combineResult _ Valid = Valid
combineResult x _ = x

buildConflictResults :: String -> Comp (Map String ConflictResult)
buildConflictResults res =
  case P.runParser (proofResultsParser >> P.getState) M.empty "proof results" res of
     Left perr -> fail (show perr)
     Right m -> return m

getAsserts :: [(Pn,CodeOrigin,Decl Pn)] -> [(Pn,AssertDecl (Clause Pn))]
getAsserts ((_,BuiltinCode,_):xs) = getAsserts xs
getAsserts ((pn,UserCode,DAssert a):xs) = (pn,a) : getAsserts xs
getAsserts ((_,UserCode,_) : xs) = getAsserts xs
getAsserts [] = []

getUnsolvedConflicts :: String -> [PConflict] -> ([(Integer, PConflict)], Bool)
getUnsolvedConflicts res conflicts =
  let m = case P.runParser (proofResultsParser >> P.getState) M.empty "proof results" res of
             Left perr -> error $ show perr
             Right m -> m
  in (concatMap (f m) $ zip [0..] conflicts, M.lookup "False.False" m == Just Valid)
  where
    f m (n, c) =
      case M.lookup ("Conflict"++show n++".impossible") m of
       Just Valid -> []
       _ -> [(n, c)]

buildConflictDocs
   :: String -> [PConflict] -> [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
buildConflictDocs proofResults conflicts files prog docs =
    do conflictResults <- buildConflictResults proofResults
       let asserts = getAsserts prog
       let asserts'   = filterAsserts conflictResults asserts
       let conflicts' = filterConflicts conflictResults conflicts

       let lm = buildLocMap conflicts asserts
       let buttons = (concatMap (mkAssertButton conflictResults) $ asserts')
                     ++
                     (concatMap (mkConflictButton conflictResults) $ conflicts')

       let buttondiv = B.div ! B.class_ (fromString "conflict-buttons")
                             ! B.id (fromString "conflict-buttons")
                        $ do B.span $ fromString (show (length buttons)++" potential conflicts/lemma failures")
                             B.br
                             B.br
                             B.form ! B.id (fromString "conflict-form")
                               $ if null buttons
                                    then BI.Empty
                                    else foldr BI.Append BI.Empty (clearButton:buttons)
       let conflictArray = "[" ++ (concat $ intersperse "," $ map (show . fst) conflicts') ++ "]"
       let lemmaArray = "[" ++ (concat $ intersperse "," $ map (show .fst) asserts') ++ "]"
       let ld = "javascript:buildConflictCSS("++conflictArray++","++lemmaArray++")"
       doBuildDocs lm (basicHead (Just ld)) buttondiv files prog docs (M.lookup "False.False" conflictResults == Just Valid)

buildConflictDocs2
   :: String -> [PConflict] -> [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Prog (Pn, IType) -> Comp String
buildConflictDocs2 proofResults conflicts files prog docs prog' =
    do conflictResults <- buildConflictResults proofResults
       if M.lookup "False0.False" conflictResults == Just Valid
         then --We know that in this case we don't need to worry about conflicts
              --There is at least one index such that false can be proven
           do let eq x =
                     case x of
                       (_,_,DAssert (AxiomDecl _)) -> True
                       (_,_,DAssert (LemmaDecl _)) -> True
                       _ -> False
              let axioms' = choose $ filter eq prog'
              let n = last $ findIndices (== Just Valid) (map (\n -> M.lookup ("False"++show n++".False") conflictResults) [0..length axioms' - 1])
              let axioms = axioms' !! n
              let ln = length axioms
              let axioms'' = filter (\(_,l) -> length l == ln) $ zip [0..] axioms'
              let res = filter (\(n,_) -> M.lookup ("False"++show n++".False") conflictResults == Just Valid) axioms''

              let lm = foldr (\(n, l) m -> foldr (\(pn,_,_) -> M.insertWith (++) pn ["rule0_"++ show n]) m l) M.empty res
              let btn x = do H.br
                             H.input
                                ! B.type_ (fromString "radio")
                                ! B.name (fromString "conflict")
                                ! B.value (fromString ("conflict0_"++show x))
                                ! B.onclick (fromString "javascript:setbody(this.value)")
                                ! B.id (fromString $ "button-conflict0_"++show x)
                             H.label
                                ! B.for (fromString $ "button-conflict0_"++show x)
                                $ fromString (" False")
              let btns = map (\(n, _) -> btn n) res
              let buttondiv = B.div ! B.class_ (fromString "conflict-buttons")
                                    ! B.id (fromString "conflict-buttons")
                               $ do B.span $ fromString ("Inconsistency detected, False is provable")
                                    B.br
                                    B.br
                                    B.form ! B.id (fromString "conflict-form")
                                      $ foldr BI.Append BI.Empty (clearButton:btns)
              let conflictArray = "["++ (concat $ intersperse "," $ map (\(n,_) -> "[0,"++show n++"]") res) ++"]"
              let ld = "javascript:buildDeepConflictCSS("++conflictArray++")"

              doBuildDocs lm (basicHead (Just ld)) buttondiv files prog docs False
         else do
           let bgs = zip [0..] $ map (\c -> snd $ getBackgrounds prog' c) conflicts
           let bgs' = map (\(n, l) -> (n, zip [0..] l)) bgs
           let numbers = concatMap (\ (n, l) -> map (\ m -> (n, fst m)) l) bgs'
           let numbers' = filterNumbers conflictResults numbers

           -- list of conflicts which all versions were solved, so need to output the original conflict at least
           let solved = map (fst . head) $ filter (\ l -> null $ filterNumbers' conflictResults l) (groupBy (\ x y -> fst x == fst y) numbers)

           let rules = rulesProg prog' --If all the rules of a predicate appear,
           let newbgs = filterBackgrounds numbers' bgs' --then it is not the cause of the conflict
           let newbgs' = map (removeBG . removeRules rules) newbgs --and we do not need to highlight it

           let numbers' = concatMap (\(n,l) -> map (\(m,_) -> (n,m)) l) newbgs'
           let lm = foldr extendLocMap (buildLocMap2 conflicts newbgs') $ (zip [0..] conflicts)
           --let lm = buildLocMap2 conflicts $ map (snd . getBackgrounds prog') conflicts
           let buttons = (concatMap (mkConflictButton2 conflictResults) numbers') ++ (concatMap mkConflictButton' solved)

           let buttondiv = B.div ! B.class_ (fromString "conflict-buttons")
                                 ! B.id (fromString "conflict-buttons")
                            $ do B.span $ fromString (show (length buttons)++" potential conflicts/lemma failures")
                                 B.br
                                 B.br
                                 B.form ! B.id (fromString "conflict-form")
                                   $ if null buttons
                                        then BI.Empty
                                        else foldr BI.Append BI.Empty (clearButton:buttons)

           let tupArray (x,y) = "["++show x++","++show y++"]"
           let conflictArray = "["++ (concat $ intersperse "," $ map tupArray numbers') ++"]"
           let solvedArray = show solved
           let ld = "javascript:buildDeepConflictCSS("++conflictArray++"," ++ solvedArray ++")"

           doBuildDocs lm (basicHead (Just ld)) buttondiv files prog docs False

removeBG :: (Int, [(Int, Prog (Pn, IType))]) -> (Int, [(Int, Prog (Pn, IType))])
removeBG (n, l) =
  case l of
    [] -> (n, l)
    [x] -> (n, l)
    (m, l'):xs ->
      let pns = map pn l'
          pns' = map (\(_,l) -> map pn l) xs
       in if find (\a -> null (a \\ pns)) pns' == Nothing then let (a, b) = removeBG (n, xs) in (a, (m, l'):b) else removeBG (n, xs)
  where pn (a,_,_) = a

-- Check if all the rules of a predicate appear
-- if so, do not highlight it
removeRules :: Map Ident [(Pn, CodeOrigin, Decl (Pn, IType))] -> (Int, [(Int, Prog (Pn, IType))]) -> (Int, [(Int, Prog (Pn, IType))])
removeRules rules (n, l) =
  case l of
    [] -> (n, l)
    [x] -> (n, l)
    (m, l'):xs ->
      let preds = map (\(_, _, DRule r@((_, nm, _), _)) -> nm) l' in
      let q = g rules preds l in
      (n, map (\(m, x) -> (m, filter (\(_, _, DRule r@((_, nm, _), _)) -> elem nm q) x)) l)
  where
    g rules (nm:nms) l =
      let decls = (M.!) rules nm
          ll = concatMap snd l
       in if length (decls `intersect` ll) == length decls && length decls > 1 then g rules nms l else nm:(g rules nms l)
    g rules [] l = []

filterBackgrounds :: [(Int, Int)] -> [(Int, [(Int, Prog (Pn, IType))])] -> [(Int, [(Int, Prog (Pn, IType))])]
filterBackgrounds numbers bgs =
  map (\(n, l) -> (n, filter (\(m, l') -> elem (n, m) numbers) l)) bgs

filterConflicts :: Map String ConflictResult -> [PConflict] -> [(Int,PConflict)]
filterConflicts proofResults cs = filter f $ zip [0..] cs
 where f (n,_) = (M.lookup ("Conflict"++show n++".impossible") proofResults) /= Just Valid

filterAsserts :: Map String ConflictResult -> [(Pn,AssertDecl (Clause Pn))] -> [(Int,(Pn,AssertDecl (Clause Pn)))]
filterAsserts proofResults as = filter f $ zip [0..] as
 where f (n,(_,LemmaDecl _)) = (M.lookup ("Background.lemma"++show n) proofResults) /= Just Valid
       f _ = False

filterNumbers :: Map String ConflictResult -> [(Int,Int)] -> [(Int,Int)]
filterNumbers proofResults = filter f
 where f (n,m) =
             case M.lookup ("Conflict"++show n++"_"++show m++".impossible") proofResults of
                Nothing -> False
                Just Valid -> False
                _ -> True

filterNumbers' :: Map String ConflictResult -> [(Int,Int)] -> [(Int,Int)]
filterNumbers' proofResults = filter f
 where f (n,m) =
             case M.lookup ("Conflict"++show n++"_"++show m++".impossible") proofResults of
                Nothing -> True
                Just Valid -> False
                _ -> True

clearButton :: Html
clearButton =
   do H.input
       ! B.type_ (fromString "radio")
       ! B.name (fromString "conflict")
       ! B.value (fromString "")
       ! B.onclick (fromString "javascript:setbody(this.value)")
       ! B.id (fromString "button-clear")
      H.label
       ! B.for (fromString "button-clear")
       $ fromString (" clear")

mkAssertButton :: Map String ConflictResult -> (Int,(Pn,AssertDecl (Clause Pn))) -> [Html]
mkAssertButton mp (n,(_,AxiomDecl _)) = []
mkAssertButton mp (n,(_,PresumeDecl _ _)) = []
mkAssertButton mp (n,(_,LemmaDecl _)) = [btn]
 where btn = let html = do H.input
                              ! B.type_ (fromString "radio")
                              ! B.name (fromString "conflict")
                              ! B.value (fromString ("lemma"++show n))
                              ! B.onclick (fromString "javascript:setbody(this.value)")
                              ! B.id (fromString $ "button-lemma"++show n)
                           H.label
                              ! B.for (fromString $ "button-lemma"++show n)
                              $ fromString (" Lemma" ++ show n)
              in do H.br
                    H.a
                       ! B.href (fromString $ "#lemma"++show n)
                       $ html

mkConflictButton :: Map String ConflictResult -> (Int,PConflict) -> [Html]
mkConflictButton mp (n,_) = [btn]
 where btnName = "Conflict"++show n
       btn = do H.br
                H.input
                   ! B.type_ (fromString "radio")
                   ! B.name (fromString "conflict")
                   ! B.value (fromString ("conflict"++show n))
                   ! B.onclick (fromString $ "javascript:setbody(this.value,"++show n ++")")
                   ! B.id (fromString $ "button-conflict"++show n)
                H.label
                   ! B.for (fromString $ "button-conflict"++show n)
                   $ fromString (" " ++ btnName)

mkConflictButton' :: Int -> [Html]
mkConflictButton' n = [btn]
  where btnName = "Conflict"++show n
        btn = do H.br
                 H.input
                    ! B.type_ (fromString "radio")
                    ! B.name (fromString "conflict")
                    ! B.value (fromString ("conflict"++show n))
                    ! B.onclick (fromString $ "javascript:setbody(this.value,"++show n ++ ")")
                    ! B.id (fromString $ "button-conflict"++show n)
                 H.label
                    ! B.for (fromString $ "button-conflict"++show n)
                    $ fromString (" " ++ btnName)

mkConflictButton2 :: Map String ConflictResult -> (Int,Int) -> [Html]
mkConflictButton2 mp (n,m) = [btn]
 where btnName = "Conflict"++show n++"_"++show m
       btn = do H.br
                H.input
                   ! B.type_ (fromString "radio")
                   ! B.name (fromString "conflict")
                   ! B.value (fromString ("conflict"++show n++"_"++show m))
                   ! B.onclick (fromString $ "javascript:setbody(this.value," ++ show n ++ ")")
                   ! B.id (fromString $ "button-conflict"++show n++"_"++show m)
                H.label
                   ! B.for (fromString $ "button-conflict"++show n++"_"++show m)
                   $ fromString (" " ++ btnName)

buildDocs :: [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String
buildDocs files prog docs = doBuildDocs M.empty (basicHead Nothing) BI.Empty files prog docs False

basicHead :: Maybe String -> Html
basicHead onld =
  do H.title (fromString "SUPPL Module Documentation")
     H.script ! B.type_ (fromString "text/javascript")
              ! B.src (fromString "suppldocs.js")
              $ BI.Empty
     H.link ! B.rel (fromString "stylesheet")
            ! B.type_ (fromString "text/css")
            ! B.href (fromString "suppldocs.css")
            ! maybe mempty (B.onload . fromString) onld


-- the boolean is used to know whether we have to print the warning (false is provable)
doBuildDocs :: LocMap -> Html -> Html -> [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Bool -> Comp String
doBuildDocs lm head bodyHeader files prog docs b = do
    sytab <- fmap comp_st $ compGetState
    let doclines = sortBy (\(x,_,_) (y,_,_) -> compare x y)
                     $ filter (\ (Pn f _ _,_,_) -> f `elem` (map snd files))
                     $ docs

    let warn = markdown def $ T.pack $ unlines ["# ERROR : FALSE IS PROVABLE #"]

    let html = if b then warn : (blockDocs lm sytab 0 [] $ mergeDocs doclines prog)
                    else blockDocs lm sytab 0 [] $ mergeDocs doclines prog

    let html' = B.docTypeHtml $ do
                    H.head $ head
                    H.body (foldr BI.Append BI.Empty (bodyHeader : html))
    return $ T.unpack $ B.renderHtml html'

-- strip all leading % signs and the first space, if there is one
stripComments :: String -> String
stripComments ('%':xs) = stripComments xs
stripComments (' ':xs) = xs
stripComments xs = xs

mergeDocs :: [(Pn,CodeOrigin,String)] -> [(Pn, CodeOrigin, Decl Pn)] -> [(Pn, Either String (Decl Pn))]
mergeDocs ((p,BuiltinCode,x):xs) ds = mergeDocs xs ds  -- ignore builtin documentation
mergeDocs xs ((p',BuiltinCode,d):ds) = mergeDocs xs ds -- ignore builtin declarations
mergeDocs [] ds = map (\ (pn,_,d) -> (pn, Right d)) ds
mergeDocs xs [] = map (\ (pn,_,x) -> (pn, Left (stripComments x))) xs
mergeDocs ((p,UserCode,x):xs) ((p',UserCode,d):ds) =
       if p <= p'
            then (p, Left (stripComments x))  : mergeDocs xs ((p',UserCode,d):ds)
            else (p',Right d) : mergeDocs ((p,UserCode,x):xs) ds

blockDocs :: LocMap -> SymbolTable -> Int -> [String] -> [(Pn, Either String (Decl Pn))] -> [Html]
blockDocs lm sytab n ls [] = if null ls then [] else [markdown def $ T.pack $ unlines ls]
blockDocs lm sytab n ls ((Pn f ln c, Left str):xs) =
    if (ln - n) > 1
       then (markdown def $ T.pack $ unlines ls) : blockDocs lm sytab ln [str] xs
       else blockDocs lm sytab ln (ls++[str]) xs
blockDocs lm sytab n ls ((pn@(Pn f ln c), Right d):xs) =
    if null ls
       then declToHtml lm sytab pn d : blockDocs lm sytab ln [] xs
       else (markdown def $ T.pack $ unlines ls) : declToHtml lm sytab pn d : blockDocs lm sytab ln [] xs

code :: Html -> Html
code x = H.pre ! B.class_ (fromString "suppl") $ x

lb :: Int -> Html
lb n = toHtml ("\n"++replicate n ' ')

kw :: String -> Html
kw nm = H.span ! B.class_ (fromString "keyword") $ toHtml nm

qvar :: String -> Html
qvar v = H.span ! B.class_ (fromString "qvar") $ fromString ("?"++v)

var :: String -> Html
var v = H.span ! B.class_ (fromString "var") $ fromString v

tyvar :: (Pn,String) -> Html
tyvar (_,v) = H.span ! B.class_ (fromString "typevar") $ fromString v

label :: String -> Html
label nm = H.span ! B.class_ (fromString "label") $ toHtml nm

lmSpan :: LocMap -> Pn -> Html -> Html
lmSpan lm pn html =
   case M.lookup pn lm of
      Nothing -> html
      Just cs -> H.span ! B.class_ (fromString $ unwords cs)
                        $ aux (filter (isPrefixOf "conflict-common-") cs) html
                        -- $ H.a ! B.id (fromString (head cs)) $ H.a ! B.id (fromString "test") $ html
   where aux l html = case l of
                       [] -> html
                       (x:xs) -> H.a ! B.id (fromString x) $ aux xs html

identClass :: SymbolTable -> Ident -> B.Attribute
identClass st ident =
  case st_lookup ident st of
     Nothing -> B.class_ $ fromString "UNKNOWN"
     Just sym -> B.class_ $ fromString $
       case sym_info sym of
         FuncSymbol (FIDecl _ _) -> "funcident"
         FuncSymbol (FIConstructor _ _ _ _ _) -> "datacon"
         PredSymbol _ _ -> "predident"
         EventSymbol _ -> "eventident"
         TableSymbol _ _ -> "tableident"
         ProcedureSymbol (ProcedureDecl _ _) -> "procident"
         ProcedureSymbol (PrimProcedureDecl _ _) -> "actionident"


declToHtml :: LocMap -> SymbolTable -> Pn -> Decl Pn -> Html
declToHtml lm st pn decl =
  case decl of
    DType td -> typeDeclToHtml st td
    DPred pd -> predDeclToHtml st pd
    DTable td -> tableDeclToHtml st td
    DEvent ed -> eventDeclToHtml st ed
    DProcedure pd -> procDeclToHtml st pd
    DMode md -> modeDeclToHtml st md
    DIndex id -> indexDeclToHtml st id
    DFunction fd -> funcDeclToHtml st fd
    DData dd -> dataDeclToHtml st dd
    DRule r -> ruleToHtml lm st r
    DHandler h -> handlerToHtml lm st h
    DAssert a -> assertToHtml lm st pn a
    DImport str -> importToHtml st str
    DProcDefn p -> procDefnToHtml lm st p
    DConflict c -> conflictToHtml st c
    DPragma p -> pragmaToHtml st p

pragmaToHtml :: SymbolTable -> Pragma Pn -> Html
pragmaToHtml st (GenericPragma ident Nothing) =
  code $
   do kw "pragma"
      toHtml " "
      toHtml (getIdent ident)
      toHtml "."
pragmaToHtml st (GenericPragma ident (Just pt)) =
  code $
   do kw "pragma"
      toHtml " "
      toHtml (getIdent ident)
      toHtml " "
      pretermToHtml pt
      toHtml "."
pragmaToHtml st (QueryPragma pn n cl) =
  code $
   do kw "pragma"
      toHtml " "
      kw "query"
      toHtml " "
      toHtml n
      toHtml " "
      clauseToHtml st cl
      toHtml "."
pragmaToHtml st (HandlePragma pn ev acts) =
  code $
   do kw "pragma"
      toHtml " "
      kw "handle"
      toHtml " "
      basicFormToHtml st ev
      toHtml " [ "
      sequence_ $ Data.List.intersperse (toHtml ", ") $ map (basicFormToHtml st) acts
      toHtml " ]."

basicFormToHtml :: SymbolTable -> BasicForm (Term Pn) -> Html
basicFormToHtml st (BasicForm pn ident args) =
   do toHtml (getIdent ident)
      argsToHtml (termToHtml st) args

pretermToHtml :: PreTerm -> Html
pretermToHtml pt =
  case pt of
    PTCommaSequence pn xs -> sequence_ $ intersperse (toHtml ", ") $ map pretermToHtml xs
    PTList _ xs -> toHtml "[" >> (sequence_ $ intersperse (toHtml ", ") $ map pretermToHtml xs) >> toHtml "]"
    PTCons _ h t -> toHtml "[ " >> pretermToHtml h >> toHtml " | " >> pretermToHtml t >> toHtml " ]"
    PTUnderscore pn -> toHtml "_"
    PTApplied _ ident args -> toHtml (getIdent ident) >> argsToHtml pretermToHtml args
    PTNumLit pn str -> toHtml str
    PTStringLit pn str -> toHtml (show str)
    PTVBar pn x y -> toHtml "(" >> pretermToHtml x >> toHtml " | " >> pretermToHtml y >> toHtml ")"
    PTOp pn op x y -> toHtml "(" >> pretermToHtml x >> opToHtml op >> pretermToHtml y >> toHtml ")"
    PTCmpOp pn op x y -> toHtml "(" >> pretermToHtml x >> cmpOpToHtml op >> pretermToHtml y >> toHtml ")"
    PTNegative pn x -> toHtml "~" >> pretermToHtml x
    PTEq pn x y -> pretermToHtml x >> toHtml " = " >> pretermToHtml y
    PTNeq pn x y -> pretermToHtml x >> toHtml " <> " >> pretermToHtml y
    PTVar pn v -> var v
    PTQVar pn v -> qvar v
    PTArrow pn x y -> pretermToHtml x >> toHtml " -> " >> pretermToHtml y
    PTNot pn x -> kw "not" >> toHtml "(" >> pretermToHtml x >> toHtml ")"
    PTEmpty _ -> BI.Empty
    PTFalse _ -> kw "false"
    PTTrue _ -> kw "true"
    PTFindall pn x (_,ident,args) v ->
        do kw "findall"
           toHtml "("
           pretermToHtml x
           toHtml ", "
           toHtml (getIdent ident)
           argsToHtml pretermToHtml args
           toHtml ", "
           var v
           toHtml ")"


conflictToHtml :: SymbolTable -> ConflictDecl (Clause Pn) -> Html
conflictToHtml st (ConflictDecl (ident1,args1) (ident2,args2) (Clause _ CEmpty)) =
  code $
   do kw "conflict"
      toHtml " "
      H.span ! B.id (fromString (getIdent ident1))
             $ fromString (getIdent ident1)
      argsToHtml maybeVarToHtml args1
      toHtml ", "
      H.span ! B.id (fromString (getIdent ident2))
             $ fromString (getIdent ident2)
      argsToHtml maybeVarToHtml args2
      toHtml "."
conflictToHtml st (ConflictDecl (ident1,args1) (ident2,args2) cl) =
  code $
   do kw "conflict"
      toHtml " "
      H.span ! B.id (fromString (getIdent ident1))
             $ fromString (getIdent ident1)
      argsToHtml maybeVarToHtml args1
      toHtml ", "
      H.span ! B.id (fromString (getIdent ident2))
             $ fromString (getIdent ident2)
      argsToHtml maybeVarToHtml args2
      toHtml " => "
      lb 2
      clauseToHtml st cl
      toHtml "."
conflictToHtml st (ForbidDecl (ident1,args1) (Clause _ CEmpty)) =
  code $
   do kw "forbid"
      toHtml " "
      H.span ! B.id (fromString (getIdent ident1))
             $ fromString (getIdent ident1)
      argsToHtml maybeVarToHtml args1
      toHtml "."
conflictToHtml st (ForbidDecl (ident1,args1) cl) =
  code $
   do kw "forbid"
      toHtml " "
      H.span ! B.id (fromString (getIdent ident1))
             $ fromString (getIdent ident1)
      argsToHtml maybeVarToHtml args1
      toHtml " => "
      lb 2
      clauseToHtml st cl
      toHtml "."

procDefnToHtml :: LocMap -> SymbolTable -> ProcDefn Pn -> Html
procDefnToHtml lm st (ident,args,body) =
  code $
   do kw "define procedure"
      toHtml " "
      H.span ! B.id (fromString (getIdent ident))
             $ fromString (getIdent ident)
      argsToHtml maybeVarToHtml args
      toHtml " :="
      lb 2
      bodyToHtml lm st 2 body
      lb 0
      kw "end"
      toHtml "."


handlerToHtml :: LocMap -> SymbolTable -> Handler Pn -> Html
handlerToHtml lm st (ident,args,body) =
  code $
   do lmSpan lm (loc ident) $
        do kw "handle"
           toHtml " "
           H.a ! identClass st ident
               ! B.href (fromString ("#"++(getIdent ident)))
               $ fromString (getIdent ident)
           argsToHtml maybeVarToHtml args
      toHtml " => "
      lb 2
      bodyToHtml lm st 2 body
      lb 0
      kw "end"
      toHtml "."

maybeVarToHtml :: Maybe String -> Html
maybeVarToHtml Nothing = toHtml "_"
maybeVarToHtml (Just x) = qvar x


bodyToHtml :: LocMap -> SymbolTable -> Int -> HBody Pn -> Html
bodyToHtml lm st n (HBody pn b) =
  case b of
    HProcedure ident args ->
        lmSpan lm (loc ident) $
        do H.a ! B.href (fromString ("#"++(getIdent ident)))
               ! identClass st ident
               $ fromString (getIdent ident)
           argsToHtml (termToHtml st) args
           toHtml ";"
    HSeq b1 b2 -> bodyToHtml lm st n b1 >> lb n >> bodyToHtml lm st n b2
    HInsert args ident ->
        lmSpan lm pn $
        do kw "queue insert"
           toHtml " "
           argsToHtml (termToHtml st) args
           toHtml " "
           kw "into"
           toHtml " "
           H.a ! B.href (fromString ("#"++(getIdent ident)))
               ! identClass st ident
               $ fromString (getIdent ident)
           toHtml ";"
    HDelete args ident ->
        lmSpan lm pn $
        do kw "queue delete"
           toHtml " "
           argsToHtml (termToHtml st) args
           toHtml " "
           kw "from"
           toHtml " "
           H.a ! B.href (fromString ("#"++(getIdent ident)))
               ! identClass st ident
               $ fromString (getIdent ident)
           toHtml ";"

    HSkip -> kw "skip" >> toHtml ";"

    HQuery qs ->
       let (cs, bs) = unzip $ map (queryBranchToHtml lm st n) qs
           cs' = nub $ concat $ cs
        in do if null cs'
                then kw "query"
                else H.span
                        ! B.class_ (fromString $ unwords ("keyword":cs'))
                        $ fromString "query"
              lb n
              sequence_ bs
              kw "end"
              toHtml ";"

    HForeach q bd ->
        do lmSpan lm pn $
              do kw "foreach"
                 toHtml " "
                 qclauseToHtml st q
                 toHtml " =>"
           lb (n+2)
           bodyToHtml lm st (n+2) bd
           lb n
           kw "end"
           toHtml ";"

queryBranchToHtml :: LocMap -> SymbolTable -> Int -> (Pn,QBranch Pn) -> ([String],Html)
queryBranchToHtml lm st n (pn,QueryBranch q b) =
  ( maybe [] id $ M.lookup pn lm
  , do lmSpan lm pn $
           do toHtml "| "
              qclauseToHtml st q
              toHtml " =>"
       lb (n+4)
       bodyToHtml lm st (n+4) b
       lb n
  )
queryBranchToHtml lm st n (pn,DefaultBranch b) =
  ( maybe [] id $ M.lookup pn lm
  , do lmSpan lm pn $ toHtml "| _ =>"
       lb (n+4)
       bodyToHtml lm st (n+4) b
       lb n
  )

qclauseToHtml :: SymbolTable -> QClause Pn -> Html
qclauseToHtml st qc0 = f qc0
 where
  f qc =
    case qc of
       QCEq _ x y -> qtermToHtml st x >> toHtml " = " >> qtermToHtml st y
       QCNeq _ x y -> qtermToHtml st x >> toHtml " <> " >> qtermToHtml st y
       QCAnd x y -> f x >> toHtml ", " >> f y
       QCComp _ op x y -> qtermToHtml st x >> cmpOpToHtml op >> qtermToHtml st y
       QCPred _ ident args ->
          do H.a ! B.href (fromString ("#"++getIdent ident))
                 ! identClass st ident
                 $ fromString (getIdent ident)
             argsToHtml (qtermToHtml st) args

importToHtml :: SymbolTable -> String -> Html
importToHtml st str =
  code $
   do kw "import"
      toHtml " "
      toHtml (show str)
      toHtml "."

assertToHtml :: LocMap -> SymbolTable -> Pn -> AssertDecl (Clause Pn) -> Html
assertToHtml lm st pn (AxiomDecl cl) =
  code $ lmSpan lm pn $
   do kw "axiom"
      toHtml " "
      clauseToHtml st cl
      toHtml "."
assertToHtml lm st pn (LemmaDecl cl) =
  code $ lmSpan lm pn $
   do kw "lemma"
      toHtml " "
      clauseToHtml st cl
      toHtml "."
assertToHtml lm st pn (PresumeDecl (ident1,args1) cl) =
  code $ lmSpan lm pn $
   do kw "presume"
      toHtml " "
      H.span ! B.id (fromString (getIdent ident1))
             $ fromString (getIdent ident1)
      argsToHtml maybeVarToHtml args1
      toHtml " => "
      lb 2
      clauseToHtml st cl
      toHtml "."

goalToHtml :: SymbolTable -> Goal Pn -> Html
goalToHtml st (pn,ident,args) =
   do H.a ! B.href (fromString ("#"++(getIdent ident)))
          ! identClass st ident
          $ fromString (getIdent ident)
      argsToHtml (termToHtml st) args

clauseToHtml :: SymbolTable -> Clause Pn -> Html
clauseToHtml st (Clause pn cl) =
  case cl of
    CEq x y -> termToHtml st x >> toHtml " = " >> termToHtml st y
    CNeq x y -> termToHtml st x >> toHtml " <> " >> termToHtml st y
    CPred ident args ->
         do H.a ! B.href (fromString ("#"++(getIdent ident)))
                ! identClass st ident
                $ fromString (getIdent ident)
            argsToHtml (termToHtml st) args
    CAnd c1 c2 -> clauseToHtml st c1 >> toHtml ", " >> clauseToHtml st c2
    COr c1 c2 -> toHtml "( " >> clauseToHtml st c1 >> toHtml " | " >> clauseToHtml st c2 >> toHtml " )"
    CNot c -> kw "not" >> toHtml "( " >> clauseToHtml st c >> toHtml " )"
    CEmpty -> BI.Empty
    CTrue  -> kw "true"
    CFalse -> kw "false"
    CComp op x y -> termToHtml st x >> cmpOpToHtml op >> termToHtml st y
    CFindall tm (pn,ident,args) v ->
           do kw "findall"
              toHtml "( "
              termToHtml st tm
              toHtml ", "
              H.a ! B.href (fromString ("#"++(getIdent ident)))
                  ! identClass st ident
                  $ fromString (getIdent ident)
              argsToHtml (qtermToHtml st) args
              toHtml ", "
              var v
              toHtml " )"

cmpOpToHtml :: CmpOP -> Html
cmpOpToHtml OpLE = toHtml " <= "
cmpOpToHtml OpLT = toHtml " < "
cmpOpToHtml OpGE = toHtml " >= "
cmpOpToHtml OpGT = toHtml " > "

termToHtml :: SymbolTable -> Term Pn -> Html
termToHtml st (TTerm pn tm) = termToHtml' st (termToHtml st) tm
termToHtml st (TVar pn v) = var v

qtermToHtml :: SymbolTable -> QTerm Pn -> Html
qtermToHtml st (QTerm pn tm) = termToHtml' st (qtermToHtml st) tm
qtermToHtml st (QRefVar pn v) = var v
qtermToHtml st (QBindVar pn v) = qvar v

termToHtml' :: SymbolTable -> (tm -> Html) -> Term' tm -> Html
termToHtml' st f t =
  case t of
    TAnonVar -> toHtml "_"
    TFunc ident args ->
       do H.a ! B.href (fromString ("#"++(getIdent ident)))
              ! identClass st ident
              $ fromString (getIdent ident)
          argsToHtml f args
    TCons h t -> toHtml "[ " >> f h >> toHtml " | " >> f t >> toHtml " ]"
    TList xs -> toHtml "[ " >> commaList (map f xs) >> toHtml " ]"
    TTuple xs -> toHtml "( " >> commaList (map f xs) >> toHtml " )"
    TNum str nlit -> numLitToHtml str --numLitToHtml nlit
    TString str -> strLitToHtml str
    TOp op x y -> toHtml "(" >> f x >> opToHtml op >> f y >> toHtml ")"
    TNegative x -> toHtml "~" >> f x


opToHtml :: OP -> Html
opToHtml OpPlus = toHtml " + "
opToHtml OpMinus = toHtml " - "
opToHtml OpTimes = toHtml " * "
opToHtml OpDiv = toHtml " / "
opToHtml OpConcat = toHtml " ++ "

numLitToHtml :: String -> Html
numLitToHtml str = H.span ! B.class_ (fromString "numlit") $ (fromString str)

--numLitToHtml :: NumericLiteral -> Html
--numLitToHtml (LitInteger x)  = H.span ! B.class_ (fromString "numlit") $ toHtml (show x)
--numLitToHtml (LitRational x) = H.span ! B.class_ (fromString "numlit") $ toHtml (showRational x)

strLitToHtml :: String -> Html
strLitToHtml s = H.span ! B.class_ (fromString "strlit") $ toHtml (show s)

commaList :: [Html] -> Html
commaList [] = BI.Empty
commaList xs = foldr1 (\x y -> x >> toHtml ", " >> y) xs


ruleToHtml :: LocMap -> SymbolTable -> Rule Pn -> Html
ruleToHtml lm st (g@(pn, _, _), Clause _ CEmpty) =
  code $ lmSpan lm (pn) $
       do goalToHtml st g
          toHtml "."
ruleToHtml lm st (g@(pn, _, _), cl) =
  code $ lmSpan lm (pn) $
   do goalToHtml st g
      toHtml " :-"
      lb 2
      clauseToHtml st cl
      toHtml "."

dataDeclToHtml :: SymbolTable -> DataDecl -> Html
dataDeclToHtml st (DataDecl ident vs []) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "data"
      toHtml " "
      H.span ! B.class_ (fromString "type")
             $ fromString (getIdent ident)
      if null vs
         then BI.Empty
         else do toHtml "("
                 sequence_ $ intersperse (toHtml ", ") $ map tyvar vs
                 toHtml ")"
      toHtml " ::= ."
dataDeclToHtml st (DataDecl ident vs bs) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "data"
      toHtml " "
      H.span ! B.class_ (fromString "type")
             $ fromString (getIdent ident)
      if null vs
         then BI.Empty
         else do toHtml "("
                 sequence_ $ intersperse (toHtml ", ") $ map tyvar vs
                 toHtml ")"
      lb 1
      toHtml "::= "
      let bs' = map dataBranchToHtml bs
      foldr1 (\x y -> x >> lb 3 >> toHtml "| " >> y) bs'
      toHtml "."

dataBranchToHtml :: (Ident, Args Type) -> Html
dataBranchToHtml (ident,tys) =
  do H.a ! B.class_ (fromString "datacon")
         ! B.id (fromString (getIdent ident))
         $ fromString (getIdent ident)
     argsToHtml typeToHtml tys

typeDeclToHtml :: SymbolTable -> TypeDecl -> Html
typeDeclToHtml st (PrimTypeDecl ident) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "primitive type"
      toHtml " "
      H.span ! B.class_ (fromString "type")
             $ fromString (getIdent ident)
      toHtml "."
typeDeclToHtml st (TypeDecl ident args ty) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "type"
      toHtml " "
      H.span ! B.class_ (fromString "type")
             $ fromString (getIdent ident)
      if null args
         then BI.Empty
         else do toHtml "("
                 sequence_ $ intersperse (toHtml ", ") $ map tyvar args
                 toHtml ")"
      toHtml " := "
      typeToHtml ty
      toHtml "."

funcDeclToHtml :: SymbolTable -> FunctionDecl -> Html
funcDeclToHtml st (PrimFunctionDecl ident args ret) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "primitive function"
      toHtml " "
      H.span ! identClass st ident $ fromString (getIdent ident)
      argsToHtml typeToHtml args
      toHtml " "
      kw "yields"
      toHtml " "
      typeToHtml ret
      toHtml "."

modeDeclToHtml :: SymbolTable -> ModeDecl -> Html
modeDeclToHtml st (ModeDecl ident args) =
  code $
   do kw "mode"
      toHtml " "
      H.a ! identClass st ident
          ! B.href (fromString ("#"++getIdent ident))
          $ fromString (getIdent ident)
      argsToHtml modeToHtml args
      toHtml "."

indexDeclToHtml :: SymbolTable -> IndexDecl -> Html
indexDeclToHtml st (IndexDecl ident args) =
  code $
   do kw "index"
      toHtml " "
      H.a ! identClass st ident
          ! B.href (fromString ("#"++getIdent ident))
          $ fromString (getIdent ident)
      argsToHtml modeToHtml args
      toHtml "."

procDeclToHtml :: SymbolTable -> ProcedureDecl -> Html
procDeclToHtml st (ProcedureDecl ident args) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "procedure"
      toHtml " "
      H.span ! identClass st ident $ fromString (getIdent ident)
      argsToHtml typeToHtml args
      toHtml "."
procDeclToHtml st (PrimProcedureDecl ident args) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "action"
      toHtml " "
      H.span ! identClass st ident $ fromString (getIdent ident)
      argsToHtml typeToHtml args
      toHtml "."

eventDeclToHtml :: SymbolTable -> EventDecl -> Html
eventDeclToHtml st (EventDecl ident args) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "event"
      toHtml " "
      H.span ! identClass st ident $ fromString (getIdent ident)
      argsToHtml typeToHtml args
      toHtml "."

tableDeclToHtml :: SymbolTable -> TableDecl -> Html
tableDeclToHtml st (TableDecl ident tys key life) =
  code ! B.id (fromString (getIdent ident)) $
   do kw "table"
      toHtml " "
      H.span ! identClass st ident $ fromString (getIdent ident)
      argsToHtml typeToHtml tys
      keyToHtml key
      maybe BI.Empty lifeToHtml life
      toHtml "."

keyToHtml :: Args Mode -> Html
keyToHtml args = toHtml " " >> kw "key" >> toHtml " " >> argsToHtml modeToHtml args

lifeToHtml :: String -> Html
lifeToHtml str = toHtml " " >> kw "lifetime" >> toHtml " " >> toHtml str

predDeclToHtml :: SymbolTable -> PredDecl -> Html
predDeclToHtml st (PredDecl ident args) =
   code ! B.id (fromString (getIdent ident)) $
    do kw "predicate"
       toHtml " "
       H.span ! identClass st ident $ fromString (getIdent ident)
       argsToHtml tmToHtml args
       toHtml "."

predDeclToHtml st (PrimPredDecl ident args) =
   code ! B.id (fromString (getIdent ident)) $
    do kw "primitive predicate"
       toHtml " "
       H.span ! identClass st ident $ fromString (getIdent ident)
       argsToHtml tmToHtml args
       toHtml "."

argsToHtml :: (a -> Html) -> Args a -> Html
argsToHtml f (PositionalArgs []) = BI.Empty
argsToHtml f (PositionalArgs xs) = do
     toHtml "( "
     let xs' = map (f . snd) xs
     foldr1 (\x y -> x >> toHtml ", " >> y) xs'
     toHtml " )"
argsToHtml f (NamedArgs xs) = do
     toHtml "{ "
     let xs' = map (\ (_,nm,x) -> label nm >> toHtml ": " >> f x) xs
     foldr1 (\x y -> x >> toHtml ", " >> y) $ xs'
     toHtml " }"

tmToHtml :: (Type, Maybe Mode) -> Html
tmToHtml (ty,mm) = typeToHtml ty >> maybe BI.Empty (\m -> toHtml " " >> modeToHtml m) mm

typeToHtml :: Type -> Html
typeToHtml (Type pn ty) =
 case ty of
   TypeName n args ->
     do H.a ! B.href (fromString ("#"++getIdent n))
            ! B.class_ (fromString "type")
            $ fromString (getIdent n)
        if null args
           then BI.Empty
           else do toHtml "("
                   sequence_ $ intersperse (toHtml ", ") $ map typeToHtml args
                   toHtml ")"

   TypeList t ->
     do H.span ! B.class_ (fromString "typeconst") $ fromString "list"
        fromString "("
        typeToHtml t
        fromString ")"
   TypeMap k v ->
     do H.span ! B.class_ (fromString "typeconst") $ fromString "map"
        fromString "("
        typeToHtml k
        fromString ", "
        typeToHtml v
        fromString ")"
   TypeSet t ->
     do H.span ! B.class_ (fromString "typeconst") $ fromString "set"
        fromString "("
        typeToHtml t
        fromString ")"
   TypeNumber ->
     do H.span ! B.class_ (fromString "typeconst") $ fromString "number"
   TypeString ->
     do H.span ! B.class_ (fromString "typeconst") $ fromString "string"
   TypeVar x ->
     do H.span ! B.class_ (fromString "typevar") $ fromString x
   TypeTuple xs ->
     do foldr1 (\x y -> x >> fromString " * " >> y) $ map typeToHtml xs

modeToHtml :: Mode -> Html
modeToHtml ModeIn     = H.span ! B.class_ (fromString "mode") $ fromString "in"
modeToHtml ModeOut    = H.span ! B.class_ (fromString "mode") $ fromString "out"
modeToHtml ModeIgnore = H.span ! B.class_ (fromString "mode") $ fromString "ignore"
