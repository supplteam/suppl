{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

{-# LANGUAGE FlexibleInstances, DeriveFunctor #-}

-- | This module defines the abstract syntax of Suppl.
module AST where

import Data.Ratio

import Tokens( Pn(..), Located(..) )
import qualified Text.ParserCombinators.ReadP as P
import Text.ParserCombinators.ReadP( (<++), (+++) )


data Ident
  = Ident Pn String
 deriving (Show)

instance Eq Ident where
  (Ident _ x) == (Ident _ y) = x == y

instance Ord Ident where
  compare (Ident _ x) (Ident _ y) = compare x y

-- | Extract the name of an identifier and surround it with quotes
qt :: Ident -> String
qt = qts . getIdent

-- | surround a string with single quotes
qts :: String -> String
qts s = '\'' : s ++ "\'"

getIdent :: Ident -> String
getIdent (Ident _ s) = s

type Var = String

-- | A program consists of a list of declarations.
type Prog ta = [(Pn, CodeOrigin, Decl ta)]

data CodeOrigin
  = BuiltinCode
  | UserCode
 deriving (Show, Eq)

type FileDecl = (Pn, CodeOrigin, Decl Pn)

-- | A declaration with annotation type @ta@ is a 'PreDecl'
--   with qterms, terms and clauses in their expected positions.
type Decl ta = PreDecl (QTerm ta) (Term ta) (QClause ta) (Clause ta)

-- | A predeclaration is a declaration with type parameters for the three
--   syntactic classes that are parsed into 'PreTerm's by the compiler:
--   'QTerm', 'Term' and 'Clause'.  These type parameters allow us to
--   transform 'PreTerm's into the more specific types in a postprocessing pass.
--   The alternative would be to have separate datatype declarations.  Instead,
--   we simply define declarations ('Decl') to be predeclarations with particular
--   instantiations for the type parameters.
--
--   This same basic pattern is repeated several times in the rest of this file.
--
data PreDecl qtm tm qcl cl
   = DRule (PreRule tm cl)              -- ^ A rule
   | DHandler (PreHandler qtm tm qcl)   -- ^ An event handler
   | DTable TableDecl                   -- ^ A table declaration
   | DIndex IndexDecl                   -- ^ An index declaration
   | DPred PredDecl                     -- ^ A predicate type declaration
   | DMode ModeDecl                     -- ^ A predicate mode declaration
   | DType TypeDecl                     -- ^ A primitive or type abbreviation declaration
   | DData DataDecl                     -- ^ A datatype declaration
   | DProcedure ProcedureDecl           -- ^ A procedure type declaration
   | DFunction FunctionDecl             -- ^ A function type declaration
   | DEvent EventDecl                   -- ^ An event declaration
   | DImport String                     -- ^ A file import declaration
   | DAssert (AssertDecl cl)            -- ^ An assertion for use in static analysis
   | DConflict (ConflictDecl cl)        -- ^ A conflict declaration
   | DPragma (PrePragma tm cl)          -- ^ A pragma
   | DProcDefn (PreProcDefn qtm tm qcl) -- ^ A procedure definition
  deriving (Eq, Show)

data AssertDecl cl
   = AxiomDecl cl
   | LemmaDecl cl
   | PresumeDecl (Ident, Args (Maybe Var)) cl
  deriving (Eq,Show)

data ConflictDecl cl
   = ConflictDecl (Ident, Args (Maybe Var)) (Ident, Args (Maybe Var)) cl
   | ForbidDecl (Ident, Args (Maybe Var)) cl
  deriving (Eq,Show)

data FunctionDecl
   = PrimFunctionDecl Ident (Args Type) Type  -- ^ Declare a primitive function: name, arguments, return type
  deriving (Eq,Show)

data ProcedureDecl
   = PrimProcedureDecl Ident (Args Type)      -- ^ Declare a primitive procedure AKA action: name, arguments
   | ProcedureDecl Ident (Args Type)          -- ^ Declare a user-defined procedure: name, arguments
  deriving (Eq,Show)

data DataDecl
   = DataDecl Ident [(Pn,Var)] [(Ident, Args Type)] -- ^ Declare a user-defined datatype: name, list of variable arguments,
                                                    --   list of data constructors
  deriving (Eq, Show)

data EventDecl
   = EventDecl Ident (Args Type)              -- ^ Declare an event: name, arguments
  deriving (Eq, Show)

data Mode
   = ModeIn                                   -- ^ In mode: the argument must be ground at the predicate call
   | ModeOut                                  -- ^ Out mode: the argument is guaranteed to be ground after the call
   | ModeIgnore                               -- ^ Ignore mode: both callee and caller agree not to use this argument
  deriving (Eq, Show)


-- | Surface-langauge types, with type variables
--   named by strings
data Type = Type Pn (Type' Type Var)
  deriving (Show)

instance Eq Type where
  (Type _ t1) == (Type _ t2) = t1 == t2

instance Loc Type where
  loc (Type pn _) = pn

-- | Internal types, with type variables named by @Integer@
newtype IType = IType (Type' IType Integer)
  deriving (Eq, Show)


-- | A generalized type, with variables drawn from type @v@ and subterms from type @ty@.
--   Surface-language have variables named by strings.
--   The typechecking algorithm instead manipulates @IType@, with type
--   variables drawn from @Integer@.
--
data Type' ty v
   = TypeName Ident [ty]
   | TypeList ty
   | TypeMap ty ty
   | TypeSet ty
   | TypeTuple [ty]
   | TypeNumber
   | TypeString
   | TypeVar v
  deriving (Eq, Show)


data TypeDecl
   = PrimTypeDecl Ident                     -- ^ Declare a primitive type
   | TypeDecl Ident [(Pn,Var)] Type         -- ^ Declare a type abbreviation
  deriving (Eq, Show)

data ModeDecl
   = ModeDecl Ident (Args Mode)             -- ^ Declare a mode for a predicate: name, modes
  deriving(Eq,Show)

data PredDecl
   = PredDecl Ident (Args (Type, Maybe Mode))      -- ^ Declare a predicate: name, argument types and optional modes
   | PrimPredDecl Ident (Args (Type, Maybe Mode))  -- ^ Declare a primitive predicate: name, argument types and optional modes
  deriving(Eq,Show)

-- | A procedure definition consists of an identifier,
--   formal arguments, and a procedure body.
type ProcDefn ta = PreProcDefn (QTerm ta) (Term ta) (QClause ta)
type PreProcDefn qtm tm qcl =
  (Ident, Args (Maybe Var), PreHBody qtm tm qcl)

type Pragma ta = PrePragma (Term ta) (Clause ta)

-- | Pragmas represent a grab-bag of features and constitute
--   a semi-organized way to communicate information to the
--   compiler.  Currently handled pragmas:
--
--   * @pragma emit \<stringlit\>.@            Emit the given string literal directly into the produced source code.
--
--   * @pragma expect error(ln,col).@          Regression test: expect a compiler error to be generated at the given line and column number.
--
--   * @pragma expect error(ln,col,msg).@      Regression test: expect a compiler error to be generated at the given line and column number
--                                                and with the given message.
--
--   * @pragma expect warning(ln,col).@        Regression test: expect a compiler warning to be generated at the given line and column number.
--
--   * @pragma expect warning(ln,col,msg).@    Regression test: expect a compiler warning to be generated at the given line and column number
--                                                and with the given message.
--
--   * @pragma debug.@                         Debug mode: causes the compiler to produce more output.
--
--   * @pragma query \<n\> \<clause\>.@            Regression test: execute the clause, expect n solutions.
--
--   * @pragma handle \<event\> \<action list\>.@  Regression test: handle the given event, expect action list as output.
--
data PrePragma tm cl
   = GenericPragma Ident (Maybe PreTerm)           -- ^ A \"generic\" pragma.  These are primarily interpreted by 'Compile.processPragma',
                                                   --   although some pragmas are handled elsewhere.

   | QueryPragma Pn String cl                      -- ^ A query pragma represents a regression test.  The user gives a number and a clause:
                                                   --   the runtime system is expected to execute the clause and count the number of solutions
                                                   --   generated.  The regresssion test passes if the number given by the user matches the
                                                   --   number of generated solutions.

   | HandlePragma Pn (BasicForm tm) [BasicForm tm] -- ^ A handle pragma is a different kind of regression test.  The runtime system is expected
                                                   --   to execute the event handler for the given event.  The returned actions are compared against
                                                   --   the user-given list of actions; the test succeedes if they match.

  deriving(Eq,Show)

-- | A rule consists of a head (a 'Goal') and a body (a 'Clause').
type Rule ta = PreRule (Term ta) (Clause ta)
type PreRule tm cl = (PreGoal tm, cl)

type Goal ta = PreGoal (Term ta)
type PreGoal tm = (Pn, Ident, Args tm)

data TableDecl
   = TableDecl Ident
       (Args Type)
       (Args Mode)
       (Maybe String)
       -- ^ Declare a table: name, types, a primary key
       --   and an optional lifetime string
  deriving (Eq,Show)

data IndexDecl
   = IndexDecl Ident (Args Mode)                   -- ^ Declare a table index: name, modes
  deriving (Eq,Show)

-- | Binary operations supported by Suppl.  All except
--   concatenation are numeric operations.  The concatenation operator \"++\"
--   works on both strings and lists.
data OP = OpPlus | OpMinus | OpTimes | OpDiv | OpConcat
  deriving (Eq, Show)

-- | Arguments in Suppl may either be unnamed (positional) arguments
--   or named.  For named arguments, the order of the argument list is irrelevant,
--   actual arguments are matched with formal arguments by name.
data Args a
   = PositionalArgs [(Pn, a)]
   | NamedArgs [(Pn, String, a)]
  deriving (Eq, Show)


-- | The standard term datatype.  A term is either a variable
--   or one of the shared term constructors.
data Term annot
   = TTerm annot (Term' (Term annot))
   | TVar annot Var
 deriving (Eq,Show)

-- | The \"query\" term datatype.  A query term is either a variable
--   reference, a binding variable, or one of the shared term constructors.
--   Query terms appear in procedure/handler bodies as in \"pattern\" portion
--   of query and foreach constructs.
data QTerm annot
   = QBindVar annot Var
   | QRefVar annot Var
   | QTerm annot (Term' (QTerm annot))
 deriving (Eq,Show)


data NumericLiteral
   = LitInteger Integer
   | LitRational Rational
  deriving (Eq, Show)

showRational :: Rational -> String
showRational r =
  "(" ++ show (numerator r) ++ ".0 / "
      ++ show (denominator r) ++ ".0)"


-- | Shared term constructors.  These constructs are allowed to appear uniformly
--   both in 'Term's and in 'QTerm's.  Explicitly \"exploding\" the term datatype
--   in this way allows us to share code in the typechecker and modechecker and it
--   reduces the number of times we have to push around annotations.
data Term' tm
   = TAnonVar                -- ^ Anonymous variable
   | TFunc Ident (Args tm)   -- ^ Function call
   | TCons tm tm             -- ^ List cons cell
   | TList [tm]              -- ^ Literal list
   | TTuple [tm]             -- ^ Tuple
   | TNum String NumericLiteral -- ^ Numeric literal
   | TString String          -- ^ String literal
   | TOp OP tm tm            -- ^ Binary operator
   | TNegative tm            -- ^ Numeric negation operator
  deriving (Eq, Show, Functor)

-- | This is a convenience class that lets us perform certain
--   operations uniformly on both 'Term's and 'QTerm's.
class TermLike tm where
  buildTerm :: a -> Term' (tm a) -> tm a
  getAnnot :: tm a -> a

instance TermLike Term where
  buildTerm = TTerm
  getAnnot (TTerm a _) = a
  getAnnot (TVar a _) = a

instance TermLike QTerm where
  buildTerm = QTerm
  getAnnot (QTerm a _) = a
  getAnnot (QBindVar a _) = a
  getAnnot (QRefVar a _) = a


-- The allowed numeric comparison operators.
data CmpOP
   = OpLE | OpLT | OpGE | OpGT
  deriving (Eq,Show)

-- | As with terms, the 'Clause' datatype is given in
--   exploded form.
data Clause ta = Clause Pn (Clause' (QTerm ta) (Term ta) (Clause ta))
  deriving (Eq, Show)

data Clause' qtm tm cl
   = CEq tm tm                  -- ^ Equality test / unification
   | CNeq tm tm                 -- ^ Disequalty test
   | CPred Ident (Args tm)      -- ^ Predicate call
   | CAnd cl cl                 -- ^ Conjunction
   | COr cl cl                  -- ^ Disjunction
   | CNot cl                    -- ^ Logical negation
   | CEmpty                     -- ^ The \"empty\" predicate, interpreted as true
   | CFalse                     -- ^ The false predicate
   | CTrue                      -- ^ The true predicate
   | CComp CmpOP tm tm          -- ^ Binary comparison operator
   | CFindall tm
        (Pn,Ident, Args qtm)
        Var
                                -- ^ The findall construct.  The given goal (middle argument) is called,
                                --   finding all solutions.  For each solution, the given term (first argument)
                                --   is instantiated with variables bound by the solution.  Finally the variable
                                --   (third argument) is bound to a list containing the terms genereted by each solution.
  deriving (Eq, Show)

-- | A \"query\" clause appears in handlers and procedure bodies
--   to the left of the arrow.  They are essentially a combination of
--   pattern-matching and conditional constructs.  A query clause is
--   basically a comma-sequence of unifications an predicate calls
--   that may bind variables.
--
data QClause ta
   = QCEq Pn (QTerm ta) (QTerm ta)        -- ^ Unification query
   | QCPred Pn Ident (Args (QTerm ta))    -- ^ Predicate call query
   | QCAnd (QClause ta) (QClause ta)      -- ^ Conjunction/sequence query
   | QCNeq Pn (QTerm ta) (QTerm ta)       -- ^ Disequality query
   | QCComp Pn CmpOP (QTerm ta) (QTerm ta)  -- ^ Comparison query
  deriving (Eq, Show)

instance Loc (QClause ta) where
   loc (QCEq pn _ _) = pn
   loc (QCPred pn _ _) = pn
   loc (QCAnd c1 _) = loc c1
   loc (QCNeq pn _ _) = pn
   loc (QCComp pn _ _ _) = pn

-- | An event handler consists of a head naming the handled event
--   and binding argument variables; and a procedure body.
type Handler ta = PreHandler (QTerm ta) (Term ta) (QClause ta)
type PreHandler qtm tm qcl = (Ident, Args (Maybe Var), PreHBody qtm tm qcl)

type HBody ta = PreHBody (QTerm ta) (Term ta) (QClause ta)

data PreHBody qtm tm qcl = HBody Pn (HBody' qtm tm qcl (PreHBody qtm tm qcl))
  deriving (Eq, Show)

data HBody' qtm tm qcl bd
  = HProcedure Ident (Args tm)                 -- ^ Call procedure/action
  | HSeq bd bd                                 -- ^ Sequential composition
  | HForeach qcl (PreHBody qtm tm qcl)         -- ^ Call the body once for each solution to the query
  | HQuery [(Pn,PreQBranch qtm tm qcl)]        -- ^ Try each query in turn: run the body for the first query to succeed.
  | HInsert (Args tm) Ident                    -- ^ Insert a row into the given table
  | HDelete (Args tm) Ident                    -- ^ Delete a row into from the given table
  | HSkip                                      -- ^ No-op statement
  deriving (Eq,Show)

type QBranch ta = PreQBranch (QTerm ta) (Term ta) (QClause ta)

-- | A query branch either tries to execute a query before running the body,
--   or is the final default branch.
data PreQBranch qtm tm qcl
  = QueryBranch qcl (PreHBody qtm tm qcl)
  | DefaultBranch (PreHBody qtm tm qcl)
  deriving (Eq,Show)

-- | An identifier applied to some arguments.  This is occasionally useful when
--   we want to be wishy-washy about what syntactic class the identifier belongs to.
data BasicForm tm
  = BasicForm Pn Ident (Args tm)
  deriving (Eq,Show)

data CLI tm cl
  = CLINull
  | CLIQuery [cl]
  | CLIEvent Ident [tm]
  deriving (Eq,Show)


-- | Map the given function over all the contained values.
mapArgs :: (a -> b) -> Args a -> Args b
mapArgs f (PositionalArgs xs) = PositionalArgs (map (\ (pn,x) -> (pn, f x)) xs)
mapArgs f (NamedArgs xs) = NamedArgs (map (\ (pn,n,x) -> (pn,n,f x)) xs)

-- | Map a monadic function over all the contained values.
mapArgsM :: (Functor m, Monad m) => (a -> m b) -> Args a -> m (Args b)
mapArgsM f (PositionalArgs xs) = fmap PositionalArgs $ g xs
  where g [] = return []
        g ((pn,x):xs) =
          do x' <- f x
             xs' <- g xs
             return ((pn,x'):xs')
mapArgsM f (NamedArgs xs) = fmap NamedArgs $ g xs
  where g [] = return []
        g ((pn,nm,x):xs) =
          do x' <- f x
             xs' <- g xs
             return ((pn,nm,x'):xs')

extractArgs :: Args a -> [a]
extractArgs (PositionalArgs xs) = map snd xs
extractArgs (NamedArgs xs) = map (\ (pn,n,x) -> x) xs

instance Functor Args where
   fmap = mapArgs

-- | This is a convenince class that allows us to extract locations from a
--   variety of different syntactic objects.
--
class Loc a where
  loc :: a -> Pn

instance Loc (Located a) where
  loc (L pn _) = pn

instance Loc Ident where
  loc (Ident pn _) = pn

instance Loc (Clause ta) where
  loc (Clause pn _) = pn

instance Loc (Term Pn) where
  loc (TTerm pn _) = pn
  loc (TVar pn _) = pn

instance Loc (QTerm Pn) where
  loc (QBindVar pn _) = pn
  loc (QRefVar pn _) = pn
  loc (QTerm pn _) = pn

instance Loc (PreHBody qcl qtm tm) where
  loc (HBody pn _) = pn

-- Interpret a basic form as a predicate call
basic_to_clause :: BasicForm (Term Pn) -> Clause Pn
basic_to_clause (BasicForm pn i ts) = Clause pn (CPred i ts)

-- Interpret a basic form as a function call
basic_to_term :: BasicForm (Term Pn) -> Term Pn
basic_to_term (BasicForm pn i ts) = TTerm pn (TFunc i ts)


-- | Preterms are produced by the parser.  Rather than have the parser
--   try to distinguish between terms, qterms and clauses (which are
--   syntactically quite similar), we do that afterward in a postprocess pass.
--   Preterms are thus essentially the union of the syntactic formers for terms
--   qterms and clauses.
--
data PreTerm
  = PTCommaSequence Pn [PreTerm]   -- ^ INVARIANT: list is nonempty
  | PTList Pn [PreTerm]
  | PTCons Pn PreTerm PreTerm
  | PTUnderscore Pn
  | PTApplied Pn Ident (Args PreTerm)
  | PTNumLit Pn String
  | PTStringLit Pn String
  | PTVBar Pn PreTerm PreTerm
  | PTOp Pn OP PreTerm PreTerm
  | PTCmpOp Pn CmpOP PreTerm PreTerm
  | PTNegative Pn PreTerm
  | PTEq Pn PreTerm PreTerm
  | PTNeq Pn PreTerm PreTerm
  | PTVar Pn Var
  | PTQVar Pn Var
  | PTArrow Pn PreTerm PreTerm
  | PTNot Pn PreTerm
  | PTFindall Pn PreTerm (Pn,Ident,Args PreTerm) Var
  | PTEmpty Pn
  | PTFalse Pn
  | PTTrue Pn
 deriving (Eq, Show)


instance Loc PreTerm where
  loc (PTCommaSequence pn _) = pn
  loc (PTList pn _) = pn
  loc (PTCons pn _ _) = pn
  loc (PTUnderscore pn) = pn
  loc (PTApplied pn _ _) = pn
  loc (PTNumLit pn _) = pn
  loc (PTStringLit pn _) = pn
  loc (PTVBar pn _ _) = pn
  loc (PTCmpOp pn _ _ _) = pn
  loc (PTArrow pn _ _) = pn
  loc (PTNegative pn _) = pn
  loc (PTEq pn _ _) = pn
  loc (PTNeq pn _ _) = pn
  loc (PTVar pn _) = pn
  loc (PTQVar pn _) = pn
  loc (PTNot pn _) = pn
  loc (PTOp pn _ _ _) = pn
  loc (PTFindall pn _ _ _) = pn
  loc (PTEmpty pn) = pn
  loc (PTFalse pn) = pn
  loc (PTTrue pn) = pn

parseNumLit :: PTMonad m => Pn -> String -> m NumericLiteral
parseNumLit pn s =
  case P.readP_to_S numLitParser s of
    (x,_):_ -> return x
    []      -> parseFail pn $ "invalid numeric literal: "++s


numLitParser :: P.ReadP NumericLiteral
numLitParser =
  (fmap LitRational parseScientific)
  <++
  (fmap LitRational parseFloat)
  <++
  (fmap LitInteger parseHexInteger)
  <++
  (fmap LitInteger parseSignInteger)


parseSign :: P.ReadP Bool
parseSign = do
  (P.char '~' >> return True)
  <++
  return False

parseHexInteger :: P.ReadP Integer
parseHexInteger = P.string "0x" >> f 0
 where f x =
        (do d <- parseHexDigit
            f (16*x + d))
        <++
        (return x)

parseSignInteger :: P.ReadP Integer
parseSignInteger = do
  sign <- parseSign
  x <- parseInteger
  return (if sign then -x else x)

parseInteger :: P.ReadP Integer
parseInteger = f 0
 where f x =
         (do d <- parseDigit
             f (10*x + d))
         <++
         (return x)

parseFrac :: P.ReadP Rational
parseFrac = f (1%10) 0
 where f base x =
         (do d <- parseDigit
             f (base / 10.0) (base*(d%1) + x))
         <++
         (return x)

parseFloat :: P.ReadP Rational
parseFloat = do
  sign <- parseSign
  whole <- parseInteger
  P.char '.'
  frac <- parseFrac
  let x = (whole%1) + frac
  return (if sign then -x else x)


parseScientific :: P.ReadP Rational
parseScientific = do
  float <- parseFloat
  (P.char 'e' <++ P.char 'E')
  exp <- parseSignInteger
  return (float * (10.0 ^^ exp))

processFrac :: [Integer] -> Rational
processFrac ls = f ls (1%10)
 where
  f [] x = 0
  f (d:ds) x = (d%1)*x + f ds (x / 10)

base10process :: [Integer] -> Integer
base10process ls = f (reverse ls) 1
 where
  f [] x = 0
  f (d:ds) x = d*x + f ds (x*10)

base16process :: [Integer] -> Integer
base16process ls = f (reverse ls) 1
 where
  f [] x = 0
  f (d:ds) x = d*x + f ds (x*16)

parseDigit :: P.ReadP Integer
parseDigit = P.choice
  [ P.char '0' >> return 0
  , P.char '1' >> return 1
  , P.char '2' >> return 2
  , P.char '3' >> return 3
  , P.char '4' >> return 4
  , P.char '5' >> return 5
  , P.char '6' >> return 6
  , P.char '7' >> return 7
  , P.char '8' >> return 8
  , P.char '9' >> return 9
  ]

parseHexDigit :: P.ReadP Integer
parseHexDigit = P.choice
  [ P.char '0' >> return 0
  , P.char '1' >> return 1
  , P.char '2' >> return 2
  , P.char '3' >> return 3
  , P.char '4' >> return 4
  , P.char '5' >> return 5
  , P.char '6' >> return 6
  , P.char '7' >> return 7
  , P.char '8' >> return 8
  , P.char '9' >> return 9
  , P.char 'a' >> return 10
  , P.char 'b' >> return 11
  , P.char 'c' >> return 12
  , P.char 'd' >> return 13
  , P.char 'e' >> return 14
  , P.char 'f' >> return 15
  , P.char 'A' >> return 10
  , P.char 'B' >> return 11
  , P.char 'C' >> return 12
  , P.char 'D' >> return 13
  , P.char 'E' >> return 14
  , P.char 'F' >> return 15
  ]


-- | A 'PTMonad' is a monad that has an operation for
--   producing error messages.  This class allows us to
--   define the preterm postprocessing functions here,
--   before the parsing moand is defined.
--
class (Functor m, Monad m) => PTMonad m where
  parseFail :: Pn -> String -> m a


-- | Take a preterm and interpret it as a query clause.
pretermToQClause :: PTMonad m => PreTerm -> m (QClause Pn)

pretermToQClause (PTCommaSequence _ pts) = do
   cs <- mapM pretermToQClause pts
   return (foldr1 (\c1 c2 -> QCAnd c1 c2) cs)

pretermToQClause (PTApplied pn ident args) = do
   args' <- pretermArgs pretermToQTerm args
   return (QCPred pn ident args')

pretermToQClause (PTEq pn t1 t2) = do
   t1' <- pretermToQTerm t1
   t2' <- pretermToQTerm t2
   return (QCEq pn t1' t2')

pretermToQClause (PTCmpOp pn op t1 t2) = do
   t1' <- pretermToQTerm t1
   t2' <- pretermToQTerm t2
   return (QCComp pn op t1' t2')

pretermToQClause (PTNeq pn t1 t2) = do
   t1' <- pretermToQTerm t1
   t2' <- pretermToQTerm t2
   return (QCNeq pn t1' t2')

pretermToQClause pt =
   parseFail (loc pt) "invalid query clause"


-- | Take a preterm and interpret it as a clause.  Emit an
--   error message if syntax appears that cannot be interpreted as a clause.
pretermToClause :: PTMonad m => PreTerm -> m (Clause Pn)

pretermToClause (PTEmpty pn) = return (Clause pn CEmpty)

pretermToClause (PTFalse pn) = return (Clause pn CFalse)

pretermToClause (PTTrue pn) = return (Clause pn CTrue)

pretermToClause (PTCommaSequence _ pts) = do
   cs <- mapM pretermToClause pts
   return (foldr1 (\c1 c2 -> Clause (loc c1) (CAnd c1 c2)) cs)

pretermToClause (PTList pn pts) = do
   cs <- mapM pretermToClause pts
   parseFail pn "list found in clause position"

pretermToClause (PTArrow pn c1 c2) = do
   c1' <- pretermToClause c1
   c2' <- pretermToClause c2
   return (Clause pn (COr (Clause pn (CNot c1')) c2'))

pretermToClause (PTCons pn h t) =
   parseFail pn "list found in clause position"

pretermToClause (PTUnderscore pn) =
   parseFail pn "anonymous variable in clause position"

pretermToClause (PTApplied pn ident args) = do
   args' <- pretermArgs pretermToTerm args
   return (Clause pn (CPred ident args'))

pretermToClause (PTNumLit pn _) =
   parseFail pn "numeric literal found in clause position"

pretermToClause (PTStringLit pn _) =
   parseFail pn "string literal found in clause position"

pretermToClause (PTVBar pn c1 c2) = do
   c1' <- pretermToClause c1
   c2' <- pretermToClause c2
   return (Clause pn (COr c1' c2'))

pretermToClause (PTOp pn _ _ _) = do
   parseFail pn "binary operator found in clause position"

pretermToClause (PTCmpOp pn op t1 t2) = do
   t1' <- pretermToTerm t1
   t2' <- pretermToTerm t2
   return (Clause pn (CComp op t1' t2'))

pretermToClause (PTNegative pn _) =
   parseFail pn "negation operator found in clause position"

pretermToClause (PTEq pn t1 t2) = do
   t1' <- pretermToTerm t1
   t2' <- pretermToTerm t2
   return (Clause pn (CEq t1' t2'))

pretermToClause (PTNeq pn t1 t2) = do
   t1' <- pretermToTerm t1
   t2' <- pretermToTerm t2
   return (Clause pn (CNeq t1' t2'))

pretermToClause (PTVar pn _) =
   parseFail pn "variable found in clause position"

pretermToClause (PTQVar pn _) =
   parseFail pn "binding variable found in clause position"

pretermToClause (PTNot pn c) = do
   c' <- pretermToClause c
   return (Clause pn (CNot c'))

pretermToClause (PTFindall pn t (pn',i,args) v) = do
   t' <- pretermToTerm t
   args' <- pretermArgs pretermToQTerm args
   return (Clause pn (CFindall t' (pn', i, args') v))

-- | Pass an interpretation function through arguments.
pretermArgs
   :: PTMonad m
    => (PreTerm -> m a)
    -> Args PreTerm
    -> m (Args a)
pretermArgs f (PositionalArgs xs) = do
   xs' <- mapM (\ (pn, x) -> fmap ((,) pn) $ f x) xs
   return (PositionalArgs xs')

pretermArgs f (NamedArgs xs) = do
   xs' <- mapM (\ (pn, n, x) -> fmap ((,,) pn n) $ f x) xs
   return (NamedArgs xs')


-- | Take a preterm and interpret it as a term.
--   error message if syntax appears that cannot be interpreted as a term.
pretermToTerm
   :: PTMonad m
   => PreTerm
   -> m (Term Pn)

pretermToTerm (PTCommaSequence pn ts) = do
   ts' <- mapM pretermToTerm ts
   return (TTerm pn (TTuple ts'))

pretermToTerm (PTEmpty pn) =
   parseFail pn "empty term not allowed"

pretermToTerm (PTFalse pn) =
   parseFail pn "'false' is not a term"

pretermToTerm (PTTrue pn) =
   parseFail pn "'true' is not a term"

pretermToTerm (PTArrow pn _ _) =
   parseFail pn "arrow found in term position"

pretermToTerm (PTList pn ts) = do
   ts' <- mapM pretermToTerm ts
   return (TTerm pn (TList ts'))

pretermToTerm (PTCons pn h t) = do
   h' <- pretermToTerm h
   t' <- pretermToTerm t
   return (TTerm pn (TCons h' t'))

pretermToTerm (PTUnderscore pn) = do
   return (TTerm pn TAnonVar)

pretermToTerm (PTApplied pn ident args) = do
   args' <- pretermArgs pretermToTerm args
   return (TTerm pn (TFunc ident args'))

pretermToTerm (PTNumLit pn n) = do
   n' <- parseNumLit pn n
   return (TTerm pn (TNum n n'))

pretermToTerm (PTStringLit pn s) =
   return (TTerm pn (TString s))

pretermToTerm (PTVBar pn _ _) =
   parseFail pn "disjunction found in term position"

pretermToTerm (PTOp pn op t1 t2) = do
   t1' <- pretermToTerm t1
   t2' <- pretermToTerm t2
   return (TTerm pn (TOp op t1' t2'))

pretermToTerm (PTCmpOp pn _ _ _) =
   parseFail pn "comparison op found in term position"

pretermToTerm (PTNegative pn t) = do
   t' <- pretermToTerm t
   return (TTerm pn (TNegative t'))

pretermToTerm (PTEq pn _ _) =
   parseFail pn "equality found in term position"

pretermToTerm (PTNeq pn _ _) =
   parseFail pn "disequality found in term position"

pretermToTerm (PTVar pn v) =
   return (TVar pn v)

pretermToTerm (PTQVar pn _) =
   parseFail pn "binding var found in term position"

pretermToTerm (PTNot pn _) =
   parseFail pn "logical negation found in term position"

pretermToTerm (PTFindall pn _ _ _) =
   parseFail pn "findall found in term position"


-- | Take a preterm and interpret it as a query term.
--   error message if syntax appears that cannot be interpreted as a qterm.
pretermToQTerm
   :: PTMonad m
   => PreTerm
   -> m (QTerm Pn)

pretermToQTerm (PTCommaSequence pn ts) = do
   ts' <- mapM pretermToQTerm ts
   return (QTerm pn (TTuple ts'))

pretermToQTerm (PTEmpty pn) =
   parseFail pn "empty term not allowed"

pretermToQTerm (PTFalse pn) =
   parseFail pn "'false' is not a term"

pretermToQTerm (PTTrue pn) =
   parseFail pn "'true' is not a term"

pretermToQTerm (PTArrow pn _ _) =
   parseFail pn "arrow found in term position"

pretermToQTerm (PTList pn ts) = do
   ts' <- mapM pretermToQTerm ts
   return (QTerm pn (TList ts'))

pretermToQTerm (PTCons pn h t) = do
   h' <- pretermToQTerm h
   t' <- pretermToQTerm t
   return (QTerm pn (TCons h' t'))

pretermToQTerm (PTUnderscore pn) = do
   return (QTerm pn TAnonVar)

pretermToQTerm (PTApplied pn ident args) = do
   args' <- pretermArgs pretermToQTerm args
   return (QTerm pn (TFunc ident args'))

pretermToQTerm (PTNumLit pn n) = do
   n' <- parseNumLit pn n
   return (QTerm pn (TNum n n'))

pretermToQTerm (PTStringLit pn s) =
   return (QTerm pn (TString s))

pretermToQTerm (PTVBar pn _ _) =
   parseFail pn "disjunction found in term position"

pretermToQTerm (PTOp pn op t1 t2) = do
   t1' <- pretermToQTerm t1
   t2' <- pretermToQTerm t2
   return (QTerm pn (TOp op t1' t2'))

pretermToQTerm (PTCmpOp pn _ _ _) =
   parseFail pn "comparison op found in term position"

pretermToQTerm (PTNegative pn t) = do
   t' <- pretermToQTerm t
   return (QTerm pn (TNegative t'))

pretermToQTerm (PTEq pn _ _) =
   parseFail pn "equality found in term position"

pretermToQTerm (PTNeq pn _ _) =
   parseFail pn "disequality found in term position"

pretermToQTerm (PTVar pn v) =
   return (QRefVar pn v)

pretermToQTerm (PTQVar pn v) =
   return (QBindVar pn v)

pretermToQTerm (PTNot pn _) =
   parseFail pn "logical negation found in term position"

pretermToQTerm (PTFindall pn _ _ _) =
   parseFail pn "findall found in term position"


preCliToCli
  :: PTMonad m
  => CLI PreTerm PreTerm
  -> m (CLI (Term Pn) (Clause Pn))
preCliToCli CLINull = return CLINull
preCliToCli (CLIQuery cs) = fmap CLIQuery $ mapM pretermToClause cs
preCliToCli (CLIEvent nm ts) = fmap (CLIEvent nm) $ mapM pretermToTerm ts


-- | Scan a predeclaration and produce a declaration by transforming
--   all the embedded preterms into the expected syntactic classes.
preDeclToDecl
   :: PTMonad m
   => PreDecl PreTerm PreTerm PreTerm PreTerm
   -> m (Decl Pn)
preDeclToDecl (DRule r) = fmap DRule $ preRuleToRule r
preDeclToDecl (DHandler h) = fmap DHandler $ preHandlerToHandler h
preDeclToDecl (DPragma p) = fmap DPragma $ prePragmaToPragma p
preDeclToDecl (DProcDefn (i,args,body)) = fmap (\b -> DProcDefn (i,args,b)) $ preBodyToBody body
preDeclToDecl (DTable td) = return (DTable td)
preDeclToDecl (DIndex id) = return (DIndex id)
preDeclToDecl (DPred pd) = return (DPred pd)
preDeclToDecl (DMode md) = return (DMode md)
preDeclToDecl (DData dd) = return (DData dd)
preDeclToDecl (DProcedure pd) = return (DProcedure pd)
preDeclToDecl (DFunction fd) = return (DFunction fd)
preDeclToDecl (DEvent ed) = return (DEvent ed)
preDeclToDecl (DImport s) = return (DImport s)
preDeclToDecl (DType td) = return (DType td)
preDeclToDecl (DAssert as) = fmap DAssert $ preAssertToAssert as
preDeclToDecl (DConflict cd) = fmap DConflict $ preConflictToConflict cd


preConflictToConflict
   :: PTMonad m
   => ConflictDecl PreTerm
   -> m (ConflictDecl (Clause Pn))
preConflictToConflict (ConflictDecl ev1 ev2 c) = fmap (ConflictDecl ev1 ev2) $ (pretermToClause c)
preConflictToConflict (ForbidDecl ev1 c) = fmap (ForbidDecl ev1) $ (pretermToClause c)

preAssertToAssert
   :: PTMonad m
   => AssertDecl PreTerm
   -> m (AssertDecl (Clause Pn))
preAssertToAssert (AxiomDecl cl) = fmap AxiomDecl $ pretermToClause cl
preAssertToAssert (LemmaDecl cl) = fmap LemmaDecl $ pretermToClause cl
preAssertToAssert (PresumeDecl ev cl) = fmap (PresumeDecl ev) $ (pretermToClause cl)

preRuleToRule
   :: PTMonad m
   => PreRule PreTerm PreTerm
   -> m (Rule Pn)
preRuleToRule ((pn,ident,args),cl) = do
   args' <- pretermArgs pretermToTerm args
   cl' <- pretermToClause cl
   return ((pn,ident,args'),cl')

preHandlerToHandler
   :: PTMonad m
   => PreHandler PreTerm PreTerm PreTerm
   -> m (Handler Pn)
preHandlerToHandler (ident,args,body) = do
   body' <- preBodyToBody body
   return (ident,args,body')

prePragmaToPragma
   :: PTMonad m
   => PrePragma PreTerm PreTerm
   -> m (Pragma Pn)
prePragmaToPragma (GenericPragma ident mt) = return (GenericPragma ident mt)
prePragmaToPragma (QueryPragma pn x cl) = do
   cl' <- pretermToClause cl
   return (QueryPragma pn x cl')
prePragmaToPragma (HandlePragma pn ev acts) = do
   ev' <- preBasicToBasic ev
   acts' <- mapM preBasicToBasic acts
   return (HandlePragma pn ev' acts')

preBasicToBasic
   :: PTMonad m
   => BasicForm PreTerm
   -> m (BasicForm (Term Pn))
preBasicToBasic (BasicForm pn ident args) = do
   args' <- pretermArgs pretermToTerm args
   return (BasicForm pn ident args')

preBodyToBody
   :: PTMonad m
   => PreHBody PreTerm PreTerm PreTerm
   -> m (HBody Pn)
preBodyToBody (HBody pn HSkip) = return (HBody pn HSkip)
preBodyToBody (HBody pn (HSeq x y)) = do
   x' <- preBodyToBody x
   y' <- preBodyToBody y
   return (HBody pn (HSeq x' y'))
preBodyToBody (HBody pn (HForeach q b)) = do
   q' <- pretermToQClause q
   b' <- preBodyToBody b
   return (HBody pn (HForeach q' b'))
preBodyToBody (HBody pn (HQuery bs)) = do
   bs' <- mapM preBranchToBranch bs
   return (HBody pn (HQuery bs'))
preBodyToBody (HBody pn (HProcedure ident args)) = do
   args' <- pretermArgs pretermToTerm args
   return (HBody pn (HProcedure ident args'))
preBodyToBody (HBody pn (HInsert args ident)) = do
   args' <- pretermArgs pretermToTerm args
   return (HBody pn (HInsert args' ident))
preBodyToBody (HBody pn (HDelete args ident)) = do
   args' <- pretermArgs pretermToTerm args
   return (HBody pn (HDelete args' ident))

preBranchToBranch
   :: PTMonad m
   => (Pn, PreQBranch PreTerm PreTerm PreTerm)
   -> m (Pn, QBranch Pn)
preBranchToBranch (pn, QueryBranch q b) = do
   q' <- pretermToQClause q
   b' <- preBodyToBody b
   return (pn, QueryBranch q' b')
preBranchToBranch (pn, DefaultBranch b) = do
   b' <- preBodyToBody b
   return (pn, DefaultBranch b')
