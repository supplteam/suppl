{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module defines an abstract syntax for Prolog
--   and pretty-printer for the same.
--   
module Prolog
( Op(..)
, COp(..)
, Prolog(..)
, renderProlog
) where

import Data.Bits
import Data.Word
import Data.Char
import Data.List( intersperse )
import qualified Text.PrettyPrint as PP
import Text.PrettyPrint (($$), (<>), (<+>))

type Op = String
type COp = String

data Prolog
  = Atom String
  | Var String
  | List [Prolog]
  | Tuple [Prolog]
  | Number String
  | Cons Prolog Prolog
  | Disjunction [Prolog]
  | Conjunction [Prolog]
  | Application String [Prolog]
  | BinOp Op Prolog Prolog
  | Not Prolog
  | CmpOp COp Prolog Prolog
  | StringLit String
  | Is Prolog Prolog
  | Equal Prolog Prolog
  | Empty
  | Cut
  | Negative Prolog
  | Underscore
  | Directive [Prolog]
  | Rule Prolog [Prolog]
  | Emit String
 deriving (Eq,Ord,Show)

-- | Pretty-print a sequence of Prolog declarations.
--   
renderProlog :: [Prolog] -> String
renderProlog = PP.render . printPrologs


printPrologs :: [Prolog] -> PP.Doc
printPrologs = PP.vcat . intersperse (PP.text "") . map pprint

pprint :: Prolog -> PP.Doc
pprint (Atom s)   = PP.text s
pprint (Var v)    = PP.text v
pprint (Number n) = prologNumEscape n
pprint (Negative n) = PP.parens (PP.text "-" <> pprint n)
pprint (List xs)  = PP.brackets $ PP.cat $ PP.punctuate PP.comma $ map pprint xs
pprint (Tuple xs) = PP.brackets $ PP.cat $ PP.punctuate PP.comma $ map pprint xs
pprint (Cons h t) = PP.brackets (pprint h <> PP.text " | " <> pprint t)
pprint (Disjunction []) = PP.text "false"
pprint (Disjunction [x]) = pprint x
pprint (Disjunction xs) =
  PP.parens $ PP.cat $ PP.punctuate PP.semi $ map pprint xs
pprint (Conjunction []) = PP.text "true"
pprint (Conjunction [x]) = pprint x
pprint (Conjunction xs) =
  PP.parens (PP.cat $ PP.punctuate PP.comma $ map pprint xs)
pprint (Application s []) = PP.text s
pprint (Application s xs) =
  PP.text s <> PP.parens (PP.cat $ PP.punctuate PP.comma $ map pprint xs)
pprint (BinOp op a b) = 
  PP.parens (pprint a <+> PP.text op <+> pprint b)
pprint (CmpOp op a b) =
  pprint a <> PP.text op <> pprint b 
pprint (Is a b) =
  pprint a <+> PP.text "is" <+> pprint b
pprint (Equal a b) =
  pprint a <+> PP.text "=" <+> pprint b
pprint Cut = PP.text "!"
pprint Empty = PP.empty
pprint (Not x) = PP.text "not" <> PP.parens (pprint x)
pprint Underscore = PP.text "_"
pprint (Directive []) = PP.empty
pprint (Directive b) = 
  PP.text ":-" <+> 
  (PP.vcat $ PP.punctuate PP.comma $ map pprint b) <> 
  PP.text "."
pprint (Rule h []) = pprint h <> PP.text "."
pprint (Rule h b) = 
  pprint h <+> PP.text ":-" $$
  PP.nest 2
    ((PP.vcat $ PP.punctuate PP.comma $ map pprint b) <>
     PP.text ".")
pprint (StringLit sl) = 
  PP.text "str_lit" <> 
  PP.parens (PP.text "\"" <> prologEscape sl <> PP.text "\"")
pprint (Emit s) = PP.text s


-- Replace the tilde negation sign from the surface language
-- with standard minus sign
filterNum = map filterNumChar
filterNumChar '~' = '-'
filterNumChar c = c

prologNumEscape = PP.parens . PP.text . filterNum

prologEscape = PP.hcat . map prologEscapeChar

prologEscapeChar '\\' = PP.text "\\\\"
prologEscapeChar '\'' = PP.text "\\u0027"
prologEscapeChar '\"' = PP.text "\\u0022"
prologEscapeChar '\n' = PP.text "\\n"
prologEscapeChar '\a' = PP.text "\\a"
prologEscapeChar '\b' = PP.text "\\b"
prologEscapeChar '\f' = PP.text "\\f"
prologEscapeChar '\r' = PP.text "\\r"
prologEscapeChar '\t' = PP.text "\\t"
prologEscapeChar '\v' = PP.text "\\v"
prologEscapeChar ' '  = PP.text " "

prologEscapeChar c 
  | isAscii c && (isPunctuation c || isAlphaNum c) = PP.text [c]
  | otherwise = PP.text "\\u" <> PP.text (hexRepresentation (toEnum (fromEnum c)))

hexRepresentation :: Word16 -> String
hexRepresentation x =
   map (intToDigit . fromEnum)
   [ (x `shiftR`  12) .&. 0x0F
   , (x `shiftR`   8) .&. 0x0F
   , (x `shiftR`   4) .&. 0x0F
   , (x `shiftR`   0) .&. 0x0F
   ]
