{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module defines the set of tokens produced by the lexer
--   and consumed by the parser.
module Tokens where

-- | A 'Pn' indicates a position in a source file.
--   It contains the name of the source file, the line number and column number.
data Pn = Pn FilePath !Int !Int
  deriving (Eq, Ord, Show)

-- | Display a position in the format 'filename:line:col:'
--   for printing error messages.
displayPn :: Pn -> String
displayPn (Pn f l c) = f ++ ":" ++ show l ++ ":" ++ show c ++ ": "

-- | @Located a@ represents an element of type @a@ paired with a location.
data Located a = L Pn a
  deriving (Show)


-- | Retrieve the element from a 'Located' package.
unloc (L _ x) = x

-- | The main token datatype.
data Token
  = LPAREN
  | RPAREN
  | LBRACK
  | RBRACK
  | DOT
  | COMMA
  | SEMI
  | TIdent String
  | Var String
  | NumberLit String
  | StringLit String
  | TURNSTILE
  | QMARK
  | ARROW
  | SM_ARROW
  | VBAR
  | PLUSPLUS
  | PLUS
  | MINUS
  | EQUAL
  | LE
  | GE
  | LT
  | GT
  | NE
  | AXIOM
  | LEMMA
  | QUERY
  | NOT
  | HANDLE
  | END
  | FINDALL
  | UNDERSCORE
  | STAR
  | SLASH
  | KEY
  | TILDE
  | PREDICATE
  | PRIMITIVE
  | MODE
  | LIST
  | MAP
  | SET
  | TABLE
  | INDEX
  | IN
  | OUT
  | IGNORE
  | TYPE
  | LBRACE
  | RBRACE
  | COLON
  | DATA
  | NUMBER
  | DEFINEDBY
  | PROCEDURE
  | FUNCTION
  | YIELDS
  | STRING
  | LIFETIME
  | IMPORT
  | EVENT
  | EOF
  | DELETE
  | INSERT
  | INTO
  | FROM
  | FOREACH
  | PRAGMA
  | DEFINE
  | COLONEQ
  | QUEUE
  | SKIP
  | ACTION
  | CONFLICT
  | FORBID
  | TRUE
  | FALSE
  | PRESUME
 deriving (Eq,Show)


-- | Given a string, this function determines if the string
--   represents a keyword.  If so, it returns the corresponding
--   token.  Otherwise, it return an identifier token containing
--   the given string.
--
keyword_or_ident :: String -> Token

keyword_or_ident "not"       = NOT
keyword_or_ident "query"     = QUERY
keyword_or_ident "handle"    = HANDLE
keyword_or_ident "end"       = END
keyword_or_ident "findall"   = FINDALL
keyword_or_ident "predicate" = PREDICATE
keyword_or_ident "mode"      = MODE
keyword_or_ident "primitive" = PRIMITIVE
keyword_or_ident "list"      = LIST
keyword_or_ident "map"       = MAP
keyword_or_ident "set"       = SET
keyword_or_ident "table"     = TABLE
keyword_or_ident "index"     = INDEX
keyword_or_ident "in"        = IN
keyword_or_ident "out"       = OUT
keyword_or_ident "ignore"    = IGNORE
keyword_or_ident "type"      = TYPE
keyword_or_ident "data"      = DATA
keyword_or_ident "number"    = NUMBER
keyword_or_ident "string"    = STRING
keyword_or_ident "procedure" = PROCEDURE
keyword_or_ident "function"  = FUNCTION
keyword_or_ident "yields"    = YIELDS
keyword_or_ident "lifetime"  = LIFETIME
keyword_or_ident "event"     = EVENT
keyword_or_ident "import"    = IMPORT
keyword_or_ident "delete"    = DELETE
keyword_or_ident "insert"    = INSERT
keyword_or_ident "from"      = FROM
keyword_or_ident "into"      = INTO
keyword_or_ident "foreach"   = FOREACH
keyword_or_ident "pragma"    = PRAGMA
keyword_or_ident "define"    = DEFINE
keyword_or_ident "skip"      = SKIP
keyword_or_ident "queue"     = QUEUE
keyword_or_ident "key"       = KEY
keyword_or_ident "axiom"     = AXIOM
keyword_or_ident "lemma"     = LEMMA
keyword_or_ident "action"    = ACTION
keyword_or_ident "conflict"  = CONFLICT
keyword_or_ident "true"      = TRUE
keyword_or_ident "false"     = FALSE
keyword_or_ident "forbid"    = FORBID
keyword_or_ident "presume"   = PRESUME

keyword_or_ident s           = TIdent s


-- | Extract a string from a token.  Fails if the token is not
--   one of 'TIdent', 'Var', 'NumberLit' or 'StringLit'.
--
token_str :: Located Token -> String
token_str (L _ (TIdent x)) = x
token_str (L _ (Var x)) = x
token_str (L _ (NumberLit x)) = x
token_str (L _ (StringLit x)) = x
token_str (L _ t) = error $ show t ++ " does not contain a string"
