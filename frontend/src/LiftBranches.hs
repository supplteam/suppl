{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements an IR-to-IR pass that lifts out the body
--   of predicates or procedures in positions where they will eventually
--   appear as callable goals in the produced Prolog code, such as
--   inside @findall@ or @once@ combinators.  This branch lifting
--   pass is similar to lambda-lifting and ensures that every callable
--   goal position will be a named predicate applied to arguments.
--
--   Invariant: we assume terms are decomposed into the weak sort of A-normal
--   form that is produced by the LiftAssigns pass.
--
module LiftBranches 
( liftProg
) where

import Control.Monad
import qualified Data.Map as M
import Data.Map ( Map )
import Data.List

import Tokens (Pn(..))
import AST (Mode(..), Ident(..), Var(..), OP(..), getIdent )
import Compile
import SupplIR

-- | Lift predicate and procedure bodies occuring in \"higher-order\" positions.
liftProg :: [Decl] -> Comp [Decl]
liftProg = fmap concat . mapM liftDecl

liftDecl :: Decl -> Comp [Decl]
liftDecl (DRule (ident,md,body)) = do
  (newdecls,body') <- liftPBodies body
  return (DRule (ident,md,body') : newdecls)

liftDecl (DHandler (ident,i,body)) = do
  (newdecls,body') <- liftHBodies body
  return (DHandler (ident,i,body') : newdecls)

liftDecl (DProcDefn (ident,i,body)) = do
  (newdecls,body') <- liftHBodies body
  return (DProcDefn (ident,i,body') : newdecls)

liftDecl d@(DRegressionQuery pn n [Instr (CallPred _ _ _)]) =
  return [d]

liftDecl (DRegressionQuery pn n body) = do
  (newdecls,body') <- liftPBodies body
  nm <- compFreshVar >>= (return . Ident (Pn "" 0 0) . ("freshPred"++) . show)
  let fvs = nub $ freePBodies body'
  let putvs = map (\ (i,x) -> Instr (Match (LArg i) (RVar x)))
                  (zip [0..] fvs)
  let mode = map (const ModeOut) fvs
  let args = map (Left . LVar) fvs
  return ( DRegressionQuery pn n [Instr (CallPred mode nm args)]
         : DRule (nm, mode, body' ++ putvs)
         : newdecls
         )

--  return (DRegressionQuery pn n body' : newdecls)

liftDecl (DRegressionHandle pn bs ev acts) =
  return [DRegressionHandle pn bs ev acts]

liftDecl (DEmit s) = return [DEmit s]

liftHBodies :: [HandleBody] -> Comp ([Decl],[HandleBody])
liftHBodies bs = do
  (ds,bs') <- fmap unzip $ mapM liftHBody bs
  return (concat ds, bs')

liftHBody :: HandleBody -> Comp ([Decl],HandleBody)
liftHBody (HInstr i) = do
    (ds,i') <- liftHInstr i
    return (ds, HInstr i')

liftHBody (HBranch xs) = liftBranches xs

liftHBody (HForeach ps bs) = do
    (ds,bs') <- liftBranches [(ps,bs)]
    return (ds, HForeach [] [bs'])

liftBranches :: [([PredBody],[HandleBody])] -> Comp ([Decl], HandleBody)
liftBranches xs = do
   nm <- compFreshVar >>= (return . Ident (Pn "" 0 0) . ("freshProc"++) . show)
   let fvs = nub $ freeHBodies [HBranch xs]
   ds <- fmap concat $ mapM (branchDecl nm fvs) xs 
   let b = HInstr (HCallProc nm (map RVar fvs))
   return (ds, b)

branchDecl :: Ident -> [Var] -> ([PredBody],[HandleBody]) -> Comp [Decl]
branchDecl nm vs (ps,bs) = do
   (ds,ps') <- liftPBodies ps
   let getvs = map (\ (i,v) -> Instr (Match (LVar v) (RArg i))) $ zip [0..] vs
   (ds',bs') <- liftHBodies bs       
   let body = HInstr (HPred (getvs ++ ps')) : bs'
   return (DProcDefn (nm, length vs, body) : ds ++ ds')

liftHInstr :: HandleInstr -> Comp ([Decl],HandleInstr)
liftHInstr (HPred ps) = do
    (ds,ps') <- liftPBodies ps
    return (ds, HPred ps')
liftHInstr i = return ([],i)

liftPBodies :: [PredBody] -> Comp ([Decl],[PredBody])
liftPBodies ps = do
    (ds, ps') <- fmap unzip $ mapM liftPBody ps 
    return (concat ds, ps')

liftPBody :: PredBody -> Comp ([Decl], PredBody)
liftPBody (Instr i) = do
   (ds,i') <- liftPInstr i
   return (ds, Instr i')
liftPBody (Disj xss) = do
   (ds, xss') <- fmap unzip $ mapM liftPBodies xss
   return (concat ds, Disj xss')
liftPBody (Not ps) = do
   (ds, ps') <- liftPBodies ps
   return (ds, Not ps')

liftPInstr :: PredInstr -> Comp ([Decl],PredInstr)
liftPInstr (Findall r ps l) = do
   nm <- compFreshVar >>= (return . Ident (Pn "" 0 0) . ("freshPred"++) . show)
   x <- compFreshVar >>= (return . ("Fresh"++) . show)
   (ds,ps') <- liftPBodies (ps ++ [Instr (Match (LVar x) r)])
   let fvs = nub $ freePBodies ps'
   let getvs = map (\ (i,v) -> Instr (Match (LVar v) (RArg i))) $ zip [1..] fvs
   let putvs = [Instr (Match (LArg 0) (RVar x))]
   let body = getvs ++ ps' ++ putvs
   let mode = ModeOut : map (const ModeIn) fvs
   let args = Left (LVar x) : map (Right . RVar) fvs
   return ( DRule (nm, mode, body) : ds
          , Findall (RVar x) [Instr (CallPred mode nm args)] l
          )

liftPInstr i = return ([],i)
