{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements the Supple modechecker.  The main
--   purpose of mode checking is to ensure that every predicate
--   rule can be implemeneted in a way so that uninstantiated
--   variables are never encountered.  Essentially, for each
--   declared mode of each predicate, we want to find an order
--   for the clauses of each rule so that data dependecies flow
--   from top to bottom; from inputs to outputs.
--
--   This particular modecheker doesn't try to do anything clever;
--   it just searches through the space of all possible clause orderings
--   and called predicate modes until it finds a combination satisfying
--   the data dependencies.  Clearly, this algorithm has the potential
--   to perform very badly.  However, it appears to perform acceptably
--   on our sample programs.
--
--   I conjecture ill-moded, long rules filled with many conjunctions
--   would cause factorial behavior to be exhibited.  A better algorithm
--   may be required if this becomes an issue.
--
module Modecheck
( modecheck
, orderTableModes
, expandNamedModes
) where

import Control.Monad
import qualified Data.Map as M
import Data.Map ( Map )
import Data.List
import Data.Maybe
       
import SymbolTable
import Compile

import AST
import Tokens( Pn(..) )
import qualified SupplIR as IR
import qualified Typecheck as TC

{- FIXME?? Does the predicate mode checking deal correctly with nonlinear patterns?
   That is, patterns where a variable appears more than once.  What about
   combined with binding variable occurences?
-} 

-- | The main entry point for the modechecker.
--   Given a typechecked source-level program AST,
--   produce an IR program implementing the source program.
--
modecheck :: Prog (Pn,IType)
          -> Comp [IR.Decl]
modecheck prog = do
   verifyModeSignatures prog
   phaseFail
   dss <- mapM (\ (pn,_,d) -> onFail [] $ modecheckDecl pn d) prog
   return (concat dss)


type VarMap = Map Var ()

varmap_glb :: VarMap -> VarMap -> VarMap
varmap_glb v1 v2 = M.intersection v1 v2

varmap_lub :: VarMap -> VarMap -> VarMap
varmap_lub v1 v2 = M.union v1 v2

newtype MC a = MC { unMC :: VarMap -> Comp [(VarMap,a)] }

mcErrMsg :: Pn -> String -> MC a
mcErrMsg pn msg = MC $ \vm -> errMsg pn msg

instance Monad MC where
  return x = MC $ \vm -> return [(vm,x)]
  m >>= f  = MC $ \vm ->
               unMC m vm >>= 
               fmap concat . mapM (\ (vm',x) -> unMC (f x) vm')
  fail msg = MC $ \vm -> fail msg

instance Functor MC where
  fmap f m = m >>= return . f

instance MonadPlus MC where
  mzero = MC $ \vm -> return []
  mplus x y = MC $ \vm -> do
                      xs <- unMC x vm
                      ys <- unMC y vm
                      return (xs++ys)

withVarMap :: (VarMap -> MC a) -> MC a
withVarMap f = MC $ \vm -> unMC (f vm) vm

setVarMap :: VarMap -> MC ()
setVarMap vm = MC $ \_ -> return [(vm,())]

addToVarMap :: [Var] -> MC ()
addToVarMap vs = MC $ \vm ->
   return [(foldr (\v m -> M.insert v () m) vm vs,())]

withST :: (SymbolTable -> MC a) -> MC a
withST f = MC $ \vm -> do
   x <- compGetState
   unMC (f (comp_st x)) vm

unravel_type :: IType -> MC IR.Type
unravel_type ty = MC $ \vm -> do
   ty' <- unravel_type' ty
   return [(vm,ty')]

unravel_type' :: IType -> Comp IR.Type
unravel_type' (IType (TypeName n args)) = do
   x <- lookupTypeIdent n
   case sym_info x of
      TypeSymbol (TypeDecl _ formalArgs t) -> 
           do t' <- TC.expandTypeDefn' (undefined {-FIXME-}) args formalArgs t -- types should probably cary Pn's like terms, clauses, etc.
              unravel_type' t'
      TypeSymbol (PrimTypeDecl _) -> 
           do args' <- mapM unravel_type' args
              return (IR.TypeName n args')
      DataSymbol _ -> 
           do args' <- mapM unravel_type' args
              return (IR.TypeName n args')
unravel_type' (IType (TypeTuple ts)) = do
      ts' <- mapM unravel_type' ts
      return (IR.TypeTuple ts')
unravel_type' (IType (TypeList t)) = do
      t' <- unravel_type' t
      return (IR.TypeList t')
unravel_type' (IType (TypeSet t)) = do
      t' <- unravel_type' t
      return (IR.TypeSet t')
unravel_type' (IType (TypeMap t1 t2)) = do
      t1' <- unravel_type' t1
      t2' <- unravel_type' t2
      return (IR.TypeMap t1' t2')
unravel_type' (IType TypeNumber) = return IR.TypeNumber
unravel_type' (IType TypeString) = return IR.TypeString
unravel_type' (IType (TypeVar v)) = do
   (pn,x) <- lookupTypeVar v 
   case x of
      UnboundTyVar -> errMsg pn $ "unbound type variable"
      RigidTyVar x -> return (IR.TypeRigidVar x)
      BoundTyVar t' -> unravel_type' t'


class TermLike tm => MCTerm tm where
    mode_check_unify :: tm (Pn,IType) ->
                        tm (Pn,IType) ->
                        MC [IR.PredInstr]
    mode_check_rval  :: tm (Pn,IType) -> MC IR.RValue
    mode_check_lval  :: tm (Pn,IType) -> MC ([Var],IR.LValue)
    mode_check_eval  :: tm (Pn,IType) -> MC IR.Evaluable

instance MCTerm Term where
    mode_check_unify (TVar _ v) tm = mode_check_match v tm
    mode_check_unify tm (TVar _ v) = mode_check_match v tm
    mode_check_unify (TTerm a1 t1) (TTerm a2 t2) =
         mode_check_unify' a1 t1 a2 t2

    mode_check_lval (TVar (_,ty) v) = 
       withVarMap $ \vm ->
         case M.lookup v vm of
            Just () ->
               do ty' <- unravel_type ty
                  return ([], IR.LRVal ty' (IR.RVar v))
            Nothing -> do
               return ([v],IR.LVar v)
    mode_check_lval (TTerm a t) = mode_check_lval' a t

    mode_check_rval (TVar _ v) = 
       withVarMap $ \vm -> maybe mzero (\_ -> return (IR.RVar v)) (M.lookup v vm)
    mode_check_rval (TTerm a t) = mode_check_rval' a t

    mode_check_eval (TVar _ v) = 
       withVarMap $ \vm -> 
          maybe mzero (\_ -> return (IR.EVar v)) (M.lookup v vm)
    mode_check_eval (TTerm a t) = mode_check_eval' a t



instance MCTerm QTerm where
    mode_check_unify (QBindVar _ v) tm = mode_check_qmatch v tm
    mode_check_unify tm (QBindVar _ v) = mode_check_qmatch v tm
    mode_check_unify (QRefVar _ v) tm  = mode_check_qmatch' v tm
    mode_check_unify tm (QRefVar _ v)  = mode_check_qmatch' v tm
    mode_check_unify (QTerm a1 t1) (QTerm a2 t2) =
         mode_check_unify' a1 t1 a2 t2

    mode_check_lval (QBindVar _ v) = 
       withVarMap $ \vm ->
         case M.lookup v vm of
            Just () -> mzero
            Nothing -> return ([v],IR.LVar v)
    mode_check_lval (QRefVar _ v) = mzero
    mode_check_lval (QTerm a t) = mode_check_lval' a t

    mode_check_rval (QBindVar _ v) = mzero
    mode_check_rval (QRefVar _ v) =
       withVarMap $ \vm -> maybe mzero (\_ -> return (IR.RVar v)) (M.lookup v vm)
    mode_check_rval (QTerm a t) = mode_check_rval' a t

    mode_check_eval (QBindVar _ v) = mzero
    mode_check_eval (QRefVar _ v) = 
       withVarMap $ \vm -> 
          maybe mzero (\_ -> return (IR.EVar v)) (M.lookup v vm)
    mode_check_eval (QTerm a t) = mode_check_eval' a t


mode_check_qmatch :: Var -> QTerm (Pn,IType) -> MC [IR.PredInstr]
mode_check_qmatch v tm =
     withVarMap $ \vm ->
        do tm' <- mode_check_rval tm
           setVarMap (M.insert v () vm)
           return [IR.Match (IR.LVar v) tm']

mode_check_qmatch' :: Var -> QTerm (Pn,IType) -> MC [IR.PredInstr]
mode_check_qmatch' v tm =
    let (pn,ty) = getAnnot tm in
    (do ty' <- unravel_type ty
        tm' <- mode_check_rval tm
        return [IR.Compare ty' IR.Equal (IR.RVar v) tm'])
    `mplus`
    (do (vs,tm') <- mode_check_lval tm
        checkNoDupsMC vs
        addToVarMap vs
        return [IR.Match tm' (IR.RVar v)])

mode_check_match :: Var -> Term (Pn,IType) -> MC [IR.PredInstr]
mode_check_match v tm = withVarMap $ \vm ->
  let (pn,ty) = getAnnot tm in
  case M.lookup v vm of
    Just () ->
       (do ty' <- unravel_type ty
           tm' <- mode_check_rval tm
           return [IR.Compare ty' IR.Equal (IR.RVar v) tm'])
       `mplus`
       (do (vs,tm') <- mode_check_lval tm
           checkNoDupsMC vs
           addToVarMap vs
           return [IR.Match tm' (IR.RVar v)])

    Nothing ->
       do tm' <- mode_check_rval tm
          setVarMap (M.insert v () vm)
          return [IR.Match (IR.LVar v) tm']


checkNoDupsMC :: [Var] -> MC ()
checkNoDupsMC [] = return ()
checkNoDupsMC (v:vs) 
   | v `elem` vs = mzero
   | otherwise = checkNoDupsMC vs 

mode_check_unify'
   :: MCTerm tm
   => (Pn, IType) -> Term' (tm (Pn, IType))
   -> (Pn, IType) -> Term' (tm (Pn, IType))
   -> MC [IR.PredInstr]

mode_check_unify' _ TAnonVar _ _ = return []
mode_check_unify' _ _ _ TAnonVar = return []
mode_check_unify' _ (TCons h t) _ (TList []) = return [IR.Fail]
mode_check_unify' _ (TList [])  _ (TCons h t) = return [IR.Fail]

mode_check_unify' _ (TCons h1 t1) a2 (TList (h2:t2)) = do
   l1 <- mode_check_unify h1 h2
   l2 <- mode_check_unify t1 (buildTerm a2 (TList t2))
   return (l1++l2)

mode_check_unify' a1 (TList (h1:t1)) _ (TCons h2 t2) = do
   l1 <- mode_check_unify h1 h2
   l2 <- mode_check_unify (buildTerm a1 (TList t1)) t2
   return (l1++l2)

mode_check_unify' _ (TCons h1 t1) _ (TCons h2 t2) = do
   l1 <- mode_check_unify h1 h2
   l2 <- mode_check_unify t1 t2
   return (l1++l2)

mode_check_unify' _ (TList xs) _ (TList ys) =
   if length xs == length ys
      then do 
        lss <- mapM (uncurry mode_check_unify) (zip xs ys)
        return (concat lss)
      else return [IR.Fail]

mode_check_unify'
            a1 t1@(TFunc n1 ags1)
            a2 t2@(TFunc n2 ags2) = 
 withST $ \st ->
   case (fmap sym_info $ st_lookup n1 st, fmap sym_info $ st_lookup n2 st) of
     (Just (FuncSymbol (FIConstructor _ _ _ _ tys)),
      Just (FuncSymbol (FIConstructor _ _ _ _ _))) ->
         if n1 == n2
           then mode_check_data_args ags1 ags2
           else return [IR.Fail]
     _ -> mode_check_unify2 (buildTerm a1 t1) (buildTerm a2 t2)

mode_check_unify' a1 t1 a2 t2 = mode_check_unify2 (buildTerm a1 t1) (buildTerm a2 t2)


mode_check_data_args :: MCTerm tm
                     => Args (tm (Pn,IType))
                     -> Args (tm (Pn,IType))
                     -> MC [IR.PredInstr]

mode_check_data_args (PositionalArgs xs) (PositionalArgs ys) = do
   xss <- mapM (uncurry mode_check_unify) (zip (map snd xs) (map snd ys))
   return (concat xss)

mode_check_data_args (NamedArgs xs) (NamedArgs ys) = do
   xss <- mapM (uncurry mode_check_unify) (zip (map (\ (_,_,x) -> x) xs) (map (\ (_,_,x) -> x) ys))
   return (concat xss)

mode_check_data_args _ _ = fail "mismatched argument types in unify decomposition"


mode_check_unify2 :: MCTerm tm
                  => tm (Pn,IType)
                  -> tm (Pn,IType)
                  -> MC [IR.PredInstr]
mode_check_unify2 t1 t2 =
  (do let (_,ty) = getAnnot t1
      ty' <- unravel_type ty
      t1' <- mode_check_rval t1
      t2' <- mode_check_rval t2
      return [IR.Compare ty' IR.Equal t1' t2'])
  `mplus`
  (do (vs,t1') <- mode_check_lval t1
      t2' <- mode_check_rval t2
      checkNoDupsMC vs
      addToVarMap vs
      return [IR.Match t1' t2'])
  `mplus`
  (do (vs,t2') <- mode_check_lval t2
      t1' <- mode_check_rval t1
      checkNoDupsMC vs
      addToVarMap vs
      return [IR.Match t2' t1'])


mode_check_lval' :: MCTerm tm
                 => (Pn, IType)
                 -> Term' (tm (Pn,IType)) 
                 -> MC ([Var],IR.LValue)

mode_check_lval' _ TAnonVar = return ([],IR.Ignore)

mode_check_lval' _ (TNum _ n) =
  return ([], IR.LRVal IR.TypeNumber (IR.REval (IR.ENumLit n)))

mode_check_lval' _ (TString s) =
  return ([], IR.LRVal IR.TypeString (IR.RStringLit s))

mode_check_lval' a@(_,ty) t@(TOp _ _ _) = 
 do t' <- mode_check_eval' a t
    ty' <- unravel_type ty
    return ([], IR.LRVal ty' (IR.REval t'))

mode_check_lval' a@(_,ty) t@(TNegative _) =
 do t' <- mode_check_eval' a t
    ty' <- unravel_type ty
    return ([], IR.LRVal ty' (IR.REval t'))

mode_check_lval' _ (TCons h t) = do
    (vs1,h') <- mode_check_lval h
    (vs2,t') <- mode_check_lval t
    return (vs1++vs2, IR.LCons h' t')

mode_check_lval' _ (TTuple xs) = do
    l <- mapM mode_check_lval xs
    let (vss, xs') = unzip l
    return (concat vss, IR.LTuple xs')

mode_check_lval' _ (TList xs) = do
    l <- mapM mode_check_lval xs
    let (vss, xs') = unzip l
    return (concat vss, IR.LList xs')

mode_check_lval' a@(pn,ty) t@(TFunc n args) =
    withST $ \st -> do
      case fmap sym_info $ st_lookup n st of
       Just (FuncSymbol (FIDecl _ _)) ->
           do t' <- mode_check_eval' a t
              ty' <- unravel_type ty
              return ([], IR.LRVal ty' (IR.REval t'))

       Just (FuncSymbol (FIConstructor _ _ _ _ _)) -> do
          l <- mode_check_args mode_check_lval args
          let (vss, args') = unzip l
          return (concat vss, IR.LData n args')

       _ -> mcErrMsg pn "invalid function found while modechecking"


mode_check_rval' :: MCTerm tm
                 => (Pn,IType) 
                 -> Term' (tm (Pn, IType))
                 -> MC IR.RValue

mode_check_rval' _ TAnonVar = mzero
mode_check_rval' _ (TNum _ n) = return (IR.REval (IR.ENumLit n))
mode_check_rval' _ (TString s) = return (IR.RStringLit s)

mode_check_rval' a@(pn,_) (TFunc n args) = withST $ \st ->
    case fmap sym_info $ st_lookup n st of
       Just (FuncSymbol (FIDecl _ _)) -> do
          args' <- mode_check_args mode_check_eval args
          return (IR.REval (IR.Funcall n args'))

       Just (FuncSymbol (FIConstructor _ _ _ _ _)) -> do
          args' <- mode_check_args mode_check_rval args
          return (IR.RData n args')

       _ -> mcErrMsg pn "invalid function found while modechecking"

mode_check_rval' _ (TCons t1 t2) = do
    t1' <- mode_check_rval t1
    t2' <- mode_check_rval t2
    return (IR.RCons t1' t2')

mode_check_rval' _ (TList ts) = do
    ts' <- mapM mode_check_rval ts
    return (IR.RList ts')

mode_check_rval' _ (TTuple ts) = do
    ts' <- mapM mode_check_rval ts
    return (IR.RTuple ts')

mode_check_rval' (pn,ty) (TOp op t1 t2) = do
    t1' <- mode_check_eval t1
    t2' <- mode_check_eval t2
    ty' <- unravel_type ty
    op' <- mode_check_op pn op ty'
    return (IR.REval (IR.EOp op' t1' t2'))

mode_check_rval' _ (TNegative t) = do
    t' <- mode_check_eval t
    return (IR.REval (IR.ENeg t'))


mode_check_op :: Pn -> OP -> IR.Type -> MC IR.OP
mode_check_op _ OpPlus  IR.TypeNumber = return IR.OpPlus
mode_check_op _ OpMinus IR.TypeNumber = return IR.OpMinus
mode_check_op _ OpTimes IR.TypeNumber = return IR.OpTimes
mode_check_op _ OpDiv   IR.TypeNumber = return IR.OpDiv
mode_check_op _ OpConcat IR.TypeString = return IR.OpStrConcat
mode_check_op _ OpConcat (IR.TypeList _) = return IR.OpListAppend
mode_check_op pn op ty =
   mcErrMsg pn $ unwords ["operator",show op,"applied at incorrect type: ",show ty]


mode_check_eval' :: MCTerm tm
                 => (Pn,IType)
                 -> Term' (tm (Pn,IType))
                 -> MC IR.Evaluable

mode_check_eval' _ TAnonVar = mzero
mode_check_eval' _ (TNum _ n) = return (IR.ENumLit n)
mode_check_eval' _ (TString s) = return (IR.EStringLit s)

mode_check_eval' (pn,_) (TFunc n args) = withST $ \st ->
    case st_lookup n st of
       Just (Symbol _ _ _ (FuncSymbol (FIDecl _ (PrimFunctionDecl _ _ _)))) -> do
          args' <- mode_check_args mode_check_eval args
          return (IR.Funcall n args')

       Just (Symbol _ _ _ (FuncSymbol (FIConstructor _ _ _ _ _))) -> do
          args' <- mode_check_args mode_check_rval args
          return (IR.EVal (IR.RData n args'))

       _ -> mcErrMsg pn "invalid function found while modechecking"

mode_check_eval' _ (TCons t1 t2) = do
    t1' <- mode_check_rval t1
    t2' <- mode_check_rval t2
    return (IR.EVal (IR.RCons t1' t2'))

mode_check_eval' _ (TList ts) = do
    ts' <- mapM mode_check_rval ts
    return (IR.EVal (IR.RList ts'))

mode_check_eval' _ (TTuple ts) = do
    ts' <- mapM mode_check_rval ts
    return (IR.EVal (IR.RTuple ts'))

mode_check_eval' (pn,ty) (TOp op t1 t2) = do
    t1' <- mode_check_eval t1
    t2' <- mode_check_eval t2
    ty' <- unravel_type ty
    op' <- mode_check_op pn op ty'
    return (IR.EOp op' t1' t2')

mode_check_eval' _ (TNegative t) = do
    t' <- mode_check_eval t
    return (IR.ENeg t')


mode_check_args :: (a -> MC x)
                -> Args a
                -> MC [x]
mode_check_args f (PositionalArgs xs) = mapM (f . snd) xs
mode_check_args f (NamedArgs xs) = mapM (\ (_,_,x) -> f x) xs

selectElement :: [a] -> MC (a,[a])
selectElement [] = mzero
selectElement (a:as) = 
    (return (a,as)) `mplus` (do (x,xs) <- selectElement as; return (x, a:xs))


scheduleClauses :: [Clause (Pn,IType)] -> MC [IR.PredBody]
scheduleClauses [] = return []
scheduleClauses cs = do
    (c,cs') <- selectElement cs
    c' <- scheduleOneClause c
    cs'' <- scheduleClauses cs'
    return (c' ++ cs'')

scheduleOneClause :: Clause (Pn,IType) -> MC [IR.PredBody]
scheduleOneClause (Clause pn (CEq t1 t2)) =
    fmap (map IR.Instr) (mode_check_unify t1 t2)

scheduleOneClause (Clause a (CNeq t1 t2)) =
    do let (_,ty) = getAnnot t1
       ty' <- unravel_type ty
       t1' <- mode_check_rval t1
       t2' <- mode_check_rval t2
       return [IR.Instr (IR.Compare ty' IR.NotEqual t1' t2')]

scheduleOneClause (Clause a (CNot c)) = 
   do c' <- scheduleNegClause c
      return [IR.Not c']

scheduleOneClause (Clause a CEmpty) = return []
scheduleOneClause (Clause a CTrue)  = return []
scheduleOneClause (Clause a CFalse) = return [IR.Instr IR.Fail]

scheduleOneClause (Clause a (COr c1 c2)) = 
  withVarMap $ \vm0 ->
    do (vm1,c1') <- (scheduleClause c1 >>= \x -> withVarMap $ \vm -> return (vm,x))
       (vm2,c2') <- (setVarMap vm0 >> scheduleClause c2 >>= \x -> withVarMap $ \vm -> return (vm,x))
       setVarMap (varmap_glb vm1 vm2)
       return [IR.Disj [c1',c2']]

scheduleOneClause (Clause a (CAnd x y)) =
    fail "impossible case in scheduleOneClause!"
    -- This is OK, because this should never happen
    -- Clauses are supposed to be decomposed first

scheduleOneClause (Clause a (CComp cop t1 t2)) =
    do let (_,ty) = getAnnot t1
       ty' <- unravel_type ty
       t1' <- mode_check_rval t1
       t2' <- mode_check_rval t2
       case cop of
         OpLE -> return [IR.Instr (IR.Compare ty' IR.LessEqual t1' t2')]
         OpLT -> return [IR.Instr (IR.Compare ty' IR.LessThan  t1' t2')]
         OpGE -> return [IR.Instr (IR.Compare ty' IR.LessEqual t2' t1')]
         OpGT -> return [IR.Instr (IR.Compare ty' IR.LessThan  t2' t1')]


scheduleOneClause (Clause _ (CFindall t (_,nm,args) v)) =
    withVarMap $ \vm -> do
      c' <- schedulePred nm args
      t' <- mode_check_rval t
      setVarMap (M.insert v () vm)
      return [IR.Instr (IR.Findall t' [IR.Instr c'] v)]

scheduleOneClause (Clause _ (CPred nm args)) = do
    c' <- schedulePred nm args
    return [IR.Instr c']


scheduleNegClause :: Clause (Pn,IType) -> MC [IR.PredBody]
scheduleNegClause (Clause _ (CEq t1 t2)) =
    do let (_,ty) = getAnnot t1
       ty' <- unravel_type ty
       t1' <- mode_check_rval t1
       t2' <- mode_check_rval t2
       return [IR.Instr (IR.Compare ty' IR.Equal t1' t2')]

scheduleNegClause (Clause _ (CNeq t1 t2)) =
    do let (_,ty) = getAnnot t1
       ty' <- unravel_type ty
       t1' <- mode_check_rval t1
       t2' <- mode_check_rval t2
       return [IR.Instr (IR.Compare ty' IR.NotEqual t1' t2')]

scheduleNegClause (Clause _ (CAnd x y)) =
    do x' <- scheduleNegClause x
       y' <- scheduleNegClause y
       return (x'++y')

scheduleNegClause (Clause _ (COr x y)) =
    do x' <- scheduleNegClause x
       y' <- scheduleNegClause y
       return [IR.Disj [x',y']]

scheduleNegClause (Clause _ (CNot x)) =
    do x' <- scheduleNegClause x
       return [IR.Not x']

scheduleNegClause (Clause _ CEmpty) = return []
scheduleNegClause (Clause _ CTrue)  = return []
scheduleNegClause (Clause _ CFalse) = return [IR.Instr IR.Fail]
 
scheduleNegClause (Clause _ (CComp cop t1 t2)) =
    do let (_,ty) = getAnnot t1
       ty' <- unravel_type ty
       t1' <- mode_check_rval t1
       t2' <- mode_check_rval t2
       case cop of
         OpLE -> return [IR.Instr (IR.Compare ty' IR.LessEqual t1' t2')]
         OpLT -> return [IR.Instr (IR.Compare ty' IR.LessThan  t1' t2')]
         OpGE -> return [IR.Instr (IR.Compare ty' IR.LessEqual t2' t1')]
         OpGT -> return [IR.Instr (IR.Compare ty' IR.LessThan  t2' t1')]

scheduleNegClause (Clause _ (CFindall tm (_,nm,args) v)) = mzero

scheduleNegClause (Clause _ (CPred nm args)) = withST $ \st ->
   let md = mapArgs (\_ -> AST.ModeIn) args in
   case st_lookup nm st of
        Just (Symbol _ _ _ (PredSymbol (PIDecl pd) modes)) -> do
           let tys = case pd of
                         PredDecl _ tys -> tys
                         PrimPredDecl _ tys -> tys
            in if elemBy (eqArgModes (mapArgs fst tys)) md modes
                 then do (md',args') <- mode_check_pred_call args md
                         return [IR.Instr (IR.CallPred md' nm args')]
                 else mzero

        Just (Symbol _ _ _ (TableSymbol (TIDecl (TableDecl _ tys _ _)) modes)) -> do
           if elemBy (eqArgModes tys) md modes
              then do (md',args') <- mode_check_pred_call args md
                      return [IR.Instr (IR.CallPred md' nm args')]
              else mzero

        _ -> mzero

elemBy :: (a -> a -> Bool) -> a -> [a] -> Bool
elemBy f a xs = isJust $ find (f a) xs 

schedulePred :: MCTerm tm
             => Ident
             -> Args (tm (Pn,IType))
             -> MC IR.PredInstr
schedulePred nm args = withST $ \st ->
   case st_lookup nm st of
        Just (Symbol _ _ _ (PredSymbol pi modes)) -> do
           let mds = orderPredModes pi modes
           (md,args') <- foldr mplus mzero $ map (mode_check_pred_call args) mds
           return (IR.CallPred md nm args')

        Just (Symbol _ _ _ (TableSymbol ti modes)) -> do
           let mds = orderTableModes ti modes
           (md,args') <- foldr mplus mzero $ map (mode_check_pred_call args) mds
           return (IR.QueryTable md nm args')

        _ -> mzero


predInfoTys :: Monad m => PredInfo -> m (Args Type)
predInfoTys (PIEmpty _) = fail "failed predinfo lookup"
predInfoTys (PIDecl (PredDecl _ tys)) = return (mapArgs fst tys)
predInfoTys (PIDecl (PrimPredDecl _ tys)) = return (mapArgs fst tys)

tableInfoTys :: Monad m => TableInfo -> m (Args Type)
tableInfoTys (TIEmpty _) = fail "failed tableinfo lookup"
tableInfoTys (TIDecl (TableDecl _ tys _ _)) = return tys 

mode_check_pred_call :: MCTerm tm
                     => Args (tm (Pn,IType))
                     -> Args Mode 
                     -> MC ([Mode],[Either IR.LValue IR.RValue])
mode_check_pred_call (PositionalArgs args) (PositionalArgs mds) =
  do xs <- mapM (uncurry mode_check_pred_arg) (zip (map snd args) (map snd mds))
     let (vs, mds', args') = unzip3 xs
     checkNoDupsMC (concat vs)
     addToVarMap (concat vs)
     return (mds',args')

mode_check_pred_call (NamedArgs args) (NamedArgs mds) =
  do xs <- mapM (uncurry mode_check_pred_arg)
                (zip (map (\ (_,_,x) -> x) args) (map (\ (_,_,x) -> x) mds))
     let (vs, mds', args') = unzip3 xs
     checkNoDupsMC (concat vs)
     addToVarMap (concat vs)
     return (mds',args')

mode_check_pred_call _ _ = fail "mismatched argument types"

mode_check_pred_arg :: MCTerm tm
                    => tm (Pn,IType)
                    -> Mode
                    -> MC ([Var], Mode, Either IR.LValue IR.RValue)
mode_check_pred_arg tm md =
      case md of
         ModeIn  -> do tm' <- mode_check_rval tm
                       return ([], ModeIn, Right tm')

         ModeOut -> do (vs,tm') <- mode_check_lval tm
                       return (vs, ModeOut, Left tm')

         ModeIgnore -> return ([], ModeIgnore, Left IR.Ignore)


scheduleClause :: Clause (Pn,IType) -> MC [IR.PredBody]
scheduleClause c = scheduleClauses (decomposeClause c)

decomposeClause :: Clause ta -> [Clause ta]
decomposeClause (Clause _ (CAnd c1 c2)) = decomposeClause c1 ++ decomposeClause c2
decomposeClause (Clause _ (CEmpty)) = []
decomposeClause x = [x]


orderTableModes :: TableInfo -> [Args Mode] -> [Args Mode]
orderTableModes (TIDecl (TableDecl _ tys pkey _)) xs =
   map (expandNamedModes tys)
     $ removeDupsBy (compareArgModes tys)
     $ sortBy (compareArgModes tys) (pkey:xs)
orderTableModes _ _ = error "orderTableModes: impossible! missing table declaration"

orderPredModes :: PredInfo -> [Args Mode] -> [Args Mode]
orderPredModes (PIDecl (PredDecl _ tys)) xs =
   map (expandNamedModes (mapArgs fst tys))
     $ removeDupsBy (compareArgModes (mapArgs fst tys))
     $ sortBy (compareArgModes (mapArgs fst tys)) xs
orderPredModes (PIDecl (PrimPredDecl _ tys)) xs =
   map (expandNamedModes (mapArgs fst tys))
     $ removeDupsBy (compareArgModes (mapArgs fst tys))
     $ sortBy (compareArgModes (mapArgs fst tys)) xs
orderPredModes _ _ = error "orderPredModes: impossible! missing predicate declaration"

expandNamedModes :: Args Type -> Args Mode -> Args Mode
expandNamedModes ty (PositionalArgs mds) = (PositionalArgs mds)
expandNamedModes (NamedArgs tys) (NamedArgs mds) = NamedArgs (map f tys)
   where f (pn,n,_) = case find (\ (_,m,_) -> m == n) mds of
                         Nothing -> (pn,n,ModeIgnore)
                         Just x -> x
expandNamedModes _ _ = error "impossible! type/mode argument sort mismatch"



removeDupsBy :: (a -> a -> Ordering) -> [a] -> [a]
removeDupsBy f [] = []
removeDupsBy f [a] = [a]
removeDupsBy f (a:a':as) =
   case f a a' of
      EQ -> removeDupsBy f (a':as)
      _  -> a : removeDupsBy f (a':as)

compareModes :: Mode -> Mode -> Ordering
compareModes ModeIgnore ModeIgnore = EQ
compareModes ModeIgnore ModeIn = LT
compareModes ModeIgnore ModeOut = LT
compareModes ModeIn ModeIgnore = GT
compareModes ModeIn ModeIn = EQ
compareModes ModeIn ModeOut = LT
compareModes ModeOut ModeIgnore = GT
compareModes ModeOut ModeIn = GT
compareModes ModeOut ModeOut = EQ


eqArgModes :: Args Type -> Args Mode -> Args Mode -> Bool
eqArgModes tys m1 m2 = compareArgModes tys m1 m2 == EQ

compareArgModes :: Args Type -> Args Mode -> Args Mode -> Ordering
compareArgModes tys (PositionalArgs md1) (PositionalArgs md2) = 
   lexOrder compareModes (map snd md1) (map snd md2)
compareArgModes (NamedArgs tys) (NamedArgs md1) (NamedArgs md2) = 
	compareNamedArgModes tys md1 md2
compareArgModes (PositionalArgs _) (NamedArgs md1) (NamedArgs md2) = undefined
compareArgModes tys (PositionalArgs _) (NamedArgs _) = LT
compareArgModes tys (NamedArgs _) (PositionalArgs _) = GT

compareMaybeModes :: Maybe (Pn,String,Mode) -> Maybe (Pn,String,Mode) -> Ordering
compareMaybeModes m1 m2 = 
  compareModes 
     (maybe ModeIgnore (\ (_,_,m) -> m) m1)
     (maybe ModeIgnore (\ (_,_,m) -> m) m2)

compareNamedArgModes :: [(Pn,String,Type)] 
   -> [(Pn,String,Mode)] -> [(Pn,String,Mode)] -> Ordering
compareNamedArgModes [] _ _ = EQ
compareNamedArgModes ((_,n,_):tys) m1 m2 =
  let o = compareMaybeModes 
             (find (\ (_,x,_) -> x == n) m1)
             (find (\ (_,x,_) -> x == n) m2)    
   in case o of
        LT -> LT
        GT -> GT
        EQ -> compareNamedArgModes tys m1 m2

lexOrder :: (a -> a -> Ordering) -> [a] -> [a] -> Ordering
lexOrder f [] [] = EQ
lexOrder f [] (y:ys) = LT
lexOrder f (x:xs) [] = GT
lexOrder f (x:xs) (y:ys) =
  case f x y of
    LT -> LT
    GT -> GT
    EQ -> lexOrder f xs ys


unwind_mode :: Monad m => Args Type -> Args Mode -> m [Mode]
unwind_mode (PositionalArgs _) (PositionalArgs mds) =
   return (map snd mds)
unwind_mode (NamedArgs tys) (NamedArgs mds) =
   mapM (\ (_,n,_) -> 
           case find (\ (_,m,_) -> n == m) mds of
              Nothing -> return ModeIgnore
              Just (_,_,x) -> return x
        ) tys
unwind_mode _ _ = fail "argument sort mismatch"


modecheck_mode_rule :: Args Type -> Args Mode -> Rule (Pn,IType) -> MC IR.RuleDefn
modecheck_mode_rule tys mds ((pn,ident,args), body) =
   withVarMap $ \vm0 ->
      do mds' <- unwind_mode tys mds
         pre <- modecheck_in_args pn tys mds args
         body' <- scheduleClause body
         post <- modecheck_out_args pn tys mds args
         setVarMap vm0
         return (ident, mds', pre++body'++post)


modecheck_one_in_arg :: (Int,Mode,Term (Pn,IType)) -> MC [IR.PredBody]
modecheck_one_in_arg (_,ModeOut,_) = return []
modecheck_one_in_arg (_,ModeIgnore,_) = return []
modecheck_one_in_arg (i,ModeIn,tm) =
  (do let (pn,ty) = getAnnot tm 
      tm' <- mode_check_rval tm
      ty' <- unravel_type ty
      return [IR.Instr (IR.Compare ty' IR.Equal (IR.RArg i) tm')])
  `mplus`
  (do (vs,tm') <- mode_check_lval tm
      addToVarMap vs
      return [IR.Instr (IR.Match tm' (IR.RArg i))])

modecheck_one_out_arg :: (Int,Mode,Term (Pn,IType)) -> MC [IR.PredBody]
modecheck_one_out_arg (_,ModeIn,_) = return []
modecheck_one_out_arg (_,ModeIgnore,_) = return []
modecheck_one_out_arg (i,ModeOut,tm) =
   do tm' <- mode_check_rval tm
      return [IR.Instr (IR.Match (IR.LArg i) tm')]


modecheck_in_args
   :: Pn 
   -> Args Type
   -> Args Mode
   -> Args (Term (Pn,IType))
   -> MC ([IR.PredBody])
modecheck_in_args pn (PositionalArgs _) (PositionalArgs mds) (PositionalArgs tms) =
   do xs <- mapM modecheck_one_in_arg (zip3 [0..] (map snd mds) (map snd tms))
      return (concat xs)

modecheck_in_args pn (NamedArgs tys) (NamedArgs mds) (NamedArgs tms) =
   do xs <- mapM (\ (i,(_,n,_)) ->
                    case find (\ (_,m,_) -> n == m) mds of
                       Nothing -> return []
                       Just (_,_,ModeIgnore) -> return []
                       Just (_,_,ModeOut) -> return []
                       Just (_,_,ModeIn) ->
                          case find (\ (_,m,_) -> n == m) tms of 
                             Nothing -> mzero
                             Just (_,_,tm) -> modecheck_one_in_arg (i,ModeIn,tm)
                 ) (zip [0..] tys)
      return (concat xs)

modecheck_in_args _ _ _ _ = fail "argument sort mismatch"



modecheck_out_args
   :: Pn
   -> Args Type
   -> Args Mode
   -> Args (Term (Pn,IType))
   -> MC ([IR.PredBody])
modecheck_out_args pn (PositionalArgs _) (PositionalArgs mds) (PositionalArgs tms) = 
   do xs <- mapM modecheck_one_out_arg (zip3 [0..] (map snd mds) (map snd tms))
      return (concat xs)

modecheck_out_args pn (NamedArgs tys) (NamedArgs mds) (NamedArgs tms) =
   do xs <- mapM (\ (i,(_,n,_)) ->
                    case find (\ (_,m,_) -> n == m) mds of
                       Nothing -> return []
                       Just (_,_,ModeIgnore) -> return []
                       Just (_,_,ModeIn) -> return []
                       Just (_,_,ModeOut) ->
                          case find (\ (_,m,_) -> n == m) tms of 
                             Nothing -> mzero
                             Just (_,_,tm) -> modecheck_one_out_arg (i,ModeOut,tm)
                 ) (zip [0..] tys)
      return (concat xs)

modecheck_out_args _ _ _ _ = fail "argument sort mismatch"


modecheck_one_rule
   :: Args Type 
   -> Args Mode
   -> Rule (Pn,IType)
   -> Comp IR.RuleDefn

modecheck_one_rule tys mds r@((pn,_,_),_) = 
 do l <- unMC (modecheck_mode_rule tys mds r) M.empty
    case l of
     [] -> errMsg pn $ "rule fails to satisfy mode: "++displayMode mds
     (_,d):_ -> return d


modecheck_rule :: Rule (Pn,IType) -> Comp [IR.RuleDefn]
modecheck_rule r@((pn,ident,_),_) = do
    st <- compSymbolTable
    case st_lookup ident st of
      Just (Symbol _ _ _ (PredSymbol pi mds)) ->
        do tys <- predInfoTys pi
           mapM (\md -> modecheck_one_rule tys md r) mds

      _ -> fail $ "failed symbol lookup: "++(getIdent ident)


displayOneMode :: Mode -> String
displayOneMode ModeIn = "in"
displayOneMode ModeOut = "out"
displayOneMode ModeIgnore = "ignore"

displayOneNamedMode :: (Pn,String,Mode) -> String
displayOneNamedMode (_,n,md) = n++":"++displayOneMode md

displayMode :: Args Mode -> String
displayMode (PositionalArgs mds) =
  "(" ++ (intercalate ", " (map (displayOneMode . snd) mds)) ++ ")"
displayMode (NamedArgs mds) =
  "{" ++ (intercalate ", " (map displayOneNamedMode mds)) ++ "}"


verifyModeSig :: Pn -> Ident -> Args Type -> Args Mode -> Comp ()
verifyModeSig pn nm (PositionalArgs tys) (PositionalArgs modes) =
   if (length tys == length modes)
      then return ()
      else errMsg pn "incorrect number of modes"

verifyModeSig pn nm (NamedArgs tys) (NamedArgs modes) = mapM_ checkName modes
 where checkName (pn,n,_) = 
         case find (\ (_,x,_) -> x == n) tys of
                Nothing -> errMsg pn $ n++" is not a declared argument name for "++qt nm
                Just _ -> return ()

verifyModeSig pn nm (PositionalArgs _) (NamedArgs _) =
    errMsg pn $ qt nm++" declared with positional args"

verifyModeSig pn nm (NamedArgs _) (PositionalArgs _) =
    errMsg pn $ qt nm++" declared with named args"

verifyModeSignature :: (Pn,CodeOrigin,Decl ta) -> Comp ()

verifyModeSignature (pn, _, DPred pd) = do
   let nm = case pd of
              PredDecl nm _ -> nm
              PrimPredDecl nm _ -> nm 
   st <- compSymbolTable
   case st_lookup nm st of
         Just (Symbol _ _ _ (PredSymbol _ mds)) -> do
            if null mds
               then warnMsg pn $ unwords ["predicate",qt nm,"has no mode declarations"]
               else return ()

         _ -> errMsg pn $ unwords ["impossible! failed predicate lookup",qt nm]

verifyModeSignature (pn, _, DMode (ModeDecl nm mode)) = do
  st <- compSymbolTable
  case st_lookup nm st of
    Just (Symbol _ _ _ (PredSymbol (PIDecl (PredDecl _ tys)) _)) -> verifyModeSig pn nm (mapArgs fst tys) mode
    Just (Symbol _ _ _ (PredSymbol (PIDecl (PrimPredDecl _ tys)) _)) -> verifyModeSig pn nm (mapArgs fst tys) mode
    _ -> errMsg pn $ "missing predicate declaration for: "++qt nm

verifyModeSignature (pn, _, DIndex (IndexDecl nm mode)) = do
  x <- lookupTermIdent nm
  case x of
    Symbol _ _ _ (TableSymbol (TIDecl (TableDecl _ tys _ _)) _) -> verifyModeSig pn nm tys mode
    _ -> errMsg pn $ "missing table declaration for: "++qt nm

verifyModeSignature _ = return ()

verifyModeSignatures :: Prog ta -> Comp ()
verifyModeSignatures = mapM_ verifyModeSignature


modecheckDecl :: Pn 
              -> Decl (Pn,IType)
              -> Comp [IR.Decl]

modecheckDecl pn (DRule r) = 
   fmap (map IR.DRule) (modecheck_rule r)

modecheckDecl pn (DHandler (nm,args,body)) = do
   st <- compSymbolTable
   case st_lookup nm st of
      Just (Symbol _ _ _ (EventSymbol (EventDecl _ tys))) -> do
        (i,vm,args') <- modecheckProcArgs pn tys args
        let args'' = IR.HInstr (IR.HPred (map IR.Instr args'))
        body' <- modecheckBody vm body
        return [IR.DHandler (nm,i,args'' : body')]

      _ -> errMsg pn $ unwords [qt nm,"is expected to be an event"]

modecheckDecl pn (DProcDefn (nm,args,body)) = do
   st <- compSymbolTable
   case st_lookup nm st of
      Just (Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ tys))) -> do
        (i,vm,args') <- modecheckProcArgs pn tys args
        let args'' = IR.HInstr (IR.HPred (map IR.Instr args'))
        body' <- modecheckBody vm body
        return [IR.DProcDefn (nm,i,args'' : body')]

      _ -> errMsg pn $ unwords [qt nm,"is expected to be a nonprimitive procedure"]

modecheckDecl pn (DPragma (GenericPragma (Ident _ "emit") (Just (PTStringLit _ s)))) =
   return [IR.DEmit s]

modecheckDecl pn (DPragma (GenericPragma (Ident _ "emit") _)) =
   errMsg pn "invalid emit pragma"

modecheckDecl pn (DPragma (QueryPragma pn' num cl)) = do
   x <- unMC (scheduleClause cl) M.empty
   case x of
    [] -> errMsg pn "query pragma fails to modecheck!"
    ((_,body):_) -> return [IR.DRegressionQuery pn' num body]

modecheckDecl pn (DPragma (HandlePragma pn' ev acts)) = do
   ev' <- modecheckBasicForm ev
   acts' <- mapM modecheckBasicForm acts
   return [IR.DRegressionHandle pn' [] ev' acts']

modecheckDecl _ _ = return []

modecheckBasicForm 
   :: BasicForm (Term (Pn, IType))
   -> Comp (Ident, [IR.RValue])
modecheckBasicForm (BasicForm pn x args) = do
   args' <- modecheckActualArgs pn M.empty x args
   return (x, args')

modecheckProcArgs
   :: Pn 
   -> Args Type
   -> Args (Maybe Var)
   -> Comp (Int, VarMap, [IR.PredInstr])

modecheckProcArgs pn _ (PositionalArgs xs) =
  let f (i,m,is) (_,Nothing) = return (i+1, m, is)
      f (i,m,is) (_,Just v)  =
         case M.lookup v m of
           Just _ -> errMsg pn $ unwords ["variable",qts v,"bound multiple times"]
           Nothing -> 
            return (i+1, M.insert v () m, is ++ [IR.Match (IR.LVar v) (IR.RArg i)])
   in foldM f (0, M.empty, []) xs

modecheckProcArgs pn (NamedArgs tys) (NamedArgs xs) =
  let f (i,m,is) (_,nm,_) =
         case find (\ (_,nm',_) -> nm == nm') xs of
           Nothing -> return (i+1,m,is)
           Just (_,_,Nothing) -> return (i+1,m,is)
           Just (_,_,Just v)  ->
             case M.lookup v m of
               Just _ -> errMsg pn $ unwords ["variable",qts v,"bound multiple times"]
               Nothing -> return
                  (i+1,M.insert v () m, is ++ [IR.Match (IR.LVar v) (IR.RArg i)])
   in foldM f (0, M.empty, []) tys

modecheckProcArgs pn _ _ = errMsg pn "argument sort mismatch"



modecheckBody :: VarMap
              -> HBody (Pn,IType)
              -> Comp ([IR.HandleBody])

modecheckBody varmap (HBody pn HSkip) = return []

modecheckBody varmap (HBody pn (HSeq b1 b2)) = do
   b1' <- modecheckBody varmap b1
   b2' <- modecheckBody varmap b2
   return (b1'++b2')

modecheckBody varmap (HBody pn (HProcedure nm args)) = do
   args' <- modecheckActualArgs pn varmap nm args
   st <- compSymbolTable
   case st_lookup nm st of
      Just (Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl _ _))) ->
         return [IR.HInstr (IR.HAction nm args')]
      Just (Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ _))) ->
         return [IR.HInstr (IR.HCallProc nm args')]
      _ -> errMsg pn $ unwords ["expected",qt nm,"to be a procedure symbol"]

modecheckBody varmap (HBody pn (HInsert args nm)) = do
   args' <- modecheckActualArgs pn varmap nm args
   return [IR.HInstr (IR.HInsert nm args')]

modecheckBody varmap (HBody pn (HDelete args nm)) = do
   args' <- modecheckDelete pn varmap nm args
   return [IR.HInstr (IR.HDelete nm args')]

modecheckBody varmap (HBody pn (HQuery bs)) = do
   xs <- mapM (modecheckQueryBranch varmap) bs
   return [IR.HBranch xs]

modecheckBody varmap (HBody pn (HForeach q bd)) = do
   (varmap', q') <- modecheckQClause varmap q
   bd' <- modecheckBody varmap' bd
   return [IR.HForeach q' bd']


modecheckDelete
   :: Pn
   -> VarMap
   -> Ident
   -> Args (Term (Pn,IType))
   -> Comp [Maybe IR.RValue]
modecheckDelete pn vm nm args =
   do sym <- lookupTermIdent nm
      case sym_info sym of
         TableSymbol (TIDecl (TableDecl _ tys key life)) _ ->
             modecheckKey pn vm args (expandNamedModes tys key)

         _ -> errMsg (loc nm) $ unwords ["expected",qt nm,"to be a table"]

modecheckKey
   :: Pn
   -> VarMap
   -> Args (Term (Pn,IType))
   -> Args Mode
   -> Comp [Maybe IR.RValue]
modecheckKey pn0 vm (PositionalArgs xs) (PositionalArgs ms) = f xs ms
  where f [] [] = return []
        f ((pn,x):xs) ((_,ModeIn):ms) =
            do x' <- unMC (mode_check_rval x) vm
               case x' of
                  [] -> errMsg pn $ "improper argument mode"
                  ((_,x''):_) -> 
                     do xs' <- f xs ms
                        return (Just x'' : xs')
        f ((pn,x):xs) (_:ms) =
            do x' <- unMC (mode_check_lval x) vm
               case x' of
                  [] -> errMsg pn $ "improper argument mode"
                  _ ->
                     do xs' <- f xs ms
                        return (Nothing : xs')
        f _ _ = errMsg pn0 $ "impossible! argument mismatch"

modecheckKey pn0 vm (NamedArgs xs) (NamedArgs ms) = f xs ms
  where
   f [] [] = return []
   f ((pn,nm,x):xs) ((_,nm',ModeIn):ms) =
      do unless (nm == nm') (errMsg pn $ "impossible!: argument/mode name mismatch")
         x' <- unMC (mode_check_rval x) vm
         case x' of
            [] -> errMsg pn $ "improper argument mode"
            ((_,x''):_) ->
                do xs' <- f xs ms
                   return (Just x'' : xs')
   f ((pn,nm,x):xs) ((_,nm',_):ms) =
      do unless (nm == nm') (errMsg pn $ "impossible!: argument/mode name mismatch")
         x' <- unMC (mode_check_lval x) vm
         case x' of
            [] -> errMsg pn $ "improper argument mode"
            _ ->
                do xs' <- f xs ms
                   return (Nothing : xs')
   f _ _ = errMsg pn0 $ "impossible! argument mismatch"


modecheckKey pn0 vm _ _ = errMsg pn0 $ "argument sort mismatch!"


modecheckActualArgs
   :: Pn
   -> VarMap
   -> Ident
   -> Args (Term (Pn,IType))
   -> Comp [IR.RValue]
modecheckActualArgs pn vm nm (PositionalArgs args) =
   mapM (\ (pn,tm) -> do
      xs <- unMC (mode_check_rval tm) vm
      case xs of
        [] -> errMsg pn $ "improper argument mode"
        ((_,x):_) -> return x)
    args

modecheckActualArgs pn vm nm (NamedArgs args) = do
   mapM (\ (pn,_,tm) -> do
     xs <- unMC (mode_check_rval tm) vm
     case xs of
        [] -> errMsg pn $ "improper argument mode"
        ((_,x):_) -> return x)
    args

modecheckQueryBranch
   :: VarMap
   -> (Pn,QBranch (Pn,IType)) 
   -> Comp ([IR.PredBody],[IR.HandleBody])

modecheckQueryBranch vm (pn,DefaultBranch bd) = do
   bd' <- modecheckBody vm bd
   return ([],bd')

modecheckQueryBranch vm (pn,QueryBranch q bd) = do
   (vm',q') <- modecheckQClause vm q
   bd' <- modecheckBody vm' bd
   return (q',bd')

getPredModes
   :: Pn 
   -> Ident
   -> Comp [Args Mode]
getPredModes pn ident = do
  st <- compSymbolTable
  case st_lookup ident st of
    Just (Symbol _ _ _ (PredSymbol pi mds)) -> return (orderPredModes pi mds)
    Just (Symbol _ _ _ (TableSymbol ti mds)) -> return (orderTableModes ti mds)
    _ -> errMsg pn $ unwords [qt ident, "expected to be a predicate or table"]


modecheckQClause
   :: VarMap
   -> QClause (Pn, IType)
   -> Comp (VarMap, [IR.PredBody])
modecheckQClause vm (QCEq pn t1 t2) = do
   x <- unMC (mode_check_unify t1 t2) vm
   case x of
     [] -> errMsg pn $ unwords ["equation is ill-moded"]
     (vm', is):_ -> return (vm', map IR.Instr is)

modecheckQClause vm (QCPred pn ident args) = do
   x <- unMC (schedulePred ident args) vm
   case x of
     [] -> errMsg pn $ unwords ["predicate",qt ident,"has no satisfactory mode"]
     (vm',i):_ -> return (vm', [IR.Instr i])

modecheckQClause vm (QCAnd c1 c2) = do
   (vm', c1')  <- modecheckQClause vm c1
   (vm'', c2') <- modecheckQClause vm' c2
   return (vm'', c1'++c2')

modecheckQClause vm (QCNeq pn t1 t2) = do
   let (_,ty) = getAnnot t1
   ty' <- unravel_type' ty
   t1' <- mode_check_rvalue_qclause vm t1
   t2' <- mode_check_rvalue_qclause vm t2
   return (vm, [IR.Instr $ IR.Compare ty' IR.NotEqual t1' t2'])

modecheckQClause vm (QCComp pn op t1 t2) = do
   let (_,ty) = getAnnot t1
   ty' <- unravel_type' ty
   t1' <- mode_check_rvalue_qclause vm t1
   t2' <- mode_check_rvalue_qclause vm t2
   case op of
         OpLE -> return (vm,[IR.Instr (IR.Compare ty' IR.LessEqual t1' t2')])
         OpLT -> return (vm,[IR.Instr (IR.Compare ty' IR.LessThan  t1' t2')])
         OpGE -> return (vm,[IR.Instr (IR.Compare ty' IR.LessEqual t2' t1')])
         OpGT -> return (vm,[IR.Instr (IR.Compare ty' IR.LessThan  t2' t1')])

mode_check_rvalue_qclause
   :: VarMap
   -> QTerm (Pn, IType)
   -> Comp (IR.RValue)
mode_check_rvalue_qclause vm tm = do
   x <- unMC (mode_check_rval tm) vm
   case x of
      [] -> errMsg (fst (getAnnot tm)) "ill-moded term: probably attempts to bind variables in illegal position"
      (_,tm'):_ ->  return tm'
