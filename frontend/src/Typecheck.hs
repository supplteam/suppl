{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements the Suppl typechecker.  Its working parts
--   somewhat resemble the classic algorithm W for typechecking
--   Hendley-Milner polymorphism.  Each predicate rule and procedure
--   body is checked bottom up, with program variables being typed via
--   fresh type variables.  These type variables are then unified with concrete
--   types based on how the variable is used.
--
--   Typechecking invariant: after typechecking, all named arguments appearing
--   in rule or procedure bodies have a node for each name, and the names are
--   sorted in the same order as the predicate/event/table/etc. declaration.
--   Thus, named arguments can be treated essentially the same as positional
--   arguments after typechecking.
--
module Typecheck
( typecheck
, typecheckTerm
, typecheckQTerm
, typecheckClause
, expandTypeDefn'
) where

import Data.List
import Control.Monad
import Data.Graph
import qualified Data.Map as M
import Data.Map ( Map )
import qualified Data.Set as S
import Data.Set ( Set )
import Toposort( occursCheck )

import Tokens
import AST
import SymbolTable
import Compile

data Env = Env
   { envVars    :: Map String Integer
   }

initialEnv = Env M.empty

newtype TC a = TC { unTC :: Env -> Comp (Env,a) }

instance Monad TC where
  return x = TC (\env -> return (env,x))
  m >>= f  = TC (\env ->
                 do (env',x) <- unTC m env
                    unTC (f x) env')
  fail m   = TC (\env -> fail m)

instance Functor TC where
  fmap f m = m >>= return . f

liftTC :: Comp a -> TC a
liftTC m = TC $ \env ->
   do x <- m
      return (env, x)

errMsg' :: Pn -> String -> TC a
errMsg' pn msg = liftTC $ errMsg pn msg

displayType' :: IType -> TC String
displayType' t = liftTC $ displayType t

getFreshTypeVar :: Pn -> TC Integer
getFreshTypeVar pn = TC $ \env ->
   do v <- compFreshTypeVar pn UnboundTyVar
      return (env,v)

tryTC :: TC a -> TC (Maybe a)
tryTC m = TC $ \env ->
   do x <- tryComp (unTC m env)
      case x of
         Nothing -> return (env, Nothing)
         Just (env',a) -> return (env, Just a)

bindFreshTypeVar :: Pn -> String -> TC IType
bindFreshTypeVar pn x = TC $ \env ->
      do n <- compFreshTypeVar pn UnboundTyVar
         let evs = M.insert x n (envVars env)
         return( env{ envVars = evs }, IType (TypeVar n) )

bindRigidTypeVar :: Pn -> String -> TC IType
bindRigidTypeVar pn x = TC $ \env ->
      do n <- compFreshTypeVar pn (RigidTyVar x)
         let evs = M.insert x n (envVars env)
         return( env{ envVars = evs }, IType (TypeVar n) )


freshenType :: Bool -> Type -> TC IType
freshenType rgd (Type _ (TypeName x args)) =
     do args' <- mapM (freshenType rgd) args
        return $ IType $ (TypeName x args')
freshenType rgd (Type _ (TypeNumber)) = return $ IType TypeNumber
freshenType rgd (Type _ (TypeString)) = return $ IType TypeString
freshenType rgd (Type _ (TypeTuple xs)) = fmap (IType . TypeTuple) $ mapM (freshenType rgd) xs
freshenType rgd (Type _ (TypeList t)) = fmap (IType . TypeList) $ freshenType rgd t
freshenType rgd (Type _ (TypeSet t)) = fmap (IType . TypeSet) $ freshenType rgd t
freshenType rgd (Type _ (TypeMap t1 t2)) =
   do t1' <- freshenType rgd t1
      t2' <- freshenType rgd t2
      return $ IType $ (TypeMap t1' t2')
freshenType rgd (Type pn (TypeVar v)) = TC $ \env ->
    case M.lookup v (envVars env) of
       Just t -> return (env, IType $ TypeVar t)
       Nothing -> if rgd
                   then unTC (bindRigidTypeVar pn v) env
                   else unTC (bindFreshTypeVar pn v) env

freshenTypes :: Bool -> [(Pn,Type)] -> TC [(Pn,IType)]
freshenTypes rgd [] = return []
freshenTypes rgd ((pn,t):ts) =
    do t' <- freshenType rgd t
       ts' <- freshenTypes rgd ts
       return ((pn,t'):ts')

freshenNamedTypes :: Bool -> [(Pn,String,Type)] -> TC [(Pn,String,IType)]
freshenNamedTypes rgd [] = return []
freshenNamedTypes rgd ((pn,n,t):ts) =
    do t' <- freshenType rgd t
       ts' <- freshenNamedTypes rgd ts
       return ((pn,n,t'):ts')

freshenArgs :: Bool -> Args Type -> TC (Args IType)
freshenArgs rgd (PositionalArgs ts) = fmap PositionalArgs $ freshenTypes rgd ts
freshenArgs rgd (NamedArgs ts) = fmap NamedArgs $ freshenNamedTypes rgd ts

freshenMaybeType :: Bool -> Maybe Type -> TC (Maybe IType)
freshenMaybeType rgd Nothing = return Nothing
freshenMaybeType rgd (Just t) = fmap Just $ freshenType rgd t

freshenProcedureArgs :: Bool -> Args Type -> TC (Args IType)
freshenProcedureArgs rgd args = freshenArgs rgd args

freshenFuncArgs :: Bool -> (Args Type, Type) -> TC (Args IType, IType)
freshenFuncArgs rgd (args,t) =
   do args' <- freshenArgs rgd args
      t' <- freshenType rgd t
      return (args',t')

rigidFreshen :: (Bool -> a -> TC b) -> a -> TC b
rigidFreshen f x = TC $ \env ->
    do let env0 = Env M.empty
       (env',x') <- unTC (f True x) env0
       return (env, x')

doFreshen :: (Bool -> a -> TC b) -> a -> TC b
doFreshen f x = TC $ \env ->
    do let env0 = Env M.empty
       (env',x') <- unTC (f False x) env0
       return (env, x')


unifyVar ::  Pn -> Integer -> IType -> TC ()
unifyVar pn v t = unifyVar' pn v t >> occurs

unifyVar' :: Pn -> Integer -> IType -> TC ()
unifyVar' pn0 v t = TC $ \env ->
 do (_,ev) <- lookupTypeVar v
    case ev of
      UnboundTyVar -> compBindTypeVar v pn0 (BoundTyVar t) >> return (env,())

      RigidTyVar vnm ->
        case t of
          IType (TypeVar v') -> unifyVar'' pn0 vnm v v' env
          _ ->
            do t' <- displayType t
               errMsg pn0 $ unwords
                  ["cannot unify",qts t',"with rigid type variable",qts vnm]

      BoundTyVar t' -> unTC (unifyTypes pn0 t' t) env

unifyVar'' :: Pn -> String -> Integer -> Integer -> Env -> Comp (Env, ())
unifyVar'' pn vnm v v' env =
 do (_,ev) <- lookupTypeVar v'
    case ev of
     UnboundTyVar -> compBindTypeVar v' pn (BoundTyVar (IType (TypeVar v))) >> return (env,())
     RigidTyVar x -> if x == vnm
                           then return (env,())
                           else errMsg pn $ unwords
                                 ["cannot unify rigid type variables",qts x,"and",qts vnm]
     BoundTyVar (IType (TypeVar v'')) -> unifyVar'' pn vnm v v'' env
     BoundTyVar t ->
        do t' <- displayType t
           errMsg pn $ unwords
              ["cannot unify",qts t',"with rigid type variable",qts vnm]

checkpointVarEnv :: TC a -> TC a
checkpointVarEnv m = TC $ \env ->
  do let varenv = envVars env
     (env',x) <- unTC m env
     return (env'{ envVars = varenv }, x)

newTermBinding :: Pn -> Var -> TC IType
newTermBinding pn v = TC $ \env ->
    case M.lookup v (envVars env) of
       Just _ -> errMsg pn (unwords [qts v,"already bound in scope"])
       Nothing -> unTC (bindFreshTypeVar pn v) env


freshenTypeDefn :: Bool -> ([(Pn,Var)], Type) -> TC ([IType],IType)
freshenTypeDefn rgd (vs,t) =
   do vs' <- mapM (\(pn,v) -> if rgd then bindRigidTypeVar pn v else bindFreshTypeVar pn v) vs
      t' <- freshenType rgd t
      return (vs',t')


expandTypeDefn :: Pn
               -> [IType]
               -> [(Pn,Var)]
               -> Type
               -> TC IType
expandTypeDefn pn actualArgs formalArgs t =
   do (args',t') <- doFreshen freshenTypeDefn (formalArgs,t)
      unifyTypeArgs pn args' actualArgs
      return t'

expandTypeDefn' :: Pn
                -> [IType]
                -> [(Pn,Var)]
                -> Type
                -> Comp IType
expandTypeDefn' pn actualArgs formalArgs t =
    fmap snd $ unTC (expandTypeDefn pn actualArgs formalArgs t) initialEnv


unifyTypeArgs :: Pn -> [IType] -> [IType] -> TC ()
unifyTypeArgs pn [] [] = return ()
unifyTypeArgs pn (v:vs) (t:ts) = unifyTypes pn v t >> unifyTypeArgs pn vs ts
unifyTypeArgs pn _ _ = errMsg' pn "argument mismatch for type operator"

unifyNamedType :: Pn -> Ident -> [IType] -> IType -> TC ()
unifyNamedType pn n1 args1 t2 =
   do msym <- liftTC (lookupTypeIdent n1)
      case sym_info msym of
         DataSymbol (DataDecl _ _ _) -> unifyDataType pn n1 args1 t2

         TypeSymbol (TypeDecl _ args t) ->
             do t' <- expandTypeDefn pn args1 args t
                unifyTypes pn t' t2

         TypeSymbol (PrimTypeDecl _) ->
             if null args1
                then unifyPrimNamedType pn n1 t2
                else liftTC (errMsg (loc n1) "primitive types may not have arguments")

unifyDataType :: Pn -> Ident -> [IType] -> IType -> TC ()
unifyDataType pn n1 args1 (IType (TypeName n2 args2)) =
   if n1 == n2
      then unifyTypeArgs pn args1 args2
      else errMsg' pn $ unwords ["unable to unify",qt n1,"and",qt n2]
unifyDataType pn n1 args1 t2 =
   do t2' <- displayType' t2
      errMsg' pn $ unwords ["unable to unify data type",qt n1,"with",qts t2']

unifyPrimNamedType :: Pn -> Ident -> IType -> TC ()
unifyPrimNamedType pn n1 (IType (TypeName n2 [])) =
        unless (n1 == n2) (errMsg' pn $ unwords ["unable to unify",qt n1,"and",qt n2])
unifyPrimNamedType pn n1 t2 =
   do t2' <- displayType' t2
      errMsg' pn $ unwords ["unable to unify",qt n1,"and",qts t2']


unifyTypes :: Pn -> IType -> IType -> TC ()

unifyTypes pn (IType (TypeVar v)) t = unifyVar pn v t
unifyTypes pn t (IType (TypeVar v)) = unifyVar pn v t

unifyTypes pn (IType (TypeName n1 args)) t2 = unifyNamedType pn n1 args t2
unifyTypes pn t1 (IType (TypeName n2 args)) = unifyNamedType pn n2 args t1

unifyTypes pn (IType (TypeTuple ts1)) (IType (TypeTuple ts2)) = f ts1 ts2
  where f [] [] = return ()
        f (t1:ts1) (t2:ts2) = unifyTypes pn t1 t2 >> f ts1 ts2
        f _ _ = errMsg' pn $ unwords ["unable to unify tuple types of different arity"]

unifyTypes pn (IType (TypeList t1)) (IType (TypeList t2)) = unifyTypes pn t1 t2

unifyTypes pn (IType (TypeSet t1)) (IType (TypeSet t2)) = unifyTypes pn t1 t2

unifyTypes pn (IType (TypeMap x1 y1)) (IType (TypeMap x2 y2)) =
   unifyTypes pn x1 x2 >> unifyTypes pn y1 y2

unifyTypes pn (IType TypeNumber) (IType TypeNumber) = return ()

unifyTypes pn (IType TypeString) (IType TypeString) = return ()

unifyTypes pn t1 t2 =
  do t1' <- displayType' t1
     t2' <- displayType' t2
     errMsg' pn $ unwords ["unable to unify",qts t1',"with",qts t2']

unifyTypeList :: Pn -> Integer -> [IType] -> TC ()
unifyTypeList pn tyv xs = mapM_ (unifyVar pn tyv) xs

typeCheckOneNamedArg ::
    (SymbolTable -> tm a -> TC (tm (a,IType), IType)) ->
    SymbolTable -> Ident
     -> [(Pn,String,IType)] -> (Pn,String,tm a) -> TC (Pn,String, tm (a, IType))
typeCheckOneNamedArg tcf st tyname tys (pn,n,tm) =
    case find (\ (_,x,_) -> x == n) tys of
        Nothing -> errMsg' pn $ unwords [qts n,"is not a valid argument name for",qt tyname]
        Just (_,_,ty) -> do
               (tm',ty') <- tcf st tm
               unifyTypes pn ty ty'
               return (pn,n,tm')


typeCheckArgs :: TermLike tm =>
    (SymbolTable -> tm Pn -> TC (tm (Pn ,IType), IType)) ->
    SymbolTable -> Pn -> Ident -> Args (tm Pn) -> Args IType -> TC (Args (tm (Pn,IType)))

typeCheckArgs tcf st pn0 nm (PositionalArgs tms) (PositionalArgs tys) =
   if length tms == length tys
      then do xs <- sequence $ map (tcf st) $ (map snd tms)
              sequence_ $ map (\ (ty,ty',pn) -> unifyTypes pn ty ty') $ zip3 (map snd tys) (map snd xs) (map fst tms)
              return (PositionalArgs (zip (map fst tms) (map fst xs)))

      else errMsg' pn0 $ unwords [qt nm,"used with the incorrect number of arguments"]

typeCheckArgs tcf st pn0 nm (NamedArgs ts) (NamedArgs tys) =
     fmap NamedArgs (f ts tys)

   where f [] [] = return []
         f ((pn,n,_):ts) [] =
              do liftTC $ tryComp $ errMsg pn $ unwords ["incorrect named argument:",qts n]
                 f ts []
         f ts ((_,n,ty):tys) =
                 case locate n ts of
                    Nothing -> do (tm',ty') <- tcf st (buildTerm pn0 TAnonVar)
                                  unifyTypes pn0 ty ty'
                                  xs <- f ts tys
                                  return ((pn0,n,tm'):xs)

                    Just ((pn,m,tm),ts') -> do
                                  (tm',ty') <- tcf st tm
                                  unifyTypes pn ty ty'
                                  xs <- f ts' tys
                                  return ((pn,m,tm'):xs)



typeCheckArgs tcf st pn0 nm (PositionalArgs _) (NamedArgs _) =
   errMsg' pn0 $ unwords [qt nm, "declared with named args but used with positional args"]
typeCheckArgs tcf st pn0 nm (NamedArgs _) (PositionalArgs _) =
   errMsg' pn0 $ unwords [qt nm, "declared with positional args but used with named args"]


locate :: String -> [(a,String,b)] -> Maybe ( (a,String,b), [(a,String,b)] )
locate n [] = Nothing
locate n ((a,m,b):xs) =
   if n == m
     then return ( (a,m,b), xs )
     else do (p, q) <- locate n xs
             return (p, (a,m,b):q)

typeCheckTerm :: SymbolTable -> Term Pn -> TC (Term (Pn, IType), IType)
typeCheckTerm st (TVar pn x) = TC (\env ->
    case M.lookup x (envVars env) of
       Just v  -> return (env,(TVar (pn, IType (TypeVar v)) x, IType (TypeVar v)))
       Nothing ->
             do (env',t) <- unTC (bindFreshTypeVar pn x) env
                return (env',(TVar (pn, t) x, t)))
typeCheckTerm st (TTerm pn x) = do
   (x',ty) <- typeCheckTerm' typeCheckTerm st pn x
   return (TTerm (pn, ty) x', ty)

typeCheckTerm' :: TermLike tm =>
   (SymbolTable -> tm Pn -> TC (tm (Pn,IType), IType)) ->
   SymbolTable -> Pn -> Term' (tm Pn) -> TC (Term' (tm (Pn,IType)), IType)

typeCheckTerm' tcf st pn TAnonVar = do
    tv <- getFreshTypeVar pn
    return (TAnonVar, IType $ TypeVar tv)

typeCheckTerm' tcf st pn (TString s) = return (TString s, IType TypeString)

typeCheckTerm' tcf st pn (TNum str n) = return (TNum str n, IType TypeNumber)

typeCheckTerm' tcf st pn (TTuple xs) = do
    xs' <- mapM (tcf st) xs
    return (TTuple (map fst xs'), IType $ TypeTuple (map snd xs'))

typeCheckTerm' tcf st pn (TList xs) = do
    tv <- getFreshTypeVar pn
    xs' <- mapM (tcf st) xs
    unifyTypeList pn tv (map snd xs')
    return (TList (map fst xs'), IType $ TypeList (IType $ TypeVar tv))

typeCheckTerm' tcf st pn (TCons x y) = do
    (x',th) <- tcf st x
    (y',tl) <- tcf st y
    unifyTypes pn (IType (TypeList th)) tl
    return (TCons x' y', tl)

typeCheckTerm' tcf st pn (TNegative x) = do
    (x',t) <- tcf st x
    unifyTypes pn t (IType TypeNumber)
    return (TNegative x', IType TypeNumber)

typeCheckTerm' tcf st pn (TOp op x y) = do
    (x',t1) <- tcf st x
    (y',t2) <- tcf st y
    tyout <- typeCheckOp pn op t1 t2
    return (TOp op x' y', tyout)

typeCheckTerm' tcf st pn (TFunc x args) =
    case st_lookup x st of
       Just (Symbol _ _ _ (FuncSymbol (FIConstructor pn' ty vs _ targs))) -> do
           let dataty = Type pn' (TypeName ty (map (\(pn,v) -> Type pn $ TypeVar v) vs))
           (targs',t') <- doFreshen freshenFuncArgs (targs, dataty)
           args' <- typeCheckArgs tcf st pn x args targs'
           return (TFunc x args', t')
       Just (Symbol _ _ _ (FuncSymbol (FIDecl _ (PrimFunctionDecl _ targs t)))) -> do
           (targs',t') <- doFreshen freshenFuncArgs (targs,t)
           args' <- typeCheckArgs tcf st pn x args targs'
           return (TFunc x args', t')
       Just i -> errMsg' (loc x) $ unwords [qt x,"used as function, but declared as",namesort (sym_info i)]
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]


typeCheckOp :: Pn -> OP -> IType -> IType -> TC (IType)

itNum = IType TypeNumber

 -- NOTE: We later check, during modechecking, that the inferred type at a concatenation
 --       operator is either string or a list of something.  At this point, we might not
 --       have enough information to do this check.  Instead, we simply ensure at this point
 --       that the types of the operands unify.
typeCheckOp pn OpConcat x y = unifyTypes pn x y >> return x

typeCheckOp pn OpPlus   x y = unifyTypes pn itNum x >> unifyTypes pn itNum y >> return itNum
typeCheckOp pn OpMinus  x y = unifyTypes pn itNum x >> unifyTypes pn itNum y >> return itNum
typeCheckOp pn OpTimes  x y = unifyTypes pn itNum x >> unifyTypes pn itNum y >> return itNum
typeCheckOp pn OpDiv    x y = unifyTypes pn itNum x >> unifyTypes pn itNum y >> return itNum

typeCheckClause :: SymbolTable -> Clause Pn -> TC (Clause (Pn, IType))
typeCheckClause st (Clause pn x) = do
    x' <- typeCheckClause' st pn x
    return (Clause pn x')

typeCheckClause' :: SymbolTable
                 -> Pn
                 -> Clause' (QTerm Pn) (Term Pn) (Clause Pn)
                 -> TC (Clause' (QTerm (Pn, IType))
                                (Term (Pn, IType))
                                (Clause (Pn, IType)))
typeCheckClause' st pn (CPred x args) = do
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]
       Just (Symbol _ _ _ (PredSymbol pi _)) ->
          case pi of
            PIEmpty pn -> errMsg' (loc x) $ unwords [qt x,"is missing a predicate declaration"]
            PIDecl decl -> do
               let targs = case decl of
                            PredDecl _ a -> a
                            PrimPredDecl _ a -> a
               targs' <- doFreshen freshenArgs (mapArgs fst targs)
	       args' <- typeCheckArgs typeCheckTerm st pn x args targs'
               return (CPred x args')

       Just (Symbol _ _ _ (TableSymbol ti _)) ->
          case ti of
            TIEmpty pn -> errMsg' (loc x) $ unwords [qt x,"is missing a table declaration"]
            TIDecl (TableDecl _ targs _ _) -> do
               targs' <- doFreshen freshenArgs targs
	       args' <- typeCheckArgs typeCheckTerm st pn x args targs'
               return (CPred x args')

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be a predicate or table,but it is a",namesort (sym_info i)]


typeCheckClause' st pn (CEq x y) = do
    (x',t1) <- typeCheckTerm st x
    (y',t2) <- typeCheckTerm st y
    unifyTypes pn t1 t2
    return (CEq x' y')

typeCheckClause' st pn (CNeq x y) = do
    (x',t1) <- typeCheckTerm st x
    (y',t2) <- typeCheckTerm st y
    unifyTypes pn t1 t2
    return (CNeq x' y')

typeCheckClause' st pn (CAnd x y) = do
    x' <- typeCheckClause st x
    y' <- typeCheckClause st y
    return (CAnd x' y')

typeCheckClause' st pn (COr x y) = do
    x' <- typeCheckClause st x
    y' <- typeCheckClause st y
    return (COr x' y')

typeCheckClause' st pn (CNot x) = do
    x' <- typeCheckClause st x
    return (CNot x')

typeCheckClause' st pn CEmpty = return CEmpty

typeCheckClause' st pn CTrue = return CTrue

typeCheckClause' st pn CFalse = return CFalse

typeCheckClause' st pn (CComp op x y) = do
    (x',t1) <- typeCheckTerm st x
    unifyTypes pn t1 itNum
    (y',t2) <- typeCheckTerm st y
    unifyTypes pn t2 itNum
    return (CComp op x' y')

typeCheckClause' st pn (CFindall tm gl v) = do
    gl' <- typeCheckGoal typeCheckQTerm False st gl
    (tm',ty) <- typeCheckTerm st tm
    (_,ty') <- typeCheckTerm st (TVar undefined v)
    unifyTypes pn (IType (TypeSet ty)) ty'
    return (CFindall tm' gl' v)


typeCheckGoal :: TermLike tm
              => (SymbolTable -> tm Pn -> TC (tm (Pn, IType), IType))
              -> Bool -> SymbolTable
              -> PreGoal (tm Pn)
              -> TC (PreGoal (tm (Pn, IType)))
typeCheckGoal tcTm rgd st (pn,x,args) =
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]
       Just (Symbol _ _ _ (PredSymbol pi _)) ->
          case pi of
            PIEmpty pn -> errMsg' (loc x) $ unwords [qt x,"is missing a predicate declaration"]
            PIDecl (PredDecl _ targs) -> do
               targs' <- if rgd
                             then rigidFreshen freshenArgs (mapArgs fst targs)
                             else doFreshen freshenArgs (mapArgs fst targs)
	       args' <- typeCheckArgs tcTm st pn x args targs'
               return (pn,x,args')

            PIDecl (PrimPredDecl _ targs) -> do
               errMsg' (loc x) $ unwords [qt x,"declared as a primitive predicate, yet has rules"]

       Just (Symbol _ _ _ (TableSymbol ti _)) ->
          case ti of
            TIEmpty pn -> errMsg' (loc x) $ unwords [qt x,"is missing a table declaration"]
            TIDecl (TableDecl _ targs _ _) -> do
               targs' <- doFreshen freshenArgs targs
	       args' <- typeCheckArgs tcTm st pn x args targs'
               return (pn,x,args')

       Just i -> errMsg' (loc x) $ unwords
          ["expected",qt x,"to be a predicate or table,but it is a",namesort (sym_info i)]


typeCheckRule :: SymbolTable -> Rule Pn
              -> TC (Rule (Pn,IType))
typeCheckRule st (g,cl) = do
    g' <- typeCheckGoal typeCheckTerm True st g
    cl' <- typeCheckClause st cl
    return (g',cl')

typeCheckBody :: SymbolTable
              -> HBody Pn
              -> TC (HBody (Pn,IType))
typeCheckBody st (HBody pn (HSeq x y)) = do
    x' <- typeCheckBody st x
    y' <- typeCheckBody st y
    return (HBody pn (HSeq x' y'))

typeCheckBody st (HBody pn (HQuery bs)) = do
    bs' <- mapM (typeCheckBranch st) bs
    return (HBody pn (HQuery bs'))

typeCheckBody st (HBody pn HSkip) = do
    return (HBody pn HSkip)

typeCheckBody st (HBody pn (HInsert args tab)) = do
    args' <- typeCheckInsertDelete st pn args tab
    return (HBody pn (HInsert args' tab))

typeCheckBody st (HBody pn (HDelete args tab)) = do
    args' <- typeCheckInsertDelete st pn args tab
    return (HBody pn (HDelete args' tab))

typeCheckBody st (HBody pn (HForeach q body)) = do
    (_,QueryBranch q' body') <- typeCheckBranch st (pn, QueryBranch q body)
    return (HBody pn (HForeach q' body'))

typeCheckBody st (HBody pn (HProcedure x args)) = do
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]

       Just (Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ targs))) -> do
             targs' <- doFreshen freshenArgs targs
             args' <- typeCheckArgs typeCheckTerm st pn x args targs'
             return (HBody pn (HProcedure x args'))

       Just (Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl _ targs))) -> do
             targs' <- doFreshen freshenArgs targs
             args' <- typeCheckArgs typeCheckTerm st pn x args targs'
             return (HBody pn (HProcedure x args'))

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be a procedure, but it is a",namesort (sym_info i)]

typeCheckInsertDelete :: SymbolTable -> Pn -> Args (Term Pn) -> Ident -> TC (Args (Term (Pn,IType)))
typeCheckInsertDelete st pn args tab = do
    case st_lookup tab st of
          Nothing -> errMsg' (loc tab) $ unwords [qt tab,"not in scope"]
          Just (Symbol _ _ _ (TableSymbol (TIDecl (TableDecl _ tys _ _)) _)) -> do
                   tys' <- doFreshen freshenArgs tys
                   typeCheckArgs typeCheckTerm st pn tab args tys'

          Just (Symbol _ _ _ (TableSymbol (TIEmpty pn) _)) -> errMsg' (loc tab) $ unwords [qt tab,"missing table declaration"]
          Just x -> errMsg' (loc tab) $ unwords ["expected",qt tab,"to be a table, but is declared as",namesort (sym_info x)]

typeCheckQClause :: SymbolTable
                 -> QClause Pn
                 -> TC (QClause (Pn, IType))
typeCheckQClause st (QCEq pn x y) = do
    (x',ty1) <- typeCheckQTerm st x
    (y',ty2) <- typeCheckQTerm st y
    unifyTypes pn ty1 ty2
    return (QCEq pn x' y')

typeCheckQClause st (QCPred pn ident args) = do
    tys <- case st_lookup ident st of
             Just (Symbol _ _ _ (PredSymbol (PIDecl (PredDecl _ tys)) _)) -> return (mapArgs fst tys)
             Just (Symbol _ _ _ (PredSymbol (PIDecl (PrimPredDecl _ tys)) _)) -> return (mapArgs fst tys)
             Just (Symbol _ _ _ (TableSymbol (TIDecl (TableDecl _ tys _ _)) _)) -> return tys
             _ -> errMsg' (loc ident) $ unwords [qt ident, "not in scope"]
    tys' <- doFreshen freshenArgs tys
    args' <- typeCheckArgs typeCheckQTerm st pn ident args tys'
    return (QCPred pn ident args')

typeCheckQClause st (QCAnd c1 c2) = do
    c1' <- typeCheckQClause st c1
    c2' <- typeCheckQClause st c2
    return (QCAnd c1' c2')

typeCheckQClause st (QCNeq pn x y) = do
    (x',ty1) <- typeCheckQTerm st x
    (y',ty2) <- typeCheckQTerm st y
    unifyTypes pn ty1 ty2
    return (QCNeq pn x' y')

typeCheckQClause st (QCComp pn op x y) = do
    (x',ty1) <- typeCheckQTerm st x
    unifyTypes pn ty1 itNum
    (y',ty2) <- typeCheckQTerm st y
    unifyTypes pn ty2 itNum
    return (QCComp pn op x' y')



typeCheckBranch :: SymbolTable -> (Pn,QBranch Pn)
                -> TC (Pn, QBranch (Pn,IType))
typeCheckBranch st (pn, QueryBranch qcl b) = checkpointVarEnv (do
    qcl' <- typeCheckQClause st qcl
    b' <- typeCheckBody st b
    return (pn, QueryBranch qcl' b'))

typeCheckBranch st (pn, DefaultBranch b) = checkpointVarEnv (do
    b' <- typeCheckBody st b
    return (pn, DefaultBranch b'))



typeCheckQTerm :: SymbolTable -> QTerm Pn -> TC (QTerm (Pn,IType), IType)
typeCheckQTerm st (QBindVar pn v)  = do
         t <- newTermBinding pn v
         return (QBindVar (pn,t) v, t)

typeCheckQTerm st (QRefVar pn x) = TC (\ env ->
    case M.lookup x (envVars env) of
       Just v  -> return (env,(QRefVar (pn, IType (TypeVar v)) x, IType (TypeVar v)))
       Nothing -> errMsg pn (unwords [qts x,"not bound"]))

typeCheckQTerm st (QTerm pn x) = do
         (x',t) <- typeCheckTerm' typeCheckQTerm st pn x
         return (QTerm (pn,t) x', t)

typeCheckProcDefn :: SymbolTable
                  -> Ident
                  -> Args (Maybe Var)
                  -> HBody Pn
                  -> TC (HBody (Pn,IType))
typeCheckProcDefn st x vars body = do
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"missing procedure declaration"]

       Just (Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl _ _))) ->
             errMsg' (loc x) $ unwords [qt x,"declared as a primitive procedure"]

       Just (Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ targs))) -> do
             targs' <- rigidFreshen freshenArgs targs
             bindHandlerArgs (loc x) x vars targs'
	     typeCheckBody st body

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be a procedure, but it is a",namesort (sym_info i)]

typeCheckHandler :: SymbolTable
                 -> Handler Pn
                 -> TC (Handler (Pn, IType))
typeCheckHandler st (x,vars,body) =
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]
       Just (Symbol _ _ _ (EventSymbol (EventDecl _ targs))) -> do
             targs' <- rigidFreshen freshenProcedureArgs targs
	     vars' <- bindHandlerArgs (loc x) x vars targs'
	     body' <- typeCheckBody st body
             return (x,vars',body')

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be an event, but it is a",namesort (sym_info i)]



bindHandlerArgs :: Pn -> Ident -> Args (Maybe Var) -> Args (IType) -> TC (Args (Maybe Var))
bindHandlerArgs pn0 nm (PositionalArgs vs) (PositionalArgs ts) = do
   unless (length vs == length ts)
     (errMsg' (loc nm) $ unwords [ qt nm,"declared with",show $ length ts
                     , "args but used with",show $ length vs])
   sequence_ $ map (uncurry bindMVarToType) $ zip vs (map snd ts)
   return (PositionalArgs vs)

bindHandlerArgs pn0 nm (NamedArgs vs) (NamedArgs tys) = fmap NamedArgs $ f vs tys
   where f [] [] = return []
         f ts [] = errMsg' pn0 $ unwords (["incorrect named arguments: "]++(map (\ (_,n,_) -> qts n) ts))
         f ts ((_,n,ty):tys) =
                 case locate n ts of
                    Nothing -> do
                                  xs <- f ts tys
                                  return ((pn0,n,Nothing):xs)

                    Just ((pn,m,mv),ts') -> do
                                  bindMVarToType (pn,mv) ty
                                  xs <- f ts' tys
                                  return ((pn,m,mv):xs)

bindHandlerArgs pn0 nm (PositionalArgs vs) (NamedArgs ts) =
   errMsg' pn0 $ unwords [qt nm,"declared with named arguments"]
bindHandlerArgs pn0 nm (NamedArgs vs) (PositionalArgs ts) =
   errMsg' pn0 $ unwords [qt nm,"declared with positional arguments"]

bindMVarToType (_,Nothing) t = return ()
bindMVarToType (pn,Just v) t = bindVarToType pn v t

bindVarToType :: Pn -> Var -> IType -> TC ()
bindVarToType pn v t = do
    t' <- newTermBinding pn v
    unifyTypes pn t t'

occurs :: TC ()
occurs = TC $ \env ->
   do tymap <- fmap comp_type_map $ compGetState
      case occursCheck tymap of
          Nothing -> return (env,())
          Just (pn,msg) -> errMsg pn msg

typeCheckPresume :: SymbolTable
                 -> Ident
                 -> Args (Maybe Var)
                 -> TC (Args (Maybe Var))
typeCheckPresume st x vars =
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]
       Just (Symbol _ _ _ (EventSymbol (EventDecl _ targs))) -> do
             targs' <- rigidFreshen freshenProcedureArgs targs
             vars' <- bindHandlerArgs (loc x) x vars targs'
             return vars'

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be an event, but it is a",namesort (sym_info i)]

typeCheckAssert :: SymbolTable
                -> AssertDecl (Clause Pn)
                -> TC (AssertDecl (Clause (Pn, IType)))
typeCheckAssert st (AxiomDecl cl) = do
    cl' <- typeCheckClause st cl
    return (AxiomDecl cl')
typeCheckAssert st (LemmaDecl cl) = do
    cl' <- typeCheckClause st cl
    return (LemmaDecl cl')
typeCheckAssert st (PresumeDecl (id, arg) cl) = do
    arg' <- typeCheckPresume st id arg
    cl' <- typeCheckClause st cl
    return (PresumeDecl (id, arg') cl')

typeCheckPragma :: SymbolTable
                -> Pragma Pn
                -> TC (Pragma (Pn, IType))

typeCheckPragma st (GenericPragma ident ptm) =
    return (GenericPragma ident ptm)

typeCheckPragma st (QueryPragma pn num cl) = do
    cl' <- typeCheckClause st cl
    return (QueryPragma pn num cl')

typeCheckPragma st (HandlePragma pn (BasicForm pn' x args) acts) = do
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]
       Just (Symbol _ _ _ (EventSymbol (EventDecl _ targs))) -> do
             targs' <- rigidFreshen freshenProcedureArgs targs
             args' <- typeCheckArgs typeCheckTerm st pn x args targs'
             acts' <- mapM (typeCheckAction st) acts
             return (HandlePragma pn (BasicForm pn' x args') acts')

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be an event, but it is a",namesort (sym_info i)]


typeCheckAction
   :: SymbolTable
   -> BasicForm (Term Pn)
   -> TC (BasicForm (Term (Pn, IType)))

typeCheckAction st (BasicForm pn x args) = do
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]

       Just (Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl _ targs))) -> do
             targs' <- doFreshen freshenArgs targs
             args' <- typeCheckArgs typeCheckTerm st pn x args targs'
             return (BasicForm pn x args')

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be a primitive procedure, but it is a",namesort (sym_info i)]



typeCheckConflictHead :: SymbolTable
                 -> Ident
                 -> Args (Maybe Var)
                 -> TC (Args (Maybe Var))
typeCheckConflictHead st x vars =
    case st_lookup x st of
       Nothing -> errMsg' (loc x) $ unwords [qt x,"not in scope"]
       Just (Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl _ targs))) -> do
             targs' <- rigidFreshen freshenProcedureArgs targs
	     vars' <- bindHandlerArgs (loc x) x vars targs'
             return vars'

       Just i -> errMsg' (loc x) $ unwords
         ["expected",qt x,"to be an action, but it is a",namesort (sym_info i)]


typeCheckConflict :: SymbolTable
                  -> ConflictDecl (Clause Pn)
                  -> TC (ConflictDecl (Clause (Pn, IType)))
typeCheckConflict st (ConflictDecl (id1,args1) (id2,args2) c) =
   do args1' <- typeCheckConflictHead st id1 args1
      args2' <- typeCheckConflictHead st id2 args2
      c' <- typeCheckClause st c
      return (ConflictDecl (id1,args1') (id2,args2') c')

typeCheckConflict st (ForbidDecl (id1,args1) c) =
   do args1' <- typeCheckConflictHead st id1 args1
      c' <- typeCheckClause st c
      return (ForbidDecl (id1,args1') c')

typeCheckDecl :: SymbolTable -> Decl Pn -> TC (Decl (Pn, IType))

typeCheckDecl st (DConflict c) = do
    c' <- typeCheckConflict st c
    return (DConflict c')

typeCheckDecl st (DRule r) = do
    r' <- typeCheckRule st r
    return (DRule r')

typeCheckDecl st (DHandler h) = do
    h' <- typeCheckHandler st h
    return (DHandler h')

typeCheckDecl st (DProcDefn (n,vars,body)) = do
    body' <- typeCheckProcDefn st n vars body
    return (DProcDefn (n,vars,body'))

typeCheckDecl st (DPragma p) = do
    p' <- typeCheckPragma st p
    return (DPragma p')

typeCheckDecl st (DAssert a) = do
    a' <- typeCheckAssert st a
    return (DAssert a')

typeCheckDecl st (DTable x)     = return (DTable x)
typeCheckDecl st (DIndex x)     = return (DIndex x)
typeCheckDecl st (DPred x)      = return (DPred x)
typeCheckDecl st (DMode x)      = return (DMode x)
typeCheckDecl st (DType x)      = return (DType x)
typeCheckDecl st (DData x)      = return (DData x)
typeCheckDecl st (DProcedure x) = return (DProcedure x)
typeCheckDecl st (DFunction x)  = return (DFunction x)
typeCheckDecl st (DEvent x)     = return (DEvent x)
typeCheckDecl st (DImport x)    = return (DImport x)



typeCheckProgram
   :: SymbolTable
   -> [(Pn,CodeOrigin,Decl Pn)]
   -> TC [Maybe (Pn,CodeOrigin,Decl (Pn,IType))]

typeCheckProgram st = mapM (\d -> tryTC (checkOneDecl d))
  where checkOneDecl (pn,orig,d) = do
             d' <- checkpointVarEnv (typeCheckDecl st d)
             return (pn,orig,d')


unmaybeList :: [Maybe a] -> [a]
unmaybeList [] = []
unmaybeList (Nothing : xs) = unmaybeList xs
unmaybeList (Just x : xs) = x : unmaybeList xs

eitherUnzip :: [Either a b] -> ([a],[b])
eitherUnzip [] = ([],[])
eitherUnzip ((Left a):xs)  = let (as,bs) = eitherUnzip xs in (a:as, bs)
eitherUnzip ((Right b):xs) = let (as,bs) = eitherUnzip xs in (as, b:bs)


-- | Typecheck a QTerm
typecheckQTerm :: Map String Integer -- ^ Map from program variable names to type variables
               -> QTerm Pn           -- ^ The QTerm to typecheck
               -> Comp (Map String Integer, QTerm (Pn,IType), IType)
                  -- ^ An updated map, the annotated qterm, and the type of the term
typecheckQTerm m tm = do
     cst <- compGetState
     let st = comp_st cst
     (env',(tm,ty)) <- unTC (typeCheckQTerm st tm) initialEnv{ envVars = m }
     return (envVars env', tm, ty)


-- | Typecheck a Term
typecheckTerm :: Map String Integer -- ^ Map from program variable names to type variables
              -> Term Pn            -- ^ The term to typecheck
              -> Comp (Map String Integer, Term (Pn,IType), IType)
                  -- ^ An updated map, the annotated term, and the type of the term
typecheckTerm m tm = do
     cst <- compGetState
     let st = comp_st cst
     (env',(tm,ty)) <- unTC (typeCheckTerm st tm) initialEnv{ envVars = m }
     return (envVars env', tm, ty)


-- | Typecheck a Clause
typecheckClause :: Map String Integer -- ^ Map from program variable names to type variables
                -> Clause Pn          -- ^ The clause to typecheck
                -> Comp (Map String Integer, Clause (Pn,IType))
                  -- ^ An updated map, and the annotated clause
typecheckClause m cl = do
     cst <- compGetState
     let st = comp_st cst
     (env',cl') <- unTC (typeCheckClause st cl) initialEnv{ envVars = m }
     return (envVars env', cl')

-- | Typecheck a Suppl program.  The returned program has the annotation
--   type @(Pn, IType)@, which means that, after typechecking,
--   each term in the program is annotated both with both its source-file
--   position and its type.  Any type variables that appear in the returned
--   types can be looked up in the type map using 'lookupTypeVar'.
--
typecheck :: [FileDecl]
          -> Comp (Prog (Pn, IType))
typecheck prog = do
     cst <- compGetState
     let st = comp_st cst
     (env',prog') <- unTC (typeCheckProgram st prog) initialEnv
     let prog'' = unmaybeList prog'
     return prog''
