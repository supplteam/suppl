{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module implements a basic interpreter for SUPPL.  It supports only a fragment of the language
--   and is primarily intended to help validate the formal operational semantics.
--
module Interpreter
( runInterpreter
) where

import qualified Data.Map as M
import Data.Map ( Map )
import qualified Data.Set as S
import Data.Set ( Set )
import Data.List
import Control.Monad
import Control.Monad.IO.Class
import Text.Read
import System.IO
import System.Exit

import qualified AST
import AST( PreTerm(..), Ident(..), getIdent, Args(..), extractArgs, Mode(..), CmpOP(..), PTMonad(..), CLI(..) )
import Tokens( Pn(..), displayPn )
import Parser( runCLIParser )
import FileLoad( loadFile, parseFiles )

import qualified Text.PrettyPrint as PP
import Text.PrettyPrint (($$), (<>), (<+>))

import System.Console.Shell.Backend.Compatline
import System.Console.Shell
import System.Console.Shell.ShellMonad


-- | Run the SUPPL interpreter.  The given files will be parsed and their
--   definitions loaded into the interpreter state before the user prompt is displayed.
runInterpreter :: [FilePath] -> IO ()
runInterpreter fs =
  do putStrLn "running SUPPL interpreter..."
     st <- loadFiles fs
     runShell (supplShell selectFirstRipeClause) compatlineBackend st
     return ()


--------
-- Set up the pieces necessary to fire up the CLI

type SupplShellState = InterpSt

data InterpSt
  = InterpSt
    { nextVar :: Integer
    , interpRules :: Map String [([Term String],[Clause String])]
    , interpFuncs :: Map String ([Term Integer] -> Maybe (Term Integer))
    , interpModes :: Map String [[Mode]]
    , interpHandlers :: Map String [([Maybe String], Body String)]
    }

supplShellInitState :: SupplShellState
supplShellInitState =
  InterpSt
  { nextVar = 0
  , interpRules = buildMap bakedinRules
  , interpFuncs = M.fromList bakedinFuncs
  , interpModes = buildMap bakedinModes
  , interpHandlers = M.empty
  }

supplShell :: SelectorFunction -> ShellDescription SupplShellState
supplShell select = 
   (mkShellDescription supplShellCommands (supplShellEval select))
   { historyEnabled = True
   , historyFile = Just "suppl.history" 
   }

supplShellCommands :: [ShellCommand SupplShellState]
supplShellCommands = 
   [ exitCommand "q"
   , helpCommand "h"
   , cmd "l" loadFileCmd "Load a file"
   , cmd "d" dumpInterpState "Dump the interpeter state"
   ]

supplShellEval :: SelectorFunction -> String -> Sh SupplShellState ()
supplShellEval select line = 
   case runCLIParser line of
      Left errs -> mapM_ (\(pn,msg) -> shellPutErrLn $ displayPn pn ++ " " ++ msg ) errs
      Right CLINull -> return ()
      Right (CLIQuery tms) ->
            liftInterp (runQuery select tms) >>= 
                  either shellPutErrLn shellPutStrLn
      Right (CLIEvent nm args) -> 
            liftInterp (runEvent select nm args) >>= 
                  either shellPutErrLn shellPutStrLn

-----------------------
-- Implement the shell commands and main evaluator modes

-- Finds all the handlers for a given event, runs them, and then
-- returns a list of the generated actions
runEvent :: SelectorFunction -> Ident -> [PreTerm] -> Interp String
runEvent select nm args =
   do xs <- mapM (pretermToTerm >=> (freshenTerm M.empty) >=> return . snd) args
      hs <- getHandlers (getIdent nm)
      hs' <- mapM freshenHandler hs
      stk <- mapM (prepareHandler xs) hs'
      as <- bodyStackCompute select stk
      return $ unlines $ map show as

prepareHandler :: [Term Integer] -> ([Integer],Body Integer) -> Interp BodyCfg
prepareHandler xs (vs,bd) = 
  let s = M.fromList (zip vs (map SvTerm xs))
   in return (BCfgStmt s bd)

-- Find all the solutions to a given query and print them
runQuery :: SelectorFunction -> [PreTerm] -> Interp String
runQuery select precls =
   do cls <- mapM pretermToClause precls
      (m,cls') <- freshenClauses M.empty cls
      let stk0 = [Left (Config cls' M.empty)]
      ss <- findAllSolutions select stk0
      if null ss 
         then return "no solutions"
         else fmap (PP.render . PP.vcat . PP.punctuate (PP.text "\n")) $ mapM (showSoln m) (zip [1..] ss)

showSoln :: Map String Integer -> (Int,Subst) -> Interp PP.Doc
showSoln m (i,s) = 
     do sln <- printSolution m s
        return (PP.text ("Solution "++(show i)) $$ sln)

printSolution :: Map String Integer -> Subst -> Interp PP.Doc
printSolution m s = fmap PP.vcat $ mapM f $ M.assocs m
 where f (x,n) =
         do t <- evalTerm s (TmVar n)
            return (PP.text x <+> PP.text "=" <+> displayTerm t)

traceExecution :: SelectorFunction -> Stack -> Interp [String]
traceExecution select [] = return []
traceExecution select stk = onFail (return [show stk]) $
  do stk' <- stackEval select stk
     lns <- traceExecution select stk'
     return (show stk : lns)

findAllSolutions :: SelectorFunction -> Stack -> Interp [Subst]
findAllSolutions select stk =
  do stk' <- stackEval select stk
     case stk' of
        [] -> return []
        Right s : stk'' -> 
              do ss <- findAllSolutions select stk''
                 return (s:ss)
        _ -> findAllSolutions select stk'

findSolution :: SelectorFunction -> Stack -> Interp (Maybe (Subst, Stack))
findSolution select stk =
  do stk' <- stackEval select stk
     case stk' of
        [] -> return Nothing
        Right s : ss -> return (Just (s, ss))
        _ -> findSolution select stk'


displayTerm :: Term Integer -> PP.Doc
displayTerm (TmVar n) = PP.text ("_#" ++ show n)
displayTerm (TmQVar n) = PP.text ("_?" ++ show n)
displayTerm (TmNum n) = PP.text (show n)
displayTerm TmNil = PP.text "[]"
displayTerm (TmCons h t) = displayTermList [displayTerm h] t
displayTerm (TmFun nm xs) =
  PP.text nm <> PP.parens (PP.cat $ PP.punctuate PP.comma $ map displayTerm xs)

displayTermList :: [PP.Doc] -> Term Integer -> PP.Doc
displayTermList xs TmNil = PP.brackets $ PP.cat $ PP.punctuate PP.comma $ reverse xs 
displayTermList xs (TmCons h t) = displayTermList (displayTerm h:xs) t
displayTermList xs t = foldl (\x y -> PP.brackets $ y <+> PP.text "|" <+> x) (displayTerm t) xs

liftInterp :: Interp a -> Sh SupplShellState (Either String a)
liftInterp m = do
    ist <- getShellSt
    case runInterp m ist of
        Left msg -> return (Left msg)
        Right (ist',a) -> do
             putShellSt ist'
             return (Right a)

onFail :: Interp a -> Interp a -> Interp a
onFail f m = Interp $ \st ->
   case runInterp m st of
      Left _ -> runInterp f st
      Right (st',x) -> Right (st',x)

----------------------------------
-- Some functions for printing out the current
-- shell state.  Mostly for debugging...

dumpInterpState :: Sh SupplShellState ()
dumpInterpState =
  do st <- getShellSt
     dumpRules (M.assocs (interpRules st))
     dumpHandlers (M.assocs (interpHandlers st))

ungroup :: [(a,[b])] -> [(a,b)]
ungroup = concatMap (\(a,bs) -> map (\b -> (a,b)) bs)

dumpHandlers :: [(String,[([Maybe String],Body String)])] -> Sh SupplShellState ()
dumpHandlers xs =
  do shellPutInfoLn "Handlers"
     shellPutInfoLn "====="
     shellPutInfoLn ""
     mapM_ dumpHandler (ungroup xs)

dumpRules :: [(String,[([Term String],[Clause String])])] -> Sh SupplShellState ()
dumpRules xs =
  do shellPutInfoLn "Rules"
     shellPutInfoLn "====="
     shellPutInfoLn ""
     mapM_ dumpRule (ungroup xs)

dumpHandler :: (String,([Maybe String],Body String)) -> Sh SupplShellState ()
dumpHandler (ev,(args,bd)) = 
  do shellPutInfoLn $ ev ++ "(" ++ (concat $ intersperse ", " $ map (maybe "_" id) args) ++ ")"
     shellPutInfoLn $ "  " ++ show bd
     shellPutInfoLn ""

dumpRule :: (String,([Term String],[Clause String])) -> Sh SupplShellState ()
dumpRule (nm,(args,cs)) = 
  do shellPutInfoLn $ nm ++ "(" ++ (concat $ intersperse ", " $ map show args) ++ ")"
     shellPutInfoLn $ "  " ++ show cs
     shellPutInfoLn ""

-------------------------
-- These definitions are concerned with parsing files and loading
-- the definitions found therin into th shell state

loadFiles :: [FilePath] -> IO SupplShellState
loadFiles [] = return supplShellInitState
loadFiles fs =
   do (errs,decls,_) <- parseFiles (map (\f -> (AST.UserCode,f)) fs)
      if null errs
         then case runInterp (loadDefinitions decls) supplShellInitState of
                  Left msg -> hPutStrLn stderr msg >> exitFailure
                  Right (st,_) -> return st
         else mapM_ (\(pn,msg) -> hPutStrLn stderr $ displayPn pn ++ msg) errs >> exitFailure

loadFileCmd :: File -> Sh SupplShellState ()
loadFileCmd (File fname) = 
  do (errs,decls) <- liftIO (loadFile fname)
     if null errs
        then do x <- liftInterp (loadDefinitions decls)
                case x of
                  Left msg -> shellPutErrLn msg
                  Right _ -> return ()
        else mapM_ (\(pn,msg) -> shellPutErrLn $ displayPn pn++msg) errs

loadDefinitions :: [AST.FileDecl] -> Interp ()
loadDefinitions defs = 
   Interp $ \st ->
     case runInterp (buildDefs (nextVar st)) st of
        Left msg -> Left msg
        Right (_,st') -> Right (st',())

 where buildDefs var = 
        do defRules <- getDefRules defs
           defModes <- getDefModes defs
           defHandlers <- getDefHandlers defs
           return InterpSt 
            { nextVar = var 
            , interpRules = buildMap (bakedinRules ++ defRules)
            , interpFuncs = M.fromList bakedinFuncs
            , interpModes = buildMap (bakedinModes ++ defModes)
            , interpHandlers = buildMap defHandlers
            }

getDefHandlers :: [AST.FileDecl] -> Interp [(String,([Maybe String],Body String))]
getDefHandlers [] = return []
getDefHandlers ((pn,_,AST.DHandler (nm,args,bd)) : xs) =
  case args of
    PositionalArgs args' ->
       do let args'' = map snd args'
          bd' <- bodyToBody bd
          hds <- getDefHandlers xs
          return ((getIdent nm,(args'',bd')):hds)
    NamedArgs _ -> parseFail pn "interpeter does not support named arguments"
getDefHandlers (_:xs) = getDefHandlers xs


getDefRules :: [AST.FileDecl] -> Interp [(String,([Term String], [Clause String]))]
getDefRules [] = return []
getDefRules ((pn,_,AST.DRule ((_,id,args),body)) : xs) = 
   case args of
     PositionalArgs args' -> 
        do args'' <- mapM (termToTerm . snd) args'
           body' <- clauseToClause body
           rs <- getDefRules xs
           return ((getIdent id, (args'', [body'])):rs)
     NamedArgs _ -> parseFail pn "interpreter does not support named arguments"

getDefRules ( _ : xs) = getDefRules xs

getDefModes :: [AST.FileDecl] -> Interp [(String,[Mode])]
getDefModes [] = return []
getDefModes ((pn,_,AST.DPred (AST.PredDecl id args)) : xs) =
   case args of
      PositionalArgs args' ->
         do let mm = getMaybeMode args'
            ms <- getDefModes xs
            case mm of
               Nothing -> return ms
               Just md -> return ((getIdent id, md):ms)
      NamedArgs _ -> parseFail pn "interpreter does not support named arguments"


getDefModes ((pn,_,AST.DMode (AST.ModeDecl id args)) : xs) =
   case args of
      PositionalArgs args' ->
         do ms <- getDefModes xs
            return ((getIdent id, map snd args'):ms)
      NamedArgs _ -> parseFail pn "interpreter does not support named arguments"

getDefModes ( _ : xs) = getDefModes xs

getMaybeMode :: [(Pn,(AST.Type,Maybe Mode))] -> Maybe [Mode]
getMaybeMode [] = Just []
getMaybeMode ( (_,(_,Nothing)) : xs) = Nothing
getMaybeMode ( (_,(_,Just m)) : xs) =
  case getMaybeMode xs of
      Nothing -> Nothing
      Just ms -> Just (m:ms)


-----------------------------------
-- Here we define the selector function we actualy use in the shell.
-- This selector chooses the leftmost clause in a clause list that
-- is ripe for evaluation.

type SelectorFunction =
  [Clause Integer] -> Subst -> Interp (Clause Integer, [Clause Integer])

selectFirstRipeClause :: SelectorFunction
selectFirstRipeClause [] s = fail "No ripe clause avaliable for selection!"
selectFirstRipeClause (c:cs) s = 
  do ripe <- clauseIsRipe s c
     if ripe
       then return (c,cs)
       else do (q,cs') <- selectFirstRipeClause cs s
               return (q,c:cs')


---------------------------------------
-- Set up the functions and predicates that
-- are baked in to the interpreter.

buildMap :: Ord a => [(a,b)] -> Map a [b]
buildMap = foldr f M.empty
  where f (a,b) m = M.alter (\x -> Just $ b : maybe [] id x)  a m

bakedinFuncs :: [(String,[Term Integer] -> Maybe (Term Integer))]
bakedinFuncs =
  [ ("+", \tms ->
       case tms of
          [TmNum n1, TmNum n2] -> Just (TmNum (n1+n2))
          _ -> Nothing )
  , ("-", \tms ->
       case tms of
          [TmNum n1, TmNum n2] -> Just (TmNum (n1-n2))
          _ -> Nothing )
  , ("*", \tms ->
       case tms of
          [TmNum n1, TmNum n2] -> Just (TmNum (n1*n2))
          _ -> Nothing )
  , ("/", \tms ->
       case tms of
          [TmNum n1, TmNum n2] -> Just (TmNum (n1 `div` n2))
          _ -> Nothing )
  , ("~", \tms ->
       case tms of
          [TmNum n] -> Just (TmNum (- n))
          _ -> Nothing )
  , ("mod", \tms ->
       case tms of
          [TmNum n1,TmNum n2] -> Just (TmNum (n1 `mod` n2))
          _ -> Nothing )

  , ("set_size", \tms ->
       case tms of
          [l] -> fmap TmNum $ termSetSize l
          _ -> Nothing )
       
  ]

termSetSize :: Term Integer -> Maybe Integer
termSetSize TmNil = Just 0
termSetSize (TmCons _ t) = fmap (+1) $ termSetSize t
termSetSize _ = Nothing

bakedinModes :: [(String,[Mode])]
bakedinModes =
  [ ]

bakedinRules :: [(String,([Term String], [Clause String]))]
bakedinRules = [ ]

--------------------------------------
-- Define the interpreter monad, which is used
-- to carry around interpreter state and handle
-- error conditions.

newtype Interp a = Interp { runInterp :: InterpSt -> Either String (InterpSt, a) }

instance Monad Interp where
  return x = Interp (\st -> Right (st,x))
  m >>= f  = Interp $ \st ->
              case runInterp m st of
                  Left m -> Left m
                  Right (st',x) -> runInterp (f x) st'
  fail x = Interp (\_ -> Left x)

instance Functor Interp where
  fmap f m = m >>= return . f

getRules :: String -> Interp [([Term String], [Clause String])]
getRules nm = Interp $ \st ->
   case M.lookup nm (interpRules st) of
      Nothing -> Left $ nm++" not found"
      Just rs -> Right (st,rs)

getHandlers :: String -> Interp [([Maybe String], Body String)]
getHandlers nm = Interp $ \st ->
   case M.lookup nm (interpHandlers st) of
      Nothing -> Left $ "event "++nm++" not found"
      Just hs -> Right (st,hs)

getModeMap :: Interp (Map String [[Mode]])
getModeMap = Interp $ \st -> Right (st, interpModes st)


--------------------------------------------
-- Freshness
--
-- Here we define a bunch of functions that walk over
-- the structure of handler bodies, clauses and terms
-- to replace the surface-syntax variables with fresh
-- internally-generated variable names.  This ensures
-- that separate calls to predicates do not try to unify
-- the same set of variables, etc.

freshenHandler :: ([Maybe String],Body String) -> Interp ([Integer], Body Integer)
freshenHandler (vs,bd) =
   do (m,vs') <- foldM freshenMaybeVar (M.empty,[]) vs
      bd' <- freshenBody m bd
      return (vs', bd')

freshenMaybeVar :: (Map String Integer,[Integer]) -> Maybe String -> Interp (Map String Integer,[Integer])
freshenMaybeVar (m,xs) Nothing =
  do x <- getFreshVar
     return (m,x:xs)
freshenMaybeVar (m,xs) (Just v) = 
  do (m',x) <- freshenVar m v
     return (m', x:xs)

freshenBody :: Map String Integer -> Body String -> Interp (Body Integer)
freshenBody m (HAction nm tms) =
    do tms' <- mapM (freshenTerm m) tms
       return (HAction nm (map snd tms'))
freshenBody m (HSeq x y) =
    do x' <- freshenBody m x
       y' <- freshenBody m y
       return (HSeq x' y')
freshenBody m HSkip = return HSkip
freshenBody m (HForeach qc bd) =
    do (m',qc') <- freshenQClause m qc
       bd' <- freshenBody m' bd
       return (HForeach qc' bd')
freshenBody m (HQuery bs) =
    do bs' <- mapM (freshenBranch m) bs
       return (HQuery bs')

freshenBranch :: Map String Integer -> QBranch String -> Interp (QBranch Integer)
freshenBranch m (QueryBranch qc bd) =
   do (m',qc') <- freshenQClause m qc
      bd' <- freshenBody m' bd
      return (QueryBranch qc' bd')
freshenBranch m (DefaultBranch bd) =
   do bd' <- freshenBody m bd
      return (DefaultBranch bd')


freshenQClause :: Map String Integer -> QClause String -> Interp (Map String Integer, QClause Integer)
freshenQClause m qc =
  case qc of
     QCEq x y ->
         do (m1,x') <- freshenTerm m x
            (m2,y') <- freshenTerm m1 y
            return (m2, QCEq x' y')
     QCNeq x y ->
         do (_,x') <- freshenTerm m x
            (_,y') <- freshenTerm m y
            return (m, QCNeq x' y')
     QCPred nm tms ->
         do (m',tms') <- foldM (\(a,xs) t -> freshenTerm a t >>= \(a',x) -> return (a',xs++[x])) (m,[]) tms
            return (m', QCPred nm tms')
     QCAnd x y ->
         do (m1,x') <- freshenQClause m x
            (m2,y') <- freshenQClause m1 y
            return (m2, QCAnd x' y')
     QCComp op x y ->
         do (_,x') <- freshenTerm m x
            (_,y') <- freshenTerm m y
            return (m, QCComp op x' y')

freshenRule :: ([Term String], [Clause String]) -> Interp ([Term Integer], [Clause Integer])
freshenRule (ts,cs) = do
   (m1,ts') <- freshenTerms M.empty ts
   (m2,cs') <- freshenClauses m1 cs
   return (ts',cs')

getFreshRules :: String -> Interp [([Term Integer],[Clause Integer])]
getFreshRules nm = getRules nm >>= mapM freshenRule

getEvaluator :: String -> Interp (Maybe ([Term Integer] -> Maybe (Term Integer)))
getEvaluator nm = Interp $ \st -> Right (st,M.lookup nm (interpFuncs st))

freshenClauses :: Map String Integer -> [Clause String] -> Interp (Map String Integer,[Clause Integer])
freshenClauses m [] = return (m,[])
freshenClauses m (c:cs) =
   do (m1,c') <- freshenClause m c
      (m2,cs') <- freshenClauses m1 cs
      return (m2,c':cs') 

freshenClause :: Map String Integer -> Clause String -> Interp (Map String Integer, Clause Integer)
freshenClause m ClTrue = return (m,ClTrue)
freshenClause m ClFalse = return (m,ClFalse)
freshenClause m (ClDisj x y) = 
  do (m1,x') <- freshenClause m x
     (m2,y') <- freshenClause m1 y
     return (m2, ClDisj x' y')
freshenClause m (ClConj x y) =
  do (m1,x') <- freshenClause m x
     (m2,y') <- freshenClause m1 y
     return (m2, ClConj x' y')
freshenClause m (ClNot x) =
  do (m',x') <- freshenClause m x
     return (m',ClNot x')
freshenClause m (ClPred nm xs) =
  do (m',xs') <- freshenTerms m xs
     return (m',ClPred nm xs')
freshenClause m (ClCompare op x y) =
  do (m1,x') <- freshenTerm m x
     (m2,y') <- freshenTerm m1 y
     return (m2, ClCompare op x' y')
freshenClause m (ClUnify x y) =
  do (m1,x') <- freshenTerm m x
     (m2,y') <- freshenTerm m1 y
     return (m2, ClUnify x' y')
freshenClause m (ClFindall t nm xs v) = 
  do (m1,t') <- freshenTerm m t
     (m2,xs') <- freshenTerms m1 xs
     (m3,v')  <- freshenVar m2 v
     return (m3, ClFindall t' nm xs' v')

getFreshVar :: Interp Integer
getFreshVar = Interp $ \st -> Right (st{ nextVar = nextVar st + 1 }, nextVar st)


freshenVar :: Map String Integer -> String -> Interp (Map String Integer, Integer)
freshenVar m nm = 
    case M.lookup nm m of
       Nothing -> do v <- getFreshVar
                     let m' = M.insert nm v m
                     return (m', v)
       Just v -> return (m, v)

freshenTerm :: Map String Integer -> Term String -> Interp (Map String Integer, Term Integer)
freshenTerm m (TmVar "_") = getFreshVar >>= \v -> return (m, TmVar v)
freshenTerm m (TmVar nm)  = 
    do (m',v) <- freshenVar m nm
       return (m', TmVar v)
freshenTerm m (TmQVar nm)  = 
    do (m',v) <- freshenVar m nm
       return (m', TmQVar v)
freshenTerm m (TmNum n) = return (m, TmNum n)
freshenTerm m (TmNil)   = return (m, TmNil)
freshenTerm m (TmCons h t) =
   do (m1,h') <- freshenTerm m h
      (m2,t') <- freshenTerm m1 t
      return (m2, TmCons h' t')
freshenTerm m (TmFun nm xs) =
   do (m',xs') <- freshenTerms m xs
      return (m',TmFun nm xs')

freshenTerms :: Map String Integer -> [Term String] -> Interp (Map String Integer, [Term Integer])
freshenTerms m [] = return (m,[])
freshenTerms m (x:xs) = 
   do (m1,x') <- freshenTerm m x
      (m2,xs') <- freshenTerms m1 xs
      return (m2,x':xs')

------------------------------------------
-- Here we define a bunch of boring coercion functions
-- that take the standard Suppl AST and transform it into
-- the more limited AST supported by the interpreter.
-- We bail out if we find an unsupported syntactic form.

instance PTMonad Interp where
  parseFail pn m = fail $ (displayPn pn)++m

qtermToTerm :: AST.QTerm Pn -> Interp (Term String)
qtermToTerm (AST.QBindVar _ v) = return (TmQVar v)
qtermToTerm (AST.QRefVar _ v) = return (TmVar v)
qtermToTerm (AST.QTerm pn t) = term'ToTerm qtermToTerm pn t

termToTerm :: AST.Term Pn -> Interp (Term String)
termToTerm (AST.TVar _ v) = return (TmVar v)
termToTerm (AST.TTerm pn t) = term'ToTerm termToTerm pn t

term'ToTerm :: (x -> Interp (Term String)) -> Pn -> AST.Term' x -> Interp (Term String)
term'ToTerm f pn t =
  case t of
     AST.TAnonVar  -> return (TmVar "_")
     AST.TCons h t -> liftM2 TmCons (f h) (f t)
     AST.TList xs  -> liftM (foldr TmCons TmNil) $ mapM f xs
     AST.TTuple xs -> parseFail pn "interpreter does not support tuple syntax"
     AST.TNum _ (AST.LitInteger n)  -> return (TmNum n)
     AST.TNum _ (AST.LitRational n) -> parseFail pn "intepreter only supports integer numeric values"
     AST.TFunc id (PositionalArgs xs) -> liftM (TmFun (getIdent id)) $ mapM (f . snd) xs
     AST.TFunc id (NamedArgs _) -> parseFail pn "interpreter does not support named arguments"
     AST.TString _ -> parseFail pn "interpreter does not support string literals"
     AST.TOp op x y -> liftM2 (\x y -> TmFun (opName op) [x,y]) (f x) (f y)
     AST.TNegative x -> liftM (\x -> TmFun "~" [x]) (f x)


clauseToClause :: AST.Clause Pn -> Interp (Clause String)
clauseToClause (AST.Clause pn c) =
  case c of
     AST.CEq t1 t2 -> liftM2 ClUnify (termToTerm t1) (termToTerm t2)
     AST.CNeq t1 t2 -> liftM2 (\x y -> ClNot (ClUnify x y)) (termToTerm t1) (termToTerm t2)
     AST.CPred id (PositionalArgs xs) -> liftM (ClPred (getIdent id)) $ mapM (termToTerm . snd) xs
     AST.CPred id (NamedArgs _) -> parseFail pn "interpreter does not support named arguments"
     AST.COr x y -> liftM2 ClDisj (clauseToClause x) (clauseToClause y)
     AST.CAnd x y -> liftM2 ClConj (clauseToClause x) (clauseToClause y)
     AST.CNot x -> liftM ClNot (clauseToClause x)
     AST.CEmpty -> return ClTrue
     AST.CComp op x y -> liftM2 (ClCompare op) (termToTerm x) (termToTerm y)
     AST.CFindall t (_,id,PositionalArgs xs) v -> 
           do t' <- termToTerm t
              xs' <- mapM (qtermToTerm . snd) xs
              return (ClFindall t' (getIdent id) xs' v)
     AST.CFindall t (pn,id,NamedArgs _) v -> parseFail pn "interpreter does not support named arguments"


bodyToBody :: AST.HBody Pn -> Interp (Body String)
bodyToBody (AST.HBody pn bd) =
  case bd of
    AST.HProcedure nm (PositionalArgs xs) ->
      do xs' <- mapM (termToTerm . snd) xs
         return (HAction (getIdent nm) xs')
    AST.HProcedure nm (NamedArgs xs) ->
      parseFail pn "interpreter does not support named arguments"
    AST.HSeq x y ->
      do x' <- bodyToBody x
         y' <- bodyToBody y
         return (HSeq x' y')
    AST.HForeach qcl bd ->
      do qcl' <- qclauseToQclause qcl
         bd' <- bodyToBody bd
         return (HForeach qcl' bd')
    AST.HQuery bs ->
      do bs' <- mapM qbranchToQbranch bs
         return (HQuery bs')    
    AST.HInsert _ _ -> parseFail pn "interpreter does not support tables"
    AST.HDelete _ _ -> parseFail pn "interpreter does not support tables"
    AST.HSkip -> return HSkip

qclauseToQclause :: AST.QClause Pn -> Interp (QClause String)
qclauseToQclause (AST.QCEq _ x y) =
   do x' <- qtermToTerm x
      y' <- qtermToTerm y
      return (QCEq x' y')
qclauseToQclause (AST.QCNeq _ x y) =
   do x' <- qtermToTerm x
      y' <- qtermToTerm y
      return (QCNeq x' y')
qclauseToQclause (AST.QCComp _ op x y) =
   do x' <- qtermToTerm x
      y' <- qtermToTerm y
      return (QCComp op x' y')
qclauseToQclause (AST.QCPred pn nm (PositionalArgs xs)) =
   do xs' <- mapM (qtermToTerm . snd) xs
      return (QCPred (getIdent nm) xs')
qclauseToQclause (AST.QCPred pn nm (NamedArgs xs)) =
   parseFail pn "interpreter does not support named arguments"
qclauseToQclause (AST.QCAnd x y) =
   do x' <- qclauseToQclause x
      y' <- qclauseToQclause y
      return (QCAnd x' y')


qbranchToQbranch :: (Pn,AST.QBranch Pn) -> Interp (QBranch String)
qbranchToQbranch (pn,AST.QueryBranch qcl bd) =
   do qcl' <- qclauseToQclause qcl
      bd' <- bodyToBody bd
      return (QueryBranch qcl' bd')
qbranchToQbranch (pn,AST.DefaultBranch bd) =
   do bd' <- bodyToBody bd
      return (DefaultBranch bd')


opName AST.OpPlus = "+"
opName AST.OpMinus = "-"
opName AST.OpTimes = "*"
opName AST.OpDiv = "/"
opName AST.OpConcat = "++"

pretermToTerm :: PreTerm -> Interp (Term String)
pretermToTerm = AST.pretermToTerm >=> termToTerm

pretermToClause :: PreTerm -> Interp (Clause String)
pretermToClause = AST.pretermToClause >=> clauseToClause

--------------------------------------------
-- Here, at last, is the definition of the AST
-- supported by the interpreter.

data Body a
  = HAction String [Term a]
  | HSeq (Body a) (Body a)
  | HForeach (QClause a) (Body a)
  | HQuery [QBranch a]
  | HSkip
 deriving (Eq,Show)

data QBranch a
  = QueryBranch (QClause a) (Body a)
  | DefaultBranch (Body a)
 deriving (Eq,Show)

data QClause a
  = QCEq (Term a) (Term a)
  | QCPred String [Term a]
  | QCAnd (QClause a) (QClause a)
  | QCNeq (Term a) (Term a)
  | QCComp CmpOP (Term a) (Term a)
 deriving (Eq,Show)

data Term a
  = TmVar a
  | TmQVar a
  | TmNum Integer
  | TmNil
  | TmCons (Term a) (Term a)
  | TmFun String [Term a]
 deriving (Eq,Show)

data SubstVal
  = SvVar String
  | SvTerm (Term Integer)
 deriving (Eq,Show)

type Subst = Map Integer SubstVal

data Clause a
  = ClTrue
  | ClFalse
  | ClDisj (Clause a) (Clause a)
  | ClConj (Clause a) (Clause a)
  | ClNot (Clause a)
  | ClPred String [Term a]
  | ClUnify (Term a) (Term a)
  | ClCompare CmpOP (Term a) (Term a)
  | ClFindall (Term a) String [Term a] a
 deriving (Eq,Show)

data Config 
  = Config [Clause Integer] Subst
  | CfgNot Stack [Clause Integer] Subst
  | CfgFindall Stack [Clause Integer] Subst (Term Integer) Integer
 deriving (Eq,Show)

type Stack = [Either Config Subst]

data BodyCfg
  = BCfgQuery [(Stack, Body Integer)]
  | BCfgForall Stack (Body Integer)
  | BCfgStmt Subst (Body Integer)
 deriving (Eq,Show)

type BodyStack = [BodyCfg]

data Action
  = Action String [Term Integer]
 deriving (Eq,Show)


---------------------------------------
-- Now we are ready to define the evaluation
-- functions that actually embody the core Suppl
-- operational semantics.
--
-- Note, some things here are done in a way that is
-- less than the most efficent.  This is sometimes done
-- on purpose to get a nicer definition.  Take care when
-- modifying to ensure the algorithm here matches the formal
-- semantics found in "docs/op-sem.txt"


-- | Given a list of handler bodies to evaluate, evaluate all of them
--   and return the generated actions.
bodyStackCompute :: SelectorFunction -> BodyStack -> Interp [Action]
bodyStackCompute f [] = return []
bodyStackCompute f (bcfg:stk) =
  do (stk',as) <- bodyCfgEval f bcfg
     as' <- bodyStackCompute f (stk'++stk)
     return (as++as')


-- | Given a list of handler bodies to evaluate, perform one step
--   of evaluation, returing a modified list of bodies and any
--   actions generated by the evaluation step.
bodyStackEval :: SelectorFunction -> BodyStack
              -> Interp (BodyStack, [Action])
bodyStackEval f [] = return ([],[])
bodyStackEval f (bcfg:stk) =
   do (stk',as) <- bodyCfgEval f bcfg
      return (stk'++stk,as)


-- | Evaluate a single body configuration, returing a
--   stack of additional configurations to evalaute and
--   any actions generated by the evaluation step.
bodyCfgEval :: SelectorFunction -> BodyCfg 
              -> Interp (BodyStack, [Action])

bodyCfgEval f (BCfgQuery []) = return ([],[])
bodyCfgEval f (BCfgQuery (([],bd):bs)) = return ([BCfgQuery bs],[])
bodyCfgEval f (BCfgQuery ((stk,bd):bs)) =
   case findSoln stk of
       Just (s,_)  -> return ([BCfgStmt s bd],[])
       Nothing -> 
           do stk' <- stackEval f stk
              return ([BCfgQuery ((stk',bd):bs)],[])
bodyCfgEval f (BCfgForall [] bd) = return ([],[])
bodyCfgEval f (BCfgForall stk bd) =
   case findSoln stk of
       Just (s,stk') -> return ([BCfgStmt s bd, BCfgForall stk' bd],[])
       Nothing ->
           do stk' <- stackEval f stk
              return ([BCfgForall stk' bd],[])
bodyCfgEval f (BCfgStmt s b) =
  case b of
    HAction nm ts -> 
       do ts' <- mapM (evalTerm s) ts
          return ([],[Action nm ts'])
    HSeq b1 b2 -> return ([BCfgStmt s b1, BCfgStmt s b2],[])
    HQuery bs ->
        let f (QueryBranch c b) = ([Left (Config [q2cl c] s)],b)
            f (DefaultBranch b) = ([Right s],b)
            bs' = map f bs
         in return ([BCfgQuery bs'],[])
    HForeach qc bd ->
       return ([BCfgForall [Left (Config [q2cl qc] s)] bd], [])
    HSkip -> return ([],[])

-- | Given a stack, find a completed solution, if any exists.
--   Return the found solution and the remaining stack.
findSoln :: Stack -> Maybe (Subst,Stack)
findSoln [] = Nothing
findSoln (Right s:stk) = Just (s,stk)
findSoln (Left c:stk) =
  case findSoln stk of
     Nothing -> Nothing
     Just (s,stk') -> Just (s, Left c : stk')

-- | Turn a query clause into a regular clause
q2cl :: QClause Integer -> Clause Integer
q2cl (QCEq x y) = ClUnify x y
q2cl (QCPred nm tms) = ClPred nm tms
q2cl (QCAnd x y) = ClConj (q2cl x) (q2cl y)
q2cl (QCNeq x y) = ClNot (ClUnify x y)
q2cl (QCComp op x y) = ClCompare op x y


-- | Is the given term ground in the given substitution?
termIsGround :: Subst -> Term Integer -> Interp Bool
termIsGround s t = fmap isGround $ evalTerm s t

-- | Is the given term ground?
isGround :: Term Integer -> Bool
isGround (TmVar _) = False
isGround (TmQVar _) = False
isGround (TmNum _) = True
isGround TmNil = True
isGround (TmCons h t) = isGround h && isGround t
isGround (TmFun nm xs) = all isGround xs

-- | Is the given clause ready for (partial) evalaution?
clauseIsRipe :: Subst -> Clause Integer -> Interp Bool
clauseIsRipe s ClTrue = return True
clauseIsRipe s ClFalse = return True
clauseIsRipe s (ClDisj _ _) = return True
clauseIsRipe s (ClConj _ _) = return True
clauseIsRipe s (ClCompare op x y) = liftM2 (&&) (termIsGround s x) (termIsGround s y)
clauseIsRipe s (ClPred nm tms) =
    do mds <- getModeMap
       case M.lookup nm mds of
          Nothing -> return False
          Just ms -> 
             do xs <- mapM (dynamicModeCheck s tms) ms
                return (or xs)
clauseIsRipe s (ClUnify x y) = liftM2 (||) (termIsGround s x) (termIsGround s y)
clauseIsRipe s (ClNot x) = clauseIsGround s x
clauseIsRipe s (ClFindall t nm xs v) =
    do mds <- getModeMap
       case M.lookup nm mds of
          Nothing -> return False
          Just ms ->
             do t' <- evalTerm s t
                xs <- mapM (dynamicFindallModeCheck s t' xs) ms
                return (or xs)

-- | Would the given term be ground if all the variables in the given set
--   were instantiated?
conditionallyGround :: Set Integer -> Term Integer -> Bool
conditionallyGround vs (TmVar v) = S.member v vs
conditionallyGround vs (TmQVar _) = False
conditionallyGround vs (TmNum _) = True
conditionallyGround vs (TmNil) = True
conditionallyGround vs (TmCons h t) = conditionallyGround vs h && conditionallyGround vs t
conditionallyGround vs (TmFun nm xs) = all (conditionallyGround vs) xs

-- | Given a set of modes, check that the given argument are ground in all positions corresponding
--   to "in" modes.  Calculate the set of variables that are bound by "out" mode arguments.
--   Finally, check if the template term would be ground provided that those variables were instantiated.
dynamicFindallModeCheck :: Subst -> Term Integer -> [Term Integer] -> [Mode] -> Interp Bool
dynamicFindallModeCheck s tm = f S.empty
 where
  f vs [] [] = return $ conditionallyGround vs tm
  f vs (t:ts) (ModeIn:ms) = liftM2 (&&) (termIsGround s t) (f vs ts ms)
  f vs (t:ts) (ModeOut:ms) =  
      do t' <- evalTerm s t
         x <- groundedOutCheck vs t'
         case x of
            Nothing  -> return False
            Just vs' -> f vs' ts ms
  f vs (t:ts) (ModeIgnore:ms) = f vs ts ms
  f vs _ _ = return False

-- | Check if the given term is valid for an "out" mode position, and extend the
--   given set of variables with the variables that would be bound.
groundedOutCheck :: Set Integer -> Term Integer -> Interp (Maybe (Set Integer))
groundedOutCheck vs = f vs
  where f vs (TmVar _) = return Nothing
        f vs (TmQVar n) = return (Just (S.insert n vs))
        f vs (TmNum _) = return (Just vs)
        f vs (TmNil) = return (Just vs)
        f vs (TmCons h t) =
             do vs1 <- f vs h
                case vs1 of
                  Nothing -> return Nothing
                  Just vs1' -> f vs1' t
        f vs (TmFun nm xs) = foldM (\v x -> case v of Nothing -> return Nothing;
                                                      Just v' -> f v' x)
                                   (Just vs) 
                                   xs

-- | Check if a list of arguments satisfies the given mode
dynamicModeCheck :: Subst -> [Term Integer] -> [Mode] -> Interp Bool
dynamicModeCheck s [] [] = return True
dynamicModeCheck s (t:ts) (ModeIn:ms) = liftM2 (&&) (termIsGround s t) (dynamicModeCheck s ts ms)
dynamicModeCheck s (t:ts) (m:ms) = dynamicModeCheck s ts ms
dynamicModeCheck _ _ _ = return False

-- | Check if all the terms appearing in the given clause are ground; also
--   make sure the clause contains no findalls (which must bind a variable).
clauseIsGround :: Subst -> Clause Integer -> Interp Bool
clauseIsGround s ClTrue = return True
clauseIsGround s ClFalse = return True
clauseIsGround s (ClDisj x y) = liftM2 (&&) (clauseIsGround s x) (clauseIsGround s y)
clauseIsGround s (ClConj x y) = liftM2 (&&) (clauseIsGround s x) (clauseIsGround s y)
clauseIsGround s (ClPred nm xs) = fmap and $ mapM (termIsGround s) xs
clauseIsGround s (ClCompare op x y) = liftM2 (&&) (termIsGround s x) (termIsGround s y)
clauseIsGround s (ClUnify x y) = liftM2 (&&) (termIsGround s x) (termIsGround s y)
clauseIsGround s (ClNot x) = clauseIsGround s x
clauseIsGround s (ClFindall _ _ _ _) = return False


-- | Perform one step of evaluation of a stack of logic configurations.
--   A stack represents the possible backtracking points from the current state.
stackEval :: SelectorFunction -> Stack -> Interp Stack
stackEval select [] = return []
stackEval select (Left cfg :stk) = 
   do xs <- cfgEval select cfg
      return (xs++stk)
stackEval select (Right subst : stk) =
   do stk' <- stackEval select stk
      return (Right subst : stk')

-- | Perform one step of evaluation in a selected configuration.
cfgEval :: SelectorFunction -> Config -> Interp Stack
cfgEval select (Config [] s) = return [Right s]
cfgEval select (Config cs s) =
  do (m,cs') <- select cs s
     stk <- transfer m cs' s
     return (map Left stk)
cfgEval select (CfgFindall stk cs s tm v) =
  case checkStackFinished stk of
     Just solns ->
         do tms <- buildSolutionSet solns tm
            let tms' = nub tms
            let s' = M.insert v (SvTerm $ foldr TmCons TmNil tms') s
            return [Left (Config cs s')]
     Nothing ->
       do stk' <- stackEval select stk
          return [Left (CfgFindall stk' cs s tm v)]

cfgEval select (CfgNot [] cs s) = return [Left (Config cs s)]
cfgEval select (CfgNot (Right _ : _) _ _) = return []
cfgEval select (CfgNot stk cs s) = 
  do stk' <- stackEval select stk
     return [Left (CfgNot stk' cs s)]

-- | Given a list of subsitutions and a template term,
--   calculate the list of terms generated by the template in each subsitution.
buildSolutionSet :: [Subst] -> Term Integer -> Interp [Term Integer]
buildSolutionSet [] tm = return []
buildSolutionSet (s:ss) tm =
  do tm' <- evalTerm s tm
     tms <- buildSolutionSet ss tm
     return (tm':tms)

-- | Does the stack consist of only completed solutions?  If so, return them.
checkStackFinished :: Stack -> Maybe [Subst]
checkStackFinished [] = Just []
checkStackFinished (Left c : _) = Nothing
checkStackFinished (Right s : stk) = fmap (s:) $ checkStackFinished stk

-- | Given a particular clause to examine, calculate the next state
--   after evaluating this clause one step.
transfer :: Clause Integer -> [Clause Integer] -> Subst -> Interp [Config]
transfer ClTrue cs s  = return [Config cs s]
transfer ClFalse cs s = return []
transfer (ClConj x y) cs s = return [Config (x:y:cs) s]
transfer (ClDisj x y) cs s = return [Config (x:cs) s, Config (y:cs) s]
transfer (ClCompare op x y) cs s =
   do x' <- evalTerm s x
      y' <- evalTerm s y
      case (x', y') of
        (TmNum n1, TmNum n2) ->
           let cmp = case op of
                      OpLE -> n1 <= n2
                      OpLT -> n1 < n2
                      OpGE -> n1 >= n2
                      OpGT -> n1 > n2
            in if cmp then return [Config cs s]
                      else return []

        _ -> fail "non-numbers in comparison predicate"

transfer (ClNot x) cs s = return [CfgNot [Left (Config [x] s)] cs s]
transfer (ClUnify t1 t2) cs s =
   do t1' <- evalTerm s t1
      t2' <- evalTerm s t2
      un <- unify s t1' t2'
-- FIXME: do we need to do an occurs check here?
-- I believe we can omit the check because modechecking
-- ensures that at least one side of the unification is ground.
      case un of
        Nothing -> return []
        Just s' -> return [Config cs s']  
transfer (ClFindall tm nm xs v) cs s = 
   return [CfgFindall [Left (Config [ClPred nm xs] s)] cs s tm v]
transfer (ClPred nm xs) cs s = 
   do rs <- getFreshRules nm
      let cfgs = map (ruleConfig xs cs s) rs
      return cfgs

-- | Given the data corresponding to a rule, build a configuration that will evaluate the body of the rule.
ruleConfig :: [Term Integer] -> [Clause Integer] -> Subst -> ([Term Integer], [Clause Integer]) -> Config
ruleConfig xs cs sub (rts, rcs) =
    let us = map (uncurry ClUnify) $ zip xs rts
     in Config (us++rcs++cs) sub

-- | Given a term, evaluate it in the context of the current substitution.
evalTerm :: Subst -> Term Integer -> Interp (Term Integer)
evalTerm s TmNil = return TmNil
evalTerm s (TmCons h t) = 
    do h' <- evalTerm s h
       t' <- evalTerm s t
       return (TmCons h' t')
evalTerm s (TmFun nm xs) =
    do xs' <- mapM (evalTerm s) xs
       ev <- getEvaluator nm
       case ev of
         Nothing -> return (TmFun nm xs')
         Just f ->
            case f xs' of
              Nothing -> return (TmFun nm xs')
              Just t  -> evalTerm s t
evalTerm s (TmNum n) = return (TmNum n)
evalTerm s (TmQVar n) = return (TmQVar n)
evalTerm s (TmVar n) =
   case M.lookup n s of
     Just (SvTerm t) -> evalTerm s t
     _ -> return (TmVar n)


-- | Try to unify two terms, generated a possibly extended subtitution if
--   the terms unify.
unify :: Subst -> Term Integer -> Term Integer -> Interp (Maybe Subst)

unify s (TmQVar n) t2 =
   case M.lookup n s of
      Just (SvTerm t) -> unify s t t2
      _ -> return (Just (M.insert n (SvTerm t2) s))

unify s (TmVar n) t2 =
   case M.lookup n s of
      Just (SvTerm t) -> unify s t t2
      _ -> return (Just (M.insert n (SvTerm t2) s))

unify s t1 (TmVar n) =
   case M.lookup n s of
      Just (SvTerm t) -> unify s t1 t
      _ -> return (Just (M.insert n (SvTerm t1) s))

unify s t1 (TmQVar n) =
   case M.lookup n s of
      Just (SvTerm t) -> unify s t1 t
      _ -> return (Just (M.insert n (SvTerm t1) s))

unify s TmNil TmNil = return (Just s)

unify s (TmNum n1) (TmNum n2) =
  return (if n1 == n2 then Just s else Nothing)

unify s (TmCons h1 t1) (TmCons h2 t2) =
  do s1 <- unify s h1 h2
     case s1 of
        Nothing -> return Nothing
        Just s1' -> unify s1' t1 t2

unify s (TmFun nm1 xs1) t2 =
  do ev1 <- getEvaluator nm1
     case ev1 of
        Just f ->
           fail $ "unevaluated function in unify: "++ nm1
        Nothing ->
           case t2 of
              TmFun nm2 xs2 ->
                    if nm1 == nm2
                       then unifyTerms s xs1 xs2 
                       else return Nothing
              _ -> return Nothing

unify s t1 (TmFun nm2 xs2) =
  do ev2 <- getEvaluator nm2
     case ev2 of
       Just f ->
           fail $ "unevaluated function in unify: "++ nm2
       Nothing ->
          case t1 of
            TmFun nm1 xs1 ->
                  if nm1 == nm2
                     then unifyTerms s xs1 xs2 
                     else return Nothing
            _ -> return Nothing

unify s _ _ = return Nothing

-- | Unify a sequence of terms
unifyTerms :: Subst -> [Term Integer] -> [Term Integer] -> Interp (Maybe Subst)
unifyTerms s [] [] = return (Just s)
unifyTerms s (x:xs) (y:ys) = 
   do s' <- unify s x y
      case s' of
        Nothing -> return Nothing
        Just s'' -> unifyTerms s'' xs ys
unifyTerms s _ _ = return Nothing

