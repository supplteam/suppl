{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module deals with command line options
--
module CmdLine where

import qualified Data.Map as M

import Control.Monad
import Control.Exception
import System.Exit
import System.IO
import Data.List (sortBy)
import System.Console.GetOpt


import Tokens( Pn(..) )
import AST
import FileLoad
import Pipelines
import Compile
import qualified Paths_SupplCompiler
import qualified Interpreter


-- | If we invoked the compiler on a static regression test,
--   we are not interested in the output of the compiler, just
--   the error messages.  Here we check that the expected messages
--   match the actually-produced messages.
regressionCompilerMode :: Handle -> CompSt -> [(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> IO ()
regressionCompilerMode h compst files prog docs =
   let errs = checkExpects compst
    in do when (comp_debug compst) (do
             hPutStrLn h "EXPECT"
             mapM_ (hPutStrLn h . show . snd)
                   (sortBy (\x y -> warnErrComparePn (snd x) (snd y)) (comp_expect compst))
             hPutStrLn h "ACTUAL"
             mapM_ (hPutStrLn h . show)
                   (sortBy warnErrComparePn (comp_messages compst)))
          let (st, x) = unComp (testPConflictPipeline files prog docs) compst

          if null errs
            then case x of
              Nothing -> hPutStrLn h "regression mode PASS"
              Just "" -> hPutStrLn h "regression mode PASS"
              Just s -> do
                hPutStrLn h "regression mode FAIL"
                hPutStrLn stderr s
            else
              case x of
                Just s -> do
                  hPutStrLn h "regression mode FAIL"
                  displayErrors errs
                  hPutStrLn stderr s
                  exitFailure
                Nothing -> do
                  hPutStrLn h "regression mode FAIL"
                  displayErrors errs
                  exitFailure

-- | After the compiler pipeline has finished, output warnings and
--   errors on stderr, and write the output (if any) into the given
--   file handle.
mainCompilerMode :: Handle -> [WarnErr] -> Maybe String -> IO ()
mainCompilerMode h msgs x =
       case x of
          Nothing -> do
               hPutStrLn stderr "compile failed!"
               displayErrors msgs
               exitFailure

          Just y -> do
               displayErrors msgs
               hPutStrLn h y

-- | Given an 'Options' record and list of input files,
--   invoke the compiler proper.
compiler :: Options -> [(CodeOrigin,String)] -> IO ()
compiler opts files =
  bracket
     (maybe (return stdout) (\f -> openFile f WriteMode) (outputFile opts))
     hClose
     (\h -> do
       (errs, prog, docs) <- if recursiveImports opts
                                then parseFiles files
                                else parseFilesNonrecursive files
       let errs' = map (\ (pn,x) -> Err (Just pn) x) errs
       pipeline <- (compilerPipeline opts)
       let (st,x) = unComp (pipeline files prog docs)
                           initialCompSt{ comp_messages = errs' }
       if comp_regression st
          then regressionCompilerMode h st files prog docs
          else mainCompilerMode h (comp_messages st) x)

-- | Main entry point to the compiler.
--   Parse the given strings as command-line arguments and invoke
--   the appropriate compiler pipeline.
runCompiler :: [String] -> IO ()
runCompiler args = do
  preludeFile <- Paths_SupplCompiler.getDataFileName "prelude.plc"

  case getOpt RequireOrder options args of

    (flags,files,[]) ->

      let opts = processFlags flags
          files' = (if (includePrelude opts) then [(BuiltinCode,preludeFile)] else [])
                     ++ (map (\x -> (UserCode,x)) files)
       in if helpMode opts
            then hPutStr stdout (usageInfo header options)
            else if interpreterMode opts
                    then Interpreter.runInterpreter files
                    else if null files
                            then hPutStr stderr ("no input files specified\n" ++
                                                usageInfo header options)
                            else compiler opts files'

    (_,_,errs) -> do
       hPutStr stderr (concat errs ++ usageInfo header options)
       exitFailure


-----------------------
-- Command line Options

header :: String
header = "Usage: supplc [options] files"

data Flag
  = Output String
  | UseIRMode
  | UseIR2Mode
  | UseIR3Mode
  | UseDocsMode
  | UseConflictDocsMode (Maybe FilePath) -- ^ FilePath: prover results
  | CmdLineHelp
  | UseSchemaMode
  | NoPrelude
  | UseConflictsMode
  | Interpreter
  | Nonrecursive
  | UseConflictsMode2 Bool -- ^ Flag: inline predicates?
  | UseDeepConflictsMode FilePath
  | UseDeepConflictDocsMode FilePath

options :: [OptDescr Flag]
options =
  [ Option ['o'] ["output"] (ReqArg Output "FILE") "output file"
  , Option [] ["ir"] (NoArg UseIRMode) "output IR after modechecking"
  , Option [] ["ir2"] (NoArg UseIR2Mode) "output IR after lifting and peephole optimizations"
  , Option [] ["ir3"] (NoArg UseIR3Mode) "output IR after inlining and modechecking"
  , Option ['d'] ["docs"] (NoArg UseDocsMode) "produce module documentation"
  , Option [] ["conflict-docs"] (OptArg UseConflictDocsMode "results-file")
                   "produce module documentation with highlighing for potential conflicts"
  , Option ['h','?'] ["help"] (NoArg CmdLineHelp) "display this message"
  , Option ['s'] ["schema"] (NoArg UseSchemaMode) "produce table schema"
  , Option [] ["conflicts"] (NoArg UseConflictsMode) "output conflict conditions"
  , Option [] ["conflicts2"] (NoArg (UseConflictsMode2 False))
                   "output conflict conditions, new analysis algorithm"
  , Option [] ["conflicts2inline"] (NoArg (UseConflictsMode2 True))
                   "output conflict conditions, new analysis algorithm, inline predicates"
  , Option [] ["noprelude"] (NoArg NoPrelude) "do not implicitly include the prelude"
  , Option ['i'] ["interpreter"] (NoArg Interpreter) "run the interpreter"
  , Option ['n'] ["nonrecursive"] (NoArg Nonrecursive) "Do not load via import statements"
  , Option [] ["deep-conflicts"] (ReqArg UseDeepConflictsMode "result-file")
         "Produce additional conflict analysis problems to help users find solutions"
  , Option [] ["deep-conflict-docs"] (ReqArg UseDeepConflictDocsMode "result-file")
         "Produce documentation from the deep conflicts problem results"
  ]

data Options
  = Options
  { outputFile :: Maybe String
     -- ^ Destination of compiler output.  @Nothing@ means stdout
  , compilerPipeline :: IO ([(CodeOrigin,FilePath)] -> [FileDecl] -> [(Pn,CodeOrigin,String)] -> Comp String)
     -- ^ Which compiler pipeline to use.  Defaults to the full compiler pipeline
  , helpMode :: Bool
     -- ^ Did the user request help on the command line?
  , includePrelude :: Bool
     -- ^ Automatically include the prelude?
  , interpreterMode :: Bool
     -- ^ Fire up the interpreter instead of compiling?
  , recursiveImports :: Bool
     -- ^ Load files via import statements?
  }

defaultOptions :: Options
defaultOptions =
  Options
  { outputFile = Nothing
  , compilerPipeline = return mainPipeline
  , helpMode = False
  , includePrelude = True
  , interpreterMode = False
  , recursiveImports = True
  }

-- | Update options based on a single command-line flag
processFlag :: Flag -> Options -> Options
processFlag (Output f)    o = o{ outputFile = Just f }
processFlag UseIRMode     o = o{ compilerPipeline = return irCompilerPipeline }
processFlag UseIR2Mode    o = o{ compilerPipeline = return ir2CompilerPipeline }
processFlag UseIR3Mode    o = o{ compilerPipeline = return ir3CompilerPipeline }
processFlag UseDocsMode   o = o{ compilerPipeline = return docsCompilerPipeline }
processFlag UseSchemaMode o = o{ compilerPipeline = return schemaCompilerPipeline }
processFlag UseConflictsMode o = o{ compilerPipeline = return conflictsCompilerPipeline }
processFlag (UseConflictsMode2 False) o = o{ compilerPipeline = return conflictsCompilerPipeline2 }
processFlag (UseConflictsMode2 True) o = o{ compilerPipeline = return conflictsCompilerPipeline2inline }

processFlag CmdLineHelp o = o{ helpMode = True }
processFlag NoPrelude     o = o{ includePrelude = False }
processFlag Interpreter   o = o{ interpreterMode = True }
processFlag Nonrecursive  o = o{ recursiveImports = False }

processFlag (UseDeepConflictsMode fp) o = o{ compilerPipeline = pipeline }
  where
    pipeline =
      do res <- openFile fp ReadMode >>= hGetContents
         return (deepConflictsPipeline res)

processFlag (UseDeepConflictDocsMode fp) o = o{ compilerPipeline = pipeline }
  where
    pipeline =
      do res <- openFile fp ReadMode >>= hGetContents
         return (displayDeepConflictsPipeline res)

processFlag (UseConflictDocsMode resFile) o = o{ compilerPipeline = pipeline  }
 where pipeline =
         do res <- case resFile of
                        Nothing -> return ""
                        Just fp -> openFile fp ReadMode >>= hGetContents
            return (displayConflictsPipeline res)


-- | Calculate compiler options based on the command-line flags
processFlags :: [Flag] -> Options
processFlags = foldr processFlag defaultOptions
