{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module performs a series of sanity checks that occur immediately
--   after symbol tables are built.  We make sure all type identifiers in the program
--   refer to some type definition in scope; we check that type abbreviations
--   do not introduce cycles; and we ensure that every named argument use in
--   declarations mentions each name at most once.
--
module Scopecheck
( checkDeclScopes
, checkProcScopes
) where

import Data.List
import Control.Monad
import Data.Graph
import qualified Data.Map as M
import Data.Map ( Map )
import qualified Data.Set as S
import Data.Set ( Set )

import Tokens
import AST
import SymbolTable
import Compile
import Toposort

-- | Examine the toposort of the type graph and look for cycles
sccError :: SCC (Symbol TypeSymbolInfo) -> [WarnErr]
sccError (AcyclicSCC _)  = []
sccError (CyclicSCC [])  = error "impossible!! empty cycle in type abbrevation graph!"
sccError (CyclicSCC nms@(s:_)) =
      [Err (Just (sym_pos s)) 
           (unwords $ "a cycle was found in type abbreviations:" : map (qt . sym_ident) nms)]

-- | Calculate the reference graph for types and check for cycles.
checkTypeAbbrevs :: TypeSymbolTable -> [WarnErr]
checkTypeAbbrevs tst = concatMap sccError $ typeDefToposort tst

-- | Check that every referenced name in a type exists in the symbol table
checkTypeScope :: Maybe (Set Var) -> TypeSymbolTable -> Type -> [WarnErr]
checkTypeScope vars tst (Type pn (TypeTuple xs)) = concatMap (checkTypeScope vars tst) xs
checkTypeScope vars tst (Type pn (TypeList t))   = checkTypeScope vars tst t
checkTypeScope vars tst (Type pn (TypeSet t))    = checkTypeScope vars tst t
checkTypeScope vars tst (Type pn (TypeMap t1 t2)) = checkTypeScope vars tst t1 ++ checkTypeScope vars tst t2
checkTypeScope vars tst (Type pn TypeNumber)     = []
checkTypeScope vars tst (Type pn TypeString)     = []
checkTypeScope Nothing tst (Type pn (TypeVar _)) = []
checkTypeScope (Just vs) tst (Type pn (TypeVar v)) =
   if S.member v vs
      then []
      else [Err (Just pn) (unwords ["type variable",qts v,"not in scope"])]
checkTypeScope vars tst (Type pn (TypeName n args)) =
      case fmap sym_info $ tst_lookup n tst of
         Nothing -> [Err (Just pn) (unwords ["type",qt n,"not in scope"])]
         Just (TypeSymbol (TypeDecl _ vs _)) -> checkArgsScope pn vars tst vs args
         Just (DataSymbol (DataDecl _ vs _)) -> checkArgsScope pn vars tst vs args
         Just (TypeSymbol (PrimTypeDecl _)) ->
            case args of
               [] -> []
               _ -> [Err (Just pn) (unwords ["primitive types may not have arguments"])]


-- Check if the arguments to a polymorphic type match the type definition,
-- and scope check the enclosed type arguments
checkArgsScope
    :: Pn
    -> Maybe (Set Var)
    -> TypeSymbolTable
    -> [(Pn,Var)]
    -> [Type]
    -> [WarnErr]
checkArgsScope pn vars tst vs ts = f vs ts
  where f [] [] = []
        f [] (_:_)  = [Err (Just pn) "too many type arguments"]
        f _  [] = [Err (Just pn) "not enough type arguments"]
        f (_:vs) (t:ts) = checkTypeScope vars tst t ++ f vs ts

-- | Check for repeated names in a list of named arguments
checkNameUniqueness :: [(Pn,String,Type)] -> Set String -> [WarnErr]
checkNameUniqueness [] s = []
checkNameUniqueness ((pn,n,_):ns) s =
     if S.member n s
         then (Err (Just pn) (unwords ["name",qts n,"repeated in named argument declaration"]))
              : checkNameUniqueness ns s
         else checkNameUniqueness ns (S.insert n s)

-- | Check a set of type arguments for scope
checkTypeArgsScope :: Maybe (Set Var) -> TypeSymbolTable -> Args Type -> [WarnErr]
checkTypeArgsScope vars tst (PositionalArgs ts) = concatMap (\ (pn,x) -> checkTypeScope vars tst x) ts
checkTypeArgsScope vars tst (NamedArgs ts) =
     checkNameUniqueness ts S.empty ++
     concatMap (\ (pn,_,x) -> checkTypeScope vars tst x) ts

-- | Check a type symbol declaration for scope
checkTypeInfoScope :: TypeSymbolTable -> Symbol TypeSymbolInfo -> [WarnErr]
checkTypeInfoScope tst (Symbol pn _ _ (TypeSymbol (PrimTypeDecl _))) = []
checkTypeInfoScope tst (Symbol pn _ _ (TypeSymbol (TypeDecl _ args t))) = 
    checkTypeScope (Just (S.fromList $ map snd args)) tst t
checkTypeInfoScope tst (Symbol pn _ _ (DataSymbol (DataDecl _ args cs))) =
    concatMap (checkTypeArgsScope (Just (S.fromList $ map snd args)) tst . snd) cs

-- | Check all the type symbol declarations in the type symbol table for scope
checkTypeScopes :: TypeSymbolTable -> [WarnErr]
checkTypeScopes tst = concatMap (checkTypeInfoScope tst) $ M.elems tst


-- | Check a term symbol declaration for type scopes
checkInfoScope :: TypeSymbolTable -> SymbolTable -> (String,Symbol SymbolInfo) -> [WarnErr]
checkInfoScope tst st (n,Symbol _ _ _ (FuncSymbol (FIDecl pn (PrimFunctionDecl _ ts t)))) =
    checkTypeArgsScope Nothing tst ts ++ checkTypeScope Nothing tst t

  -- constructor types are checked when the data declaration is checked;
  -- no need to check them here
checkInfoScope tst st (n,Symbol _ _ _ (FuncSymbol (FIConstructor _ _ _ _ _))) = [] 

checkInfoScope tst st (n,Symbol _ _ _ (PredSymbol pi modes)) =
     case pi of
          PIEmpty pn -> [Err (Just pn) (unwords ["predicate",qts n, "has rules but is missing predicatate declaration"])]
          PIDecl (PredDecl _ ts) -> checkTypeArgsScope Nothing tst (mapArgs fst ts)
          PIDecl (PrimPredDecl _ ts) -> checkTypeArgsScope Nothing tst (mapArgs fst ts)

checkInfoScope tst st (n,Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl _ ts))) =
    checkTypeArgsScope Nothing tst ts

checkInfoScope tst st (n,Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ ts))) =
    checkTypeArgsScope Nothing tst ts

checkInfoScope tst st (n,Symbol _ _ _ (EventSymbol ei)) =
    case ei of
       (EventDecl _ ts) ->
           checkTypeArgsScope Nothing tst ts

checkInfoScope tst st (n,Symbol _ _ _ (TableSymbol ti _)) =
    case ti of
         TIEmpty pn -> [Err (Just pn) (unwords ["table",qts n,"has index declarations but is missing table declaration"])]
         TIDecl (TableDecl _ ts _ _) -> checkTypeArgsScope (Just S.empty) tst ts

-- | Check all the term symbol declarations for type scope
checkScopes :: TypeSymbolTable -> SymbolTable -> [WarnErr]
checkScopes tst st = concatMap (checkInfoScope tst st) $ M.assocs st

-- | Perform sanity checks on type scopes and type abbreviations
checkDeclScopes :: TypeSymbolTable -> SymbolTable -> Comp ()
checkDeclScopes tst st = do
   addErrs (checkTypeScopes tst)
   addErrs (checkTypeAbbrevs tst)
   addErrs (checkScopes tst st)

-- | Check that every declared nonprimitive procedure is defined exactly once
--   and that procedure definitions are nonrecursive
checkProcScopes :: [FileDecl] -> SymbolTable -> Comp ()
checkProcScopes prog st = do
   ps <- foldM (checkProcScope st) M.empty prog
   checkNonrecursiveProcedures ps
   mapM_ (checkProcInfo ps) $ M.assocs st

checkProcInfo :: Map Ident ([String],Pn) -> (String, Symbol SymbolInfo) -> Comp ()
checkProcInfo ps (_,Symbol pn _ _ (ProcedureSymbol (ProcedureDecl nm _))) =
   case M.lookup nm ps of
     Just _ -> return ()
     Nothing -> addErrs [Err (Just pn) $ unwords ["procedure",qt nm,"has no definition"]]
checkProcInfo _ _ = return ()

checkProcScope :: SymbolTable -> Map Ident ([String],Pn) -> FileDecl -> Comp (Map Ident ([String],Pn))
checkProcScope st ps (pn,_,DProcDefn (nm,_,h)) = do
   case M.lookup nm ps of
      Just _ -> addErrs [Err (Just pn) $ unwords ["procedure",qt nm,"has multiple definitions"]] >> return ps
      Nothing -> return (M.insert nm (referencedProc h,pn) ps)
checkProcScope st ps _ = return ps

checkNonrecursiveProcedures :: Map Ident ([String],Pn) -> Comp ()
checkNonrecursiveProcedures ps = addErrs $ concatMap f $ toposortMap ps
  where f (AcyclicSCC _) = []
        f (CyclicSCC []) = error "impossible!! empty cycle in procedure call graph!"
        f (CyclicSCC nms@((ident,pn):_)) = 
           [Err (Just pn) 
              (unwords $ "recursive procedures not allowed:" : map (qt . fst) nms)]
