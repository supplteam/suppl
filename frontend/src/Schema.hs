{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module is responsible for producing the Prolog code 
--   that sets up tables and indexes.  For the current toProlog
--   interface, this mostly comes down to producing a \"setupTable\"
--   directive for each declared table, and defining the rules that
--   iterate through a table using a cursor.  Index information is
--   currently ignored.
--
module Schema
( TableSchema(..)
, Schema
, compileSchema
, extractSchema
) where

import qualified Data.Map as M

import AST
import SymbolTable
import Compile
import Modecheck (orderTableModes, expandNamedModes)
import qualified Prolog as P

data TableSchema 
    = TableSchema Ident [Type] [Int] (Maybe String) [Args Mode]
    -- ^ The schema for a single table: name, column types, primary key, lifetime, index modes
  deriving (Eq,Show)
type Schema = [TableSchema]

-- | Produce the Prolog declarations necessary to implement the
--   program schema.
compileSchema :: Comp [P.Prolog]
compileSchema = do
   st <- compSymbolTable
   fmap concat $ mapM compileTableSchema $ extractSchema st

-- | Extract the table and index information from the symbol table.
extractSchema :: SymbolTable -> Schema
extractSchema = concatMap f . M.assocs
 where f (_, Symbol _ _ _ (TableSymbol ti@(TIDecl (TableDecl ident tys pkey life)) mds)) =
          let mds' = orderTableModes ti mds
              keyCols = getKeyCols tys pkey
           in [TableSchema ident (extractArgs tys) keyCols life mds']
       f (_,_) = []

getKeyCols :: Args Type -> Args Mode -> [Int]
getKeyCols tys mds =
    let mds' = expandNamedModes tys mds
     in map snd $ filter (\ (m,_) -> m == ModeIn) $ zip (extractArgs mds') [0..]

numArgs :: Args a -> Int
numArgs (PositionalArgs xs) = length xs
numArgs (NamedArgs xs) = length xs

tableArgs :: [a] -> [String]
tableArgs as = map (("Col"++) . show) [0..(length as - 1)]

compileTableSchema :: TableSchema -> Comp [P.Prolog]
compileTableSchema (TableSchema ident tys key life mds) = do
  let lifetime = maybe "0" id life
  let setup = P.Directive
                 [P.Application "setupKeyTable" 
                   [ P.Atom (getIdent ident)
                   , P.Number (show (length tys))
                   , P.Number lifetime
                   , P.List (map (P.Number . show) key)
                   ]
                 ]
  let targs = tableArgs tys
  let rowrule = 
         P.Rule (P.Application (getIdent ident) (map P.Var targs))
               [ P.Application "tableCursor" [P.Atom (getIdent ident), P.Var "X"]
               , P.Application "iterateRows" [P.Var "X", P.List (map P.Var targs)]]
  return [setup, rowrule]
