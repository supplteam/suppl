{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module provides an abstract syntax for interfacing with SMT solvers.
--   Printers targeting SMTLib2 and Why3 syntax are provided.
module SMT where

import Data.Ratio

import qualified Text.PrettyPrint as PP
import Text.PrettyPrint ( (<>), (<+>), ($$) )

import AST ( Var, OP(..), CmpOP(..), NumericLiteral(..) )

data Ident
  = IdName String
  | IdFresh Int
  | IdArg Int
 deriving (Eq, Show)

data Type
  = TApp String [Type]
  | TVar String
  | Tuple [Type]
  | String
  | Real
  | List Type
  | Set Type
  | Map Type Type
 deriving (Eq,Show)

data Expr
  = App Ident [Expr]
  | DataApp String [Expr]
  | EVar Ident
  | Op OP Expr Expr
  | Neg Expr
  | Cons Expr Expr
  | Nil
  | NumLit NumericLiteral
  | ETuple [Expr]
  | EString Int
 deriving (Eq, Show)

data Binder = Bind { bindVar :: Ident, bindType :: Type }
 deriving (Eq, Show)

data Formula
  = And [Formula]
  | Or [Formula]
  | Not Formula
  | Pred Ident [Expr]
  | Implies Formula Formula
  | Iff Formula Formula
  | Forall [Binder] Formula
  | Exists [Binder] Formula
  | False
  | True
  | Eq Expr Expr
  | Comp CmpOP Expr Expr
 deriving (Eq, Show)

data Cmd
  = Axiom String Formula
  | Lemma String Formula
  | Goal String Formula
  | NegGoal String Formula
  | DeclareFun Ident [Type] Type
  | DefineFun Ident [Binder] Type Expr
  | DeclarePred Ident [Type]
  | DefinePred Ident [Binder] Formula
  | DeclareType String [Var]
  | DefineType String [Var] Type
  | DeclareData [DataDecl]
  | DeclareEvent String [Type]
  | DeclareAction String [Type]
  | Information String
  | SubProblem String [Cmd]
  | DeclareString Int String
 deriving (Eq, Show)

data DataDecl
   = DataDecl String [Var] [(String, [Type])]
   | DataTypeDecl String [Var] Type
 deriving (Eq, Show)

-- | "Smart" constructor for existential quantification
exists_ :: [Binder] -> Formula -> Formula
exists_ [] f = f
exists_ bs f = Exists bs f

-- | "Smart" constructor for universal quantification
forall_ :: [Binder] -> Formula -> Formula
forall_ [] f = f
forall_ bs f = Forall bs f

-- | "Smart" constructor for disjunction that does some basic simplifications
or :: [Formula] -> Formula
or = aux []
 where
   aux []  []               = SMT.False
   aux [f] []               = f
   aux fs  []               = Or (reverse fs)
   aux fs  (SMT.False : xs) = aux fs xs
   aux fs  (SMT.True : xs)  = SMT.True
   aux fs  (Or x : xs)      = aux fs (x++xs)
   aux fs  (x : xs)         = aux (x:fs) xs


-- | "Smart" constructor for conjunction that does some basic simplifications
and :: [Formula] -> Formula
and = aux []
 where
   aux []  []               = SMT.True
   aux [f] []               = f
   aux fs  []               = And (reverse fs)
   aux fs  (SMT.True : xs)  = aux fs xs
   aux fs  (SMT.False : xs) = SMT.False
   aux fs  (And x : xs)     = aux fs (x++xs)
   aux fs  (x : xs)         = aux (x:fs) xs


why3type :: Type -> PP.Doc
why3type (TApp x ts) = PP.parens (PP.text x <+> PP.sep (map why3type ts))
why3type (TVar x) = PP.text "\'z" <> PP.text x
why3type Real = PP.text "real"
why3type String = PP.text "string"
why3type (List t) = PP.parens (PP.text "list" <+> why3type t)
why3type (Set t) = PP.parens (PP.text "set" <+> why3type t)
why3type (Map t1 t2) =
  PP.parens (PP.text "set" <+> why3type t1 <+> why3type t2)
why3type (Tuple ts) =
  PP.parens (PP.sep $ PP.punctuate PP.comma $ map why3type ts)

why3ident :: Ident -> PP.Doc
why3ident (IdName x) = PP.text x
why3ident (IdFresh i) = PP.text "_x" <> PP.int i
why3ident (IdArg i) = PP.text "_arg" <> PP.int i

why3expr :: Expr -> PP.Doc
why3expr (App x []) = why3ident x
why3expr (App x xs) = PP.parens (why3ident x <+> PP.sep (map why3expr xs))
why3expr (DataApp x []) = PP.text "Z_" <> PP.text x
why3expr (DataApp x xs) = PP.parens (PP.text "Z_" <> PP.text x <+> PP.sep (map why3expr xs))
why3expr (Op op x y) = PP.parens (why3expr x <+> why3op op <+> why3expr y)
why3expr (EVar v) = why3ident v
why3expr (Cons x y) = PP.parens (PP.text "Cons" <+> why3expr x <+> why3expr y)
why3expr Nil = PP.text "Nil"
why3expr (NumLit s) = why3numlit s
why3expr (ETuple xs) = PP.parens (PP.sep $ PP.punctuate PP.comma $ map why3expr xs)
why3expr (EString i) = PP.text "string" <> PP.int i
why3expr (Neg x) = PP.parens (PP.text "-" <+> why3expr x)

why3numlit :: NumericLiteral -> PP.Doc
why3numlit (LitInteger n)  = PP.text (show n) <> PP.text ".0"
why3numlit (LitRational r) =
  PP.parens (PP.text (show (numerator r)++".0")
             <+> PP.text "/"
             <+> PP.text (show (denominator r)++".0")
            )

why3op :: OP -> PP.Doc
why3op OpPlus   = PP.text "+"
why3op OpMinus  = PP.text "-"
why3op OpTimes  = PP.text "*"
why3op OpDiv    = PP.text "/"
why3op OpConcat = PP.text "++"

why3formula :: Formula -> PP.Doc
why3formula (Pred x xs) = PP.parens (why3ident x <+> PP.sep (map why3expr xs))
why3formula (And []) = PP.text "true"
why3formula (And xs) =  foldr1 (\x y -> PP.parens (x <+> PP.text "/\\" $$ y))
                            $ map why3formula xs
why3formula (Or []) = PP.text "false"
why3formula (Or xs) =  foldr1 (\x y -> PP.parens (x <+> PP.text "\\/" $$ y))
                            $ map why3formula xs
why3formula (Not x) = PP.parens (PP.text "not" <+> why3formula x)
why3formula (Implies x y) = PP.parens (why3formula x <+> PP.text "->" $$ why3formula y)
why3formula (Iff x y) = PP.parens (why3formula x <+> PP.text "<->" $$ why3formula y)
why3formula SMT.False = PP.text "false"
why3formula SMT.True = PP.text "true"
why3formula (Eq x y) = PP.parens (why3expr x <+> PP.text "=" <+> why3expr y)
why3formula (Comp op x y) = PP.parens (why3expr x <+> why3cop op <+> why3expr y)
why3formula (Forall [] f) = why3formula f
why3formula (Forall bs f) =
     PP.parens (PP.text "forall" <+> why3quantbind bs
                <> PP.text "." $$ why3formula f)
why3formula (Exists [] f) = why3formula f
why3formula (Exists bs f) =
     PP.parens (PP.text "exists" <+> why3quantbind bs
                <> PP.text "." $$ why3formula f)

why3bind :: Binder -> PP.Doc
why3bind (Bind x t) = why3ident x <+> PP.colon <+> why3type t

why3cop :: CmpOP -> PP.Doc
why3cop OpLE = PP.text "<="
why3cop OpLT = PP.text "<"
why3cop OpGE = PP.text ">="
why3cop OpGT = PP.text ">"

why3cmd :: Cmd -> PP.Doc
why3cmd (Axiom x f) = PP.text "axiom" <+> PP.text x <+> PP.colon <+> why3formula f
why3cmd (Lemma x f) = PP.text "lemma" <+> PP.text x <+> PP.colon <+> why3formula f
why3cmd (Goal x f)  = PP.text "goal" <+> PP.text x <+> PP.colon <+> why3formula f
why3cmd (NegGoal x f) = PP.text "goal" <+> PP.text x <+> PP.colon <+> why3formula (Not f)
why3cmd (DeclareFun x ts ret) =
  PP.text "function" <+> why3ident x <+> PP.sep (map why3type ts)
           <+> PP.colon <+> why3type ret
why3cmd (DefineFun x bs ret e) =
  PP.text "function" <+> why3ident x <+> why3funbind bs
           <+> PP.colon <+> why3type ret
           $$ PP.nest 2 (PP.text "=" <+> why3expr e)
why3cmd (DeclarePred x ts) =
  PP.text "predicate" <+> why3ident x <+> PP.sep (map why3type ts)
why3cmd (DefinePred x bs f) =
  PP.text "predicate" <+> why3ident x <+> why3funbind bs
     $$ PP.nest 2 (PP.text "=" <+> why3formula f)
why3cmd (DeclareType x vs) =
  PP.text "type" <+> PP.text x <+> PP.sep (map why3tvar vs)
why3cmd (DefineType x vs ty) =
  PP.text "type" <+> PP.text x <+> PP.sep (map why3tvar vs)
  $$ PP.nest 2 (PP.text "=" <+> why3type ty)
why3cmd (Information s) =
  PP.vcat
      $ map (\x -> PP.text "(*" <+> PP.text x <+> PP.text "*)")
      $ lines s

why3cmd (SubProblem nm cs) =
  PP.text "theory" <+> PP.text nm
  $$
  PP.nest 2 (PP.text "use import Background")
  $$
  PP.nest 2 (PP.vcat $ map why3cmd cs)
  $$
  PP.text "end"
why3cmd (DeclareData []) = PP.empty
why3cmd (DeclareData (d:ds)) =
  PP.vcat $
    ( PP.text "type" <+> why3data d ) :
    ( map (\d' -> PP.text "with" <+> why3data d') ds )
why3cmd (DeclareString i s) =
  PP.text "constant" <+> PP.text "string" <> PP.int i <+>
       PP.colon <+> PP.text "string" <+> PP.text "=" <+> why3string s
why3cmd (DeclareEvent x tys) =
  PP.text "function" <+> PP.text x <+> PP.sep (map why3type tys)
           <+> PP.colon <+> PP.text "event"
why3cmd (DeclareAction x tys) =
  PP.text "function" <+> PP.text x <+> PP.sep (map why3type tys)
           <+> PP.colon <+> PP.text "action"

why3cmd' (SubProblem nm cs) =
  PP.nest 2 (PP.vcat $ map why3cmd cs)
  
why3cmd' s = why3cmd s

why3string [] = PP.text "Nil"
why3string (c:cs) = PP.parens (PP.text "Cons" <+> PP.int (fromEnum c) <+> why3string cs)

why3data :: DataDecl -> PP.Doc
why3data (DataDecl x vs cs) =
  PP.text x <+> PP.sep (map why3tvar vs)
  $$ PP.nest 2 (PP.text "=" <+> PP.sep (PP.punctuate (PP.text " |") (map why3con cs)))
why3data (DataTypeDecl x vs ty) =
  PP.text x <+> PP.sep (map why3tvar vs)
  $$ PP.nest 2 (PP.text "=" <+> why3type ty)


-- add a capital Z to the front of every data constructor to satisfy
-- the why3 lexical conditions
why3con :: (String, [Type]) -> PP.Doc
why3con (c,ts) = PP.text "Z_" <> PP.text c <+> PP.sep (map why3type ts)

why3tvar :: String -> PP.Doc
why3tvar x = PP.text "\'z" <> PP.text x

why3funbind :: [Binder] -> PP.Doc
why3funbind bs = PP.sep $ map (PP.parens . why3bind) bs

why3quantbind :: [Binder] -> PP.Doc
why3quantbind bs = PP.sep $ PP.punctuate PP.comma $ map why3bind bs


cvc4type :: Type -> PP.Doc
cvc4type (TApp x []) = PP.text x
cvc4type (TApp x ts) = PP.parens (PP.text x <+> PP.sep (map cvc4type ts))
cvc4type (TVar x) = PP.text x
cvc4type (Tuple ts) = PP.parens $ PP.sep $ PP.punctuate (PP.text " * ") (map cvc4type ts)
cvc4type Real = PP.text "Real"
cvc4type (List t) = PP.parens (PP.text "List" <+> cvc4type t)
cvc4type (Set t) = PP.parens (PP.text "Set" <+> cvc4type t)
cvc4type (Map t1 t2) = PP.parens (PP.text "Map" <+> cvc4type t1 <+> cvc4type t2)
cvc4type String = PP.text "String"

cvc4ident :: Ident -> PP.Doc
cvc4ident (IdName x) = PP.text x
cvc4ident (IdFresh i) = PP.text "X$" <> PP.int i
cvc4ident (IdArg i) = PP.text "Arg$" <> PP.int i

cvc4expr :: Expr -> PP.Doc
cvc4expr (App x []) = cvc4ident x
cvc4expr (App x es) = PP.parens (cvc4ident x <+> PP.sep (map cvc4expr es))
cvc4expr (DataApp x []) = PP.text x
cvc4expr (DataApp x es) = PP.parens (PP.text x <+> PP.sep (map cvc4expr es))
cvc4expr (Op op t1 t2) = PP.parens (cvc4op op <+> cvc4expr t1 <+> cvc4expr t2)
cvc4expr (Neg t) = PP.parens (PP.text "-" <+> cvc4expr t)
cvc4expr (EVar v) = cvc4ident v
cvc4expr (Cons h t) = PP.parens (PP.text "insert" <+> cvc4expr h <+> cvc4expr t)
cvc4expr Nil = PP.text "nil"
cvc4expr (NumLit n) = cvc4numlit n
cvc4expr (ETuple xs) = PP.parens $ PP.sep $ PP.punctuate PP.comma (map cvc4expr xs)
cvc4expr (EString i) = PP.text "string$" <> PP.int i

cvc4numlit :: NumericLiteral -> PP.Doc
cvc4numlit (LitInteger n)  = PP.text (show n)
cvc4numlit (LitRational r) =
  PP.parens (PP.text "/" <+>
             PP.text (show (numerator r)) <+>
             PP.text (show (denominator r)))

cvc4op :: OP -> PP.Doc
cvc4op OpPlus   = PP.text "+"
cvc4op OpMinus  = PP.text "-"
cvc4op OpTimes  = PP.text "*"
cvc4op OpDiv    = PP.text "/"
cvc4op OpConcat = PP.text "append"

cvc4formula :: Formula -> PP.Doc
cvc4formula (Pred x []) = cvc4ident x
cvc4formula (Pred x xs) = PP.parens (cvc4ident x <+> PP.sep (map cvc4expr xs))
cvc4formula (And []) = PP.text "true"
cvc4formula (And [x]) = cvc4formula x
cvc4formula (And xs) = PP.parens (PP.text "and" <+> PP.sep (map cvc4formula xs))
cvc4formula (Or []) = PP.text "false"
cvc4formula (Or [x]) = cvc4formula x
cvc4formula (Or xs) = PP.parens (PP.text "or" <+> PP.sep (map cvc4formula xs))
cvc4formula (Not x) = PP.parens (PP.text "not" <+> cvc4formula x)
cvc4formula (Implies a b) = PP.parens (PP.text "=>" <+> cvc4formula a <+> cvc4formula b)
cvc4formula (Iff a b) = PP.parens (PP.text "=" <+> cvc4formula a <+> cvc4formula b)
cvc4formula (Forall [] x) = cvc4formula x
cvc4formula (Forall bs x) =
     PP.parens (PP.text "forall" <+> PP.parens (PP.sep $ map cvc4bind bs)
                                 <+> cvc4formula x)
cvc4formula (Exists [] x) = cvc4formula x
cvc4formula (Exists bs x) =
     PP.parens (PP.text "exists" <+> PP.parens (PP.sep $ map cvc4bind bs)
                                 <+> cvc4formula x)
cvc4formula SMT.False = PP.text "false"
cvc4formula SMT.True = PP.text "true"
cvc4formula (Eq x y) = PP.parens (PP.text "=" <+> cvc4expr x <+> cvc4expr y)
cvc4formula (Comp op x y) = PP.parens (cvc4cop op <+> cvc4expr x <+> cvc4expr y)

cvc4cop :: CmpOP -> PP.Doc
cvc4cop OpLE = PP.text "<="
cvc4cop OpLT = PP.text "<"
cvc4cop OpGE = PP.text ">="
cvc4cop OpGT = PP.text ">"

cvc4bind :: Binder -> PP.Doc
cvc4bind (Bind x t) = PP.parens (cvc4ident x <+> cvc4type t)

cvc4cmd :: Cmd -> PP.Doc
cvc4cmd (Axiom _ f) = PP.parens (PP.text "assert" <+> cvc4formula f)
cvc4cmd (Lemma _ f) = error "CVC4 output mode does not support lemmas"
cvc4cmd (Goal _ f)  =
    PP.parens (PP.text "assert" <+> cvc4formula (Not f))
    $$
    PP.parens (PP.text "check-sat")
cvc4cmd (NegGoal _ f)  =
    PP.parens (PP.text "assert" <+> cvc4formula f)
    $$
    PP.parens (PP.text "check-sat")
cvc4cmd (DeclareString i s) =
    PP.parens (PP.text "define-fun" <+> PP.text "string$" <> PP.int i
              <+> PP.text "()" <+> PP.text "String"
              <+> PP.doubleQuotes (PP.text s))
cvc4cmd (DeclareFun x ts ret) =
  PP.parens (PP.text "declare-fun" <+> cvc4ident x <+>
             PP.parens (PP.sep $ map cvc4type ts) <+> cvc4type ret)
cvc4cmd (DefineFun x args ret def) =
  PP.parens (PP.text "define-fun" <+> cvc4ident x <+>
             PP.parens (PP.sep $ map cvc4bind args) <+> cvc4type ret
             $$
             cvc4expr def)
cvc4cmd (DeclarePred x ts) =
  PP.parens (PP.text "declare-fun" <+> cvc4ident x <+>
             PP.parens (PP.sep $ map cvc4type ts) <+> PP.text "Bool")
cvc4cmd (DefinePred x args def) =
  PP.parens (PP.text "define-fun" <+> cvc4ident x <+>
             PP.parens (PP.sep $ map cvc4bind args) <+> PP.text "Bool"
             $$
             cvc4formula def)
cvc4cmd (DeclareType x args) =
  PP.parens (PP.text "declare-sort" <+> PP.text x <+> PP.int (length args))
cvc4cmd (DefineType x args ty) =
  PP.parens (PP.text "define-type" <+> PP.text x <+>
             PP.parens (PP.sep $ map PP.text args) <+>
             cvc4type ty)
cvc4cmd (DeclareData bs) =
  PP.parens (PP.text "declare-datatypes" <+> PP.text "()" <+>
             PP.parens (PP.vcat $ map cvc4data bs))
cvc4cmd (Information s) =
  PP.vcat
    $ map (\x -> PP.parens (PP.text "echo" <+> PP.doubleQuotes (PP.text x)))
    $ lines s

cvc4cmd (SubProblem s xs) =
  PP.parens (PP.text "push" <+> PP.int 1)
  $$
  PP.nest 2 (PP.parens (PP.text "echo"))
  $$
  PP.nest 2 (PP.parens (PP.text "echo" <+> PP.doubleQuotes (PP.text s)))
  $$
  PP.nest 2 (PP.vcat $ map cvc4cmd xs)
  $$
  PP.parens (PP.text "pop" <+> PP.int 1)
cvc4cmd (DeclareEvent x ts) =
  PP.parens (PP.text "declare-fun" <+> PP.text x <+>
             PP.parens (PP.sep $ map cvc4type ts) <+> PP.text "event")
cvc4cmd (DeclareAction x ts) =
  PP.parens (PP.text "declare-fun" <+> PP.text x <+>
             PP.parens (PP.sep $ map cvc4type ts) <+> PP.text "action")

cvc4data :: DataDecl -> PP.Doc
cvc4data (DataDecl x vs cs) =
  PP.parens (PP.text x <+> (PP.sep $ map cvc4con cs))
cvc4data (DataTypeDecl x args ty) =
  PP.parens (PP.text "define-type" <+> PP.text x <+>
             PP.parens (PP.sep $ map PP.text args) <+>
             cvc4type ty)

cvc4con :: (String,[Type]) -> PP.Doc
cvc4con (c,ty) = PP.parens (PP.text c <+> (PP.sep $ map (cvc4conty c) (zip [0..] ty)))

cvc4conty :: String -> (Int,Type) -> PP.Doc
cvc4conty c (i,ty) = PP.parens (PP.text c <> PP.text "$" <> PP.int i <+> cvc4type ty)
