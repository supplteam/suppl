{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module examines handler and procedure bodies and finds all control-flow
--   paths in them.  These paths are called Slices, and are used in the conflict
--   analysis phase.
module Slice where

import Data.List
import Control.Monad
import Data.Graph
import qualified Data.Map as M
import Data.Map ( Map )
import qualified Data.Set as S
import Data.Set ( Set )

import Tokens( Pn(..), Located(..) )
import AST
import SymbolTable
import Compile

data PPoint =
     PAction Pn Ident (Args (Term (Pn,IType)))  -- invoke action
   | PPcall Ident (Args (Term (Pn,IType)))      -- invoke procedure
   | PGuard Pn (QClause (Pn,IType))   -- positive guard
   | PNGuard Pn (QClause (Pn,IType))  -- negated guard
   | PEGuard Pn (QClause (Pn,IType))  -- foreach guard
   | PEvent Pn Ident (Args (Maybe Var)) [Type]    -- initiating handler event
   | PProc Pn Ident (Args (Maybe Var)) [Type]	-- initiating procedure
   deriving (Eq,Show)

type Slice = [PPoint]

{- NB : Given something like
            query
              _ => action1
              _ => action2

we generate two slices, one headed by each action.
Although the logical conditions leading to the two
actions are (correctly) disjoint, we don't otherwise
explicitly record the fact that only one of the
slices can actually occur in any any given execution.
Perhaps we should, to reduce the number of possibly
conflicting slices to investigate later.
-}

findInBody :: SymbolTable -> Slice -> HBody (Pn,IType) -> [Slice]
findInBody st sl (HBody pn (HProcedure x args)) =
    case st_lookup x st of
       Nothing -> error (unwords [qt x," not in scope"])
       Just (Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ _))) -> [PPcall x args:sl]
       Just (Symbol _ _ _ (ProcedureSymbol (PrimProcedureDecl pid _))) -> [PAction pn pid args:sl]
       Just i -> error (unwords ["expected",qt x,"to be a procedure, but it is a",namesort (sym_info i)])

findInBody st sl (HBody pn (HSeq x y)) =
    findInBody st sl x ++ findInBody st sl y

findInBody st sl (HBody pn (HForeach q body)) =
    findInBody st (PEGuard pn q:sl) body

findInBody st sl (HBody pn (HQuery bs)) =
    findInQuery sl bs
    where
      findInQuery ::  Slice -> [(Pn,QBranch (Pn,IType))] -> [Slice]
      findInQuery sl [] = []
      findInQuery sl ((pn,QueryBranch q b):qbs) =
            findInBody st (PGuard pn q:sl) b ++
	    findInQuery (PNGuard pn q:sl) qbs
      findInQuery sl ((pn,DefaultBranch b):qbs) =
	    findInBody st sl b ++
	    findInQuery sl qbs

findInBody st sl (HBody pn (HInsert args tab)) = []

findInBody st sl (HBody pn (HDelete args tab)) = []

findInBody st sl (HBody pn HSkip) = []

findInDecl :: SymbolTable -> (Pn,CodeOrigin, Decl (Pn,IType)) -> [Slice]
findInDecl st (pn, _, DHandler (ename,eargs,body)) =
   findInBody st [PEvent pn ename eargs targs] body
    where targs =
            case st_lookup ename st of
              Nothing -> error (unwords [qt ename," not in scope"])
              Just (Symbol _ _ _ (EventSymbol (EventDecl _ targs))) -> extractArgs targs
              Just i -> error (unwords ["expected",qt ename,"to be an event, but it is a",namesort (sym_info i)])
findInDecl st (pn, _, DProcDefn (x,args,body)) =
   findInBody st [PProc pn x args targs] body
    where targs =
            case st_lookup x st of
              Nothing -> error (unwords [qt x," not in scope"])
              Just (Symbol _ _ _ (ProcedureSymbol (ProcedureDecl _ targs))) -> extractArgs targs
              Just i -> error (unwords ["expected",qt x,"to be an procedure, but it is a",namesort (sym_info i)])
findInDecl st _ = []

findInProg :: SymbolTable -> Prog (Pn,IType) -> [Slice]
findInProg st prog = concat $ map (findInDecl st) prog
