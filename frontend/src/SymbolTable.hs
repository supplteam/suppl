{- Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. -}

-- | This module defines symbol tables, both for type identifiers
--   and for term identifiers, which occupy separate namespaces.
--   The 'processDecls' function will construct symbol tables
--   from a program.  The 'st_lookup' and 'tst_lookup' functions
--   should be used to look up identifiers in the symbol tables.
--
module SymbolTable
( SymbolInfo(..)
, FuncInfo(..)
, PredInfo(..)
, TableInfo(..)
, TypeSymbolInfo(..)
, SymbolTable
, TypeSymbolTable
, Symbol(..)
, processDecls
, namesort
, tnamesort
, st_lookup
, tst_lookup
) where


import Control.Monad
import qualified Data.Map as M
import Data.Map ( Map )

import Tokens
import AST


data FuncInfo
   = FIDecl Pn FunctionDecl  -- ^ A function declaration
   | FIConstructor Pn Ident [(Pn,Var)] Ident (Args Type)  
         -- ^ A data constructor: position, type name, type arguments,
         --   constructor name, constructor argument types

 deriving (Eq, Show)

data PredInfo
   = PIEmpty Pn       -- ^ An \"empty\" predicate info.  If a predicate symbol has this info
                      --   after the symbol table is complete, it means a predicate had mode
                      --   declarations but no corresponding predicate declaration.
   | PIDecl PredDecl
 deriving (Eq, Show)

data TableInfo
   = TIEmpty Pn       -- ^ An \"empty\" table info.  If a table symbol has this info
                      --   after the symbol table is complete, it means a table had index
                      --   declarations but no corresponding table declaration.
   | TIDecl TableDecl
 deriving (Eq, Show)

data TypeSymbolInfo
  = TypeSymbol TypeDecl  -- ^ A type introduced by a primitive or regular type declaration
  | DataSymbol DataDecl  -- ^ A type introduced by a data declaration
 deriving (Eq,Show)

data SymbolInfo
  = FuncSymbol FuncInfo                -- ^ A function symbol
  | PredSymbol PredInfo [Args Mode]    -- ^ A predicate symbol, with its declared modes
  | ProcedureSymbol ProcedureDecl      -- ^ A procedure symbol
  | EventSymbol EventDecl              -- ^ An event symbol
  | TableSymbol TableInfo [Args Mode]  -- ^ A table symbol, with its declared indexes
 deriving (Eq, Show)

data Symbol a = 
  Symbol
  { sym_pos   :: Pn
  , sym_orig  :: CodeOrigin
  , sym_ident :: Ident
  , sym_info  :: a
  }
 deriving Show

type SymbolTable = Map String (Symbol SymbolInfo)
type TypeSymbolTable = Map String (Symbol TypeSymbolInfo)

type ST = (TypeSymbolTable,SymbolTable) -> Either (Pn,String) (TypeSymbolTable,SymbolTable)

-- | Given a symbol info, return a string descibing the sort of symbol it is.
namesort :: SymbolInfo -> String
namesort (PredSymbol _ _) = "predicate"
namesort (FuncSymbol _) = "function"
namesort (ProcedureSymbol _) = "procedure"
namesort (TableSymbol _ _) = "table"
namesort (EventSymbol _) = "event"

-- | Given a type symbol info, return a string descibing the sort of symbol it is.
tnamesort :: TypeSymbolInfo -> String
tnamesort (TypeSymbol _) = "type"
tnamesort (DataSymbol _) = "data type"

unexpectedError :: Pn -> Ident -> String -> String -> Either (Pn, String) a
unexpectedError pn n x i = 
  Left (pn, unwords 
     [qt n,"declared to be a",x,"but is previously declared to be a",i])

unexpectedError' :: Pn -> Ident -> String -> Either (Pn, String) a
unexpectedError' pn n i = 
  Left (pn, unwords 
     [qt n,"is previously declared to be a",i])

-- | Lookup an identifier in the symbol table
st_lookup :: Ident -> SymbolTable -> Maybe (Symbol SymbolInfo)
st_lookup n st = M.lookup (getIdent n) st

-- | Lookup a type identifier in the type symbol table
tst_lookup :: Ident -> TypeSymbolTable -> Maybe (Symbol TypeSymbolInfo)
tst_lookup n tst = M.lookup (getIdent n) tst

updateType :: Pn -> CodeOrigin -> TypeDecl -> Ident -> ST
updateType pn orig decl n (tst,st) =
   case tst_lookup n tst of
     Nothing -> Right (M.insert (getIdent n) (Symbol pn orig n (TypeSymbol decl)) tst,st)
     Just x@(Symbol _ _ _ (TypeSymbol decl')) -> 
              if equivTypeDecls decl decl'
                 then Right (tst,st)
                 else unexpectedError' pn n (tnamesort (sym_info x))
     Just x -> unexpectedError' pn n (tnamesort (sym_info x))


updateFunc :: Pn -> CodeOrigin -> FuncInfo -> Ident -> ST
updateFunc pn orig fi n (tst,st) =
   case st_lookup n st of
     Nothing -> Right (tst,M.insert (getIdent n) (Symbol pn orig n (FuncSymbol fi)) st)
     Just x@(Symbol _ _ _ (FuncSymbol fi')) -> 
        if equivFuncInfo fi fi'
           then Right (tst,st)
           else unexpectedError' pn n (namesort (sym_info x))
     Just x -> unexpectedError' pn n (namesort (sym_info x))

updateEvent :: Pn -> CodeOrigin -> EventDecl -> Ident -> ST
updateEvent pn orig ed n (tst,st) =
   case st_lookup n st of
     Nothing -> Right (tst, M.insert (getIdent n) (Symbol pn orig n (EventSymbol ed)) st)
     Just x@(Symbol _ _ _ (EventSymbol ed')) ->
         if equivEventDecls ed ed'
            then Right (tst,st)
            else unexpectedError pn n "event" (namesort (sym_info x))
     Just x -> unexpectedError pn n "event" (namesort (sym_info x))

updateData :: Pn -> CodeOrigin -> DataDecl -> Ident -> ST
updateData pn orig decl@(DataDecl _ vs cs) n (tst,st) =
   case tst_lookup n tst of
     Nothing -> do
         (_,st') <- foldM (\x (cnm,cargs) ->
                      updateFunc pn orig (FIConstructor pn n vs cnm cargs) cnm x) (tst,st) cs
         let tst' = M.insert (getIdent n) (Symbol pn orig n (DataSymbol decl)) tst         
         return (tst',st')
     Just x@(Symbol _ _ _ (DataSymbol decl')) ->
             if equivDataDecls decl decl'
                 then Right (tst,st)
                 else unexpectedError' pn n (tnamesort (sym_info x))
     Just x -> unexpectedError' pn n (tnamesort (sym_info x))

updatePred :: Pn -> CodeOrigin 
           -> (Pn -> CodeOrigin -> PredInfo -> [Args Mode] -> Either (Pn,String) (Symbol SymbolInfo))
           -> Ident -> ST
updatePred pn orig f n (tst,st) =
   case st_lookup n st of
     Nothing ->
         fmap (\x -> (tst,M.insert (getIdent n) x st)) (f pn orig (PIEmpty pn) [])
     Just (Symbol pn' orig' _ (PredSymbol info ms)) ->
         fmap (\x -> (tst, M.insert (getIdent n) x st)) (f pn' orig' info ms)
     Just x -> unexpectedError pn n "predicate" (namesort (sym_info x))

updateTable :: Pn -> CodeOrigin 
            -> (Pn -> CodeOrigin -> TableInfo -> [Args Mode] -> Either (Pn,String) (Symbol SymbolInfo)) 
            -> Ident -> ST
updateTable pn orig f n (tst,st) =
   case st_lookup n st of
     Nothing ->
         fmap (\x -> (tst,M.insert (getIdent n) x st)) (f pn orig (TIEmpty pn) [])
     Just (Symbol pn' orig' _ (TableSymbol info ms)) ->
         fmap (\x -> (tst,M.insert (getIdent n) x st)) (f pn' orig' info ms)
     Just x -> unexpectedError pn n "table" (namesort (sym_info x))

updateProcedure :: Pn -> CodeOrigin -> ProcedureDecl -> Ident -> ST
updateProcedure pn orig decl n (tst,st) =
   case st_lookup n st of
     Nothing -> Right (tst,M.insert (getIdent n) (Symbol pn orig n (ProcedureSymbol decl)) st)
     Just x@(Symbol _ _ _ (ProcedureSymbol decl')) ->
          if equivProcDecls decl decl'
             then Right (tst,st)
             else unexpectedError' pn n (namesort (sym_info x))
     Just x -> unexpectedError' pn n (namesort (sym_info x))

decl :: Pn -> CodeOrigin -> Decl ta -> ST

decl pn orig (DImport _) = Right
decl pn orig (DPragma _) = Right
decl pn orig (DRule r@((_,n,_),_)) = Right
decl pn orig (DHandler h@(n,_,_)) = Right
decl pn orig (DProcDefn _ ) = Right
decl pn orig (DAssert _) = Right
decl pn orig (DConflict _) = Right

decl pn orig (DTable t@(TableDecl n _ _ _)) =
    updateTable pn orig (\pn' orig' i ms ->
            case i of
               TIEmpty _ -> Right $ Symbol pn orig n (TableSymbol (TIDecl t) ms)
               TIDecl t' ->
                 if equivTableDecls t t'
                    then Right $ Symbol pn' orig' n (TableSymbol i ms)
                    else Left (pn, unwords ["table",qt n,"redeclared"])) n

decl pn orig (DIndex (IndexDecl n m)) =
    updateTable pn orig (\pn' orig' i ms -> Right $ Symbol pn' orig' n $ TableSymbol i (m:ms)) n

decl pn orig (DPred p) =
    updatePred pn orig (\pn' orig' i ms ->
            case i of
               PIEmpty _ ->
                 case checkInlineMode p of
                    Left msg  -> Left msg
                    Right ms' -> Right $ Symbol pn orig n $ PredSymbol (PIDecl p) (ms' ++ ms)
               PIDecl p' -> 
                  if equivPredDecls p p' 
                     then Right $ Symbol pn' orig' n $ PredSymbol i ms
                     else Left (pn, unwords ["predicate",qt n,"redeclared with nonidentical parameters"])) n

 where n = case p of
              PredDecl nm _ -> nm
              PrimPredDecl nm _ -> nm

decl pn orig (DMode (ModeDecl n m)) =
    updatePred pn orig (\pn' orig' i ms -> Right $ Symbol pn' orig' n $ PredSymbol i (m:ms)) n

decl pn orig (DType t) =
    updateType pn orig t n
 where n = case t of
                PrimTypeDecl nm -> nm
                TypeDecl nm _ _ -> nm

decl pn orig (DFunction f@(PrimFunctionDecl n _ _)) =
    updateFunc pn orig (FIDecl pn f) n

decl pn orig (DProcedure d@(PrimProcedureDecl n _)) =
    updateProcedure pn orig d n

decl pn orig (DProcedure d@(ProcedureDecl n _)) =
    updateProcedure pn orig d n

decl pn orig (DData d@(DataDecl n _ _)) =
    updateData pn orig d n

decl pn orig (DEvent d@(EventDecl n _)) =
    updateEvent pn orig d n

equivMaybe :: (a -> a -> Bool) -> Maybe a -> Maybe a -> Bool
equivMaybe test (Just x) (Just y) = test x y
equivMaybe test Nothing Nothing = True
equivMaybe test _ _ = False

equivArgs :: (a -> a -> Bool) -> Args a -> Args a -> Bool
equivArgs test (PositionalArgs xs) (PositionalArgs ys) =
   length xs == length ys &&
   (and $ map (\ ((_,x),(_,y)) -> test x y) (zip xs ys))
equivArgs test (NamedArgs xs) (NamedArgs ys) =
   length xs == length ys &&
   (and $ map (\ ((_,n,x),(_,m,y)) -> n == m && test x y) (zip xs ys))
equivArgs test _ _ = False

equivTypeDecls :: TypeDecl -> TypeDecl -> Bool
equivTypeDecls (PrimTypeDecl n) (PrimTypeDecl m) = n == m
equivTypeDecls (TypeDecl n vs t) (TypeDecl m vs' t') = n == m && vs == vs' && t == t'
equivTypeDecls _ _ = False

equivPredDecls :: PredDecl -> PredDecl -> Bool
equivPredDecls (PredDecl ident args) (PredDecl ident' args') =
    ident == ident' && equivArgs (==) args args'
equivPredDecls (PrimPredDecl ident args) (PrimPredDecl ident' args') =
    ident == ident' && equivArgs (==) args args'
equivPredDecls _ _ = False

equivDataDecls (DataDecl n vs bs) (DataDecl m vs' bs') = 
  n == m && vs == vs' && length bs == length bs' &&
  (and $ map (\ ((a, xs),(b,ys)) -> a == b && equivArgs (==) xs ys) $ zip bs bs')

equivFuncDecls (PrimFunctionDecl n tys ret) (PrimFunctionDecl m tys' ret') =
  n == m && ret == ret' && equivArgs (==) tys tys'

equivFuncInfo (FIDecl _ fd) (FIDecl _ fd') = equivFuncDecls fd fd'
equivFuncInfo (FIConstructor _ n vs c tys) (FIConstructor _ m vs' c' tys') =
  n == m && vs == vs' && c == c' && equivArgs (==) tys tys'
equivFuncInfo _ _ = False

equivEventDecls (EventDecl n tys) (EventDecl m tys') =
  n == m && equivArgs (==) tys tys'

equivTableDecls (TableDecl n tys key life) (TableDecl m tys' key' life') =
  n == m && equivArgs (==) key key' &&
    equivArgs (==) tys tys' && life == life'

equivProcDecls (PrimProcedureDecl n tys) (PrimProcedureDecl m tys') =
  n == m && equivArgs (==) tys tys'
equivProcDecls (ProcedureDecl n tys) (ProcedureDecl m tys') =
  n == m && equivArgs (==) tys tys'
equivProcDecls _ _ = False

checkInlineMode :: PredDecl -> Either (Pn,String) [Args Mode]
checkInlineMode (PredDecl _ args) = checkInlineMode' (mapArgs snd args)
checkInlineMode (PrimPredDecl _ args) = checkInlineMode' (mapArgs snd args)

checkInlineMode' :: Args (Maybe Mode) -> Either (Pn,String) [Args Mode]

-- special case: predicate with no arguments automatically
-- is assumed to have the "empty" mode
checkInlineMode' (PositionalArgs []) = Right [PositionalArgs []]
checkInlineMode' (NamedArgs []) = Right [NamedArgs []]

checkInlineMode' (PositionalArgs ((pn,Just m):xs)) =
       fmap ((:[]) . PositionalArgs . ((pn,m):) ) $ extractInlinePosMode xs 

checkInlineMode' (PositionalArgs ((pn,Nothing):xs)) =
       if lacksPosModes xs
          then Right []
          else Left (pn, "expected mode declaration")



checkInlineMode' (NamedArgs ((pn,nm,Just m):xs)) =
       fmap ((:[]) . NamedArgs . ((pn,nm,m):) ) $ extractInlineNamedMode xs 

checkInlineMode' (NamedArgs ((pn,nm,Nothing):xs)) =
       if lacksNamedModes xs 
          then Right []
          else Left (pn, "expected mode declaration")


lacksPosModes :: [(Pn,Maybe Mode)] -> Bool
lacksPosModes [] = True
lacksPosModes ((_,Nothing):xs) = lacksPosModes xs
lacksPosModes ((_,Just _):_) = False

lacksNamedModes :: [(Pn,String,Maybe Mode)] -> Bool
lacksNamedModes [] = True
lacksNamedModes ((_,_,Nothing):xs) = lacksNamedModes xs
lacksNamedModes ((_,_,Just _):_) = False

extractInlinePosMode :: [(Pn,Maybe Mode)] -> Either (Pn,String) [(Pn,Mode)]
extractInlinePosMode [] = Right []
extractInlinePosMode ((pn,Nothing):_) = Left (pn, "expected mode declaration")
extractInlinePosMode ((pn,Just m):xs) = fmap ((pn,m):) $ extractInlinePosMode xs

extractInlineNamedMode :: [(Pn,String,Maybe Mode)] -> Either (Pn,String) [(Pn,String,Mode)]
extractInlineNamedMode [] = Right []
extractInlineNamedMode ((pn,nm,Nothing):_) = Left (pn, "expected mode declaration")
extractInlineNamedMode ((pn,nm,Just m):xs) = fmap ((pn,nm,m):) $ extractInlineNamedMode xs


-- | Scan the declarations in a program and build the type
--   and term-level symbol tables.  Return any error messages
--   that occur along with the constructed tables.
processDecls :: [(Pn,CodeOrigin,Decl ta)] 
             -> ([(Pn,String)],TypeSymbolTable,SymbolTable)

processDecls = go [] M.empty M.empty
 where go msgs tst ts [] = (msgs,tst,ts)
       go msgs tst ts ((pn,orig,d):ds) =
            case decl pn orig d (tst,ts) of
               Left msg -> go (msg:msgs) tst ts ds
               Right (tst',ts') -> go msgs tst' ts' ds
