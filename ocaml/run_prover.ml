(* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. *)

(* to compile :
   ocamlfind ocamlopt -package why3 -linkpkg -o run_prover run_prover.ml
*)

(* Usage : ./run_prover file.why
   By default, run Alt-Ergo for 1s for each goal in the file.
   Prints the result in a *.res file.
   The results are mainly Valid or Timeout.
   http://why3.lri.fr/doc-0.83/manual005.html
   Valid: the task is valid according to the prover.
   Invalid: the task is invalid.
   Timeout: the prover exceeds the time or memory limit.
   Unknown msg: the prover can’t determine if the task is valid;
                the string parameter msg indicates some extra information.
   Failure msg: the prover reports a failure, i.e. it was unable to read correctly its input task.
   HighFailure: an error occurred while trying to call the prover,
                or the prover answer was not understood (i.e. none of the given regular expressions
                in the driver file matches the output of the prover).
*)

open Format
open Why3
open Why3.Whyconf
open Why3.Session
open Why3.Ident
open Why3.Call_provers

exception Finished

let filename_list = ref []
let includes = ref []
let why3_conf_filename = ref ""
let timeout = ref 1
let provers_to_use = ref []
let all = ref false

let keygen ?parent () = ()

let read_config conf =
  let config =
    match conf with
    | "" -> Whyconf.read_config None
    | _ -> Whyconf.read_config (Some conf)
  in
  let main = Whyconf.get_main config in
  let env = Env.create_env ((Whyconf.loadpath main) @ (List.rev !includes)) in
  let provers =
    Whyconf.Mprover.fold
    (fun _ p acc ->
      try
        let d = Driver.load_driver env p.Whyconf.driver [] in
        (p,d)::acc
      with e ->
        let p = p.Whyconf.prover in
        eprintf "Failed to load driver for %s %s: %a@."
          p.Whyconf.prover_name p.Whyconf.prover_version
          Exn_printer.exn_printer e;
        exit 1)
    (Whyconf.get_provers config) [] in
  (env, config, List.rev provers)

let print_goal provers oc g =
  try
    List.iter
      (fun (p, d) ->
        if (!provers_to_use = []) then (provers_to_use := ["Alt-Ergo"]) else begin () end;
        if not (List.mem p.Whyconf.prover.prover_name !provers_to_use || !all)  then () else
        match Session.goal_task_option g with
        | Some t ->
          let result = Call_provers.wait_on_call
          (Driver.prove_task
            ~command: p.Whyconf.command
            ~timelimit: !timeout
            d t ()) () in
          begin
            match g.goal_parent with
            | Parent_theory t ->
                fprintf (formatter_of_out_channel oc) "@[%s.%s : %a by %s in %5.2f seconds.@]@."
                t.theory_name.id_string
                g.goal_name.id_string
                Call_provers.print_prover_answer result.Call_provers.pr_answer
                p.Whyconf.prover.prover_name
                result.Call_provers.pr_time;
                if result.Call_provers.pr_answer = Valid then raise Finished else ()
            | _ -> ()
          end
        | None -> ())
      provers
  with
  | Finished -> ()

let print_res filename =
  if not (Filename.check_suffix filename ".why") then failwith "Wrong file extension" else ();
  let dirname = (Filename.dirname filename) in
  let (env, config, provers) = read_config !why3_conf_filename in
  let res = (Filename.chop_suffix filename ".why") ^ ".res" in
  let oc = open_out res in
  let dummy = Session.create_session dirname in
  let (env_session, _, _) = Session.update_session ~keygen ~allow_obsolete: true dummy env config in
  let file = Session.add_file keygen env_session (Filename.basename filename) in
  let theories = file.Session.file_theories in
  List.iter (fun th -> List.iter (print_goal provers oc) th.Session.theory_goals) theories;
  close_out oc

let parse_cmd () =
  let anon_fun s = filename_list := s :: !filename_list in
  let speclist =
    [ ("-C", Arg.Set_string why3_conf_filename, "Read this Why3 configuration file");
      ("-I", Arg.String (fun s -> includes := s :: !includes),
       "Include directory");
      ("-L", Arg.String (fun s -> includes := s :: !includes),
       "Same as -I");
      ("--prover", Arg.String (fun s -> provers_to_use := s :: !provers_to_use),
       "Which provers to use, will only use Alt-Ergo if not specified");
      ("-p", Arg.String (fun s -> provers_to_use := s :: !provers_to_use),
       "Same as --prover");
      ("-t", Arg.Set_int timeout, "Prover timeout limit, default : 1s");
      ("--all", Arg.Set all, "Use all available provers")
    ] in
  let usage_msg = "run_prover -I .. -p Alt-Ergo -p CVC4 file.why" in
  Arg.parse speclist anon_fun usage_msg

let _ =
  parse_cmd ();
  try
    List.iter print_res !filename_list
  with
    e -> eprintf "%a" Exn_printer.exn_printer e;
         exit 1
