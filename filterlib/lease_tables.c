/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>

#include "api.h"
#include "internal.h"

#define MAX_LEASE_AMT  100000
#define LEASE_RENEW_THR 10000

pthread_mutex_t lease_table_mutex;

struct lease_table_entry lease_table[MAX_LEASES];
struct lease_table_entry* lease_table_freelist;

int calc_lease_index( struct lease_table_entry* e ) {
  int idx = e - lease_table;
  assert( &lease_table[idx] == e );
  return idx;
}

void teardown_lease_table() {
  if( pthread_mutex_destroy( &lease_table_mutex ) < 0 )
    { perror("destroy lease table mutex"); exit(1); }
}

void setup_lease_table() {
  int i;

  pthread_mutexattr_t mx_attr;
  pthread_mutexattr_init(&mx_attr);
  pthread_mutexattr_settype(&mx_attr, PTHREAD_MUTEX_NORMAL );

  if( pthread_mutex_init( &lease_table_mutex, NULL ) < 0 )
    { perror("create lease table mutex"); exit(1); }

  pthread_mutexattr_destroy(&mx_attr);

  for(i=0;i<MAX_LEASES-1; i++) {
    lease_table[i].freelist = &lease_table[i+1];
  }
  lease_table[MAX_LEASES-1].freelist = NULL;
  lease_table_freelist = &lease_table[0];
}


void free_lte(struct lease_table_entry* lte)
{
  lte->freelist = lease_table_freelist;
  lease_table_freelist = lte;
}

struct lease_table_entry* alloc_lte()
{
  struct lease_table_entry *x = lease_table_freelist;

  if(x) {
    lease_table_freelist = lease_table_freelist->freelist;

    x->cl = NULL;
    x->stats.orig_bytes = 0;
    x->stats.orig_packets = 0;
    x->stats.repl_bytes = 0;
    x->stats.repl_packets = 0;
    x->connmark = 0;
    x->active = 0;
    x->freelist = NULL;
  }
    
  return x;
}
