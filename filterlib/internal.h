/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

struct conn_list
{
  /* This field is initilized at the object init. */
  pthread_mutex_t lk;
  
  /* These fields are protected by the embedded lock 'lk' */
  int refcount;
  void (* free_callback) (void* arg);
  void* callback_arg;

  /* These fields are protected by the global 'conn_list_mutex' */
  conn_callback_t attach_callback;
  conn_callback_t destroy_callback;

  struct connection_desc cdesc;

  struct lease_table_entry* lease;
  struct connection_stats final_stats;
  struct conn_list* next;
  struct conn_list* prev;
};

struct lease_table_entry
{
  /* All fields protected by the global 'lease_table_mutex' */
  struct conn_list* cl;
  struct connection_stats stats;
  u_int32_t connmark;
  int active;
  struct lease_table_entry* freelist;
};

extern pthread_mutex_t lease_table_mutex;
extern pthread_mutex_t conn_list_mutex;

/* These global fields are protected by the global 'conn_list mutex' */
extern struct conn_list installed_list_head;
extern struct conn_list installed_list_tail;
extern struct conn_list active_list_head;
extern struct conn_list active_list_tail;
extern struct conn_list teardown_list_head;
extern struct conn_list teardown_list_tail;


#define MAX_LEASES 256

extern struct lease_table_entry lease_table[MAX_LEASES];
extern struct lease_table_entry* alloc_lte();
