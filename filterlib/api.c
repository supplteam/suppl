/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>

#include "api.h"
#include "internal.h"

int init_connection_library()
{
  printf("setting up connection library\n");

  setup_conn_lists();
  setup_lease_table();

  /* make sure we start with a clean slate */
  flush_conntracks();

  /* fire up the various threads */
  launch_filter_threads();

  return 0;
}

int teardown_connection_library()
{
  printf("tearing down connection library\n");

  kill_filter_threads();

  teardown_lease_table();
  teardown_conn_lists();

  return 0;
}


conn_t install_connection(struct connection_desc *cdesc
                         , conn_callback_t attach_callback
                         , conn_callback_t destroy_callback
		         , void (* free_callback)(void* arg)
                         , void* arg)
{
  struct conn_list* x = (struct conn_list*) malloc(sizeof(struct conn_list));

  memcpy(&(x->cdesc),cdesc,sizeof(struct connection_desc));
  x->attach_callback = attach_callback;
  x->destroy_callback = destroy_callback;
  x->free_callback = free_callback;
  x->callback_arg = arg;
  x->lease = NULL;
  x->refcount = 1;
  memset( &(x->final_stats), 0, sizeof(struct connection_stats) );

  pthread_mutexattr_t mx_attr;
  pthread_mutexattr_init(&mx_attr);
  pthread_mutexattr_settype(&mx_attr, PTHREAD_MUTEX_NORMAL );

  pthread_mutex_init(&(x->lk), &mx_attr );

  pthread_mutexattr_destroy(&mx_attr);

  if(pthread_mutex_lock(&conn_list_mutex) < 0) { perror("conn list lock"); exit(1); }
    insert_conn_list(x, &installed_list_tail);
  if(pthread_mutex_unlock(&conn_list_mutex) < 0) { perror("conn list unlock"); exit(1); }

  return (void*) x;
}

conn_t get_reference(struct conn_list* x) {
  if( pthread_mutex_lock(&(x->lk)) < 0 ) { perror("conn object lock"); exit(1); }
  x->refcount++;
  if( pthread_mutex_unlock(&(x->lk)) < 0 ) { perror("conn object lock"); exit(1); }
  return (conn_t)x;
}

conn_t fresh_reference(conn_t x) {
  return get_reference((struct conn_list*) x);
}

void free_connection(conn_t conn) {
  struct conn_list* x = (struct conn_list*) conn;
  int cnt;

  /* LOCK ORDERING: take the list mutex before any conn_list node mutex */
  if( pthread_mutex_lock(&conn_list_mutex) < 0 ) { perror("conn list lock"); exit(1); }
  if( pthread_mutex_lock(&(x->lk)) < 0 ) { perror("conn object lock"); exit(1); }
    cnt = x->refcount - 1;
    x->refcount = cnt;
    if(x->lease!=NULL || x->final_stats.status == CONN_STAT_INSTALLED) cnt++;
  if( pthread_mutex_unlock(&(x->lk)) < 0 ) { perror("conn object lock"); exit(1); }

  assert( cnt >= 0 );

  if( cnt == 0 && x->next != NULL ) {
    unlink_conn_list(x);
  }

  if( pthread_mutex_unlock(&conn_list_mutex) < 0 ) { perror("conn list unlock"); exit(1); }

  if( cnt == 0 ) {
    printf("freeing a conn object inside free_connection\n");

    if( pthread_mutex_destroy(&(x->lk)) < 0 ) { perror("conn object mutex destroy"); exit(1); }

    if(x->free_callback) {
      x->free_callback(x->callback_arg);
    }
    free(x);
  }
}

int teardown_connection(conn_t conn)
{
  struct conn_list* x = (struct conn_list*) conn;
  struct lease_table_entry* l;
  u_int32_t connmark = 0;

  /* LOCK ORDERING: take the conn list mutex before the lease table mutex */
  if(pthread_mutex_lock(&conn_list_mutex) < 0) { perror("conn list lock"); exit(1); }

    /* Grab the lease value. If lease is NULL by the time we grab it,
       the connection was never bound or somebody else is doing the teardown.
     */
    l = x->lease;

    if( l != NULL ) {
      if( pthread_mutex_lock(&lease_table_mutex) < 0 ) { perror("lease table lock"); exit(1); }
      if( l->active != 0 ) {
	l->active = 0;
	connmark = l->connmark;
      }

      if( pthread_mutex_unlock(&lease_table_mutex) < 0 ) { perror("lease table unlock"); exit(1); }
    } else {
      unlink_conn_list(x);
      insert_conn_list(x, &teardown_list_tail);
      x->final_stats.status = CONN_STAT_DESTROYED;
    }
  if(pthread_mutex_unlock(&conn_list_mutex) < 0) { perror("conn list unlock"); exit(1); }

  if( connmark != 0 ) {
    teardown_conntrack(connmark, &(x->cdesc));
  }

  return 0;
}

void connection_get_counters(conn_t conn, struct connection_stats* stats ) {
  struct conn_list* cl = (struct conn_list*) conn;
  struct lease_table_entry *lte;

  if(pthread_mutex_lock(&conn_list_mutex) < 0) { perror("conn list lock"); exit(1); }
  lte = cl->lease;
  if(lte) {
    if( pthread_mutex_lock(&lease_table_mutex) < 0 ) { perror("lease table lock"); exit(1); }
    memcpy( stats, &(lte->stats), sizeof(struct connection_stats) );
    if( pthread_mutex_unlock(&lease_table_mutex) < 0 ) { perror("lease table unlock"); exit(1); }
  } else {
    memcpy( stats, &(cl->final_stats), sizeof(struct connection_stats) );
  }
  if(pthread_mutex_unlock(&conn_list_mutex) < 0) { perror("conn list unlock"); exit(1); }
}


int dump_connections(dump_callback_t cb, void* arg) {
  struct conn_list* x;
  struct conn_list* y;
  struct conn_list* tail;
  struct lease_table_entry* lte;
  struct connection_stats* stats;
  int i;
  int result;

  if(cb == NULL) return;

  printf("dumping connections\n");

  if( pthread_mutex_lock(&conn_list_mutex) < 0 ) { perror("conn list lock"); exit(1); }
  if( pthread_mutex_lock(&lease_table_mutex) < 0 ) { perror("lease table lock"); exit(1); }
  
  result = 0;
  for(i=0;i<2 && result == 0;i++) {
    switch(i) {
    case 0: 
      x = installed_list_head.next;
      tail = &installed_list_tail;
      break;
    case 1:
      x = active_list_head.next;
      tail = &active_list_tail;
      break;
      /*
    case 2:
      x = teardown_list_head.next;
      tail = &teardown_list_tail;
      break;
      */
    }

    while( x != tail && result == 0) {
      y = x;
      x = x->next;
      lte = y->lease;
      if(lte) {
	stats = &(lte->stats);
      } else {
	stats = &(y->final_stats);
      }
      
      fprintf(stderr,"entering dump callback\n");
      result = cb( y, &(y->cdesc), stats, arg );
    }
  }

  if(pthread_mutex_unlock(&lease_table_mutex) < 0 ) { perror("lease table unlock"); exit(1); }
  if(pthread_mutex_unlock(&conn_list_mutex) < 0) { perror("conn list unlock"); exit(1); }

  return result;
}
