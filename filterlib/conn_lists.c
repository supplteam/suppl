/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>

#include "api.h"
#include "internal.h"

pthread_mutex_t conn_list_mutex;


/* These global fields are protected by the global 'conn_list mutex' */
struct conn_list installed_list_head;
struct conn_list installed_list_tail;
struct conn_list active_list_head;
struct conn_list active_list_tail;
struct conn_list teardown_list_head;
struct conn_list teardown_list_tail;

void init_conn_list(struct conn_list* head, struct conn_list* tail)
{
  memset(head,0,sizeof(struct conn_list));
  memset(tail,0,sizeof(struct conn_list));

  head->next = tail;
  tail->prev = head;
}


void flush_connection_list(struct conn_list* head, struct conn_list* tail)
{
  struct conn_list* x = head->next;
  struct conn_list* y;

  while( x != tail ) {
    y = x; x = x->next;
    if( y->refcount == 0 ) free(y);
  }
}

void setup_conn_lists() {
  init_conn_list(&installed_list_head, &installed_list_tail);
  init_conn_list(&active_list_head, &active_list_tail);
  init_conn_list(&teardown_list_head, &teardown_list_tail);


  pthread_mutexattr_t mx_attr;
  pthread_mutexattr_init(&mx_attr);
  pthread_mutexattr_settype(&mx_attr, PTHREAD_MUTEX_NORMAL );

  if( pthread_mutex_init( &conn_list_mutex, &mx_attr ) < 0 )
    { perror("create conn list mutex"); exit(1); }

  pthread_mutexattr_destroy(&mx_attr);
}

void teardown_conn_lists() {
  flush_connection_list(&installed_list_head, &installed_list_tail);
  flush_connection_list(&active_list_head, &active_list_tail);
  flush_connection_list(&teardown_list_head, &teardown_list_tail);

  if( pthread_mutex_destroy( &conn_list_mutex ) < 0 )
    { perror("conn list mutex destroy");  exit(1); }
}

void insert_conn_list(struct conn_list* x, struct conn_list* tail)
{
  x -> next = tail;
  x -> prev = tail->prev;

  tail->prev->next = x;
  tail->prev = x;
}

void unlink_conn_list(struct conn_list* x) {
  struct conn_list *p = x -> prev;
  struct conn_list *n = x -> next;

  p->next = n;
  n->prev = p;

  x->next = NULL;
  x->prev = NULL;
}

void remove_conn_list(struct conn_list* x) {
  if(x->next != NULL) { unlink_conn_list(x); }
}
