/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <pthread.h>

#include "api.h"
#include "internal.h"

int my_destroy_callback( const struct connection_desc* cdesc
		       , const struct connection_stats* stats
		       , void* arg ) {
  printf("in destroy callback, total counters: %llu %llu %llu %llu\n",
	 (unsigned long long int) stats->orig_packets,
	 (unsigned long long int) stats->orig_bytes,
	 (unsigned long long int) stats->repl_packets,
	 (unsigned long long int) stats->repl_bytes );

  return 0;
}

int my_dump_callback( conn_t conn_handle
		    , const struct connection_desc* cdesc
	            , const struct connection_stats* stats
	            , void* arg
		    ) {
  printf("dump counters: %llu %llu %llu %llu\n",
	 (unsigned long long int) stats->orig_packets,
	 (unsigned long long int) stats->orig_bytes,
	 (unsigned long long int) stats->repl_packets,
	 (unsigned long long int) stats->repl_bytes );

  return 0;
}


void my_free_callback( void* arg ) {
  printf("freeing!\n");
  free(arg);
}

int main(int argc, char** argv) {
  printf("hello!\n");

  init_connection_library();

  struct connection_desc desc;  
  int i;
  for(i=0; i<100; i++) {
    desc.src_addr.s_addr  = inet_addr("192.168.101.2");
    desc.dest_addr.s_addr = inet_addr("192.168.102.2");

    desc.protocol = IPPROTO_TCP;
    desc.src_port  = 0;
    desc.dest_port = htons(22);

    char* asdf = malloc(10);
    conn_t conn = install_connection(&desc, &my_destroy_callback, NULL, &my_free_callback, asdf );
    teardown_connection(conn);
    free_connection(conn);
  }
}

int main2(int argc, char** argv) {
  printf("hello!\n");

  init_connection_library();
  
  struct connection_desc desc;
  struct connection_stats stats;

  memset(&desc,0,sizeof(struct connection_desc));
  
  desc.src_addr.s_addr  = inet_addr("192.168.101.2");
  desc.dest_addr.s_addr = inet_addr("192.168.102.2");

  printf("%u\n", desc.src_addr.s_addr);
  printf("%u\n", desc.dest_addr.s_addr);

  /*
  desc.protocol  = IPPROTO_ICMP;
  desc.icmp_type = 8;
  desc.icmp_code = 0;
  */

  desc.protocol = IPPROTO_TCP;
  desc.src_port  = 0;
  desc.dest_port = htons(22);

  conn_t conn = install_connection(&desc, &my_destroy_callback, NULL, NULL, NULL );

  int i;
  for(i=0; i<5; i++) {
    dump_connections(&my_dump_callback, NULL);

    /*
    connection_get_counters(conn, &packets, &bytes);
    printf("counters: %llu %llu\n",
	   (unsigned long long int)packets, (unsigned long long int) bytes);
    */

    sleep(5);
  }

  printf("take a fresh reference\n");
  conn_t conn2 = fresh_reference(conn);

  printf("teardown!\n");
  teardown_connection(conn);

  sleep(10);

  printf("free conn!\n");
  free_connection(conn);

  connection_get_counters(conn2, &stats);
  printf("counters: %llu %llu %llu %llu\n",
	 (unsigned long long int) stats.orig_packets,
	 (unsigned long long int) stats.orig_bytes,
	 (unsigned long long int) stats.repl_packets,
	 (unsigned long long int) stats.repl_bytes );

  sleep(10);

  printf("free conn2!\n");
  free_connection(conn2);

  sleep(5);

  printf("shutdown!\n");
  teardown_connection_library();

  return 0;
}
