/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>

typedef void* conn_t;

/* address and port numbers in network order! */
struct connection_desc {
  struct in_addr src_addr;
  struct in_addr dest_addr;
  uint8_t protocol;
  in_port_t src_port;
  in_port_t dest_port;
  uint8_t icmp_type;
  uint8_t icmp_code;
  uint16_t icmp_echo_id;
};

enum {
  CONN_STAT_INSTALLED = 0,
  CONN_STAT_ACTIVE,
  CONN_STAT_DESTROYED
};

struct connection_stats {
  int status;
  uint64_t orig_packets;
  uint64_t orig_bytes;
  uint64_t repl_packets;
  uint64_t repl_bytes;
};

typedef int (* conn_callback_t)
            ( const struct connection_desc*
            , const struct connection_stats*
            , void* arg
	    );

typedef int (* dump_callback_t)
            ( conn_t conn_handle
            , const struct connection_desc*
            , const struct connection_stats*
            , void* arg
            );

int init_connection_library();
int teardown_connection_library();

conn_t install_connection(struct connection_desc *cdesc
                         , conn_callback_t attach_callback
                         , conn_callback_t destroy_callback
		         , void (* free_callback)(void* arg)
                         , void* arg);

conn_t fresh_reference(conn_t x);
void free_connection(conn_t conn);
int teardown_connection(conn_t conn);
void connection_get_counters(conn_t conn, struct connection_stats* stats);
int dump_connections(dump_callback_t dump_callback, void* arg);
