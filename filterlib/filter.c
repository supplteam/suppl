/* Copyright (c) 2013-2014. Robert Dockins and Andrew Tolmach and Alix Trieu. */

#include <assert.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include <linux/types.h>
#include <linux/netfilter.h>

#include <libnetfilter_conntrack/libnetfilter_conntrack.h>
#include <libnetfilter_queue/libnetfilter_queue.h>

#include <pthread.h>

#include "api.h"
#include "internal.h"

void cleanup_nfct_close(void* arg) {
  printf("shutting down an nfct handle\n");
  struct nfct_handle* hdl = (struct nfct_handle*) arg;
  nfct_close(hdl);
}

struct conn_list* find_matching_connection( u_int8_t* data, int datalen )
{
  struct conn_list* x = installed_list_head.next;
  struct iphdr *hdr = (struct iphdr*) data;
  
  assert( datalen >= sizeof(struct iphdr) );

  if( hdr->version != 4 ) {
    printf("unknown IP version %d\n", hdr->ihl );
    return NULL;
  }

  int hdrlen = hdr->ihl * 4;
  assert( datalen >= hdrlen );
  assert( datalen == ntohs(hdr->tot_len) );
  
  uint32_t saddr = hdr->saddr;
  uint32_t daddr = hdr->daddr;
  uint8_t protocol = hdr->protocol;
  uint16_t sport = 0;
  uint16_t dport = 0;

  uint8_t icmp_type = 0;
  uint8_t icmp_code = 0;
  uint16_t icmp_echo_id = 0;

  switch(hdr->protocol) {
  case IPPROTO_ICMP:
    assert( datalen >= hdrlen + sizeof(struct icmphdr) );
    struct icmphdr *icmph = (struct icmphdr*) (data + hdrlen);
    icmp_type = icmph->type;
    icmp_code = icmph->code;
    if(icmp_type == 8 && icmp_code == 0) {
      icmp_echo_id = icmph->un.echo.id;
    }
    break;
  case IPPROTO_TCP:
    assert( datalen >= hdrlen + sizeof(struct tcphdr) );
    struct tcphdr *tcph = (struct tcphdr*) (data + hdrlen);
    sport = tcph->source;
    dport = tcph->dest;
    break;
  case IPPROTO_UDP:
    assert( datalen >= hdrlen + sizeof(struct udphdr) );
    struct udphdr *udph = (struct udphdr*) (data + hdrlen);
    sport = udph->source;
    dport = udph->dest;
    break;
  default:
    printf("unknown IP4 protocol %d\n", hdr->protocol);
    return NULL;
  }

  while( x != &installed_list_tail ) {
    struct conn_list* y = x;
    x = x->next;

    struct connection_desc* cdesc = &(y->cdesc);
    
    if(! (cdesc->src_addr.s_addr == 0  || cdesc->src_addr.s_addr == saddr )) continue;
    if(! (cdesc->dest_addr.s_addr == 0 || cdesc->dest_addr.s_addr == daddr )) continue;
    if(! (cdesc->protocol == protocol )) continue;
    if(! (cdesc->src_port == 0 || cdesc->src_port == sport )) continue;
    if(! (cdesc->dest_port == 0 || cdesc->dest_port == dport )) continue;
    if( cdesc->protocol == IPPROTO_ICMP ) {
      if(! (cdesc->icmp_type == icmp_type) ) continue;
      if(! (cdesc->icmp_code == icmp_code) ) continue;
    }

    printf("matching connection found %u %u %u %u %u\n",
	   ntohl(saddr), ntohl(daddr), protocol,
	   ntohs(sport), ntohs(dport) );

    cdesc->src_addr.s_addr = saddr;
    cdesc->dest_addr.s_addr = daddr;
    cdesc->src_port = sport;
    cdesc->dest_port = dport;
    cdesc->icmp_type = icmp_type;
    cdesc->icmp_code = icmp_code;
    cdesc->icmp_echo_id = icmp_echo_id;

    return y;
  }
  
  return NULL;
}

int queue_callback
  ( struct nfq_q_handle *q
  , struct nfgenmsg *nfmsg
  , struct nfq_data *nfad
  , void* data ) {

  struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfad);
  if(!ph) { perror("get msg packet hdr"); return -1; }
  int id = ntohl(ph->packet_id);

  unsigned char* payload;
  int ret = nfq_get_payload(nfad, &payload);
  
  struct conn_list* cl = NULL;
  struct lease_table_entry *l = NULL;
  u_int32_t idx = 0;
  u_int8_t bucket = 0xFF;
  u_int32_t connmark = 0;

  conn_callback_t cb = NULL;
  struct connection_desc cdesc;
  struct connection_stats stats;

  if(pthread_mutex_lock(&conn_list_mutex) < 0) { perror("conn list lock"); exit(1); }

  cl = find_matching_connection( payload, ret );
  
  if(cl) {
    printf("found matching connection!\n");

    if( pthread_mutex_lock(&lease_table_mutex) < 0 ) { perror("lease table lock"); exit(1); }

    l = alloc_lte();
    if(l) {
      printf("allocated lease entry\n");

      unlink_conn_list(cl);
      insert_conn_list(cl, &active_list_tail);
      cl->lease = l;

      idx = calc_lease_index(l);

      connmark = (idx & 0x00FFFFFF) | (bucket << 24);
      
      l->cl = cl;
      memset( &(l->stats), 0, sizeof(struct connection_stats) );
      l->stats.status = CONN_STAT_ACTIVE;
      l->connmark = connmark;
      l->active = 1;

      printf("lease binding %u %u %u\n", idx, connmark, id);

    } else {
      printf("unable to allocate lease\n");
    }

    if( pthread_mutex_unlock(&lease_table_mutex) < 0 ) { perror("lease table unlock"); exit(1); }

    cb = cl->attach_callback;
    cl->attach_callback = NULL;

    if( cb ) {
      memcpy( &cdesc, &(cl->cdesc), sizeof(struct connection_desc) );
    }

  } else {
    printf("no matching connection found\n");
  }

  if(pthread_mutex_unlock(&conn_list_mutex) < 0) { perror("conn list unlock"); exit(1); }

  if( cb ) {
    memset( &stats, 0, sizeof(struct connection_stats) );
    if(pthread_mutex_lock(&(cl->lk)) < 0 ) { perror("conn object lock"); exit(1); }
    cb( &cdesc, &stats, cl->callback_arg );
    if(pthread_mutex_unlock(&(cl->lk)) < 0 ) { perror("conn object unlock"); exit(1); }
  }

  if( connmark == 0 ) {
    return nfq_set_verdict2( q, id, NF_DROP, 0, 0, NULL );
  } else {
    return nfq_set_verdict2( q, id, NF_REPEAT, connmark, 0, NULL );
  }

}

struct queue_data {
  struct nfq_handle* hdl;
  struct nfq_q_handle* q;
};

void cleanup_queue_data(void* arg) {
  struct queue_data *qd = (struct queue_data*) arg;

  printf("shutting down an nfq queue\n");

  nfq_destroy_queue(qd->q);
  nfq_close( qd->hdl );
}

void watch_queue(int queue_num) {

  struct nfq_handle* hdl = nfq_open();
  if(!hdl) { perror("handle open"); exit(1); }

  if (nfq_unbind_pf(hdl, AF_INET) < 0 )
    { perror("unbind"); exit(1); }

  if (nfq_bind_pf(hdl, AF_INET) < 0 )
    { perror("bind"); exit(1); }

  struct nfq_q_handle *q = nfq_create_queue(hdl, queue_num, &queue_callback, NULL);
  if(!q) { perror("create queue"); exit(1); }

  if(nfq_set_mode(q, NFQNL_COPY_PACKET, 0xffff) < 0)
    { perror("set queue mode"); exit(1); }

  struct queue_data qd;
  qd.hdl = hdl;
  qd.q = q;

  pthread_cleanup_push(&cleanup_queue_data, &qd);

  int fd = nfq_fd(hdl);
  char buf[4096];
  memset(buf, 0, sizeof(buf));

  int rv;
  while ((rv = recv(fd,buf,sizeof(buf),0)) >= 0) {
    nfq_handle_packet(hdl,buf,rv);
  }

  pthread_cleanup_pop(1);
}


int destroy_callback
  ( const struct nlmsghdr* hdr
  , enum nf_conntrack_msg_type type
  , struct nf_conntrack *ct
  , void* data
  ) {

  struct connection_desc cdesc;
  struct connection_stats stats;
  char buf[1024];

  nfct_snprintf(buf, sizeof(buf),
		ct,
		NFCT_T_UNKNOWN,
		NFCT_O_DEFAULT,
		NFCT_OF_SHOW_LAYER3 |
		NFCT_OF_TIMESTAMP );
  printf("conntrack destroy: %s\n", buf);

  u_int32_t connmark = nfct_get_attr_u32(ct, ATTR_MARK);

  if(connmark == 0) {
    printf("ignoring unmarked connection\n");
    return NFCT_CB_CONTINUE;
  }

  u_int8_t bucket = (connmark >> 24) & 0xFF;
  unsigned int idx = connmark & 0x00FFFFFF;

  u_int64_t orig_bytes = nfct_get_attr_u64(ct, ATTR_ORIG_COUNTER_BYTES );
  u_int64_t repl_bytes = nfct_get_attr_u64(ct, ATTR_REPL_COUNTER_BYTES );
  u_int64_t orig_pkts  = nfct_get_attr_u64(ct, ATTR_ORIG_COUNTER_PACKETS );
  u_int64_t repl_pkts  = nfct_get_attr_u64(ct, ATTR_REPL_COUNTER_PACKETS );

  conn_callback_t cb = NULL;

  if(idx < MAX_LEASES) {
    /* LOCK ORDERING: take the conn list mutex before the lease table mutex */    
    if( pthread_mutex_lock(&conn_list_mutex) < 0 ) { perror("conn list lock"); exit(1); }
    if( pthread_mutex_lock(&lease_table_mutex) < 0 ) { perror("lease table lock"); exit(1); }

    struct lease_table_entry* lte = &lease_table[idx];
    assert(lte->connmark == connmark);

    struct conn_list* cl = lte->cl;
    assert(cl->lease == lte);

    cl->lease = NULL;

    stats.orig_packets = orig_pkts + lte->stats.orig_packets;
    stats.orig_bytes = orig_bytes + lte->stats.orig_bytes;
    stats.repl_packets = repl_pkts + lte->stats.repl_packets;
    stats.repl_bytes = repl_bytes + lte->stats.repl_bytes;
    stats.status = CONN_STAT_DESTROYED;

    free_lte(lte);

    memcpy( &(cl->final_stats), &stats, sizeof(struct connection_stats) );

    int cnt;
    /* LOCK ORDERING: take the list mutex before any conn_list node mutex */
    if( pthread_mutex_lock(&(cl->lk)) < 0 ) { perror("conn object lock"); exit(1); }
      cnt = cl->refcount;
    if( pthread_mutex_unlock(&(cl->lk)) < 0 ) { perror("conn object unlock"); exit(1); }

    assert(cnt >= 0);

    if(cl->next) { 
      unlink_conn_list(cl);
      if( cnt > 0 ) {
	insert_conn_list(cl, &teardown_list_tail);
      }
    }

    cb = cl->destroy_callback;
    cl->destroy_callback = NULL;

    if( cb ) {
      memcpy( &cdesc, &(cl->cdesc), sizeof(struct connection_desc) );
    }
    
    if( pthread_mutex_unlock(&lease_table_mutex) < 0 ) { perror("lease table unlock"); exit(1); }
    if( pthread_mutex_unlock(&conn_list_mutex) < 0 ) { perror("conn list unlock"); exit(1); }

    if( cb ) {
      if(pthread_mutex_lock(&(cl->lk)) < 0 ) { perror("conn object lock"); exit(1); }
      cb( &cdesc, &stats, cl->callback_arg );
      if(pthread_mutex_unlock(&(cl->lk)) < 0 ) { perror("conn object unlock"); exit(1); }
    }

    if( cnt == 0 ) {
      printf("freeing a connection object in destroy handler\n");
      if( pthread_mutex_destroy(&(cl->lk)) < 0 ) { perror("conn object mutex destroy"); exit(1); }
      if(cl->free_callback) {
	cl->free_callback(cl->callback_arg);
      }
      free(cl);
    }
  } else {
    printf("reference to out of bounds lease entry! %d\n", idx);
  }

  return NFCT_CB_CONTINUE;
}


void watch_destroy_conntracks() {
  struct nfct_handle* hdl = nfct_open( CONNTRACK
                                     , NF_NETLINK_CONNTRACK_DESTROY
                                     );

  if(!hdl) { perror("handle open"); exit(1); }
  
  if(nfct_callback_register2(hdl, NFCT_T_ALL, &destroy_callback, NULL) < 0)
    { perror("register callback"); exit(1); }

  pthread_cleanup_push(&cleanup_nfct_close, (void*) hdl);

  if(nfct_catch(hdl) < 0)
    { perror("catch"); exit(1); }

  pthread_cleanup_pop(1);
}


int dump_callback
  ( const struct nlmsghdr* hdr
  , enum nf_conntrack_msg_type type
  , struct nf_conntrack *ct
  , void* data
  ) {

  u_int32_t connmark = nfct_get_attr_u32(ct, ATTR_MARK);
  if(connmark == 0) { return NFCT_CB_CONTINUE; }

  u_int8_t bucket = (connmark >> 24) & 0xFF;
  unsigned int idx = connmark & 0x00FFFFFF;

  u_int64_t orig_bytes = nfct_get_attr_u64(ct, ATTR_ORIG_COUNTER_BYTES );
  u_int64_t repl_bytes = nfct_get_attr_u64(ct, ATTR_REPL_COUNTER_BYTES );
  u_int64_t orig_pkts  = nfct_get_attr_u64(ct, ATTR_ORIG_COUNTER_PACKETS );
  u_int64_t repl_pkts  = nfct_get_attr_u64(ct, ATTR_REPL_COUNTER_PACKETS );

  if(idx < MAX_LEASES) {
    if( pthread_mutex_lock(&lease_table_mutex) < 0 ) { perror("lease table lock"); exit(1); }

    struct lease_table_entry* lte = &lease_table[idx];
    assert(lte->connmark == connmark);

    lte->stats.orig_bytes += orig_bytes;
    lte->stats.repl_bytes += repl_bytes;
    lte->stats.orig_packets += orig_pkts;
    lte->stats.repl_packets += repl_pkts;

    if( pthread_mutex_unlock(&lease_table_mutex) < 0 ) { perror("lease table unlock"); exit(1); }
  } else {
    printf("reference to out of bounds lease entry! %d\n", idx);
  }

  return NFCT_CB_CONTINUE;
}


void dump_conntracks_loop() {
  u_int32_t family = AF_INET;

  struct nfct_handle* hdl = nfct_open( CONNTRACK, 0 );
  if(!hdl) { perror("handle open"); exit(1); }

  if(nfct_callback_register2(hdl, NFCT_T_ALL, &dump_callback, NULL) < 0)
    { perror("register callback"); exit(1); }

  pthread_cleanup_push(&cleanup_nfct_close, (void*) hdl);

  while(1) {
    if(nfct_query(hdl, NFCT_Q_DUMP_RESET, &family) < 0)
      { perror("query"); exit(1); }

    sleep(1);
  }

  pthread_cleanup_pop(1);
}

void* event_listen_thread(void* arg) {
  watch_destroy_conntracks();
  return NULL;
}

void* queue_listen_thread(void* arg) {
  watch_queue( 1 );
  return NULL;
}

void* dump_conntracks_thread(void* arg) {
  dump_conntracks_loop();
  return NULL;
}

void flush_conntracks()
{
  u_int32_t family = AF_INET;

  struct nfct_handle* hdl = nfct_open( CONNTRACK, 0 );
  if(!hdl) { perror("handle open"); exit(1); }

  if( nfct_query( hdl, NFCT_Q_FLUSH, &family ) < 0 )
    { perror("nfct query FLUSH"); exit(1); }

  nfct_close(hdl);
}


void teardown_conntrack(u_int32_t mark, struct connection_desc* cdesc)
{
  struct nfct_handle* h;
  struct nf_conntrack* ct;

  ct = nfct_new();
  if(!ct) { perror("nfct_new"); exit(1); }

  nfct_set_attr_u8(ct, ATTR_L3PROTO, AF_INET);
  nfct_set_attr_u32(ct, ATTR_IPV4_SRC, cdesc->src_addr.s_addr);
  nfct_set_attr_u32(ct, ATTR_IPV4_DST, cdesc->dest_addr.s_addr);
  nfct_set_attr_u8(ct, ATTR_L4PROTO, cdesc->protocol);
  nfct_set_attr_u32(ct, ATTR_MARK, mark);

  switch(cdesc->protocol) {
  case IPPROTO_TCP:
  case IPPROTO_UDP:
    nfct_set_attr_u16(ct, ATTR_PORT_SRC, cdesc->src_port);
    nfct_set_attr_u16(ct, ATTR_PORT_DST, cdesc->dest_port);
    break;

  case IPPROTO_ICMP:
    nfct_set_attr_u8(ct, ATTR_ICMP_TYPE, cdesc->icmp_type);
    nfct_set_attr_u8(ct, ATTR_ICMP_CODE, cdesc->icmp_code);
    if( cdesc->icmp_type == 8 && cdesc->icmp_code == 0 ) 
      nfct_set_attr_u16(ct, ATTR_ICMP_ID, cdesc->icmp_echo_id);
    break;

  default:
    printf("unknown protocol: %d\n",cdesc->protocol);
  }

  h = nfct_open(CONNTRACK, 0 );
  if(!h) { perror("nfct open"); exit(1); }

  printf("send destroy query\n");
  if( nfct_query(h, NFCT_Q_DESTROY, ct) < 0 ) 
    { perror("nfct query DESTROY"); exit(1); }

  nfct_close(h);
  nfct_destroy(ct);
}


pthread_t evt_thread;
pthread_t queue_thread;
pthread_t dump_thread;

void launch_filter_threads()
{
  pthread_attr_t thread_attr;
  pthread_attr_init(&thread_attr);

  pthread_create(&queue_thread, &thread_attr, queue_listen_thread, NULL );
  pthread_create(&evt_thread, &thread_attr, event_listen_thread, NULL );
  pthread_create(&dump_thread, &thread_attr, dump_conntracks_thread, NULL );

  pthread_attr_destroy(&thread_attr);
}

void kill_filter_threads()
{
  pthread_cancel(evt_thread);
  pthread_cancel(queue_thread);
  pthread_cancel(dump_thread);

  pthread_join(evt_thread, NULL);
  pthread_join(queue_thread, NULL);
  pthread_join(dump_thread, NULL);
}
